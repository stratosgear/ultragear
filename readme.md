# links:

- https://github.com/jakehamilton/config
- https://github.com/snowfallorg/lib


# snippets

nix flake init -t github:snowfallorg/templates#<template-name>

empty 	A NixOS system and modules ready to modify.
home 	A Nix Flake that exports home manager.
system 	A NixOS system and modules ready to modify.
package 	A Nix Flake that exports packages and an overlay.
module 	A Nix Flake that exports NixOS modules.
lib 	A Nix Flake that exports a custom lib