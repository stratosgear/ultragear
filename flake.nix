{
  description = "Ultragear configs and dotfiles";

  inputs = {
    # nixpkgs.url = "github:nixos/nixpkgs/nixos-23.11";
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";

    # Home Manager
    home-manager = {
      # Setting it up as recommended on docs:
      # https://nix-community.github.io/home-manager/#sec-flakes-nixos-module
      url = "github:nix-community/home-manager/master";
      # url = "github:nix-community/home-manager/master";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    # Useful git aliases
    git-alias = {
      url = "github:GitAlias/gitalias/main";
      flake = false;
    };

    # Snowfall lib
    snowfall-lib = {
      url = "github:snowfallorg/lib";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    # Snowfall Flake
    flake = {
      url = "github:snowfallorg/flake?ref=v1.3.1";
      # url = "github:snowfallorg/flake";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    # Snowfall Thaw
    thaw.url = "github:snowfallorg/thaw?ref=v1.0.4";
    snowfall-docs = {
      url = "github:snowfallorg/docs";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    # System Deployment
    deploy-rs = {
      url = "github:serokell/deploy-rs";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    # Generate System Images
    nixos-generators.url = "github:nix-community/nixos-generators";
    nixos-generators.inputs.nixpkgs.follows = "nixpkgs";

    disko = {
      url = "github:nix-community/disko";
    };

    # GPG default configuration
    gpg-base-conf = {
      url = "github:drduh/config";
      flake = false;
    };

    # VScode server
    vscode-server = {
      url = "https://github.com/nix-community/nixos-vscode-server/tarball/master";
    };

    # Impermanence
    impermanence.url = "github:nix-community/impermanence";

    # Secrets handling with sops
    sops-nix = {
      url = "github:mic92/sops-nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    # hardware (loading framework laptop settings)
    hardware.url = "github:nixos/nixos-hardware";

    agenix = {
      url = "github:ryantm/agenix";
      inputs.nixpkgs.follows = "nixpkgs";
      # optionally choose not to download darwin deps (saves some resources on Linux)
      inputs.darwin.follows = "";
    };

    hyprland.url = "github:hyprwm/Hyprland";

    hyprland-plugins = {
      url = "github:hyprwm/hyprland-plugins";
      inputs.hyprland.follows = "hyprland";
    };

    # hyprcursor theme
    rose-pine-hyprcursor.url = "github:ndom91/rose-pine-hyprcursor";

    # waybar mediaplayer
    # From: https://github.com/nomisreual/mediaplayer
    mediaplayer = {
      url = "github:nomisreual/mediaplayer";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    # wpaperd
    # From: https://github.com/nomisreual/mediaplayer
    wpaperd = {
      url = "github:danyspin97/wpaperd";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    ghostty = {
      url = "github:ghostty-org/ghostty";
    };

    flox = {
      url = "github:flox/flox/v1.3.12";
    };

  };

  outputs =
    inputs:
    let
      lib = inputs.snowfall-lib.mkLib {
        inherit inputs;
        src = ./.;

        snowfall = {
          meta = {
            name = "ultragear";
            title = "Ultra Gear";
          };

          namespace = "ultragear";
        };
      };
    in
    lib.mkFlake {
      channels-config = {
        allowUnfree = true;

        # FIXME: This is needed while obsidian is still using that
        # electron version that is considered insecure.
        permittedInsecurePackages = [
          # "electron-29.4.6"
          # "nix-2.16.2"
        ];
      };

      # homes.modules = with inputs; [ sops-nix.homeManagerModules.sops ];
      # Does not work!!!
      # system.modules.home = with inputs; [ sops-nix.homeManagerModules.sops ];

      overlays = with inputs; [
        # neovim.overlays.default
        # tmux.overlay
        flake.overlays.default
        # thaw.overlays.default
        # cowsay.overlays.default
        # icehouse.overlays.default
      ];

      systems.modules.nixos = with inputs; [
        home-manager.nixosModules.home-manager
        disko.nixosModules.disko
        vscode-server.nixosModules.default
        impermanence.nixosModules.impermanence
        sops-nix.nixosModules.sops
        agenix.nixosModules.default
        # ghostty.packages.x86_64-linux.default
      ];

      systems.hosts.vezzini.modules = with inputs; [
        hardware.nixosModules.framework-13-7040-amd
      ];

      systems.hosts.pi4.modules = with inputs; [
        hardware.nixosModules.raspberry-pi-4
      ];

      deploy = lib.mkDeploy {
        inherit (inputs) self;
      };

      # checks =
      #   builtins.mapAttrs
      #     (system: deploy-lib:
      #       deploy-lib.deployChecks inputs.self.deploy)
      #     inputs.deploy-rs.lib;

    };
}
