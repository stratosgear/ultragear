#!/usr/bin/env python3

import glob
import os
import random
import sys

try:
    import notify2

    notify2.init("x11-wallpapers-rotate")
    notify = True
except ModuleNotFoundError:
    notify = False


def say(msg):
    if notify:
        notification = notify2.Notification(msg)
        notification.show()
    else:
        print(msg)


home = os.path.expanduser("~")
pattern = home + "/assets/wallpapers/*.jpg" if len(sys.argv) < 2 else sys.argv[1]

files = glob.glob(pattern)
num_wallpapers = len(files)
if num_wallpapers == 0:
    say("No wallpaper files found to rotate!")
    exit(1)

random.shuffle(files)

with open("/tmp/wallpapers.txt", "w") as f:
    f.write(files[0] + "\n")
    if num_wallpapers > 1:
        f.write(files[1] + "\n")
    if num_wallpapers > 2:
        f.write(files[2] + "\n")

shown = min(3, num_wallpapers)
image_names = " ".join([f'"{i}"' for i in files[:shown]])
command = f"feh --no-fehbg --bg-fill {image_names}"
failed = os.system(command)

if failed:
    say("Failed to rotate wallpaper. Missing feh?")
else:
    say("Wallpapers rotated.")
