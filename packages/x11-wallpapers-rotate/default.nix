{ pkgs, lib, ... }:

let
  a = 3;
in
pkgs.stdenv.mkDerivation {
  name = "x11-wallpapers-rotate";
  propagatedBuildInputs = [
    (pkgs.python3.withPackages (pythonPackages: with pythonPackages; [
      # install required python packages here
      notify2
    ]))
  ];
  dontUnpack = true;
  installPhase = "install -Dm755 ${./x11-wallpapers-rotate.py} $out/bin/x11-wallpapers-rotate";
}
