{ pkgs, lib, ... }:

pkgs.stdenv.mkDerivation {
  name = "ug-ultragear";
  version = "v1.3";

  propagatedBuildInputs = [
    (pkgs.python3.withPackages (pythonPackages: with pythonPackages; [
      # install required python packages here
      fire
      rich
      inquirer
      paramiko
      bcrypt
      requests
      pynacl
    ]))
  ];

  dontUnpack = true;

  installPhase = "install -Dm755 ${./ultragear.py} $out/bin/ultragear";

  preFixup = ''
    makeWrapperArgs+=(--prefix PATH : ${lib.makeBinPath [
      # Hard requirements

    ]})
  '';

  meta = with lib; {
    description = "Management script to manage Ultragear";
  };
}
