#!/usr/bin/env python3

from __future__ import absolute_import, unicode_literals

import configparser
import getpass
import ipaddress
import json
import os
import platform
import re
import socket
import subprocess
import sys
import zipfile
from datetime import datetime
from io import BytesIO
from pathlib import Path
from typing import Any

import bcrypt

import fire
import inquirer
import paramiko
import requests
from rich.console import Console
from rich.prompt import Confirm
from rich.text import Text

console = Console()


def header(text):
    """Display a header."""
    txt = Text()
    txt.append(">> ", style="blue")
    txt.append(text, style="cyan")
    console.print(txt)
    console.print("-" * len(txt), style="cyan")


def info(text, *args, **kwargs):
    """Display information."""
    text = text.format(*args, **kwargs)
    txt = Text()
    txt.append(">>> ", style="yellow")
    txt.append(text)
    console.print(txt)


def warn(text):
    console.print(f"o  {text}", style="yellow")


def error(text):
    console.print(f"✘  {text}", style="red")


def success(text):
    console.print(f"👍  {text}", style="green")


def custom_exit(text=None, code=-1):
    """Exit with a specific error message and error code."""
    if text:
        error(text)
    sys.exit(code)


#  __   ____ _ _ __ ___
#  \ \ / / _` | '__/ __|
#   \ V / (_| | |  \__ \
#    \_/ \__,_|_|  |___/

CONFIG_DIR = Path.home() / ".config" / "ultragear"
CONFIG_FILE = CONFIG_DIR / "config.ini"
STATIC_LEASES = CONFIG_DIR / "static-dhcp-leases.txt"

CONF = configparser.ConfigParser()
CONF.read(CONFIG_FILE)

CLEAN_PATTERNS = [
    "build",
    "dist",
    "cover",
    "docs/_build",
    "**/*.pyc",
    ".tox",
    "**/__pycache__",
    "reports",
    "*.egg-info",
]

socketHost = socket.gethostname()
platformHost = platform.node()

if socketHost != platformHost:
    custom_exit(
        f"Hostnames do not match: [socket!=platform] {socketHost} != {platformHost}"
    )

currentHost = socketHost

ROOT = os.path.dirname(__file__)
ULTRAGEAR = Path.home() / "ultragear"
if not ULTRAGEAR.exists():
    custom_exit(
        f"Ultragear repository not found: {ULTRAGEAR}. Have you checked it out?"
    )

#                                     _   _               _
#    __ _ _   ___  __  _ __ ___   ___| |_| |__   ___   __| |___
#   / _` | | | \ \/ / | '_ ` _ \ / _ \ __| '_ \ / _ \ / _` / __|
#  | (_| | |_| |>  <  | | | | | |  __/ |_| | | | (_) | (_| \__ \
#   \__,_|\__,_/_/\_\ |_| |_| |_|\___|\__|_| |_|\___/ \__,_|___/


# ======================================================================================
def run(cmd, cwd=None):
    warn(f"Executing: {cmd}")

    try:
        out = subprocess.run(
            cmd.split(),
            text=True,
            check=True,
            stdout=subprocess.PIPE,
            cwd=cwd,
        ).stdout.strip()
        return out
    except Exception:
        custom_exit("Kinda failed")


# ======================================================================================
def extractHosts(hosts):
    try:
        data = json.loads(hosts)
        nixos_configurations = [
            config
            for config in data.get("nixosConfigurations", {})
            if config != "bootstrap"
        ]
        return nixos_configurations
    except json.JSONDecodeError as e:
        print(f"Error decoding JSON output: {e}")


# ======================================================================================
def getLatestGeneration():
    p1 = subprocess.Popen(
        "sudo nix-env -p /nix/var/nix/profiles/system --list-generations".split(),
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )
    p2 = subprocess.Popen(
        "grep current".split(),
        stdin=p1.stdout,
        stdout=subprocess.PIPE,
    )
    p3 = subprocess.Popen(
        ["awk", "{print $1}"],
        stdin=p2.stdout,
        stdout=subprocess.PIPE,
    )

    return (p3.communicate()[0]).decode()


# ======================================================================================
def getEarliestGeneration():
    p1 = subprocess.Popen(
        "sudo nix-env -p /nix/var/nix/profiles/system --list-generations".split(),
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )
    p2 = subprocess.Popen(
        "tail -n +1".split(),
        stdin=p1.stdout,
        stdout=subprocess.PIPE,
    )
    p3 = subprocess.Popen(
        "head -1".split(),
        stdin=p2.stdout,
        stdout=subprocess.PIPE,
    )
    p4 = subprocess.Popen(
        ["awk", "{print $1}"],
        stdin=p3.stdout,
        stdout=subprocess.PIPE,
    )

    return (p4.communicate()[0]).decode()


def is_valid_mac(mac_address):
    # ======================================================================================
    mac_address_pattern = re.compile(r"^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})$")
    return bool(mac_address_pattern.match(mac_address))


# ======================================================================================
def getStaticLeases():
    leases = []
    with open(STATIC_LEASES, "r") as file:
        for line in file:
            line = line.strip()
            if len(line) > 0 and line[0] != "#" and line != "":
                leases.append(line.split())
    return leases


# ======================================================================================
def writeStaticLeases(allLeases):
    # resort according to ip address
    allLeases = sorted(allLeases, key=lambda x: ipaddress.IPv4Address(x[2]))

    # rewrite the new STATIC_LEASES file
    with open(STATIC_LEASES, "w") as leases:
        for aLease in allLeases:
            leases.write(
                f"{aLease[0].upper()}  {aLease[1].ljust(16, ' ')}  {aLease[2].ljust(16, ' ')}  {aLease[3]}\n"
            )


# ======================================================================================
def applyAllStaticLeasesToDdwrt(dryrun: bool):
    if not STATIC_LEASES.exists():
        custom_exit("No static leases file found!")

    router = CONF["hosts"]["router"]
    if not router:
        custom_exit("No DD-WRT router defined in config! (hosts.router)")

    allLeases = STATIC_LEASES.read_text().splitlines()
    totLeases = len(allLeases)
    cmd = f"ssh {router} \"nvram set static_leases='"
    cmd += " ".join(
        (
            "=".join(re.split(r"\s+", aLease.strip()))
            for aLease in allLeases
            if aLease.strip()
        )
    )
    cmd += "'\""

    if dryrun:
        print(cmd)
    else:
        info("Setting DD-WRT DHCP leases...")

    if dryrun:
        print(f"ssh {router} nvram set static_leasenum={totLeases}")
    else:
        info("Setting DD-WRT DHCP number of static leases...")

    if dryrun:
        print(f"ssh {router} nvram commit")
    else:
        info("Commiting DD-WRT nvram changes...")


# ======================================================================================
def addStaticLeaseToADGuardHome(name: str, ip: str, dryrun: bool):
    if not STATIC_LEASES.exists():
        custom_exit("No static leases file found!")

    adguard = CONF["hosts"]["adguardhome"]
    if not adguard:
        custom_exit("No ADGuardHome server defined in config! (hosts.adguardhome)")

    url = f"https://{adguard}/control/clients/add"
    info(f"Adding ADGuardHome client: {name.ljust(16, ' ')} IP: {ip}")
    if not dryrun:
        reply = requests.post(
            url,
            json={
                "name": name,
                "ids": [ip],
                "tags": [],
                "use_global_settings": True,
                "use_global_blocked_services": True,
                "safe_search": {
                    "enabled": False,
                    "bing": True,
                    "duckduckgo": True,
                    "google": True,
                    "pixabay": True,
                    "yandex": True,
                    "youtube": True,
                },
                "upstreams": [],
            },
        )
        if reply.status_code == 200:
            success("    OK!")
        else:
            error(
                f"    Ooops! reply: {reply.status_code} - {reply.reason} - {reply.text}"
            )


# ======================================================================================
def updateStaticLeaseToADGuardHome(name: str, ip: str, dryrun: bool):
    adguard = CONF["hosts"]["adguardhome"]
    if not adguard:
        custom_exit("No ADGuardHome server defined in config! (hosts.adguardhome)")

    url = f"https://{adguard}/control/clients/add"
    info(f"Adding ADGuardHome client: {name.ljust(16, ' ')} IP: {ip}")
    if not dryrun:
        reply = requests.post(
            url,
            json={
                "name": name,
                "ids": [ip],
                "tags": [],
                "use_global_settings": True,
                "use_global_blocked_services": True,
                "safe_search": {
                    "enabled": False,
                    "bing": True,
                    "duckduckgo": True,
                    "google": True,
                    "pixabay": True,
                    "yandex": True,
                    "youtube": True,
                },
                "upstreams": [],
            },
        )
        if reply.status_code == 200:
            success("    OK!")
        else:
            error(
                f"    Ooops! reply: {reply.status_code} - {reply.reason} - {reply.text}"
            )


# ======================================================================================
def deleteStaticLeaseFromADGuardHome(name: str, dryrun: bool):
    adguard = CONF["hosts"]["adguardhome"]
    if not adguard:
        custom_exit("No ADGuardHome server defined in config! (hosts.adguardhome)")

    url = f"https://{adguard}/control/clients/delete"
    info(f"Deleting existing ADGuardHome client: {name}")
    if not dryrun:
        reply = requests.post(url, json={"name": name})
        if reply.status_code == 200:
            success("    OK!")
        else:
            error(f"reply: {reply.status_code} - {reply.reason} - {reply.text}")


# ======================================================================================
def applyAllStaticLeasesToADGuardHome(dryrun: bool):
    if not STATIC_LEASES.exists():
        custom_exit("No static leases file found!")

    adguard = CONF["hosts"]["adguardhome"]
    if not adguard:
        custom_exit("No ADGuardHome server defined in config! (hosts.adguardhome)")

    allLeases = STATIC_LEASES.read_text().splitlines()
    # first get all ADH clients
    reply = requests.get(f"https://adguard.gerakakis.net/control/clients").json()

    # delete them all
    for c in reply.get("clients"):
        clientName = c.get("name")
        deleteStaticLeaseFromADGuardHome(name=clientName, dryrun=dryrun)

    # create new clients
    for aLease in allLeases:
        if not aLease.strip():
            continue
        data = aLease.split()
        clientName = data[1]
        clientIp = data[2]

        addStaticLeaseToADGuardHome(name=clientName, ip=clientIp, dryrun=dryrun)


#                                                                               _
#    __ _ _ __ ___  _   _ _ __     ___ ___  _ __ ___  _ __ ___   __ _ _ __   __| |___
#   / _` | '__/ _ \| | | | '_ \   / __/ _ \| '_ ` _ \| '_ ` _ \ / _` | '_ \ / _` / __|
#  | (_| | | | (_) | |_| | |_) | | (_| (_) | | | | | | | | | | | (_| | | | | (_| \__ \
#   \__, |_|  \___/ \__,_| .__/   \___\___/|_| |_| |_|_| |_| |_|\__,_|_| |_|\__,_|___/
#   |___/                |_|


# ======================================================================================
# ======================================================================================
class Conf(object):
    def __init__(self):
        pass

    def show(self):
        for section in CONF.sections():
            print(f"\n[{section}]")
            for option in CONF.options(section):
                print(f"{option} = {CONF.get(section, option)}")

    # ======================================================================================
    def set(self, section: str, key: str, value: str):
        if section not in CONF:
            CONF[section] = {}

        CONF[section][key] = value
        success(f"Added {section}.{key} as: {value}")

        with open(CONFIG_FILE, "w") as confFile:
            CONF.write(confFile)

    # ======================================================================================
    def rm(self, section: str, key: str):
        del CONF[section][key]
        success(f"Removed {section}.{key}")

        with open(CONFIG_FILE, "w") as confFile:
            CONF.write(confFile)


# ======================================================================================
# ======================================================================================
class Dhcp(object):
    """Show/list various info"""

    def __init__(self):
        pass

    # ======================================================================================
    def show(self, mac: str = None):
        """Show a list of all currently set DD-WRT Static DHCP leases

        Positional Arguments:
            mac=str: The MAC address of a host, if any
        """

        print(f"Reading static leases from: {STATIC_LEASES}\n")

        if not STATIC_LEASES.exists():
            custom_exit("No static leases file found!")

        allLeases = STATIC_LEASES.read_text().splitlines()
        if not mac:
            for aLease in allLeases:
                print(aLease)
        else:
            found = next((aLease for aLease in allLeases if mac in aLease), None)
            if found:
                print(found)
            else:
                print(f"No static lease found for MAC {mac}")

    # ======================================================================================
    def _addUpdate(
        self,
        mode: str,
        mac: str = None,
        ip: str = None,
        name: str = None,
        duration: int = 480,
        autoApply: bool = False,
    ):
        """Edit an existing static IP lease based on its MAC address

        Positional Arguments:
            mac=str: The MAC address of the host
            ip=str: The IP address of the host
            name=str: The name of the host
            duration=int: The duration of the lease in minutes
            autoApply=bool: Automatically apply the changes to DD-WRT/ADGuardHome
        """
        if not STATIC_LEASES.exists():
            custom_exit("No static leases file found!")

        if not mac:
            custom_exit("No MAC address provided!")

        if not ip:
            custom_exit("No IP address provided!")

        if not name:
            custom_exit("No hostname provided!")

        # check if duration is an int, larger than 30 and smaller than 1440
        if not isinstance(duration, int) or duration < 30 or duration > 1440:
            custom_exit("Duration must be an integer between 30 and 1440!")

        # Check for valid IP address
        try:
            ipaddress.ip_address(ip)
        except ValueError:
            custom_exit(f"{ip} is not a valid IP address!")

        # check if valid MAC address
        if not is_valid_mac(mac):
            custom_exit(f"{mac} is not a valid MAC address!")

        # Check if name is longer than 16 chars
        if len(name) > 16:
            custom_exit("Name must be shorter than 16 chars!")

        allLeases = getStaticLeases()

        prevName: str = ""
        if mode == "add":
            # Check if any of ip, mac or name already in the STATIC_LEASES file
            for aLease in allLeases:
                if mac == aLease[0] or name == aLease[1] or ip == aLease[2]:
                    custom_exit(
                        f"IP: {ip}, MAC: {mac} or {name} already in static leases file!"
                    )

            allLeases.append([mac, name, ip, f"{duration}m"])
            success(f"MAC {mac} added to the static leases file!")
        elif mode == "update":
            # Check if any of ip, or name already in the STATIC_LEASES file
            macExists = False
            for aLease in allLeases:
                if (name == aLease[1] or ip == aLease[2]) and mac != aLease[0]:
                    custom_exit(
                        f"IP: {ip}, MAC: {mac} or {name} already in static leases file!"
                    )
                if mac == aLease[0]:
                    macExists = True

            if not macExists:
                custom_exit(f"MAC [{mac}] not found in static leases file!")

            for aLease in allLeases:
                if aLease[0] == mac:
                    prevName = aLease[1]
                    aLease[1] = name
                    aLease[2] = ip
                    aLease[3] = f"{duration}m"
            success(f"MAC {mac} edited in the static leases file!")
        else:
            custom_exit(f"Unknown mode: {mode}")

        writeStaticLeases(allLeases)

        if autoApply:
            info("Auto-updating Static DHCP leases...")
            applyAllStaticLeasesToDdwrt(dryrun=False)
            if mode == "add":
                addStaticLeaseToADGuardHome(name=name, ip=ip, dryrun=False)
            else:
                deleteStaticLeaseFromADGuardHome(name=prevName, dryrun=False)
                addStaticLeaseToADGuardHome(name=name, ip=ip, dryrun=False)
        else:
            info(
                "Changes are not auto-applied. "
                "To apply them them run: ultragear dhcp apply -d False"
            )

    # ======================================================================================
    def add(
        self,
        mac: str,
        ip: str,
        name: str,
        duration: int = 480,
        autoApply: bool = False,
    ):
        """Add a new static IP lease.

        Positional Arguments:
            mac=str: The MAC address of the host
            ip=str: The IP address of the host
            name=str: The name of the host
            duration=int: The duration of the lease in minutes
            autoApply=bool: Automatically apply the changes to DD-WRT/ADGuardHome
        """
        self._addUpdate(
            "add", mac=mac, ip=ip, name=name, duration=duration, autoApply=autoApply
        )

    # ======================================================================================
    def update(
        self,
        mac: str = None,
        ip: str = None,
        name: str = None,
        duration: int = 480,
        autoApply: bool = False,
    ):
        """Update an existing static IP lease.

        Positional Arguments:
            mac=str: The MAC address of the host
            ip=str: The IP address of the host
            name=str: The name of the host
            duration=int: The duration of the lease in minutes
            autoApply=bool: Automatically apply the changes to DD-WRT/ADGuardHome
        """
        self._addUpdate(
            "update", mac=mac, ip=ip, name=name, duration=duration, autoApply=autoApply
        )

    # ======================================================================================
    def rm(self, mac: str, autoApply: bool = False):
        """Remove static lease based on the MAC address

        Positional Arguments:
            mac=str: The MAC address of the host
            autoApply=bool: Automatically apply the changes to DD-WRT/ADGuardHome
        """
        if not is_valid_mac(mac):
            custom_exit(f"{mac} is not a valid MAC address!")

        allLeases = getStaticLeases()
        mac_exists = False
        for aLease in allLeases:
            if mac == aLease[0]:
                mac_exists = True
                break

        if not mac_exists:
            custom_exit(f"MAC [{mac}] not found in static leases file!")

        allLeases = [lease for lease in allLeases if lease[0] != mac]
        success(f"MAC {mac} removed from static leases file!")

        writeStaticLeases(allLeases)

    # ======================================================================================
    def apply(self, dryrun: bool = True):
        """Apply from scratch ALL static leases from the static leases file to both
        router and ADGuardHome server.

            Positional Arguments:
                dryrun=bool: Dryrun will only print the commands to be executed, otherwise
                it will apply them.
        """

        # apply all to DDWRT
        applyAllStaticLeasesToDdwrt(dryrun=dryrun)

        # apply all to ADGuardHome
        applyAllStaticLeasesToADGuardHome(dryrun=dryrun)


# ======================================================================================
# ======================================================================================
class Backups(object):
    """Suite of commands to help with backup/restore operations"""

    def __init__(self):
        pass

    # Internal helpers ================================================================
    # ==================================================================================
    def _showLs(self, snapshot: str, dir: Path):
        info(f"Listing backups for {dir}...")
        # p1 = subprocess.Popen(
        #     f"sudo restic-system_onLocal ls -l {snapshot} dir".split(),
        #     stdout=subprocess.PIPE,
        #     stderr=subprocess.PIPE,
        # )
        # p2 = subprocess.Popen(
        #     ["sed", f"s#{dir}/##g"],
        #     stdin=p1.stdout,
        #     stdout=subprocess.PIPE,
        # )

        files = run(f"sudo restic-system_onLocal ls -l {snapshot} {dir}")

        print(
            re.sub(
                rf"{dir}\n|{dir}/",
                lambda match: "" if match.group(0) == f"{dir}/" else ".\n",
                files,
            )
        )

    # ==================================================================================
    def ls(self, dir: Path = None, latest: bool = True):
        """Show list of backuped files for the specified directory.

        Positional Arguments:
            dir=Path: The backup directory, or the current dir if not specified.
            latest=bool: Show backups from latest backup
        """

        backupDir = dir if dir else Path().resolve()

        if latest:
            self._showLs(snapshot="latest", dir=backupDir)
        else:
            snapshots = json.loads(run("sudo restic-system_onLocal snapshots --json"))

            for snap in snapshots:
                print(f"{snap.get('id')} {snap.get('time')} {snap.get('paths')}")

    def cat(self, file: str = "", latest: bool = True):
        """List contents of file, pulled from a previous backup"""

        if not file:
            custom_exit("Error: You have to provide the name of file")

        file = file if file[0] == "/" else Path().resolve() / file

        if latest:
            snapshot = "latest"
        else:
            snapshots = json.loads(run("sudo restic-system_onLocal snapshots --json"))
            snapshot = snapshots[0].get("id")

        print(run(f"sudo restic-system_onLocal dump {snapshot} {file}"))


# ======================================================================================
# ======================================================================================
class Generators(object):
    """Miscellaneous generators"""

    def __init__(self):
        pass

    # ======================================================================================
    def bcryptPass(self):
        """Use bcrypt to hash a password. Same encryption as by htpasswd."""

        passwd = getpass.getpass("Password: ")

        return bcrypt.hashpw(passwd.encode("utf-8"), bcrypt.gensalt()).decode("utf-8")


#                                                 _
#    ___ ___  _ __ ___  _ __ ___   __ _ _ __   __| |___
#   / __/ _ \| '_ ` _ \| '_ ` _ \ / _` | '_ \ / _` / __|
#  | (_| (_) | | | | | | | | | | | (_| | | | | (_| \__ \
#   \___\___/|_| |_| |_|_| |_| |_|\__,_|_| |_|\__,_|___/


# ======================================================================================
def update(caches=True):
    """
    Rebuild and switch current flake

    Positional Arguments:
       caches=Boolean: whether to use our custom binary caches

     Notes:
       Potential info here
    """
    # header(update.__doc__)

    cacheOpt = "" if caches else "--option binary-caches https://cache.nixos.org/ "
    run(f"sudo nixos-rebuild switch {cacheOpt}--flake {ULTRAGEAR}/#{currentHost}")


# ======================================================================================
def upgrade():
    """Update flake and then rebuild and switch"""
    header(upgrade.__doc__)

    info("Updating flake...")
    run("nix flake update", cwd=ULTRAGEAR)

    update()


# ======================================================================================
def packages(caches=True, upcoming: bool = False, latest: int = 1, since: int = 0):
    """Check the versions of installed, or to be installed, packages.

    Positional Arguments:
        upcoming=Boolean: Whether to list the upcoming updates
        latest=int: The number of previous generations to check against
        since=int: The number of previous generations to skip
    """
    # header(upgrade.__doc__)

    if upcoming:
        info("Updating flake...")
        run("nix flake update", cwd=ULTRAGEAR)
        info("Building based on new flake.lock...")
        cacheOpt = "" if caches else "--option binary-caches https://cache.nixos.org/ "
        run(f"sudo nixos-rebuild build {cacheOpt}--flake {ULTRAGEAR}/#{currentHost}")

        info("List of upcoming updated packages:")
        run("nvd diff /var/run/current-system ./result")
    else:
        earlGen = int(getEarliestGeneration())
        lastGen = int(getLatestGeneration())
        start = lastGen - since - latest
        end = lastGen - since
        if start < earlGen or start > lastGen or end <= start or end > lastGen:
            print(
                f"Latest generation: {lastGen}. Cannot list packages in the range: {start}-{end}"
            )
        else:
            print(
                run(
                    f"nvd --color=always diff /nix/var/nix/profiles/system-{start}-link /nix/var/nix/profiles/system-{end}-link"
                )
            )


# ======================================================================================
def generations():
    """List generations."""
    # header(upgrade.__doc__)

    print(f"Earliest Generation: {getEarliestGeneration()}")
    print(f"Latest Generation: {getLatestGeneration()}")


# ======================================================================================
def gc(days=21):
    """Delete old generations and garbage collect.

    Positional Arguments:
       days=Int: number of days to keep generations
    """

    info(f"Deleting generations older than {days} days...")
    run(f"nix-env --delete-generations {days}d")

    info("Garbage collecting...")
    run("nix-store --gc")


# ======================================================================================
def deploy(host=None):
    """Deploy flake to remote host

    Positional Arguments:
       host=String: name of host to deploy to
    """

    if not host:
        process = run(f"nix --offline flake show --all-systems --json {ULTRAGEAR}")
        if process:
            hosts = extractHosts(process)
            hosts.remove(currentHost)
            questions = [
                inquirer.List("host", message="Which host to deploy?", choices=hosts),
            ]
            answers = inquirer.prompt(questions)
            if not answers:
                custom_exit("Error: No host selected")
            host = answers["host"]
        else:
            custom_exit(f"Error extracting the hosts from the flake: {error}")

    info("Deploying...")
    # run(f"deploy --magic-rollback false --skip-checks {ULTRAGEAR}/#{host}")
    run(f"deploy --skip-checks {ULTRAGEAR}/#{host}")


# ======================================================================================
class UG(object):
    def __init__(self):
        self.gen = Generators()
        self.dhcp = Dhcp()
        self.conf = Conf()
        self.backups = Backups()
        self.update = update
        self.upgrade = upgrade
        self.gc = gc
        self.deploy = deploy
        self.packages = packages
        self.generations = generations


def main():
    fire.core.Display = lambda lines, out: out.write("\n".join(lines) + "\n")
    fire.Fire(name="ultragear", component=UG)


if __name__ == "__main__":
    main()
