{ pkgs, lib, ... }:

pkgs.writeShellApplication  {
  name = "ug-sqlitize-keybr-stats";
  # version = "v1.0";

runtimeInputs = [ pkgs.sqlite-utils ];

text = builtins.readFile ./sqlitize-keybr-stats.sh;
}
