#!/usr/bin/env bash

DB=data.db
rm -f $DB

# Check if the required parameter is missing
if [ $# -eq 0 ]; then
    echo "Error: Missing required parameter. Please provide the keybr.com json file to sqlitize."
    exit 1
fi

echo "Inserting rows to DB..."
sqlite-utils insert $DB lessons "$1" --flatten

echo "Converting histogram..."
sqlite-utils convert $DB lessons histogram \
'import json
cols = {}
for i in json.loads(value):
    ltr = chr(i["codePoint"]) if i["codePoint"] != 32 else "spc"
    cols[f"{ltr}_hitCount"] = i["hitCount"]
    cols[f"{ltr}_missCount"] = i["missCount"]
    cols[f"{ltr}_timeToType"] = i["timeToType"]
return cols' --multi

echo "Dropping histogram..."
sqlite-utils transform $DB lessons --drop histogram

echo "Vacuum DB..."
sqlite-utils vacuum $DB

echo Sqlite Db created at $DB
