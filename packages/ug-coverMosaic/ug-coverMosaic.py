#!/usr/bin/env python3

import math
import os
from io import BytesIO

from mutagen.id3 import APIC, ID3
from mutagen.mp3 import MP3
from PIL import Image, ImageOps


def extract_cover_image(mp3_file):
    audio = MP3(mp3_file, ID3=ID3)
    for tag in audio.tags.values():
        if isinstance(tag, APIC):
            # Extract the cover image and return it
            return tag.data
    return None


def create_mosaic_image(cover_images):
    # Convert the cover image data to PIL Image objects
    cover_images_pil = [Image.open(BytesIO(image_data)) for image_data in cover_images]

    # Calculate the dimensions of the mosaic image based on the number of cover images
    num_images = len(cover_images_pil)
    num_cols = math.ceil(math.sqrt(num_images))
    num_rows = num_cols

    # Calculate the size of each individual cover image in the mosaic
    cover_size = 1200 // num_cols  # Set the size of each cover image
    cover_images_resized = [
        ImageOps.fit(image, (cover_size, cover_size)) for image in cover_images_pil
    ]

    # Determine the size of the final mosaic image
    mosaic_width = num_cols * cover_size
    mosaic_height = num_rows * cover_size

    # Create a new blank image for the mosaic
    mosaic_image = Image.new("RGB", (mosaic_width, mosaic_height))

    # Paste each cover image into the mosaic image
    for i, cover_image in enumerate(cover_images_resized):
        row = i // num_cols
        col = i % num_cols
        x = col * cover_size
        y = row * cover_size
        mosaic_image.paste(cover_image, (x, y))

    return mosaic_image


# Replace 'path_to_folder' with the actual path to the folder containing the .mp3 files
folder_path = "."

# Create a list to store all the cover images
cover_images = []

for filename in os.listdir(folder_path):
    if filename.endswith(".mp3"):
        # Extract the cover image from the .mp3 file and add it to the list
        cover_image = extract_cover_image(os.path.join(folder_path, filename))
        if cover_image:
            cover_images.append(cover_image)

# Create the mosaic image
mosaic_image = create_mosaic_image(cover_images)

# Save the mosaic image
mosaic_image.save("mosaic_cover.jpg")

# Print a success message
print("Mosaic image created successfully!")
