{ pkgs, lib, ... }:

let
  a = 3;
in
pkgs.stdenv.mkDerivation {
  name = "ug-coverMosaic";
  version = "v1.1";

  propagatedBuildInputs = [
    (pkgs.python3.withPackages (pythonPackages: with pythonPackages; [
      # install required python packages here
      pillow
      mutagen
    ]))
  ];

  dontUnpack = true;

  installPhase = "install -Dm755 ${./ug-coverMosaic.py} $out/bin/ug-coverMosaic";

  preFixup = ''
    makeWrapperArgs+=(--prefix PATH : ${lib.makeBinPath [
      # Hard requirements

    ]})
  '';

  meta = with lib; {
    description = "Create a compilation album cover image from the collection of the cover images of each track.";
  };
}
