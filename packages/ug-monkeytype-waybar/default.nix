{ pkgs, lib, ... }:

let
  a = 3;
in
pkgs.stdenv.mkDerivation {
  name = "ug-monkeytype-waybar";
  version = "v1.0";

  propagatedBuildInputs = [
    (pkgs.python3.withPackages (pythonPackages: with pythonPackages; [
      # install required python packages here
      requests
    ]))
  ];

  dontUnpack = true;

  installPhase = "install -Dm755 ${./ug-monkeytype-waybar.py} $out/bin/ug-monkeytype-waybar";

  preFixup = ''
    makeWrapperArgs+=(--prefix PATH : ${lib.makeBinPath [
      # Hard requirements
    ]})
  '';

  meta = with lib; {
    description = "Provide a waybar module that tracks MonkeyType practice time.";
  };
}
