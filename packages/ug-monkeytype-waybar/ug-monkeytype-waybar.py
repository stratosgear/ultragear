#!/usr/bin/env python3
import json
import os
from datetime import datetime

import requests

with open(os.path.expanduser("~/.config/ug-monkeytype-waybar/apekey")) as f:
    ape_key = f.read()


required_training_time = 40  # in mins

training_mins_per_min = required_training_time / 60 / 24


def get_trained_so_far():
    url = "https://api.monkeytype.com/users/stats"

    headers = {"Authorization": f"ApeKey {ape_key}"}
    response = requests.get(url, headers=headers)
    if response.status_code == 200:
        data = response.json()
        # print(f"Data: {data}")
        return round(data["data"]["timeTyping"] / 60)
    else:
        return -1


def run():

    trained_so_far = get_trained_so_far()
    # trained_so_far = 30
    # print(f"Trained so far: {trained_so_far} mins")

    if trained_so_far == -1:
        print("ERR")

    minutes_so_far = datetime.now().hour * 60 + datetime.now().minute

    training_threshold_for_now = minutes_so_far * training_mins_per_min

    progress = round(trained_so_far - training_threshold_for_now)

    clazz = ""
    text = f"{progress}"
    tooltip = f"Remaining training time: {required_training_time - trained_so_far} mins"

    if progress < 0:
        clazz = "lagging"

    if trained_so_far >= required_training_time:
        clazz = "reached"
        text = f"{progress} ✅"

    print(f"{text}\n{tooltip}\n{clazz}")


if __name__ == "__main__":
    run()
