{ pkgs, lib, ... }:

let
  a = 3;
in
pkgs.stdenv.mkDerivation {
  name = "ug-albumify";
  version = "v1.2";

  propagatedBuildInputs = [
    (pkgs.python3.withPackages (pythonPackages: with pythonPackages; [
      # install required python packages here
      click
    ]))
  ];

  dontUnpack = true;

  installPhase = "install -Dm755 ${./ug-albumify.py} $out/bin/ug-albumify";

  preFixup = ''
    makeWrapperArgs+=(--prefix PATH : ${lib.makeBinPath [
      # Hard requirements
      pkgs.ffmpeg
      pkgs.id3v2
      pkgs.yt-dlp
    ]})
  '';

  meta = with lib; {
    description = "Download a youtube album and convert it to a local mp3 playlist.";
  };
}
