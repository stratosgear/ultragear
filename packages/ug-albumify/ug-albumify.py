#!/usr/bin/env python3

import datetime
import os
import re
import subprocess
import sys
from pathlib import Path
from subprocess import PIPE, Popen

import click

# https://regex101.com/r/yxIeVd/5
regex = re.compile(
    r"^(?P<id>\d+.?)?\s*(?P<title>.*?)(\((?P<artist>.*)\))?\s*\(?(?P<hrs>\d?\d:)?(?P<mins>\d?\d):(?P<secs>\d\d)\)?\s*$"
)


class Albumify:

    def __init__(self):
        self.url = ""
        self.artist = ""
        self.album = ""
        self.length = ""
        self.tracks = []

    def get_name(self):
        return self.album

    def get_valid_filename(self, s):
        """Get a valid filename from a string"""
        s = str(s).strip().replace(" ", "_")
        return re.sub(r"(?u)[^-\w.]", "", s)

    def extract_track_info(self, track_line):
        """Tries to parse a string line and extract track name, artist if exists
        and start time.
        Sample lines that can currently be handled are demonstrated at regex101 at:
        https://regex101.com/r/yxIeVd/4
        Basically looks like:
        "7. River tale (Silent Island) 22:01\n"

        """
        if not track_line:
            return

        # print(f"Line: {track_line}")
        match = regex.match(track_line)
        groups = match.groupdict()

        if groups.get("hrs"):
            start = f"{groups.get('hrs')}"
        else:
            start = f"00:"

        start = f"{start}{groups.get('mins')}:{groups.get('secs')}"
        groups["start"] = start

        # remove period from track id
        if groups.get("id"):
            groups["id"] = groups["id"][0:-1]

        # remove open/close parenthesis
        if groups.get("artist"):
            groups["artist"] = groups["artist"].replace("(", "")
            groups["artist"] = groups["artist"].replace(")", "").strip()
        else:
            groups["artist"] = self.artist

        groups["title"] = groups["title"].strip()

        return groups

    def print_tracks(self):
        """Print the list of tracks so the user can validate if parsing was correct"""

        print("This is how I parsed the playlist file:\n")

        print(f"   Album Title: {self.album}")
        print(f"        Artist: {self.artist}")
        print(f"Album Duration: {self.length}\n")

        print(f"ID. Track Title, Artist, Start-End, Duration (secs)")
        for i in range(len(self.tracks)):
            t = self.tracks[i]
            if not t.get("id"):
                self.tracks[i]["id"] = i + 1

            l = f"{self.tracks[i]['id']}. {t.get('title')}"

            if t.get("artist"):
                l = f"{l}, {t.get('artist')}"

            l = f"{l}, {t.get('start')}-{t.get('end', self.length)}, {t.get('duration')} ({t.get('seconds')} sec)"
            print(l)

    def generate_playlist(self, playlist_name, tracks):
        """Generate a mp3 playlist of the ordered tracks"""

        with open(playlist_name, "w") as pl:
            pl.write("#EXTM3U\n")
            for t in tracks:
                print(f"Playlist entry for: {int(t.get('id'))}. {t.get('title')}")
                trackfilename = self.get_valid_track_filename(
                    int(t.get("id")), t.get("title"), t.get("artist")
                )
                track_len = f"ffprobe -show_streams {trackfilename} -v fatal | grep duration= | cut -d '=' -f 2 | cut -d '.' -f 1"
                secs_output = subprocess.run(
                    track_len, shell=True, stdout=subprocess.PIPE, encoding="utf8"
                )
                secs = secs_output.stdout.strip()
                # #EXTINF:419,Alice in Chains - Rotten Apple
                pl.write(f"#EXTINF:{secs},{t.get('artist')} - {t.get('title')}\n")
                pl.write(f"{trackfilename}\n")

    def get_valid_track_filename(self, id, title, artist):
        """Get a sane filename for a saved track"""
        return self.get_valid_filename(f"{id:02d}.{title}-{artist}.mp3")

    def call_external_program(self, desc, cmd, show=False):
        """Call an external program and show the std output"""
        print(f"{desc}")
        print(f"   {cmd}")
        p = subprocess.Popen(cmd, shell=True, stderr=subprocess.PIPE, encoding="utf8")
        if show:
            ## But do not wait till finish, start displaying output immediately ##
            while True:
                out = p.stderr.read(1)
                if out == "" and p.poll() != None:
                    break
                if out != "":
                    # if show:
                    sys.stdout.write(out)
                    sys.stdout.flush()
        else:
            out, err = p.communicate()

    def parse_playlist_file(self, playlist_file):
        print(f"Parsing: {playlist_file}")

        # Playlist file format:
        # youtube file url
        # Album Name
        # Artist Name
        # 1. some title (some artist) 00:00
        # 2. other title (other artist) 05:46
        # ...
        lineno = 0
        with open(playlist_file, "r") as f:
            for line in f:
                # First line contains the youtube url to download
                if lineno == 0:
                    self.url = line.strip()
                elif lineno == 1:
                    self.album = line.strip()
                elif lineno == 2:
                    self.artist = line.strip()
                elif lineno == 3:
                    self.length = line.strip()
                else:
                    i = self.extract_track_info(line.strip())
                    self.tracks.append(i)
                lineno += 1

        # add end times to each track
        for i in range(len(self.tracks)):
            s = self.tracks[i]["start"]
            if i == len(self.tracks) - 1:
                e = self.length
            else:
                e = self.tracks[i + 1]["start"]
            d = datetime.timedelta(
                hours=int(e.split(":")[0]),
                minutes=int(e.split(":")[1]),
                seconds=int(e.split(":")[2]),
            ) - datetime.timedelta(
                hours=int(s.split(":")[0]),
                minutes=int(s.split(":")[1]),
                seconds=int(s.split(":")[2]),
            )
            self.tracks[i]["end"] = e
            self.tracks[i]["duration"] = d
            self.tracks[i]["seconds"] = d.seconds

        self.print_tracks()

        answer = "z"
        while answer.lower() not in ("y", "n", "yes", "no"):
            answer = input("\nDoes it look right? Should I proceed? (y/n) ")

        return answer.lower() in ("y", "yes")

    def download_cut_id(self):

        # Download youtube file
        outfile = self.get_valid_filename(f"{self.album}-{self.artist}")
        cmd = f"yt-dlp --no-post-overwrites -x --audio-format mp3 --audio-quality 3 -o {outfile}.%\(ext\)s {self.url}"
        self.call_external_program("Downloading file from youtube", cmd)

        # Iterate over parsed tracks
        for t in self.tracks:
            # Cut to length
            trackfilename = self.get_valid_track_filename(
                int(t.get("id")), t.get("title"), t.get("artist")
            )
            if t.get("end", ""):
                cut_command = f"ffmpeg -ss {t.get('start')} -to {t.get('end')} -i {outfile}.mp3 -c copy {trackfilename}"
            else:
                cut_command = f"ffmpeg -ss {t.get('start')} -i {outfile}.mp3 -c copy {trackfilename}"
            self.call_external_program(
                f"Cutting track {trackfilename} to size...", cut_command
            )

            # and add mp3 tags
            q_title = t.get("title").replace("'", r"\'")
            q_artist = t.get("artist").replace("'", r"\'")
            q_album = self.album.replace("'", r"\'")
            edit_mp3metata_cmd = (
                f"id3v2 -T '{t.get('id')}' -t '{q_title}' "
                + f"-a '{q_artist}' -A '{q_album}' {trackfilename}"
            )
            self.call_external_program("Adding mp3 tags", edit_mp3metata_cmd)

        # generate an mp3 playlist
        playlist_name = self.get_valid_filename(f"{self.album}-{self.artist}.m3u")
        self.generate_playlist(playlist_name, self.tracks)

        # remove the large continuous .mp3 file
        print(f"Removing intermediate file: {outfile}.mp3")
        os.remove(f"{outfile}.mp3")

        print(
            "\nYou might want to enhance the mp3 ID3 tags by running them over Piccard."
        )
        print(
            "Also, for compilation albums with separate track covers, you can create a mosaic with: ug-coverMosaic"
        )


@click.command("", context_settings={"show_default": True})
@click.option("--playlist", default="playlist.txt", help="Playlist file")
def download(playlist):
    """
    Allows you to download a video from youtube and cut it into separate mp3s.

    Requires a .txt configuration file with the following format:

    \b
    youtube file url
    Album Name
    Artist Name
    1. some title ([optional]: artist somename, [default=Artist Name]) 00:00
    2. another title ([optional]: other artist, [default=Artist Name]) 05:46
    ...

    """
    if not Path(playlist).exists():
        print(f"Playlist file [{playlist}] not found. Exiting...")
        sys.exit(-1)

    alb = Albumify()

    parsed_ok = alb.parse_playlist_file(playlist)

    if parsed_ok:
        alb.download_cut_id()


if __name__ == "__main__":
    download()
