{ pkgs, lib, ... }:

pkgs.stdenv.mkDerivation {
  name = "ug-keybr-stats";
  version = "v1.0";

  propagatedBuildInputs = [
    (pkgs.python3.withPackages (pythonPackages: with pythonPackages; [
      # install required python packages here
      sqlite-utils
      datasette
      matplotlib
      numpy
      pandas
    ]))
   pkgs.sqlite-utils
  ];

  dontUnpack = true;

  installPhase = "install -Dm755 ${./keybr-stats.py} $out/bin/ug-keybr-stats";

  preFixup = ''
    makeWrapperArgs+=(--prefix PATH : ${lib.makeBinPath [
      # Hard requirements

    ]})
  '';

  meta = with lib; {
    description = "Script to generate typing statistics from keybr.com site.";
  };
}
