# From: https://github.com/suderman/nixos/blob/d4bc3e6e350fb530e9dab914dbe5619dad9d1923/repl.nix
let
  flake = builtins.getFlake (toString ./.);
  default = import ./. { inherit (flake) inputs; };
  # this = flake.nixosConfigurations.cog.pkgs.this;
in
# { inherit flake default this; }
{ inherit flake default; }
// flake
// builtins
// flake.inputs.nixpkgs
// flake.inputs.nixpkgs.lib
  // flake.nixosConfigurations
# // this.lib
