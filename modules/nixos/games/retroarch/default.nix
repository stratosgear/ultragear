{
  options,
  config,
  pkgs,
  lib,
  ...
}:

with lib;
with lib.ultragear;
let
  cfg = config.ultragear.games.retroarch;
in
{
  options.ultragear.games.retroarch = with types; {
    enable = mkBoolOpt false "Whether or not to enable retroarch";
  };

  config = mkIf cfg.enable {

    environment.systemPackages = with pkgs; [
      (retroarch.override {
        cores = with libretro; [
          mame
        ];
      })
    ];

  };

}
