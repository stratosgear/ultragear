{
  options,
  config,
  pkgs,
  lib,
  ...
}:

with lib;
with lib.ultragear;
let
  cfg = config.ultragear.games.steam;
in
{
  options.ultragear.games.steam = with types; {
    enable = mkBoolOpt false "Whether or not to enable steam";
  };

  config = mkIf cfg.enable {

  programs.steam = {
    enable = true;
    remotePlay.openFirewall = true; # Open ports in the firewall for Steam Remote Play
    dedicatedServer.openFirewall = true; # Open ports in the firewall for Source Dedicated Server
    localNetworkGameTransfers.openFirewall = true; # Open ports in the firewall for Steam Local Network Game Transfers
  };

  };

}
