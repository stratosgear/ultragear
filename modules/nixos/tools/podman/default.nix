{ options, config, lib, pkgs, ... }:

with lib;
with lib.ultragear;
let cfg = config.ultragear.tools.podman;
in
{
  options.ultragear.tools.podman = with types; {
    enable = mkBoolOpt false "Whether or not to enable Podman.";
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      podman-compose
      podman-tui
    ];

    ultragear.home.extraOptions = {
      home.shellAliases = { "docker-compose" = "podman-compose"; };
    };

    virtualisation = {
      podman = {
        enable = cfg.enable;
        # dockerCompat = true;
      };
    };
  };
}
