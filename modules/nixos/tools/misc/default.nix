{ options, config, lib, pkgs, ... }:

with lib;
with lib.ultragear;
let cfg = config.ultragear.tools.misc;
in
{
  options.ultragear.tools.misc = with types; {
    enable = mkBoolOpt false "Whether or not to enable common utilities.";
  };

  config = mkIf cfg.enable {
    ultragear.home.configFile."wgetrc".text = "";

    environment.systemPackages = with pkgs; [
      fzf
      killall
      unzip
      file
      jq
      clac
      wget
    ];
  };
}
