{
  options,
  config,
  lib,
  pkgs,
  namespace,
  ...
}:
with lib;
with lib.${namespace};
let
  cfg = config.${namespace}.tools.comma;
in
{
  options.${namespace}.tools.comma = with types; {
    enable = mkBoolOpt false "Whether or not to enable comma (run any app without installing it).";
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      comma
      ultragear.nix-update-index
    ];

    ultragear.home = {
      configFile = {
        "wgetrc".text = "";
      };

      extraOptions = {
        programs.nix-index.enable = true;
      };
    };
  };
}