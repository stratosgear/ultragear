{ options, config, lib, pkgs, ... }:

with lib;
with lib.ultragear;
let
  cfg = config.ultragear.apps.obsidian;
in
{
  options.ultragear.apps.obsidian = with types; {
    enable = mkBoolOpt false "Whether or not to enable obsidian.";
    dataDir = mkOpt str "" "The main stratosgear vault for obsidian.";
  };

  config = mkIf cfg.enable {

    environment.systemPackages = with pkgs; [
      obsidian
    ];

    # Stanza for system restic backups
    ultragear.services.backup.instances.system = mkIf (cfg.dataDir != "") {
      sourceDirectories = [ cfg.dataDir ];
    };


    # FIXME: Also look for a hack added on flake.nix
    # in regards to allowing an insecure: electron-25.9.0 package

    # services.syncthing.settings.folders = mkIf config.ultragear.services.syncthing.enable {
    #   "ObsidianStratosgear" = {
    #     id = "ztess-fzabo";
    #     path = "/home/stratos/Documents/syncthing/Obsidian/stratosgear";
    #     devices = [ "iocaine" ];
    #     versioning = {
    #       # type = "staggered";
    #       # params = {
    #       #   cleanInterval = "3600";
    #       #   maxAge = "15768000";
    #       # };
    #       type = "simple";
    #       params = {
    #         keep = "12";
    #       };
    #     };
    #   };
    #
    # };

  };
}
