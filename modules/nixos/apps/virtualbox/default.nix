{ options, config, lib, pkgs, ... }:

with lib;
with lib.ultragear;
let
  cfg = config.ultragear.apps.virtualbox;
in
{
  options.ultragear.apps.virtualbox = with types; {
    enable = mkBoolOpt false "Whether or not to enable Virtualbox.";
  };

  config = mkIf cfg.enable {
    virtualisation.virtualbox.host = {
      enable = true;
      enableExtensionPack = true;
    };

    ultragear.user.extraGroups = [ "vboxusers" ];
  };
}
