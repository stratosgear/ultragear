{
  options,
  config,
  lib,
  pkgs,
  ...
}:

with lib;
with lib.ultragear;
let
  cfg = config.ultragear.apps.firefox;
  lock-false = {
    Value = false;
    Status = "locked";
  };
  lock-true = {
    Value = true;
    Status = "locked";
  };

  defaultPreferences = {
    "browser.bookmarks.showMobileBookmarks" = lock-true;
    "browser.contentblocking.category" = {
      Value = "strict";
      Status = "locked";
    };
    "browser.newtabpage.activity-stream.feeds.section.topstories" = lock-false;
    "browser.newtabpage.activity-stream.feeds.snippets" = lock-false;
    # "browser.newtabpage.activity-stream.section.highlights.includeDownloads" = lock-false;
    "browser.newtabpage.activity-stream.section.highlights.includePocket" = lock-false;
    "browser.newtabpage.activity-stream.section.highlights.includeVisited" = lock-false;
    "browser.newtabpage.activity-stream.showSponsored" = lock-false;
    "browser.newtabpage.activity-stream.showSponsoredTopSites" = lock-false;
    "browser.newtabpage.activity-stream.system.showSponsored" = lock-false;
    "browser.search.suggest.enabled.private" = lock-false;
    "browser.search.suggest.enabled" = lock-false;
    "browser.ssb.enabled" = lock-true;
    "browser.startup.homepage" = "https://dash.gerakakis.net";
    "browser.topsites.contile.enabled" = lock-false;
    "browser.urlbar.showSearchSuggestionsFirst" = lock-false;
    "browser.urlbar.suggest.searches" = lock-false;
    "browser.urlbar.suggest.quicksuggest.sponsored" = lock-false;
    "extensions.pocket.enabled" = lock-false;
    # "browser.formfill.enable" = lock-false;
    # "browser.newtabpage.activity-stream.section.highlights.includeBookmarks" = lock-false;
    # "extensions.screenshots.disabled" = lock-true;
    "layout.css.devPixelsPerPx" = cfg.pixelsPerPx;

  };

  defaultPolicies = {
    DisableTelemetry = true;
    DisableFirefoxStudies = true;
    EnableTrackingProtection = {
      Value = true;
      Locked = true;
      Cryptomining = true;
      Fingerprinting = true;
    };
    DisablePocket = true;
    DisableFirefoxAccounts = false;
    DisableAccounts = false;
    DisableFirefoxScreenshots = true;
    OverrideFirstRunPage = "";
    OverridePostUpdatePage = "";
    DontCheckDefaultBrowser = true;
    DisplayBookmarksToolbar = "never"; # alternatives: "always" or "newtab"
    DisplayMenuBar = "default-off"; # alternatives: "always", "never" or "default-on"
    SearchBar = "unified"; # alternative: "separate"!

    # ---- PREFERENCES ----
    # Check about:config for options.
    Preferences = defaultPreferences;

    # ---- EXTENSIONS ----
    ExtensionSettings =
      with builtins;
      let
        extension = shortId: uuid: {
          name = uuid;
          value = {
            install_url = "https://addons.mozilla.org/en-US/firefox/downloads/latest/${shortId}/latest.xpi";
            installation_mode = "normal_installed";
          };
        };
      in
      listToAttrs [
        (extension "tree-style-tab" "treestyletab@piro.sakura.ne.jp")
        (extension "ublock-origin" "uBlock0@raymondhill.net")
        (extension "bitwarden-password-manager" "{446900e4-71c2-419f-a6a7-df9c091e268b}")
        (extension "clearurls" "{74145f27-f039-47ce-a470-a662b129930a}")
        (extension "the-camelizer-price-history-ch" "izer@camelcamelcamel.com")
        (extension "wallabager" "{7a7b1d36-d7a4-481b-92c6-9f5427cb9eb1}")
        (extension "xbrowsersync" "{019b606a-6f61-4d01-af2a-cea528f606da}")
      ];
    # To add additional extensions, find it on addons.mozilla.org, find
    # the short ID in the url (like https://addons.mozilla.org/en-US/firefox/addon/!SHORT_ID!/)
    # Then, download the XPI by filling it in to the install_url template, unzip it,
    # run `jq .browser_specific_settings.gecko.id manifest.json` or
    # `jq .applications.gecko.id manifest.json` to get the UUID
  };

in
{
  options.ultragear.apps.firefox = with types; {
    enable = mkBoolOpt false "Whether or not to enable Firefox.";
    pixelsPerPx = mkOpt str "1.0" "Set firefox zoom level. Used in config as layout.css.devPixelsPerPx";
    extraConfig = mkOpt str "" "Extra configuration for the user profile JS file.";
    userChrome = mkOpt str "" "Extra configuration for the user chrome CSS file.";
    settings = mkOpt attrs defaultPreferences "Settings to apply to the profile.";
  };

  config = mkIf cfg.enable {

    ultragear.home = {

      extraOptions = {
        programs.firefox = {
          enable = true;

          policies = defaultPolicies;

          profiles.${config.ultragear.user.name} = {
            # inherit (cfg) extraConfig userChrome settings;
            inherit (cfg) extraConfig userChrome;
            id = 0;
            isDefault = true;
            name = config.ultragear.user.name;

            search = {
              force = true;
              default = "Google";
              order = [
                "DuckDuckGo"
                "Google"
              ];
              engines = {
                "Amazon.es".metaData.alias = "@a";
                "Bing".metaData.hidden = true;
                "eBay".metaData.hidden = true;
                "Google".metaData.alias = "@g";
                "Wikipedia (en)".metaData.alias = "@w";

                "GitHub" = {
                  urls = [
                    {
                      template = "https://github.com/search";
                      params = [
                        {
                          name = "q";
                          value = "{searchTerms}";
                        }
                      ];
                    }
                  ];
                  icon = "/home/stratos/assets/svg/github.svg";
                  definedAliases = [ "@gh" ];
                };

                "Nix Packages" = {
                  urls = [
                    {
                      template = "https://search.nixos.org/packages";
                      params = [
                        {
                          name = "channel";
                          value = "unstable";
                        }
                        {
                          name = "query";
                          value = "{searchTerms}";
                        }
                      ];
                    }
                  ];
                  icon = "/home/stratos/assets/svg/nixos.svg";
                  definedAliases = [ "@np" ];
                };

                "Nix OLD Packages" = {
                  urls = [
                    {
                      template = "https://lazamar.co.uk/nix-versions";
                      params = [
                        {
                          name = "channel";
                          value = "nixpkgs-unstable";
                        }
                        {
                          name = "package";
                          value = "{searchTerms}";
                        }
                      ];
                    }
                  ];
                  icon = "/home/stratos/assets/svg/nixos-old-packages.svg";
                  definedAliases = [ "@nop" ];
                };

                "NixOS Wiki" = {
                  urls = [
                    {
                      template = "https://nixos.wiki/index.php";
                      params = [
                        {
                          name = "search";
                          value = "{searchTerms}";
                        }
                      ];
                    }
                  ];
                  icon = "/home/stratos/assets/svg/nixos-wiki.svg";
                  definedAliases = [ "@nw" ];
                };

                "Nixpkgs Issues" = {
                  urls = [
                    {
                      template = "https://github.com/NixOS/nixpkgs/issues";
                      params = [
                        {
                          name = "q";
                          value = "{searchTerms}";
                        }
                      ];
                    }
                  ];
                  icon = "/home/stratos/assets/svg/nixos-issues.svg";
                  definedAliases = [ "@ni" ];
                };

                "HomeManager options" = {
                  urls = [
                    {
                      template = "https://home-manager-options.extranix.com/";
                      params = [
                        {
                          name = "query";
                          value = "{searchTerms}";
                        }
                        {
                          name = "release";
                          value = "master";
                        }
                      ];
                    }
                  ];
                  icon = "/home/stratos/assets/svg/home-manager.svg";
                  definedAliases = [ "@nh" ];
                };

                "Github Nixos nuggets" = {
                  urls = [
                    {
                      template = "https://github.com/search";
                      params = [
                        {
                          name = "q";
                          value = "language%3Anix+{searchTerms}";
                        }
                        {
                          name = "type";
                          value = "code";
                        }
                      ];
                    }
                  ];
                  icon = "/home/stratos/assets/svg/nixos-github-nuggets.svg";
                  definedAliases = [ "@ng" ];
                };

                "Youtube" = {
                  urls = [
                    {
                      template = "https://www.youtube.com/results";
                      params = [
                        {
                          name = "search_query";
                          value = "{searchTerms}";
                        }
                      ];
                    }
                  ];
                  icon = "${pkgs.fetchurl {
                    url = "www.youtube.com/s/desktop/8498231a/img/favicon_144x144.png";
                    sha256 = "sha256-lQ5gbLyoWCH7cgoYcy+WlFDjHGbxwB8Xz0G7AZnr9vI=";
                  }}";
                  definedAliases = [ "@y" ];
                };
              };
            };
          };

          profiles.work = {
            # inherit (cfg) extraConfig userChrome settings;
            inherit (cfg) extraConfig userChrome;
            id = 1;
            isDefault = false;
            name = "work";
          };
        };
      };
    };
  };
}
