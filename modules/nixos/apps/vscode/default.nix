{ options, config, lib, pkgs, ... }:

with lib;
with lib.ultragear;
let cfg = config.ultragear.apps.vscode;
in
{
  options.ultragear.apps.vscode = with types; {
    enable = mkBoolOpt false "Whether or not to enable vscode.";
  };

  config = mkIf cfg.enable {

    environment.systemPackages = with pkgs; [
      vscode.fhs

      # Language server for typst
      tinymist
    ];

  };
}
