{ options, config, lib, pkgs, ... }:

with lib;
with lib.ultragear;
let
  cfg = config.ultragear.apps.chromium;
in
{
  options.ultragear.apps.chromium = with types; {
    enable = mkBoolOpt false "Whether or not to enable chromium.";
  };

  config = mkIf cfg.enable {

    environment.systemPackages = [
      pkgs.chromium
    ];

    programs.chromium = {
      enable = true;
      extensions = [
        "cjpalhdlnbpafiamejdnhcphjbkeiagm" # uBlock Origin
        "kglhbbefdnlheedjiejgomgmfplipfeb" # Jitsi Meetings
        "nngceckbapebfimnlniiiahkandclblb" # Bitwarden
        "fihnjjcciajhdojfnbdddfaoknhalnja" # I don't care about cookies
        "aapbdbdomjkkjkaonfhkkikfgjllcleb" # Google Translate
        "fdpohaocaechififmbbbbbknoalclacl" # Full Page Screen Capture
        "edibdbjcniadpccecjdfdjjppcpchdlm" # I still dont care about cookies
        "ldpochfccmkkmhdbclfhpagapcfdljkj" # Decentraleyes
        # "bkkmolkhemgaeaeggcmfbghljjjoofoh" # Theme Catppuccin Mocha
        "cmpdlhmnmjhihmcfnigoememnffkimlk" # Theme Catppuccin Macchiato
      ];
      extraOpts = {
        "BrowserSignin" = 0;
        "SyncDisabled" = true;
        "PasswordManagerEnabled" = false;
        "BuiltInDnsClientEnabled" = false;
        "DeviceMetricsReportingEnabled" = false;
        "ReportDeviceCrashReportInfo" = false;
        "SpellcheckEnabled" = true;
        "SpellcheckLanguage" = [
          "en-US"
        ];
        "CloudPrintSubmitEnabled" = false;
      };
    };

  };
}
