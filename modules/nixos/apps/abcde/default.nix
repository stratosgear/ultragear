{
  options,
  config,
  lib,
  pkgs,
  ...
}:

with lib;
with lib.ultragear;
let
  cfg = config.ultragear.apps.abcde;
  boolYN = (b: if b then "y" else "n");
  abcdeConfigFile =
    let
      OUTPUTFORMAT = "\${OUTPUT}/\${ARTISTFILE}/\${ALBUMFILE}/\${TRACKNUM} \${TRACKFILE}";
      VAOUTPUTFORMAT = "\${OUTPUT}/Various Artists/\${ALBUMFILE}/\${TRACKNUM} \${ARTISTFILE} - \${TRACKFILE}";
      ONETRACKOUTPUTFORMAT = "\${OUTPUT}/\${ARTISTFILE}/\${ALBUMFILE}/\${ALBUMFILE}";
      VAONETRACKOUTPUTFORMAT = "\${OUTPUT}/Various Artists/\${ALBUMFILE}/\${ALBUMFILE}";
      PLAYLISTFORMAT = "\${OUTPUT}/\${ARTISTFILE}/\${ALBUMFILE}/\${ALBUMFILE}.m3u";
      VAPLAYLISTFORMAT = "\${OUTPUT}/Various Artists/\${ALBUMFILE}/\${ALBUMFILE}.m3u";
      configText = ''
        INTERACTIVE=n
        FLACENCODERSYNTAX=flac
        FLAC=flac
        CDROMREADERSYNTAX=cdparanoia

        OUTPUTFORMAT='${OUTPUTFORMAT}'
        VAOUTPUTFORMAT='${VAOUTPUTFORMAT}'
        ONETRACKOUTPUTFORMAT='${ONETRACKOUTPUTFORMAT}'
        VAONETRACKOUTPUTFORMAT='${VAONETRACKOUTPUTFORMAT}'
        PLAYLISTFORMAT='${PLAYLISTFORMAT}'
        VAPLAYLISTFORMAT='${VAPLAYLISTFORMAT}'
        mungefilename ()
        {
          echo "$@" | sed -e 's/^\.*//' | tr -d ":><|*/\"'?[:cntrl:]"
        }
        EXTRAVERBOSE=2
        COMMENT='${replaceStrings [ "/nix/store/" ] [ "" ] (toString pkgs.abcde)}'
        EJECTCD=y
        MAXPROCS=${toString cfg.maxEncoderProcesses}
        PADTRACKS=y
        OUTPUTDIR='${cfg.outputPath}'
        FLACOPTS='${cfg.flacOpts}'
        OUTPUTTYPE=flac
        WAVOUTPUTDIR=/tmp
      '';
    in
    pkgs.writeTextFile {
      name = "abcde.conf";
      text = configText;
    };
in
{
  options.ultragear.apps.abcde = with types; {
    enable = mkBoolOpt false "Whether or not to enable Audio CD ripping with abcde.";
    outputPath = mkOpt str "/data/safe/music/ripped" "Output path to save ripped CDs";
    maxEncoderProcesses = mkOpt int 4 "Maximum number of encoder processes.";
    flacOpts = mkOpt str "-s -e -V -8" "Options passed to FLAC encoder.";
  };

  config = mkIf cfg.enable {

    systemd.tmpfiles.rules = [
      "d ${cfg.outputPath} 0750 root root -"
    ];
    
    environment = {
      systemPackages = [
        pkgs.abcde
      ];
      etc."abcde/abcde.conf" = {
        mode = "0644";
        source = abcdeConfigFile;
      };
    };

  };
}
