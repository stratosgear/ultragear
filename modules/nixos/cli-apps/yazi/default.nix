{ lib
, config
, pkgs
, ...
}:
with lib;
with lib.ultragear; let
  cfg = config.ultragear.cli-apps.yazi;
in
{
  options.ultragear.cli-apps.yazi = {
    enable = mkBoolOpt false "Whether or not to enable yazi.";
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      yazi
    ];
    ultragear.home.configFile."yazi/keymap.toml".source = ./keymap.toml;

    ultragear.home.extraOptions = {
      programs.zsh.initExtra = ''
        function y() {
        	local tmp="$(mktemp -t "yazi-cwd.XXXXXX")" cwd
        	yazi "$@" --cwd-file="$tmp"
        	if cwd="$(command cat -- "$tmp")" && [ -n "$cwd" ] && [ "$cwd" != "$PWD" ]; then
        		builtin cd -- "$cwd"
        	fi
        	rm -f -- "$tmp"
        }
      '';
    };
  };
}
