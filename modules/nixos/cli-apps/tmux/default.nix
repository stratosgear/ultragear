{
  lib,
  config,
  pkgs,
  ...
}:
with lib;
with lib.ultragear; let
  cfg = config.ultragear.cli-apps.tmux;
in {
  options.ultragear.cli-apps.tmux = {
    enable = mkEnableOption "Tmux";
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      # ultragear.tmux
      tmux
    ];
    ultragear.home.configFile."tmux/tmux.conf".source = ./tmux.conf;
  };
}
