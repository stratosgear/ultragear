{
  lib,
  config,
  pkgs,
  ...
}:
with lib;
with lib.ultragear; let
  cfg = config.ultragear.cli-apps.zellij;
in {
  options.ultragear.cli-apps.zellij = {
    enable = mkBoolOpt false "Whether or not to enable zellij.";
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      zellij
    ];
    ultragear.home.configFile."zellij/config.kdl".source = ./config.kdl;
  };
}
