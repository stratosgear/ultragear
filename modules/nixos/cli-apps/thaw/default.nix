{
  config,
  lib,
  pkgs,
  ...
}:
with lib;
with lib.ultragear; let
  cfg = config.ultragear.cli-apps.thaw;
in {
  options.ultragear.cli-apps.thaw = with types; {
    enable = mkBoolOpt false "Whether or not to enable thaw.";
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      snowfallorg.thaw
    ];
  };
}
