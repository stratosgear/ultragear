{ options, config, lib, pkgs, ... }:

with lib;
with lib.ultragear;
let
  cfg = config.ultragear.cli-apps.proton;
in
{
  options.ultragear.cli-apps.proton = with types; {
    enable = mkBoolOpt false "Whether or not to enable Proton.";
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [ proton-caller ];
  };
}
