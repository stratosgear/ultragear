{
  options,
  config,
  pkgs,
  inputs,
  lib,
  ...
}:

with lib;
with lib.ultragear;
let
  cfg = config.ultragear.desktop.hyprland;
in
{
  options.ultragear.desktop.hyprland = with types; {
    enable = mkBoolOpt false "Whether or not to enable hyprland";
    monitors = mkOpt (listOf str) [
      ", preferred, auto, 1"
    ] "Default setting for accepting random monitors";
    # We are not ready to add the monkeytype module yet, until fixed
    # "custom/monkeytype",
    waybar_modules_right = mkOpt str ''
      "custom/media",
      "pulseaudio",
      "network",
      "cpu",
      "memory",
      "backlight",
      "hyprland/language",
      "idle_inhibitor",
      "power-profiles-daemon",
      "tray",
      "custom/power",
      "clock",
      "custom/notification"
    '' "List of modules for right part of waybar";
  };

  config = mkIf cfg.enable {

    # Login manager
    services.displayManager.sddm = {
      enable = true;
      wayland.enable = true;
      theme = "sddm-astronaut-theme";
      package = pkgs.kdePackages.sddm;
      extraPackages = with pkgs; [
        qt6.qt5compat
        qt6.qtmultimedia
      ];
    };

    programs.hyprland = {
      enable = true;

      # set the flake package
      # package = inputs.hyprland.packages."${pkgs.system}".hyprland;
      package = inputs.hyprland.packages.${pkgs.stdenv.hostPlatform.system}.hyprland;

      xwayland.enable = true;

      # make sure to also set the portal package, so that they are in sync
      portalPackage =
        inputs.hyprland.packages.${pkgs.stdenv.hostPlatform.system}.xdg-desktop-portal-hyprland;
    };

    # Desktop additions
    ultragear.desktop.addons = {
      kitty = enabled;
      ghostty = enabled;
      swaync = enabled;
      pcmanfm = enabled;
      xdg-portal = enabled;
      rofi = enabled;
    };

    environment = {
      systemPackages = with pkgs; [

        # https://wiki.hyprland.org/FAQ/#screenshare--obs-no-worky
        # qt6-wayland
        qadwaitadecorations-qt6
        arc-theme

        pyprland
        hyprpicker
        hyprcursor
        inputs.rose-pine-hyprcursor.packages.${pkgs.system}.default
        rose-pine-cursor
        hyprlock
        hypridle

        # used by wpaperd
        mesa
        egl-wayland

        # hyprland bar
        gnome-bluetooth
        libgtop
        brightnessctl
        gpu-screen-recorder
        hyprsunset
        matugen

        # allows the use of gsettings
        glib

        # clipboard manager
        copyq
        wl-clipboard
        wtype

        #screenshotting
        grim
        slurp
        jq
        satty

        # bar
        networkmanagerapplet
        playerctl
        waybar

        # install custom scripts
        inputs.mediaplayer.packages.${pkgs.system}.default
        inputs.wpaperd.packages.${pkgs.system}.default

        # install custom waybar modules
        ultragear.ug-monkeytype-waybar

        # sddm theme
        sddm-astronaut
      ];

      sessionVariables = {
        # tell electron apps to use wayland
        NIXOS_OZONE_WL = "1";
      };

    };

    # opengl
    hardware.graphics = {
      enable = true;
      enable32Bit = true;
    };

    # xdg.portal.enable = true;
    # xdg.portal.extraPortals = [ pkgs.xdg-desktop-portal-gtk ];

    ultragear.nix.extra-substituters = {
      "https://hyprland.cachix.org".key =
        "hyprland.cachix.org-1:a7pgxzMz7+chwVL3/pzj6jIBMioiJM7ypFP8PwtkuGc=";
    };

  };
}
