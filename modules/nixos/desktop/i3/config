# i3 config file (v4)
#
# Please see http://i3wm.org/docs/userguide.html for a complete reference!

# fonts and font cheatsheet from:
# https://github.com/ryanoasis/nerd-fonts


set $fontSize      10

#           _
#  ___  ___| |_ _   _ _ __
# / __|/ _ \ __| | | | '_ \
# \__ \  __/ |_| |_| | |_) |
# |___/\___|\__|\__,_| .__/
#                    |_|

# Solarized colors
# set $base03    #002b36
# set $base02    #073642
# set $base01    #586e75
# set $base00    #657b83
# set $base0     #839496
# set $base1     #93a1a1
# set $base2     #eee8d5
# set $base3     #fdf6e3
# set $yellow    #b58900
# set $orange    #cb4b16
# set $red       #dc322f
# set $magenta   #d33682
# set $violet    #6c71c4
# set $blue      #268bd2
# set $cyan      #2aa198
# set $green     #859900

# https://i3wm.org/docs/userguide.html#_changing_colors
######################################
# color settings for bar and windows #
######################################

# Define colors variables:
set $darkbluetrans      #08052be6
set $darkblue           #08052b
set $lightblue          #5294e2
set $urgentred          #e53935
set $white              #ffffff
set $black              #000000
set $purple             #e345ff
set $darkgrey           #383c4a
set $grey               #b0b5bd
set $mediumgrey         #8b8b8b
set $yellowbrown        #e1b700

# define colors for windows:
#class                    border        bground        text        indicator    child_border
client.focused           $lightblue    $darkblue     $white        $purple      $mediumgrey
client.unfocused         $darkblue     $darkblue     $grey         $purple      $darkgrey
client.focused_inactive  $darkblue     $darkblue     $grey         $purple      $black
client.urgent            $urgentred    $urgentred    $white        $purple      $yellowbrown


set $mod Mod4
set $alt Mod1

# www
set $wrksp1 "1"

#terms
set $wrksp2 "2"

#ide
set $wrksp3 "3"

# Preview
set $wrksp4 "4"

# Email
set $wrksp5 "5"

# Comms
set $wrksp6 "6"

# Dev
set $wrksp7 "7"

# AUX
set $wrksp8 "8"

# Music
set $wrksp9 "9"

#VMs
set $wrksp0 "0"


# Font for window titles. Will also be used by the bar unless a different font
# is used in the bar {} block below. ISO 10646 = Unicode
# font -misc-fixed-medium-r-normal--13-120-75-75-C-70-iso10646-1
# The font above is very space-efficient, that is, it looks good, sharp and
# clear in small sizes. However, if you need a lot of unicode glyphs or
# right-to-left text rendering, you should instead use pango for rendering and
# chose a FreeType font, such as:
font pango:Iosevka Nerd Font, Font Awesome 6 Free $fontSize

# Use Mouse+$mod to drag floating windows to their wanted position
floating_modifier $mod

# Delay reseting the urgency state hinting when switching workspaces
force_display_urgency_hint 2000 ms

# Switch to previous workspace by pressing the same workspace number again
workspace_auto_back_and_forth yes

new_window pixel 1
# hide_edge_borders both

# Set inner/outer gaps
gaps inner 8px
gaps outer 2px
smart_gaps on


#      _                _             _
#  ___| |__   ___  _ __| |_ ___ _   _| |_ ___
# / __| '_ \ / _ \| '__| __/ __| | | | __/ __|
# \__ \ | | | (_) | |  | || (__| |_| | |_\__ \
# |___/_| |_|\___/|_|   \__\___|\__,_|\__|___/

# How to read keybinding codes:
# xev | awk -F'[ )]+' '/^KeyPress/ { a[NR+2] } NR in a { printf "%-3s %s\n", $5, $8 }'

bindsym $mod+Return exec @term@
bindsym $mod+Shift+Return exec pcmanfm

# kill focused window, key:q
bindcode $mod+Shift+24 kill

# focus window with the cursor keys:
bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

# move windows with the cursor keys:
bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right

# split in horizontal orientation, key:h
bindcode $mod+43 split h

# split in vertical orientation, key:v
bindcode $mod+55 split v

# enter fullscreen mode for the focused container, key:f
bindcode $mod+41 fullscreen

# change container layout
#(stacked key:s, tabbed key:w, toggle split key:e)
bindcode $mod+39 layout stacking
bindcode $mod+25 layout tabbed
bindcode $mod+26 layout toggle split

# toggle tiling / floating
bindsym $mod+Shift+space floating toggle

# change focus between tiling / floating windows
bindsym $mod+space focus mode_toggle

# focus the parent container, key=p
bindcode $mod+33 focus parent

# focus the child container, key:c
bindcode $mod+54 focus child

# switch to workspace
bindsym $mod+1 workspace $wrksp1
bindsym $mod+2 workspace $wrksp2
bindsym $mod+3 workspace $wrksp3
bindsym $mod+4 workspace $wrksp4
bindsym $mod+5 workspace $wrksp5
bindsym $mod+6 workspace $wrksp6
bindsym $mod+7 workspace $wrksp7
bindsym $mod+8 workspace $wrksp8
bindsym $mod+9 workspace $wrksp9
bindsym $mod+0 workspace $wrksp0

bindsym $mod+Control+Left workspace prev
bindsym $mod+Control+Right workspace next

bindsym $mod+$alt+Left move workspace to output left
bindsym $mod+$alt+Right move workspace to output right
bindsym $mod+$alt+Up move workspace to output up
bindsym $mod+$alt+Down move workspace to output down

# move focused container to workspace
bindsym $mod+Shift+1 move container to workspace number $wrksp1
bindsym $mod+Shift+2 move container to workspace number $wrksp2
bindsym $mod+Shift+3 move container to workspace number $wrksp3
bindsym $mod+Shift+4 move container to workspace number $wrksp4
bindsym $mod+Shift+5 move container to workspace number $wrksp5
bindsym $mod+Shift+6 move container to workspace number $wrksp6
bindsym $mod+Shift+7 move container to workspace number $wrksp7
bindsym $mod+Shift+8 move container to workspace number $wrksp8
bindsym $mod+Shift+9 move container to workspace number $wrksp9
bindsym $mod+Shift+0 move container to workspace number $wrksp0

# reload the configuration file, key:c
bindcode $mod+Shift+54 reload

# restart i3 inplace key:r (preserves your layout/session, can be used to upgrade i3)
bindcode $mod+Shift+27 restart

# keys: t, y, u
bindcode $mod+28 border normal
bindcode $mod+29 border pixel
bindcode $mod+30 border none

# laptop monitor screen brightnes controls
bindsym XF86MonBrightnessUp exec brightnessctl s +5%
bindsym XF86MonBrightnessDown exec brightnessctl s 5%-

# Pulse Audio controls
# increase sound volume
bindsym XF86AudioRaiseVolume exec --no-startup-id pactl set-sink-volume 0 +5%
bindsym $mod+F3 exec --no-startup-id pactl set-sink-volume 0 +5%
# decrease sound volume
bindsym XF86AudioLowerVolume exec --no-startup-id pactl set-sink-volume 0 -5%
bindsym $mod+F2 exec --no-startup-id pactl set-sink-volume 0 -5%
# mute sound
bindsym XF86AudioMute exec --no-startup-id pactl set-sink-mute 0 toggle
# bindsym $mod+F1 exec --no-startup-id pactl set-sink-mute 0 toggle

# Screenshots
bindsym --release $mod+P exec --no-startup-id flameshot gui
bindsym --release $mod+Shift+P exec --no-startup-id "sleep 5 && flameshot gui"

# scratchpad
# Make the currently focused window a scratchpad
bindsym $mod+Shift+minus move scratchpad
# Show the first scratchpad window
bindsym $mod+minus scratchpad show

# rotate wallpapers
# X11 option:
bindsym $mod+slash exec --no-startup-id x11-wallpapers-rotate
# Sway option:
# bindsym $mod+slash exec --no-startup-id ~/dotfiles/bin/set_walls

# All available i3 keybinds, in a fancy rofi menu (automated):
bindsym $mod+F1 exec --no-startup-id i3-keybindings

# Jump to urgent window (mod + z)
bindcode $mod+52 [urgent=latest] focus

# logout, reboot, restart
bindsym $mod+x exec --no-startup-id i3-powermenu
# #HOWTO: Debugging misbehaving scripts, with output to logfile
# bindsym $mod+x exec "i3-blur-lock &> /tmp/i3blurlock.log"

# Videoconference bindings
# TODO: Fix shell script
bindsym $mod+Shift+v exec --no-startup-id ~/.local/bin/webcamtoggle

# Rofi menus
# Main application menu
bindsym $mod+period exec rofi -modi drun -show drun -config ~/.config/rofi/appsMenu.rasi
# Running apps menu
bindsym $mod+comma exec rofi -show window -config ~/.config/rofi/appsMenu.rasi

#                _                                  _
#   __ _ ___ ___(_) __ _ _ __   __      _____  _ __| | _____ _ __   __ _  ___ ___  ___
#  / _` / __/ __| |/ _` | '_ \  \ \ /\ / / _ \| '__| |/ / __| '_ \ / _` |/ __/ _ \/ __|
# | (_| \__ \__ \ | (_| | | | |  \ V  V / (_) | |  |   <\__ \ |_) | (_| | (_|  __/\__ \
#  \__,_|___/___/_|\__, |_| |_|   \_/\_/ \___/|_|  |_|\_\___/ .__/ \__,_|\___\___||___/
#                  |___/                                    |_|

# move applications to specific workspaces
# find window class and instance with xprop (from a terminal)
# xprop replies with: WM_CLASS(STRING) = "instance", "class"
assign [class="thunderbird" instance="Mail"] $wrksp5
#assign [class="Rhythmbox" instance="rhythmbox"] $wrksp9
assign [class="VirtualBox" instance="Qt-subapplication"] $wrksp0


#                      _
#  _ __ ___   ___   __| | ___  ___
# | '_ ` _ \ / _ \ / _` |/ _ \/ __|
# | | | | | | (_) | (_| |  __/\__ \
# |_| |_| |_|\___/ \__,_|\___||___/

# resize panes
#--------------------------------------------------

mode "resize" {
        # These bindings trigger as soon as you enter the resize mode

        # same bindings, but for the arrow keys
        bindsym Left resize shrink width 10 px or 10 ppt
        bindsym Down resize grow height 10 px or 10 ppt
        bindsym Up resize shrink height 10 px or 10 ppt
        bindsym Right resize grow width 10 px or 10 ppt

        # back to normal: Enter or Escape
        bindsym Return mode "default"
        bindsym Escape mode "default"
}
# key:r
bindcode $mod+27 mode "resize"


set $mode_gaps Gaps: (o) outer, (i) inner
set $mode_gaps_outer Outer Gaps: use  [+|-|0] for local window or +Shift for global
set $mode_gaps_inner Inner Gaps: use  [+|-|0] for local window or +Shift for global
bindsym $mod+$alt+g mode "$mode_gaps"

mode "$mode_gaps" {
        bindsym o      mode "$mode_gaps_outer"
        bindsym i      mode "$mode_gaps_inner"
        bindsym Return mode "default"
        bindsym Escape mode "default"
}

mode "$mode_gaps_inner" {
        bindsym plus  gaps inner current plus 5
        bindsym minus gaps inner current minus 5
        bindsym 0     gaps inner current set 0

        bindsym Shift+plus  gaps inner all plus 5
        bindsym Shift+minus gaps inner all minus 5
        bindsym Shift+0     gaps inner all set 0

        bindsym Return mode "default"
        bindsym Escape mode "default"
}
mode "$mode_gaps_outer" {
        bindsym plus  gaps outer current plus 5
        bindsym minus gaps outer current minus 5
        bindsym 0     gaps outer current set 0

        bindsym Shift+plus  gaps outer all plus 5
        bindsym Shift+minus gaps outer all minus 5
        bindsym Shift+0     gaps outer all set 0

        bindsym Return mode "default"
        bindsym Escape mode "default"
}


#      _        _               _
#  ___| |_ __ _| |_ _   _ ___  | |__   __ _ _ __
# / __| __/ _` | __| | | / __| | '_ \ / _` | '__|
# \__ \ || (_| | |_| |_| \__ \ | |_) | (_| | |
# |___/\__\__,_|\__|\__,_|___/ |_.__/ \__,_|_|

# bar {
# # Bars are nandled by Polybar.
# }
# Start our status bars

exec_always --no-startup-id start-polybar

#              _               __ _             _
#   __ _ _   _| |_ ___        / _| | ___   __ _| |_
#  / _` | | | | __/ _ \ _____| |_| |/ _ \ / _` | __|
# | (_| | |_| | || (_) |_____|  _| | (_) | (_| | |_
#  \__,_|\__,_|\__\___/      |_| |_|\___/ \__,_|\__|

# To get the class and instance of a window you want to float:
#  * Open the window
#  * From a console run `xprop WM_CLASS` and click on the opened window
#  * Look for a reference of: WM_CLASS(STRING) = "instance", "class"

# For convenience float certain kind of windows, floaters
for_window [class="File-roller" instance="file-roller"] floating enable
for_window [class="Yad" instance="yad"] floating enable
for_window [class="copyq" instance="copyq"] floating enable
for_window [title="Write:.*" class="Thunderbird"] floating enable

# Bindings to make the webcam float and stick.
for_window [title="mpvfloat"] floating enable
for_window [title="mpvfloat"] sticky enable
for_window [title="mpvfloat"] border pixel 0
no_focus [title="mpvfloat"]
for_window [title="vlcwebcamfeed"] floating enable
for_window [title="vlcwebcamfeed"] sticky enable
for_window [title="vlcwebcamfeed"] border pixel 0
no_focus [title="vlcwebcamfeed"]

for_window [class="Xsane" instance="xsane"] floating enable
for_window [class="Pavucontrol" instance="pavucontrol"] floating enable
for_window [class="qt5ct" instance="qt5ct"] floating enable
for_window [class="Bluetooth-sendto" instance="bluetooth-sendto"] floating enable
for_window [class="Mirage"] floating enable


#      _             _
#  ___| |_ __ _ _ __| |_ _   _ _ __     __ _ _ __  _ __  ___
# / __| __/ _` | '__| __| | | | '_ \   / _` | '_ \| '_ \/ __|
# \__ \ || (_| | |  | |_| |_| | |_) | | (_| | |_) | |_) \__ \
# |___/\__\__,_|_|   \__|\__,_| .__/   \__,_| .__/| .__/|___/
#                             |_|           |_|   |_|

# transparency
# uncomment one of them to be used (picom package is installed per default)
# options could need changes, related to used GPU and drivers.
# to find the right setting consult the archwiki or ask at the forum.
# xcompmgr: https://wiki.archlinux.org/title/Xcompmgr
#exec --no-startup-id xcompmgr -C -n &
# or an more specialized config like this:
#exec --no-startup-id xcompmgr -c -C -t-5 -l-5 -r4.2 -o.55 &
# or:
# picom: https://wiki.archlinux.org/title/Picom
#exec --no-startup-id picom -CGb

# TODO: Activate only if picom is enabled
# exec_always --no-startup-id /usr/bin/picom --config ~/.config/picom/picom.conf

# set powersavings for display:
exec --no-startup-id xset s 480 dpms 600 600 600

# start a script to setup displays
# uncomment the next line, use arandr to setup displays and save the file as monitor:
# exec --no-startup-id ~/.screenlayout/monitor.sh

# This maybe should be handled outside of here?
# exec_always --no-startup-id xrandr --dpi $dpi

# Desktop notifications
exec --no-startup-id dbus-launch dunst --config ~/.config/dunst/dunstrc

# Load a startup wallpaper image
exec --no-startup-id x11-wallpapers-rotate

# Volume control manager
exec_always --no-startup-id volctl

# NetworkManager gui
exec_always --no-startup-id nm-applet

# Bluetooth manager
exec_always --no-startup-id blueman-applet

# clipboard manager
exec_always --no-startup-id copyq

# Screenshot
exec_always --no-startup-id flameshot

#exec --no-startup-id     gnome-settings-daemon
# exec --no-startup-id     xflux -l 40.44689 -g -4.0048
#exec --no-startup-id     davmail
#exec --no-startup-ip    variety

# do not beep on terminal
exec --no-startup-id     xset -b

