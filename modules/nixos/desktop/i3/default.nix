{ options, config, lib, pkgs, ... }:

with lib;
with lib.ultragear;
let
  cfg = config.ultragear.desktop.i3;
  substitutedConfig = pkgs.substituteAll {
    src = ./config;
    term = "kitty";
  };
  i3-powermenu = pkgs.writeShellScriptBin "i3-powermenu"
    (builtins.readFile ./scripts/powermenu);
  i3-keybindings = pkgs.writeShellScriptBin "i3-keybindings"
    (builtins.readFile ./scripts/keybindings);
  i3-blur-lock = pkgs.writeShellScriptBin "i3-blur-lock"
    (builtins.readFile ./scripts/blur-lock);
in
{
  options.ultragear.desktop.i3 = with types; {
    enable = mkBoolOpt false "Whether or not to enable i3.";
    wallpaper = mkOpt (nullOr package) null "The wallpaper to display.";
    extraConfig =
      mkOpt str "" "Additional configuration for the i3 config file.";
  };

  config = mkIf cfg.enable {
    # Desktop additions
    ultragear.desktop.addons = {
      gtk = enabled;
      kitty = enabled;
      rofi = enabled;
      dunst = enabled;
      keyring = enabled;
      pcmanfm = enabled;
      polybar = enabled;
      # xdg-portal = enabled;
      electron-support = enabled;
      redshift = enabled;
      idle-suspend = enabled;
    };

    ultragear.home.configFile."i3/config".text =
      fileWithText substitutedConfig ''
        #            _                                __ _
        #   _____  _| |_ _ __ __ _    ___ ___  _ __  / _(_) __ _
        #  / _ \ \/ / __| '__/ _` |  / __/ _ \| '_ \| |_| |/ _` |
        # |  __/>  <| |_| | | (_| | | (_| (_) | | | |  _| | (_| |
        #  \___/_/\_\\__|_|  \__,_|  \___\___/|_| |_|_| |_|\__, |
        #                                                  |___/

        # ${optionalString (cfg.wallpaper != null) ''
        #   output * {
        #     bg ${cfg.wallpaper.gnomeFilePath or cfg.wallpaper} fill
        #   }
        # ''}

        ${cfg.extraConfig}
      '';

    # as explained in: https://nixos.wiki/wiki/I3
    services = {
      displayManager.defaultSession = "none+i3";
      libinput.touchpad.naturalScrolling = true;
      xserver = {
        enable = true;
        desktopManager.xterm.enable = false;
        windowManager.i3 = {
          enable = true;
          # these are packages needed by i3, and are configuration free
          extraPackages = with pkgs; [
            i3lock
            scrot
            udiskie
            flameshot
            volctl
            networkmanagerapplet
            copyq
            feh
            ultragear.x11-wallpapers-rotate
            i3-powermenu
            i3-keybindings
            i3-blur-lock
          ];
        };

        # also switch keyboard layout
        xkb = {
          layout = mkForce "us,gr";
          options = "grp:alt_space_toggle,grp_led:scroll";
        };
      };
    };

    # install custom packages
    environment.systemPackages = [ pkgs.perl538Packages.Apppapersway ];
  };
}
