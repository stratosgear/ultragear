{ options, config, lib, pkgs, ... }:

with lib;
with lib.ultragear;
let cfg = config.ultragear.desktop.addons.dropbox;
in
{
  options.ultragear.desktop.addons.dropbox = with types; {
    enable = mkBoolOpt false "Whether to enable dropbox.";
  };

  config = mkIf cfg.enable {

    environment.systemPackages = with pkgs; [ dropbox ];

    systemd.user.services.dropbox = {
      description = "Dropbox service";
      wantedBy = [ "default.target" ];
      serviceConfig = {
        ExecStart = "${pkgs.dropbox}/bin/dropbox";
        Restart = "on-failure";
      };
    };
  };
}
