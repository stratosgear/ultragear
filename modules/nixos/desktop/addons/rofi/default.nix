{ options, config, lib, pkgs, ... }:

with lib;
with lib.ultragear;
let cfg = config.ultragear.desktop.addons.rofi;
in
{
  options.ultragear.desktop.addons.rofi = with types; {
    enable =
      mkBoolOpt false "Whether to enable Rofi in the desktop environment.";
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [

      (if config.ultragear.desktop.hyprland.enable then pkgs.rofi-wayland else pkgs.rofi)

    ];

    ultragear.home.configFile."rofi/config.rasi".source = ./styles/config.rasi;
    ultragear.home.configFile."rofi/powerMenu.rasi".source = ./styles/powerMenu.rasi;
    ultragear.home.configFile."rofi/appsMenu.rasi".source = ./styles/appsMenu.rasi;
    ultragear.home.configFile."rofi/transparentColors.rasi".source = ./styles/transparentColors.rasi;
  };
}
