{
  options,
  config,
  lib,
  pkgs,
  ...
}:

with lib;
with lib.ultragear;
let
  cfg = config.ultragear.desktop.addons.obs;
in
{
  options.ultragear.desktop.addons.obs = with types; {
    enable = mkBoolOpt false "Whether to enable OBS Studio";
  };

  config = mkIf cfg.enable {

    # install OBS as per: https://wiki.nixos.org/w/index.php?title=OBS_Studio
    environment.systemPackages = [
      (pkgs.wrapOBS {
        plugins = with pkgs.obs-studio-plugins; [
          obs-backgroundremoval
          obs-pipewire-audio-capture
          obs-shaderfilter
        ];
      })
    ];

    # install virtual camera
    boot.extraModulePackages = with config.boot.kernelPackages; [
      v4l2loopback
    ];
    boot.kernelModules = [ "v4l2loopback" ];
    boot.extraModprobeConfig = ''
      options v4l2loopback devices=1 video_nr=1 card_label="OBS Cam" exclusive_caps=1
    '';
    security.polkit.enable = true;
  };
}
