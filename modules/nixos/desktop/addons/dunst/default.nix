{ options, config, lib, pkgs, ... }:

with lib;
with lib.ultragear;
let cfg = config.ultragear.desktop.addons.dunst;
in
{
  options.ultragear.desktop.addons.dunst = with types; {
    enable = mkBoolOpt false "Whether to enable the dunst notification deamon.";
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [ dunst ];
    ultragear.home.configFile."dunst/dunstrc".source = ./dunstrc;

  };
}
