{ options, config, lib, inputs, pkgs, ... }:

with lib;
with lib.ultragear;

let cfg = config.ultragear.desktop.addons.ghostty;
in
{
  options.ultragear.desktop.addons.ghostty = with types; {
    enable = mkBoolOpt false "Whether to enable the ghostty terminal.";
  };

  config = mkIf cfg.enable {

    environment.systemPackages = with pkgs;
      [
        inputs.ghostty.packages.x86_64-linux.default
      ];

    ultragear.home.configFile."ghostty/config".source = ./config;
  };
}
