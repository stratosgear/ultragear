{ options, config, lib, pkgs, ... }:

with lib;
with lib.ultragear;
let cfg = config.ultragear.desktop.addons.pcmanfm;
in
{
  options.ultragear.desktop.addons.pcmanfm = with types; {
    enable = mkBoolOpt false "Whether to enable the PCManFM file manager.";
  };

  # As described in:
  # https://nixos.wiki/wiki/PCManFM
  config = mkIf cfg.enable {
    # Enable support for browsing samba shares.
    services.gvfs.enable = true;
    networking.firewall.extraCommands =
      "iptables -t raw -A OUTPUT -p udp -m udp --dport 137 -j CT --helper netbios-ns";

    environment.systemPackages = with pkgs; [
      pcmanfm
      lxmenu-data
      shared-mime-info
    ];
  };
}
