{ options, config, lib, pkgs, ... }:

with lib;
with lib.ultragear;
let
  cfg = config.ultragear.desktop.addons.redshift;
in
{
  options.ultragear.desktop.addons.redshift = with types; {
    enable = mkBoolOpt false "Whether to enable Redshift, time based monitor brigthness and color calibrator.";

    temperature = {
      day = mkOpt int 6500 "Daylight temperature";
      night = mkOpt int 3800 "Nighttime temperature";
    };
    brightness = {
      day = mkOpt str "1.0" "Daylight brightness";
      night = mkOpt str "0.8" "Nighttime brightness";
    };
  };

  config = mkIf cfg.enable {
    services.redshift = {
      enable = true;
      temperature.day = cfg.temperature.day;
      temperature.night = cfg.temperature.night;
      brightness.day = cfg.brightness.day;
      brightness.night = cfg.brightness.night;
      extraOptions = [
        "-l geoclue2"
      ];
    };
  };
}




