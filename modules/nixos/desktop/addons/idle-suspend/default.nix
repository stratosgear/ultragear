{ options, config, lib, pkgs, ... }:

with lib;
with lib.ultragear;
let
  cfg = config.ultragear.desktop.addons.idle-suspend;
  sysInfo = config.ultragear.system.info;

  x11-idle-suspend = pkgs.writeShellScriptBin "x11-idle-suspend"
    (builtins.readFile ./x11-idle-suspend);
in
{
  options.ultragear.desktop.addons.idle-suspend = with types; {
    enable = mkBoolOpt false "Whether to enable suspending the system when idle.";
  };

  config = mkIf cfg.enable {

    environment.systemPackages = with pkgs; [
      xidlehook
      x11-idle-suspend
    ];


    # As per: https://gitlab.com/jD91mZM2/xidlehook#configuring-via-systemd
    ultragear.home.extraOptions = {
      systemd.user.services.x11-idle-suspend = {
        Unit.Description = "X11 Idle suspend";
        Service.ExecStart = "${x11-idle-suspend}/bin/x11-idle-suspend";
        Service.ExecStartPre = "${pkgs.coreutils}/bin/sleep 30";
        Install.WantedBy = [ "default.target" ];
      };
    };

  };
}
