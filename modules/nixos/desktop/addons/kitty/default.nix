{ options, config, lib, pkgs, ... }:

with lib;
with lib.ultragear;
let cfg = config.ultragear.desktop.addons.kitty;
in
{
  options.ultragear.desktop.addons.kitty = with types; {
    enable = mkBoolOpt false "Whether to enable the kitty terminal.";
  };

  config = mkIf cfg.enable {

    # install kitty's terminfo system-wide (additionally to in home) so that `sudo -s` works via SSH
    environment.systemPackages = with pkgs;
      [
        kitty
        kitty.terminfo
      ];

    ultragear.home.configFile."kitty/kitty.conf".source = ./kitty.conf;
    ultragear.home.configFile."kitty/current-theme.conf".source = ./current-theme.conf;
  };
}
