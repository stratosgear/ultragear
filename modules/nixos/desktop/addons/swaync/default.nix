{ options, config, lib, pkgs, ... }:

with lib;
with lib.ultragear;
let cfg = config.ultragear.desktop.addons.swaync;
in
{
  options.ultragear.desktop.addons.swaync = with types; {
    enable = mkBoolOpt false "Whether to enable Sway Notification Center.";
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [ swaynotificationcenter libnotify ];
    ultragear.home.configFile."swaync/style.css".source = ./styles/macchiato.css;
  };
}
