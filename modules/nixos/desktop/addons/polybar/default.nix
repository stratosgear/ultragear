{ options, config, lib, pkgs, ... }:

with lib;
with lib.ultragear;
let
  cfg = config.ultragear.desktop.addons.polybar;
  sysInfo = config.ultragear.system.info;

  start-polybar = pkgs.writeShellScriptBin "start-polybar" ''
    # Terminate already running bar instances
    # If all your bars have ipc enabled, you can use
    polybar-msg cmd quit
    # Otherwise you can use the nuclear option:
    # killall -q polybar

    # Launch bar1
    echo "---" | tee -a /tmp/polybar-primary.log
    polybar primary 2>&1 | tee -a /tmp/polybar-primary.log & disown

    ${optionalString (cfg.barSecondary.screen != "") ''
      # Launch bar2
      echo "---" | tee -a /tmp/polybar-secondary.log
      polybar secondary 2>&1 | tee -a /tmp/polybar-secondary.log & disown
      ''}

    echo "Bars launched..."
  '';
  configIni = pkgs.substituteAll {
    src = ./config.ini;
    screen1 = cfg.barPrimary.screen;
    screen2 = cfg.barSecondary.screen;
    primaryLeftModules = cfg.barPrimary.modulesLeft;
    primaryRightModules = cfg.barPrimary.modulesRight;
    secondaryLeftModules = cfg.barSecondary.modulesLeft;
    secondaryRightModules = cfg.barSecondary.modulesRight;
    batteryClass = sysInfo.batteryClass;
    backlightClass = sysInfo.backlightClass;
  };
in
{
  options.ultragear.desktop.addons.polybar = with types; {
    enable =
      mkBoolOpt false "Whether to enable Polybar in the desktop environment.";
    barPrimary = {
      screen = mkOpt str sysInfo.display1 "Screen name for Primary Bar";
      modulesLeft = mkOpt str "i3" "Left side modules for Primary bar";
      modulesRight = mkOpt str "filesystem pulseaudio xkeyboard memory cpu wlan eth date" "Right side modules for Primary bar";
    };
    barSecondary = {
      screen = mkOpt str sysInfo.display2 "Screen name for Secondary Bar";
      modulesLeft = mkOpt str cfg.barPrimary.modulesLeft "Left side modules for Secondary bar";
      modulesRight = mkOpt str cfg.barPrimary.modulesRight "Right side modules for Secondary bar";
    };
  };

  config = mkIf cfg.enable {

    # trace cfg.barPrimary.modulesLeft;
    assertions = [
      {
        assertion = (hasInfix "battery" cfg.barPrimary.modulesLeft || hasInfix "battery" cfg.barPrimary.modulesRight
          || hasInfix "battery" cfg.barSecondary.modulesLeft || hasInfix "battery" cfg.barSecondary.modulesRight) -> sysInfo.batteryClass != "";
        message = "Polybar needs ultragear.system.info.batteryClass to be set";
      }
      {
        assertion = (hasInfix "backlight" cfg.barPrimary.modulesLeft || hasInfix "backlight" cfg.barPrimary.modulesRight
          || hasInfix "backlight" cfg.barSecondary.modulesLeft || hasInfix "backlight" cfg.barSecondary.modulesRight) -> sysInfo.backlightClass != "";
        message = "Polybar needs ultragear.system.info.backlightClass to be set";
      }
    ];

    environment.systemPackages = with pkgs; [
      polybarFull
      start-polybar
    ];

    ultragear.home.configFile."polybar/config.ini".text = fileWithText
      configIni
      ''
    '';

  };
}
