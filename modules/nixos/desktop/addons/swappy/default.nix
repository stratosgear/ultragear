{ options, config, lib, pkgs, ... }:

with lib;
with lib.ultragear;
let cfg = config.ultragear.desktop.addons.swappy;
in
{
  options.ultragear.desktop.addons.swappy = with types; {
    enable =
      mkBoolOpt false "Whether to enable Swappy in the desktop environment.";
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [ swappy ];

    ultragear.home.configFile."swappy/config".source = ./config;
    ultragear.home.file."Pictures/screenshots/.keep".text = "";
  };
}
