{ options, config, pkgs, lib, ... }:

with lib;
with lib.ultragear;
let cfg = config.ultragear.services.scrutiny;
in
{
  options.ultragear.services.scrutiny = with types; {
    enable = mkBoolOpt false "Whether or not to enable scrutiny service";
    port = mkOpt int CONSTS.ports.services.scrutiny "The server port number to run as";
    domainName = mkOpt str CONSTS.urls.scrutiny "The domain name to expose service as";
    collector = mkBoolOpt false "Whether or not to enable the collector service";
  };

  config = mkIf (cfg.enable || cfg.collector) {

    services.scrutiny = {
      enable = cfg.enable;
      settings = {
        web.listen.port = cfg.port;
      };

      collector = {
        enable = cfg.collector;
        settings = {
          host.id = config.networking.hostName;
          api.endpoint = "https://${cfg.domainName}";
        };

      };
    };

    # Proxy through Traefik
    services.traefik.dynamicConfigOptions.http = {
      routers.scrutiny = {
        entryPoints = [ "https" ];
        rule = "Host(`${cfg.domainName}`)";
        service = "scrutiny";
        middlewares = "local@file";
      };
      services.scrutiny.loadBalancer = {
        passHostHeader = true;
        servers = [{ url = "http://127.0.0.1:${toString cfg.port}"; }];
      };
    };

  };
}
