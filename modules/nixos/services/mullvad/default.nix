{ options, config, pkgs, lib, ... }:

with lib;
with lib.ultragear;
let cfg = config.ultragear.services.mullvad;
in
{
  options.ultragear.services.mullvad = with types; {
    enable = mkBoolOpt false "Whether or not to enable mullvad vpn service.";
  };

  config = mkIf cfg.enable {
    services.mullvad-vpn.enable = true;

    # This was suggested as a potential solution to a mullvad connection problem
    # https://nixos.wiki/wiki/Mullvad_VPN
    # networking.resolvconf.enable = false;
    # networking.resolvconf.extraConfig = ''
    #   dynamic_order='tap[0-9]* tun[0-9]* vpn vpn[0-9]* wg* wg[0-9]* ppp[0-9]* ippp[0-9]*'
    # '';
    # services.resolved = {
    #   enable = true;
    #   dnssec = "true";
    #   domains = [ "gerakakis.net" "bearded-python.ts.net" ];
    #   fallbackDns = [ "1.1.1.1#one.one.one.one" "1.0.0.1#one.one.one.one" ];
    #   dnsovertls = "true";
    # };

    # Install programs related to mullvad
    environment.systemPackages = with pkgs;
      [
        # Desktop client for mullvad
        mullvad-vpn
      ];
  };
}
