{ options, config, pkgs, lib, ... }:

with lib;
with lib.ultragear;
let cfg = config.ultragear.services.paperless;
in
{
  options.ultragear.services.paperless = with types; {
    enable = mkBoolOpt false "Whether or not to enable paperless-ngx support.";
    port = mkOpt int 8000 "Port where the paperless service will run";
    dataDir = mkOpt str "/data/safe/apps/paperless" "Location where paperless-ngx can save files";
    dbName = mkOpt str "paperless" "Postgres db name";
    dbUser = mkOpt str "paperless" "Postgres db user";
    runAs = mkOpt str "paperless" "System user to run service as";
  };

  config = mkIf cfg.enable {

    # Nice collection of services:
    # https://github.com/skogsbrus/os

    assertions = [
      {
        assertion = config.ultragear.services.postgres.enable;
        message = "Postgres must also be enabled before paperless-ngx starts.";
      }
    ];

    environment.systemPackages = [
      pkgs.paperless-ngx
    ];

    systemd.tmpfiles.rules = [ "d ${cfg.dataDir} 0750 ${cfg.runAs} ${cfg.runAs} -" ];

    services = {
      paperless = {
        enable = true;
        user = cfg.runAs;
        dataDir = cfg.dataDir;
        consumptionDirIsPublic = true;
        port = cfg.port;
        address = "localhost";
        settings = {
          PAPERLESS_AUTO_LOGIN_USERNAME = "admin";
          PAPERLESS_OCR_LANGUAGE = "eng";
          PAPERLESS_DBENGINE = "postgresql";
          PAPERLESS_DBHOST = "/run/postgresql";
          PAPERLESS_DBPORT = "5432";
          PAPERLESS_DBNAME = cfg.dbName;
          PAPERLESS_DBUSER = cfg.dbUser;
          PAPERLESS_DBPASS = "postgres";
          PAPERLESS_DBSSLMODE = "allow";
          HOME = "/tmp"; # Prevent GNUPG home dir error
        };
      };

      postgresql = {
        ensureUsers = [
          {
            name = cfg.dbUser;
            ensureDBOwnership = true;
          }
        ];
        ensureDatabases = [ cfg.dbName ];
      };

    };

    # Stanza for system restic backups
    ultragear.services.backup.instances.system = {
      sourceDirectories = [ cfg.dataDir ];
      excludePatterns = [ "${cfg.dataDir}/log" ];
      databases = [ cfg.dbName ];
    };

    # Proxy through Traefik
    services.traefik.dynamicConfigOptions.http = {
      routers.paperlessngx = {
        entryPoints = [ "https" "http" ];
        rule = "Host(`${CONSTS.urls.paperlessngx}`)";
        service = "paperlessngx";
        middlewares = "local@file";
      };

      services.paperlessngx.loadBalancer = {
        passHostHeader = true;
        servers = [{ url = "http://127.0.0.1:${toString cfg.port}"; }];
      };
    };

  };
}
