Upgrading the db failed with:

django.db.utils.ProgrammingError: permission denied for table django_migrations


Fixed with:

```
psql -U postgres
\c paperless
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO myuser;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO myuser;
```

# Drop and create database from scratch

\c postgres
drop database paperless
\c paperless
create database paperless

and apply the grants from above.

Restarting postgres.service seems to be required (?)