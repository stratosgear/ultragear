{ options, config, pkgs, lib, ... }:

with lib;
with lib.ultragear;
let cfg = config.ultragear.services.homepage;
in
{
  options.ultragear.services.homepage = with types; {
    enable = mkBoolOpt false "Whether or not to enable the homepage dashboard service";
    port = mkOpt int CONSTS.ports.services.homepage "The port number to run as";
    domainName = mkOpt str "" "The domain name to expose service as";
    dataDir = mkOpt str "" "The location to store data";
  };

  config = mkIf cfg.enable {

    systemd.tmpfiles.rules = [
      "d ${cfg.dataDir}/config 0750 root root -"
    ];

    virtualisation.oci-containers = {
      containers = {
        homepage = {
          image = "ghcr.io/gethomepage/homepage:v0.8.8";
          autoStart = true;
          volumes = [
            "${cfg.dataDir}/config:/app/config"
            "/var/run/podman/podman.sock:/var/run/docker.sock:ro"
            "/storage-box:/storage-box:ro"
          ];
          environment = {
            TZ = config.ultragear.system.time.tz;
          };
          # environmentFiles = [
          #   config.age.secrets.paperless.path
          # ];
          ports = [ "${builtins.toString cfg.port}:3000" ];
        };
      };
    };

    # Proxy through Traefik
    services.traefik.dynamicConfigOptions.http = {
      routers.homepage = {
        entryPoints = [ "https" ];
        rule = "Host(`${cfg.domainName}`)";
        service = "homepage";
        middlewares = "local@file";
      };

      services.homepage.loadBalancer = {
        passHostHeader = true;
        servers = [{ url = "http://127.0.0.1:${toString cfg.port}"; }];
      };
    };

    # Stanza for system restic backups
    ultragear.services.backup.instances.system = {
      sourceDirectories = [ cfg.dataDir ];
    };

  };
}
