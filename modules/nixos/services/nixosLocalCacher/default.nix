{ options, config, pkgs, lib, ... }:

with lib;
with lib.ultragear;
let cfg = config.ultragear.services.nixosLocalCacher;
in
{
  options.ultragear.services.nixosLocalCacher = with types; {
    enable = mkBoolOpt false "Whether or not to configure a local Nixos Cache service.";
    cacheDir = mkOpt str "" "The location to store data";
    domainName = mkOpt str "nixoscache.${CONSTS.urls.privRootDomain}" "The domain name where the nixosLocalCache will be accessible";
    serverPort = mkOpt int CONSTS.ports.services.nixosLocalCache "The port where nginx listens to";

  };

  # From:
  # https://github.com/nh2/nix-binary-cache-proxy/blob/master/nginx-binary-cache-proxy.nix
  # https://dataswamp.org/~solene/2022-06-02-nixos-local-cache.html

  config = mkIf cfg.enable {

    assertions = [
      {
        assertion = cfg.cacheDir != "";
        message = "If nixosLocalCache is enabled you also have to provide a cacheDir";
      }
    ];

    systemd.tmpfiles.rules = [
      "d ${cfg.cacheDir} 0755 nginx nginx -"
      "Z ${cfg.cacheDir} 0755 nginx nginx -"
      "d /var/public-nix-cache 0750 nginx nginx -"
    ];

    services.nginx = {
      enable = true;
      appendHttpConfig = ''
        proxy_cache_path ${cfg.cacheDir} levels=1:2 keys_zone=cachecache:100m max_size=40g inactive=182d use_temp_path=off;

        # Cache only success status codes; in particular we don't want to cache 404s.
        # See https://serverfault.com/a/690258/128321
        map $status $cache_header {
          200     "public";
          302     "public";
          default "no-cache";
        }
        access_log /var/log/nginx/nixosLocalCache.log;
      '';

      virtualHosts."nixosLocalCache" = {
        listen = [{
          addr = "127.0.0.1";
          port = cfg.serverPort;
        }];
        locations."/" = {
          root = "/var/public-nix-cache";
          extraConfig = ''
            expires max;
            add_header Cache-Control $cache_header always;
            # Ask the upstream server if a file isn't available locally
            error_page 404 = @fallback;
          '';
        };

        extraConfig = ''
          # Using a variable for the upstream endpoint to ensure that it is
          # resolved at runtime as opposed to once when the config file is loaded
          # and then cached forever (we don't want that):
          # see https://tenzer.dk/nginx-with-dynamic-upstreams/
          # This fixes errors like
          #   nginx: [emerg] host not found in upstream "upstream.example.com"
          # when the upstream host is not reachable for a short time when
          # nginx is started.
          resolver 8.8.8.8 ipv6=off;
          set $upstream_endpoint http://cache.nixos.org;
        '';

        locations."@fallback" = {
          proxyPass = "$upstream_endpoint";
          extraConfig = ''
            proxy_cache cachecache;
            proxy_cache_valid  200 302  60d;
            expires max;
            add_header Cache-Control $cache_header always;
          '';
        };

        # We always want to copy cache.nixos.org's nix-cache-info file,
        # and ignore our own, because `nix-push` by default generates one
        # without `Priority` field, and thus that file by default has priority
        # 50 (compared to cache.nixos.org's `Priority: 40`), which will make
        # download clients prefer `cache.nixos.org` over our binary cache.
        locations."= /nix-cache-info" = {
          # Note: This is duplicated with the `@fallback` above,
          # would be nicer if we could redirect to the @fallback instead.
          proxyPass = "$upstream_endpoint";
          extraConfig = ''
            proxy_cache cachecache;
            proxy_cache_valid  200 302  60d;
            expires max;
            add_header Cache-Control $cache_header always;
          '';
        };
      };
    };

    # https://nixos.wiki/wiki/Nginx#Read-only_Filesystem_for_nginx_upgrade_to_20.09
    # (that was a bitch, figuring out)
    systemd.services.nginx.serviceConfig.ReadWritePaths = [ cfg.cacheDir ];

    # Proxy through Traefik, if resticServer is enabled
    services.traefik.dynamicConfigOptions.http = {
      routers.nixosLocalCache = {
        entryPoints = [ "https" ];
        rule = "Host(`${cfg.domainName}`)";
        service = "nixosLocalCache";
      };

      services.nixosLocalCache.loadBalancer = {
        passHostHeader = true;
        servers = [{ url = "http://127.0.0.1:${toString cfg.serverPort}"; }];
      };
    };

  };
}
