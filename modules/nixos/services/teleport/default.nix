{ options, config, pkgs, lib, ... }:

with lib;
with lib.ultragear;
let cfg = config.ultragear.services.teleport;
in
{
  options.ultragear.services.teleport = with types; {
    enable = mkBoolOpt false "Whether or not to configure teleport service.";
  };


  # Some links:
  # - https://github.com/mlieberman85/homelab/blob/dc7804455a13de505ebdd0f74c7659367d9f4675/nixos/users.nix#L61

  config = mkIf cfg.enable {
    services.teleport = {
      enable = true;
    };
  };
}
