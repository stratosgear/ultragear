{ options, config, pkgs, lib, ... }:

# Based on:
# https://github.com/ibizaman/selfhostblocks/blob/bc0a081bba8fda554cc672076d374b90e4c8956d/modules/blocks/backup.nix#L1

with lib;
with lib.ultragear;
let

  cfg = config.ultragear.services.backup;

  pgDumpsDir = "/backups/dbDumps";
  pgsu = "${pkgs.sudo}/bin/sudo -u ${config.services.postgresql.superUser}";
  dbDumpCmd = db: "${pgsu} ${pkgs.postgresql}/bin/pg_dump --clean -U postgres -d ${db} > ${pgDumpsDir}/${db}.sql";
  dbDumpIncludeCmd = db: "${pgDumpsDir}/${db}.sql";
  dbDumpCleanupCmd = db: "rm ${pgDumpsDir}/${db}.sql";

  instanceOptions = with types; {
    enable = mkBoolOpt true "Enable backup instance";
    healthcheckPingUrl = mkOpt str "https://${CONSTS.urls.healthchecks}/ping" "The healthchecks server to hit endpoint at";
    sourceDirectories = mkOpt (nonEmptyListOf str) [ ] "Source directories.";
    excludePatterns = mkOpt (listOf str) [ ] "Exclude patterns.";
    databases = mkOpt (listOf str) [ ] "Postgres databases to dump and backup";
    repositories = mkOpt
      (listOf (submodule {
        options = {
          path = mkOpt str "/backups" "Repository location";
          name = mkOpt str "" "A friendly repository name";
          hcPingUid = mkOpt str "" "A healthcheck uuid to trigger on every backup";
          # TODO: Figure out how to properly use this!!!
          # timerConfig = mkOpt (attrsOf utils.systemdUtils.unitOptions.unitOption)
          timerConfig = mkOpt (attrsOf (oneOf [ int nonEmptyStr bool ]))
            {
              OnCalendar = "daily";
              RandomizedDelaySec = "3h";
              Persistent = true;
            } ''When to run the backup. See {manpage}`systemd.timer(5)` for details.
                example = {
                  OnCalendar = "00:05";
                  RandomizedDelaySec = "5h";
                  Persistent = true;
                };
            '';
        };
      })) [ ] "Repositories to back this instance to";
    retention = mkOpt (attrsOf (oneOf [ int nonEmptyStr ]))
      {
        keep_within = "1d";
        keep_hourly = 32;
        keep_daily = 8;
        keep_weekly = 5;
        keep_monthly = 6;
      } "Retention options.";
    hooks = mkOpt
      (submodule {
        options = {
          before_backup = mkOpt (listOf str) [ ] "Hooks to run before backup";
          after_backup = mkOpt (listOf str) [ ] "Hooks to run after backup";
        };
      })
      { } "Hooks to execute before/after the backup";
  };

  repoSlugName = repo:
    if (repo.name == "") then
      builtins.replaceStrings [ "/" ":" ] [ "_" "_" ] (strings.removePrefix "/" repo.path)
    else
      repo.name;

in
{
  options.ultragear.services.backup = with types; {

    hostBackups = mkBoolOpt false "Whether to act as a Restic backup server";
    backupsDataDir = mkOpt str "/backups/resticServer" "Data dir where the Restic Server will save backups to";
    serverPort = mkOpt int CONSTS.ports.services.resticServer "Port where the Restic Server is listening to";
    serverDomain = mkOpt str "" "Domain name that the restic server is exposed as";
    user = mkOpt str "root" "Unix user doing the backups.";
    group = mkOpt str "root" "Unix group doing the backups.";
    instances = mkOpt (attrsOf (submodule { options = instanceOptions; })) { } "Each instance is a backup setting";


    # Taken from https://github.com/HubbeKing/restic-kubernetes/blob/73bfbdb0ba76939a4c52173fa2dbd52070710008/README.md?plain=1#L23
    performance = mkOpt
      (submodule {
        options = {
          niceness = mkOpt (ints.between (-20) 19) 15 "nice priority adjustment, defaults to 15 for ~20% CPU time of normal-priority process";
          ioSchedulingClass = mkOpt (enum [ "idle" "best-effort" "realtime" ]) "best-effort" "ionice scheduling class, defaults to best-effort IO. Only used for `restic backup`, `restic forget` and `restic check` commands.";
          ioPriority = mkOpt (nullOr (ints.between 0 7)) 7 "ionice priority, defaults to 7 for lowest priority IO. Only used for `restic backup`, `restic forget` and `restic check` commands.";
        };
      })
      { } "Reduce performance impact of backup jobs.";

  };

  config = mkIf (cfg.instances != { })

    (

      let
        enabledInstances = attrsets.filterAttrs (k: i: i.enable) cfg.instances;
      in
      mkMerge [
        # Secrets configuration
        {
          # I do not see the need to dynamically create users/groups here.  We will
          # assume they already exist!
          # users.users = {
          #   ${cfg.user} = {
          #     name = cfg.user;
          #     group = cfg.group;
          #     home = "/backups";
          #     createHome = true;
          #     isSystemUser = true;
          #   };
          # };
          # users.groups = {
          #   ${cfg.group} = {
          #     name = cfg.group;
          #   };
          # };

          sops.secrets =
            let
              mkSopsSecret = name: instance: (
                [{
                  "backups/passphrases/${name}" = {
                    mode = "0440";
                    owner = cfg.user;
                    group = cfg.group;
                  };
                }] ++ optional ((filter ({ path, ... }: strings.hasPrefix "s3" path || strings.hasPrefix "rest" path) instance.repositories) != [ ]) {
                  "backups/environmentFiles/${name}" = {
                    mode = "0440";
                    owner = cfg.user;
                    group = cfg.group;
                  };
                }
              );
            in
            mkMerge (flatten (attrsets.mapAttrsToList mkSopsSecret enabledInstances));
        }

        # Backups configuration
        {
          systemd.tmpfiles.rules = [ "d ${pgDumpsDir} 0750 root root -" ];

          assertions = [
            {
              assertion = cfg.hostBackups -> cfg.serverDomain != "";
              message = "If services.backup.hostBackups is active you also have to provide a services.backup.serverDomain";
            }
          ];

          environment.systemPackages = optionals (enabledInstances != { }) [ pkgs.restic ];

          # Proxy through Traefik, if resticServer is enabled
          services.traefik.dynamicConfigOptions.http = mkIf cfg.hostBackups {
            routers.resticserver = {
              entryPoints = [ "https" ];
              rule = "Host(`${cfg.serverDomain}`)";
              service = "resticserver";
            };

            services.resticserver.loadBalancer = {
              passHostHeader = true;
              servers = [{ url = "http://127.0.0.1:${toString cfg.serverPort}"; }];
            };
          };

          # Create a resticServer passwords file, if resticServer is enabled
          # Create passwords with: `htpasswd -B -n username`
          environment.etc = mkIf cfg.hostBackups
            {
              "resticServerPassword" = {
                target = "resticServer/.htpasswd";
                text = ''
                  dread:$2y$05$TaT1DXCH9lnsvbFKGkN.L..7hvG6IshibQ/Yllwz27PybUU2WmahK
                  vern:$2y$05$Q.8hbiXo.ikLLC1BKCVST.4ea3wGpkN8QeHHvYnjjDzNzSew./yWi
                  iocaine:$2y$05$2o6Jk5Q8E8zUJRj8xK3gVuJIFyDksW1Hj03jzjIakuW9lJ.OsVhSO
                  vezzini:$2y$05$ncvE8uHct3G.d1fYZgXbd.AlWLGavX9avzKXDgeGhqs4rXkRPsFsi
                  fezzik:$2y$05$M2jBLfGOWKXcY7tphjO/Pu/xUzEb2sK2GIzJS37J4QOgbNI64bOWi
                '';
                mode = "0700";
                user = "restic";
                group = "restic";
              };
            };

          services.restic = {

            # Start ResticServer, if resticServer is enabled
            server = mkIf cfg.hostBackups {
              enable = true;
              dataDir = cfg.backupsDataDir;
              privateRepos = true;
              listenAddress = "0.0.0.0:${toString cfg.serverPort}";
              extraFlags = [
                "--htpasswd-file=/etc/resticServer/.htpasswd"
              ];
            };

            backups =
              let
                mkRepositorySettings = name: instance: repository: {
                  "${name}_${repoSlugName repository}" = {
                    inherit (cfg) user;

                    # DEBUGGING?
                    # repository = (traceVal repository.path);
                    repository = "${repository.path}";
                    # paths = instance.sourceDirectories;
                    paths = instance.sourceDirectories ++
                      optionals (instance.databases != [ ]) (map (db: dbDumpIncludeCmd db) instance.databases);
                    passwordFile = "/run/secrets/backups/passphrases/${name}";
                    initialize = true;
                    inherit (repository) timerConfig;
                    pruneOpts = mapAttrsToList
                      (name: value:
                        "--${builtins.replaceStrings ["_"] ["-"] name} ${builtins.toString value}"
                      )
                      instance.retention;
                    backupPrepareCommand = strings.concatStringsSep "\n"
                      ([ ] ++
                        optionals (repository.hcPingUid != "") [
                          ''
                            ${pkgs.runitor}/bin/runitor -api-url ${instance.healthcheckPingUrl} -uuid ${repository.hcPingUid}/start -no-start-ping -- echo Runitor claims Backup started.
                          ''
                        ] ++
                        instance.hooks.before_backup ++
                        optionals (instance.databases != [ ]) (map (db: dbDumpCmd db) instance.databases));
                    backupCleanupCommand = strings.concatStringsSep "\n" (instance.hooks.after_backup ++
                      optionals (instance.databases != [ ]) (map (db: dbDumpCleanupCmd db) instance.databases) ++
                      optionals (repository.hcPingUid != "") [
                        ''
                          ${pkgs.runitor}/bin/runitor -api-url ${instance.healthcheckPingUrl} -uuid ${repository.hcPingUid}/0 -no-start-ping -- echo Runitor claims Backup ended.
                        ''
                      ]);
                    extraBackupArgs = [
                      ("--exclude-file=" + (pkgs.writeText "restic-excludes.txt"
                        (builtins.readFile ./excludes.txt + concatStringsSep "\n" instance.excludePatterns)))
                    ];
                  } // attrsets.optionalAttrs (strings.hasPrefix "s3" repository.path || strings.hasPrefix "rest" repository.path) {
                    environmentFile = "/run/secrets/backups/environmentFiles/${name}";
                  };
                };

                mkSettings = name: instance: builtins.map (mkRepositorySettings name instance) instance.repositories;
              in
              mkMerge
                (flatten (attrsets.mapAttrsToList mkSettings enabledInstances));
          };

          systemd.services =
            let
              mkRepositorySettings = name: instance: repository: {
                "restic-backups-${name}_${repoSlugName repository}".serviceConfig = {
                  Nice = cfg.performance.niceness;
                  IOSchedulingClass = cfg.performance.ioSchedulingClass;
                  IOSchedulingPriority = cfg.performance.ioPriority;
                };
              };
              mkSettings = name: instance: builtins.map (mkRepositorySettings name instance) instance.repositories;
            in
            mkMerge
              (flatten (attrsets.mapAttrsToList mkSettings enabledInstances));
        }
      ]
    );
}
