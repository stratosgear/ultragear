{ options, config, pkgs, lib, ... }:

with lib;
with lib.ultragear;
let cfg = config.ultragear.services.cockpit;
in
{
  options.ultragear.services.cockpit = with types; {
    enable = mkBoolOpt false "Whether or not to enable cockpit service";
    port = mkOpt int CONSTS.ports.services.cockpit "The port number to run as";
    domainName = mkOpt str "cockpit.${config.networking.hostName}.${CONSTS.urls.privRootDomain}" "The domain name to expose service as";
  };

  config = mkIf cfg.enable {

    services.cockpit = {
      enable = true;
      port = cfg.port;
      settings = {
        WebService = {
          Origins = "https://${cfg.domainName}";
          UrlRoot = "https://${cfg.domainName}";
        };
      };
    };

    # Proxy through Traefik
    services.traefik.dynamicConfigOptions.http = {
      routers.cockpit = {
        entryPoints = [ "https" ];
        rule = "Host(`${cfg.domainName}`)";
        service = "cockpit";
        middlewares = "local@file";
      };
      services.cockpit.loadBalancer = {
        passHostHeader = true;
        servers = [{ url = "http://127.0.0.1:${toString cfg.port}"; }];
      };
    };
  };
}
