{ options, config, pkgs, lib, ... }:

with lib;
with lib.ultragear;
let cfg = config.ultragear.services.munin;
in
{
  options.ultragear.services.munin = with types; {
    enable = mkBoolOpt false "Whether or not to enable munin monitoring agent";
    server = mkBoolOpt false "Whether to enable the munin collecting service";
    domainName = mkOpt str CONSTS.urls.munin "The domain name to expose service as";
    port = mkOpt int CONSTS.ports.services.munin "The munin server port";
  };

  config = mkIf cfg.enable {

    services.munin-node.enable = true;

    services.nginx = mkIf cfg.server {
      enable = true;
      virtualHosts."munin" = {
        listen = [{
          addr = "127.0.0.1";
          port = cfg.port;
        }];
        addSSL = false;
        enableACME = false;
        root = "/var/www/munin/";
        locations = {
          "/" = {
            root = "/var/www/munin/";
          };
        };
      };
    };

    services.munin-cron = mkIf cfg.server {
      enable = true;
      hosts = ''
        [dread]
          address 100.73.238.99

        [iocaine]
          address 100.75.178.122

      '';
    };


    # Proxy through Traefik, if resticServer is enabled
    services.traefik.dynamicConfigOptions.http = mkIf cfg.server{
      routers.munin = {
        entryPoints = [ "https" ];
        rule = "Host(`${cfg.domainName}`)";
        service = "munin";
      };

      services.munin.loadBalancer = {
        passHostHeader = true;
        servers = [{ url = "http://127.0.0.1:${toString cfg.port}"; }];
      };
    };

  };
}
