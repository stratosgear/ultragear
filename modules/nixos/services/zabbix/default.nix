{ options, config, pkgs, lib, ... }:

with lib;
with lib.ultragear;
let cfg = config.ultragear.services.zabbix;
in
{
  options.ultragear.services.zabbix = with types; {
    enable = mkBoolOpt false "Whether or not to enable zabbix services.";
    server = mkBoolOpt false "Whether or not to enable zabbix server service.";
    agent = mkBoolOpt false "Whether or not to enable zabbix agent service.";
    agentOpenFirewall = mkBoolOpt false "Whether or not to open the zabbix port in the firewall.";
    agentConnectTo = mkOpt str "" "The IP address or hostname of the Zabbix server to connect to.";
    web = mkBoolOpt false "Whether or not to enable zabbix web service.";
    webDomainName = mkOpt str CONSTS.urls.zabbix "The domain name to expose the wzabbix web service";
  };

  config = mkIf cfg.enable {

    assertions = [
      {
        assertion = cfg.agent -> cfg.agentConnectTo != "";
        message = "If Zabbix Agent is activated, a connecting Zabbix Server is also required in agentConnectTo.";
      }
    ];

    services.zabbixServer = mkIf cfg.server {
      enable = true;
      openFirewall = true;
    };

    services.zabbixAgent = mkIf cfg.agent {
      enable = true;
      server = cfg.agentConnectTo;
      settings = {
        RefreshActiveChecks = 60;
      };
      openFirewall = cfg.agentOpenFirewall;
    };

    services.zabbixWeb = mkIf cfg.web {
      enable = true;
      virtualHost = {
        hostName = cfg.webDomainName;
        adminAddr = "stratos@gerakakis.net";
        listen = [{
          ip = "0.0.0.0";
          port = CONSTS.ports.services.zabbixWeb;
        }];
      };
    };

    # Proxy through Traefik
    services.traefik.dynamicConfigOptions.http = {
      routers.zabbix = {
        entryPoints = [ "https" ];
        rule = "Host(`${cfg.webDomainName}`)";
        service = "zabbix";
        tls.certResolver = "letsencrypt";
      };

      services.zabbix.loadBalancer = {
        passHostHeader = true;
        servers = [{ url = "http://127.0.0.1:${builtins.toString CONSTS.ports.services.zabbixWeb}"; }];
      };
    };

  };
}
