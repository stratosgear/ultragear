{ options, config, pkgs, lib, ... }:

with lib;
with lib.ultragear;
let
  cfg = config.ultragear.services.syncthing;
in
{
  options.ultragear.services.syncthing = with types; {
    enable = mkBoolOpt false "Whether or not to configure syncthing support.";
    domainName = mkOpt str "sync.${config.networking.hostName}.${CONSTS.urls.privRootDomain}" "The domain name to expose service as";
    # user = mkOpt str "syncthing" "User to run as";
    # group = mkOpt str "syncthing" "Group to run as";
    configDir = mkOpt str "/data/syncthing_config" "Directory to store config files";
    dataDir = mkOpt str "/data/syncthing" "Directory to store synced files";
  };

  config = mkIf cfg.enable {

    systemd.tmpfiles.rules = [
      "d ${cfg.configDir} 0750 ${config.services.syncthing.user} ${config.services.syncthing.group} -"
      "d ${cfg.dataDir} 0750 ${config.services.syncthing.user} ${config.services.syncthing.group} -"
    ];

    # Iliana is doing something interesting with multiple folders and devices:
    # https://github.com/iliana/nixos-configs/blob/0837841975bd7245189c685bec942119b8e44005/modules/base/syncthing.nix#L28

    services.syncthing = {
      enable = true;
      # FIX: hardcoding values here because I cannot make ultragear.home work!
      # dataDir = "/home/stratos";
      openDefaultPorts = true;
      # FIX: hardcoding values here because I cannot make ultragear.home work!
      # configDir = "/home/stratos/.config/syncthing";
      configDir = cfg.configDir;
      dataDir = cfg.dataDir;
      # FIX: hardcoding values here because I cannot make ultragear.home work!
      # user = cfg.user;
      # group = cfg.group;

      guiAddress = "0.0.0.0:8384";
      overrideDevices = false;
      overrideFolders = false;
      # settings = {
      #   devices = {
      #     "iocaine" = { id = "VSEUSTY-QAEB7ZG-G6D2E4K-EHCYBW3-EPUEU4H-S4A2SDL-4SAAZD7-UBQFHAD"; };
      #   };
      #   folders = {
      #     # This is syncing the Boox notes from the Onyx Boox Air 2 eink tablet.
      #     "Notes" = {
      #       id = "vsek1-ytr6c";
      #       path = "/home/stratos/Documents/syncthing/Boox";
      #       devices = [ "iocaine" ];
      #       versioning = {
      #         # type = "staggered";
      #         # params = {
      #         #   cleanInterval = "3600";
      #         #   maxAge = "15768000";
      #         # };
      #         type = "simple";
      #         params = {
      #           keep = "12";
      #         };
      #       };
      #     };
      #   };
      # };
    };

    # Add main user to syncthing group
    users.users."${config.ultragear.user.name}" = {
      extraGroups = [ config.services.syncthing.group ];
    };

    systemd.services.syncthing = {
      serviceConfig = {
        Umask = "002";
    #     ReadWritePaths = [ "/home/stratos/.config/espanso/img/" ];
    #     PrivateUsers = mkForce false;
    #     PrivateMounts = mkForce false;
    #     # ProtectSystem = false;
    #     # ProtectHome = false;
    #     # NoNewPrivileges = mkForce  false;
      };
    };

    # Proxy through Traefik
    services.traefik.dynamicConfigOptions.http = {
      routers.syncthing = {
        entryPoints = [ "https" ];
        rule = "Host(`${cfg.domainName}`)";
        service = "syncthing";
      };

      services.syncthing.loadBalancer = {
        passHostHeader = true;
        servers = [{ url = "http://127.0.0.1:8384"; }];
      };
    };

    # Stanza for system restic backups
    ultragear.services.backup.instances.system = {
      sourceDirectories = [ cfg.configDir ];
    };

  };
}
