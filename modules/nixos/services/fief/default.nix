{ options, config, pkgs, lib, ... }:

with lib;
with lib.ultragear;
let cfg = config.ultragear.services.fief;
in
{
  options.ultragear.services.fief = with types; {
    enable = mkBoolOpt false "Whether or not to enable the Fief authentication service";
    port = mkOpt int CONSTS.ports.services.fief "The port number to run as";
    domainName = mkOpt str CONSTS.urls.fief "The domain name to expose service as";
  };

  config = mkIf cfg.enable {

    services.postgresql = {
      ensureUsers = [
        {
          name = "fief";
          ensureDBOwnership = true;
        }
      ];
      ensureDatabases = [ "fief" ];
    };

    sops.secrets = {
      "fief/env" = { };
      "fief/redis-pass" = { };
    };

    virtualisation.oci-containers.containers = {
      "fief_server" = {
        image = "ghcr.io/fief-dev/fief:0.28.6";
        environment = {
          "DATABASE_TYPE" = "POSTGRESQL";
          "DATABASE_USERNAME" = "fief";
          "DATABASE_HOST" = "10.88.0.1";
          "DATABASE_NAME" = "fief";
          "TELEMETRY_ENABLED" = "False";
          "FORWARDED_ALLOW_IPS" = "*";
          "EMAIL_PROVIDER" = "SMTP";
        };
        environmentFiles = [ config.sops.secrets."fief/env".path ];
        cmd = [ "fief" "run-server" ];
        ports = [ "${builtins.toString cfg.port}:8000" ];
      };
      "fief_worker" = {
        image = "ghcr.io/fief-dev/fief:0.28.6";
        environment = {
          "DATABASE_TYPE" = "POSTGRESQL";
          "DATABASE_USERNAME" = "fief";
          "DATABASE_HOST" = "10.88.0.1";
          "DATABASE_NAME" = "fief";
          "TELEMETRY_ENABLED" = "False";
          "FORWARDED_ALLOW_IPS" = "*";
          "EMAIL_PROVIDER" = "SMTP";
        };
        environmentFiles = [ config.sops.secrets."fief/env".path ];
        cmd = [ "fief" "run-worker" "-p" "1" "-t" "1" ];
      };
    };

    services.redis.servers.fief = {
      enable = true;
      port = CONSTS.ports.redis.fief;
      databases = 1;
      openFirewall = true;
      bind = null;
      requirePassFile = config.sops.secrets."fief/redis-pass".path;
    };

    # Proxy through Traefik
    services.traefik.dynamicConfigOptions.http = {
      routers.fief = {
        entryPoints = [ "https" ];
        rule = "Host(`${cfg.domainName}`)";
        service = "fief";
        middlewares = "local@file";
      };
      services.fief.loadBalancer = {
        passHostHeader = true;
        servers = [{ url = "http://127.0.0.1:${toString cfg.port}"; }];
      };
    };

    # Stanza for system restic backups
    ultragear.services.backup.instances.system = {
      databases = [ "fief" ];
    };

  };
}
