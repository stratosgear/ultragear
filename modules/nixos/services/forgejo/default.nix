{ options, config, pkgs, lib, ... }:

with lib;
with lib.ultragear;
let cfg = config.ultragear.services.forgejo;
in
{
  options.ultragear.services.forgejo = with types; {
    enable = mkBoolOpt false "Whether or not to enable the forgejo service";
    serverPort = mkOpt int CONSTS.ports.services.forgejo.server "The server port number to run as";
    gitPort = mkOpt int CONSTS.ports.services.forgejo.git "The git/ssh port number to run as";
    domainName = mkOpt str CONSTS.urls.forgejo "The domain name to expose service as";
    dataDir = mkOpt str "" "The location to store data and git repos";
  };

  config = mkIf cfg.enable {

    # sops.secrets.smtp2go-pwd = {
    #   owner = "forgejo";
    # };

    networking.firewall.allowedTCPPorts = [ cfg.gitPort ];

    # users.users.git = {
    #   group = "git";
    #   isSystemUser = true;
    # };
    # users.groups.git = { };

    systemd.tmpfiles.rules = [
      "d ${cfg.dataDir} 0750 ${config.services.forgejo.user} ${config.services.forgejo.group} -"
    ];

    services.postgresql = {
      ensureUsers = [
        {
          name = "forgejo";
          ensureDBOwnership = true;
        }
      ];
      ensureDatabases = [ "forgejo" ];
    };

    services.redis.servers.forgejo = {
      enable = true;
      port = CONSTS.ports.redis.forgejo;
      databases = 1;
      # openFirewall = true;
      bind = null;
      # requirePassFile = config.sops.secrets."weblate/redis-pass".path;
    };

    services.forgejo = {
      enable = true;
      # user = "git";
      # group = "git";
      package = pkgs.forgejo;
      stateDir = cfg.dataDir;
      settings = {
        service.DISABLE_REGISTRATION = true;
        DEFAULT.APP_NAME = "forgejo";
        log.LEVEL = "Debug";
        ui = {
          DEFAULT_THEME = "forgejo-dark";
          SHOW_USER_EMAIL = true;
        };
        actions = {
          ENABLED = false;
          DEFAULT_ACTIONS_URL = "https://code.forgejo.org";
        };
        server = {
          HTTP_PORT = cfg.serverPort;
          DOMAIN = cfg.domainName;
          ROOT_URL = "https://${cfg.domainName}";
          LANDING_PAGE = "/explore/repos";
          # START_SSH_SERVER = true;
          SSH_DOMAIN = cfg.domainName;
          SSH_PORT = cfg.gitPort;
          SSH_LISTEN_PORT = cfg.gitPort;
          # SSH_LISTEN_HOST = "100.121.201.47";
          SSH_LISTEN_HOST = "0.0.0.0";
        };
        "git.timeout" = {
          DEFAULT = 3600;
          MIGRATE = 3600;
          MIRROR = 3600;
          CLONE = 3600;
        };
        session = {
          COOKIE_SECURE = true;
        };
        security = {
          LOGIN_REMEMBER_DAYS = 14;
        };
        database = {
          DB_TYPE = lib.mkForce "postgres";
          HOST = "localhost:5432";
          NAME = "forgejo";
          USER = "forgejo";
          PASSWD = "forgejo";
        };
        cache = {
          ENABLED = true;
          ADAPTER = lib.mkForce "redis";
          HOST = "redis://localhost:${builtins.toString CONSTS.ports.redis.forgejo}";
        };
        metrics = {
          ENABLED = true;
          ENABLED_ISSUE_BY_REPOSITORY = true;
          ENABLED_ISSUE_BY_LABEL = true;
        };
        # mailer = {
        #   ENABLED = true;
        #   FROM = "forgejo@flake.sh";
        #   PROTOCOL = "smtp+starttls";
        #   SMTP_ADDR = "mail.smtp2go.com";
        #   SMTP_PORT = 587;
        #   USER = "forgejo-mailer";
        # };
      };
      # mailerPasswordFile = config.sops.secrets.smtp2go-pwd.path;
    };

    services.fail2ban.jails.forgejo = {
      settings = {
        filter = "forgejo";
        action = ''iptables-allports'';
        mode = "aggressive";
        maxretry = 3;
        findtime = 3600;
        bantime = 900;
      };
    };

    environment.etc = {
      "fail2ban/filter.d/forgejo.conf".text = ''
        [Definition]
        failregex = ^.*(Failed authentication attempt|invalid credentials|Attempted access of unknown user).* from <HOST>$
        journalmatch = _SYSTEMD_UNIT=forgejo.service
      '';
    };

    # Proxy through Traefik
    services.traefik.dynamicConfigOptions.http = {
      routers.forgejo = {
        entryPoints = [ "https" ];
        rule = "Host(`${cfg.domainName}`)";
        service = "forgejo";
        middlewares = "local@file";
      };
      services.forgejo.loadBalancer = {
        passHostHeader = true;
        servers = [{ url = "http://127.0.0.1:${toString cfg.serverPort}"; }];
      };
    };

  };

}
