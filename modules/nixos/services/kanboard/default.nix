{ options, config, pkgs, lib, ... }:

with lib;
with lib.ultragear;
let cfg = config.ultragear.services.kanboard;
in
{
  options.ultragear.services.kanboard = with types; {
    enable = mkBoolOpt false "Whether or not to enable the kanboard service";
    port = mkOpt int CONSTS.ports.services.homepage "The port number to run as";
    domainName = mkOpt str "kanboard" "The domain name to expose service as";
    dataDir = mkOpt str "" "The location to store data";
  };

  config = mkIf cfg.enable {

    systemd.tmpfiles.rules = [
      "d ${cfg.dataDir}/data 0750 root root -"
      "d ${cfg.dataDir}/plugins 0750 root root -"
    ];

    virtualisation.oci-containers.backend = "docker";
    virtualisation.oci-containers.containers.kanboard = {

      image = "kanboard/kanboard:v1.2.36";
      ports = [ "${builtins.toString cfg.port}:80" ];
      volumes = [
        "${cfg.dataDir}/data:/var/www/app/data"
        "${cfg.dataDir}/plugins:/var/www/app/plugins"
      ];
      autoStart = true;
      environment = {
        TZ = config.ultragear.system.time.tz;
        PLUGIN_INSTALLER = "true";
      };
    };

    # Proxy through Traefik
    services.traefik.dynamicConfigOptions.http = {
      routers.kanboard = {
        entryPoints = [ "https" ];
        rule = "Host(`${cfg.domainName}.${config.networking.hostName}.${CONSTS.urls.privRootDomain}`)";
        service = "kanboard";
        middlewares = "local@file";
      };

      services.kanboard.loadBalancer = {
        passHostHeader = true;
        servers = [{ url = "http://127.0.0.1:${toString cfg.port}"; }];
      };
    };

    # Stanza for system restic backups
    ultragear.services.backup.instances.system = {
      sourceDirectories = [ cfg.dataDir ];
    };

  };
}

