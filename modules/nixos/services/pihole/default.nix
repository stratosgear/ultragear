{ options, config, pkgs, lib, ... }:

with lib;
with lib.ultragear;
let cfg = config.ultragear.services.pihole;
in
{
  options.ultragear.services.pihole = with types; {
    enable = mkBoolOpt false "Whether or not to enable pihole service";
    dataDir = mkOpt str "/data/pihole" "The location to store config data";
    domainName = mkOpt str CONSTS.urls.pihole "The domain name to expose service as";
    port = mkOpt int CONSTS.ports.services.pihole.external "Port for the pihole server";
  };

  config = mkIf cfg.enable {

    systemd.tmpfiles.rules = [
      "d ${cfg.dataDir}/config 0750 root root -"
      "d ${cfg.dataDir}/dnsmasq.d 0750 root root -"
      "d ${cfg.dataDir}/logs 0750 root root -"
    ];

    networking.firewall = {
      allowedTCPPorts = [ 53 ];
      allowedUDPPorts = [ 53 ];
    };

    services.dnsmasq.enable = false;

    virtualisation.oci-containers.containers.pihole = {
      image = "docker.io/pihole/pihole:2024.02.2";
      autoStart = true;
      volumes = [
        "${cfg.dataDir}/config:/etc/pihole"
        "${cfg.dataDir}/dnsmasq.d:/etc/dnsmasq.d"
        "${cfg.dataDir}/logs:/var/log/pihole"
      ];
      ports = [
        "${builtins.toString cfg.port}:${builtins.toString CONSTS.ports.services.pihole.internal}"
        "53:53/udp"
        "53:53/tcp"
      ];

      environment = {
        TZ = config.ultragear.system.time.tz;
        DNSSEC = "false";
        VIRTUAL_HOST = "${cfg.domainName}";
        WEB_PORT = "${builtins.toString CONSTS.ports.services.pihole.internal}";
        WEBPASSWORD = "testing";
      };
      extraOptions = [
        "--cap-add=NET_ADMIN"
        "--dns=127.0.0.1"
        "--dns=1.1.1.1"
      ];
    };

    # Proxy through Traefik
    services.traefik.dynamicConfigOptions.http = {
      routers.pihole = {
        entryPoints = [ "https" ];
        rule = "Host(`${cfg.domainName}`)";
        service = "pihole";
        middlewares = "local@file";
      };

      services.pihole.loadBalancer = {
        passHostHeader = true;
        servers = [{ url = "http://127.0.0.1:${toString cfg.port}"; }];
      };
    };

    # Stanza for system restic backups
    ultragear.services.backup.instances.system = {
      sourceDirectories = [ "${cfg.dataDir}/config" "${cfg.dataDir}/dnsmasq.d" ];
    };

  };
}
