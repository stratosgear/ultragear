{ options, config, pkgs, lib, ... }:

with lib;
with lib.ultragear;
let cfg = config.ultragear.services.ha;
in
{
  options.ultragear.services.ha = with types; {
    enable = mkBoolOpt false "Whether or not to enable Home Assistant";
    dataDir = mkOpt str "" "The location where Home Assistant data are stored";
    port = mkOpt int CONSTS.ports.services.homeassistant "The port number to run as";
    domainName = mkOpt str CONSTS.urls.homeassistant "The domain name to expose service as";
  };

  config = mkIf cfg.enable {

    assertions = [
      {
        assertion = cfg.dataDir != "";
        message = "If Home Assistant is active you also have to provide a dataDir too";
      }
    ];

    systemd.tmpfiles.rules = [
      "d ${cfg.dataDir} 0750 root root -"
    ];

    virtualisation.oci-containers.containers."homeassistant" = {
      autoStart = true;
      image = "ghcr.io/home-assistant/home-assistant:stable";
      volumes = [
        "${cfg.dataDir}:/config"
        "/etc/localtime:/etc/localtime:ro"
      ];
      extraOptions = [
        "--device=/dev/ttyUSB0"
        "--network=host"
        "--privileged"
      ];
      # ports = [ "${builtins.toString cfg.port}:8123" ];
    };

    # Proxy through Traefik
    services.traefik.dynamicConfigOptions.http = {
      routers.homeassistant = {
        entryPoints = [ "https" ];
        rule = "Host(`${cfg.domainName}`)";
        service = "homeassistant";
        middlewares = "local@file";
      };

      services.homeassistant.loadBalancer = {
        passHostHeader = true;
        # servers = [{ url = "http://127.0.0.1:${toString cfg.port}"; }];
        servers = [{ url = "http://127.0.0.1:8123"; }];
      };
    };

    # Stanza for system restic backups
    ultragear.services.backup.instances.system = {
      sourceDirectories = [ cfg.dataDir ];
    };
  };
}
