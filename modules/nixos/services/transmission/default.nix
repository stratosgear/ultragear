{ options, config, pkgs, lib, ... }:

with lib;
with lib.ultragear;
let cfg = config.ultragear.services.transmission;
in
{
  options.ultragear.services.transmission = with types; {
    enable = mkBoolOpt false "Whether or not to enable the shlink service";
    dataDir = mkOpt str "" "The location to store data";
    port = mkOpt int CONSTS.ports.services.shlink "The port number to run as";
    domainName = mkOpt str CONSTS.urls.shlink "The domain name to expose service as";
  };

  config = mkIf cfg.enable {

    services.postgresql = {
      ensureUsers = [
        {
          name = "shlink";
          ensureDBOwnership = true;
        }
      ];
      ensureDatabases = [ "shlink" ];
    };

    sops.secrets."shlink/env" = { };

    virtualisation.oci-containers.containers = {
      transmission = {
        image = "shlinkio/shlink:stable";
        environment = {
          "DEFAULT_DOMAIN" = "${cfg.domainName}";
          "USE_HTTPS" = "false";
          "IS_HTTPS_ENABLED" = "true";
          "DB_DRIVER" = "postgres";
          "DB_USER" = "shlink";
          "DB_HOST" = "10.88.0.1";
          "SHELL_VERBOSITY" = "3";
        };
        extraOptions = [ "--network=container:gluetun" ];
        environmentFiles = [ config.sops.secrets."shlink/env".path ];
        ports = [ "${builtins.toString cfg.port}:8080" ];
        volumes = [ ];
      };

      gluetun = {
        image = "qmcgaw/gluetun:latest";
        autoStart = true;
        extraOptions = [
          "--cap-add=NET_ADMIN"
          # "-l=traefik.enable=true"
          # "-l=traefik.http.routers.deluge.rule=Host(`deluge.${vars.domainName}`)"
          # "-l=traefik.http.routers.deluge.service=deluge"
          # "-l=traefik.http.services.deluge.loadbalancer.server.port=8112"
          "--device=/dev/net/tun:/dev/net/tun"
          # "-l=homepage.group=Arr"
          # "-l=homepage.name=Gluetun"
          # "-l=homepage.icon=gluetun.svg"
          # "-l=homepage.href=https://deluge.${vars.domainName}"
          # "-l=homepage.description=VPN killswitch"
          # "-l=homepage.widget.type=gluetun"
          # "-l=homepage.widget.url=http://gluetun:8000"
        ];
        ports = [ "${builtins.toString cfg.port}:8080" ];

        environmentFiles = [
          config.age.secrets.wireguardCredentials.path
        ];
        environment = {
          VPN_TYPE = "wireguard";
          WIREGUARD_PRIVATE_KEY = "base64"; # FIX
          WIREGUARD_ADDRESSES = "10.64.222.21/32"; # FIX
          SERVER_CITIES = "Amsterdam";
        };
      };
    };

    # Proxy through Traefik
    services.traefik.dynamicConfigOptions.http = {
      routers.shlink = {
        entryPoints = [ "https" ];
        rule = "Host(`${cfg.domainName}`)";
        service = "shlink";
        middlewares = "local@file";
      };
      services.shlink.loadBalancer = {
        passHostHeader = true;
        servers = [{ url = "http://127.0.0.1:${toString cfg.port}"; }];
      };
    };

    # Stanza for system restic backups
    # ultragear.services.backup.instances.system = {
    #   databases = [ "shlink" ];
    # };

  };
}
