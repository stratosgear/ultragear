{ options, config, pkgs, lib, ... }:

with lib;
with lib.ultragear;
let cfg = config.ultragear.services.mpd;
in
{
  options.ultragear.services.mpd = with types; {
    enable = mkBoolOpt false "Whether or not to configure mpd service.";
    # Satellite setup as described by myMPD app:
    # https://jcorporation.github.io/myMPD/additional-topics/mpd-satellite-setup
    satellite = mkBoolOpt false "Whether or not to run mpd as a satellite server";
    masterMpdHost = mkOpt str "vern" "Hostname of the master mpd server";
    dataDir = mkOpt str "" "The location where mpd data are stored";
    musicDir = mkOpt str "" "The location where the music library is";
    playlistDir = mkOpt str "" "The location where the playlists are";
    # serverPort = mkOpt int CONSTS.ports.services.mpd.server "Port for the mpd server";
    streamPort = mkOpt int CONSTS.ports.services.mpd.stream "Port to stream music from";
  };


  # From:
  # https://github.com/cyber-murmel/dome-sound-system/blob/3caa76066823e63589337b8075476f6852de0152/nixos/mpd/default.nix
  config = mkIf cfg.enable {

    assertions = [
      {
        assertion = cfg.satellite != true -> cfg.dataDir != "";
        message = "If services.mpd is active and this is the master mpd server you also have to provide a location for data store (dataDir)";
      }
      {
        assertion = cfg.musicDir != "";
        message = "If services.mpd is active you also have to provide a location of where the music lib is (musicDir)";
      }
      {
        assertion = cfg.playlistDir != "";
        message = "If services.mpd is active you also have to provide a location of where the playlists are (playlistDir)";
      }
    ];

    systemd.tmpfiles.rules = mkIf (cfg.satellite == false)
      [
        "d ${cfg.dataDir} 0700 ${config.services.mpd.user} ${config.services.mpd.group} -"
        "d ${cfg.playlistDir} 0700 ${config.services.mpd.user} ${config.services.mpd.group} -"
      ];


    users.users."${config.services.mpd.user}".extraGroups = [ "musiclib" ];

    services = {
      mpd = {
        enable = true;
        # dbFile is null, because we set custom database plugin (later on)
        dbFile = null;
        dataDir = cfg.dataDir;
        musicDirectory = cfg.musicDir;
        playlistDirectory = cfg.playlistDir;
        network = {
          # listenAddress = "100.64.0.0/10"; # vpn network
          listenAddress = "any"; # vpn network
          # port = cfg.serverPort; # vpn network
        };
        extraConfig =
          let
            satOrMaster =
              if cfg.satellite then ''
                database {
                  plugin "proxy"
                  host "${cfg.masterMpdHost}"
                  port "443"
                  keepalive "yes"
                }
                bind_to_address "/run/mpd/socket"
              '' else ''
                database {
                  plugin "simple"
                  path "${cfg.dataDir}/tag_cache"
                  cache_directory "${cfg.dataDir}/cache"
                }
                sticker_file "${cfg.dataDir}/sticker.sql"
                bind_to_address "0.0.0.0"
              '';
          in
          # mpd configuration as suggested by myMPD
          # https://jcorporation.github.io/myMPD/additional-topics/recommended-mpd-configuration
          ''
            auto_update           "yes"
            restore_paused        "yes"

            ${satOrMaster}

            # Enable metadata. If set to none, you can only browse the filesystem
            # metadata_to_use         "AlbumArtist,Artist,Album,Title,Track,Disc,Genre,Name"
            # Enable also the musicbrainz_* tags if you want integration with MusicBrainz and ListenBrainz
            # musicbrainz_albumid is the fallback for musicbrainz_releasegroupid (MPD 0.24)
            metadata_to_use         "AlbumArtist,Artist,Album,Title,Track,Disc,Genre,Name,musicbrainz_artistid,musicbrainz_albumid,musicbrainz_albumartistid,musicbrainz_trackid,musicbrainz_releasetrackid"

            # audio_output {
            #   type "pulse"
            #   name "Pulseaudio"
            #   server "127.0.0.1" # add this line - MPD must connect to the local sound server
            # }

            # audio_output {
            # 	type                "fifo"
            # 	name                "Visualizer"
            # 	format              "44100:16:2"
            # 	path                "/tmp/mpd.fifo"
            # }

            audio_output {
            	type                "httpd"
            	name                "Streaming Audio"
            	encoder             "lame"
            	port                "${toString cfg.streamPort}"
              quality             "1"
            	max_clients         "8"
            	mixer_type          "software"
            	format              "44100:16:2"
              tags                "yes"
              always_on           "yes"
            }
          '';
        # extraConfig = ''
        #   audio_output {
        #     type "pulse"
        #     name "PulseAudio" # this can be whatever you want
        #   }
        # '';

        # Optional:
        # startWhenNeeded = true; # systemd feature: only start MPD service upon connection to its socket
      };
    };


    # users.extraUsers.nixos.extraGroups = [ config.services.mpd.group ];
    # users.extraUsers.mpd.extraGroups = [ "pulse-access" ];

    environment = {
      variables = {
        MPD_MUSIC_DIR = config.services.mpd.musicDirectory;
      };
      systemPackages = [ pkgs.flac pkgs.lame ];
    };

    # Proxy through Traefik
    # First time we are proxying TCP traefik
    services.traefik.dynamicConfigOptions.tcp = {
      routers.mpd = {
        entryPoints = [ "https" ];
        rule = "HostSNI(`mpd.${config.networking.hostName}.${CONSTS.urls.privRootDomain}`)";
        service = "mpd";
        tls.passthrough = true;
        # middlewares = "local@file";
      };
      services.mpd.loadBalancer = {
        # passHostHeader = true;
        # servers = [{ url = "http://127.0.0.1:${toString cfg.serverPort}"; }];
        servers = [{ address = "127.0.0.1:${toString config.services.mpd.network.port}"; }];
      };
    };

    services.traefik.dynamicConfigOptions.http = {
      routers.mpdStream = {
        entryPoints = [ "https" ];
        rule = "Host(`mpdStream.${config.networking.hostName}.${CONSTS.urls.privRootDomain}`)";
        service = "mpdStream";
        middlewares = "local@file";
      };
      services.mpdStream.loadBalancer = {
        passHostHeader = true;
        servers = [{ url = "http://127.0.0.1:${toString cfg.streamPort}"; }];
      };
    };

  };
}
