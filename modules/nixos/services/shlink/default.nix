{ options, config, pkgs, lib, ... }:

with lib;
with lib.ultragear;
let cfg = config.ultragear.services.shlink;
in
{
  options.ultragear.services.shlink = with types; {
    enable = mkBoolOpt false "Whether or not to enable the shlink service";
    port = mkOpt int CONSTS.ports.services.shlink "The port number to run as";
    domainName = mkOpt str CONSTS.urls.shlink "The domain name to expose service as";
  };

  config = mkIf cfg.enable {

    services.postgresql = {
      ensureUsers = [
        {
          name = "shlink";
          ensureDBOwnership = true;
        }
      ];
      ensureDatabases = [ "shlink" ];
    };

    sops.secrets."shlink/env" = { };

    virtualisation.oci-containers.containers."shlink" = {
      image = "shlinkio/shlink:stable";
      environment = {
        "DEFAULT_DOMAIN" = "${cfg.domainName}";
        "USE_HTTPS" = "false";
        "IS_HTTPS_ENABLED" = "true";
        "DB_DRIVER" = "postgres";
        "DB_USER" = "shlink";
        "DB_HOST" = "10.88.0.1";
        "SHELL_VERBOSITY" = "3";
      };
      environmentFiles = [ config.sops.secrets."shlink/env".path ];
      ports = [ "${builtins.toString cfg.port}:8080" ];
      volumes = [ ];
    };

    # Proxy through Traefik
    services.traefik.dynamicConfigOptions.http = {
      routers.shlink = {
        entryPoints = [ "https" ];
        rule = "Host(`${cfg.domainName}`)";
        service = "shlink";
        middlewares = "local@file";
      };
      services.shlink.loadBalancer = {
        passHostHeader = true;
        servers = [{ url = "http://127.0.0.1:${toString cfg.port}"; }];
      };
    };

    # Stanza for system restic backups
    ultragear.services.backup.instances.system = {
      databases = [ "shlink" ];
    };

  };
}
