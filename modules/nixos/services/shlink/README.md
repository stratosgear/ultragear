Exposes healthcheck endpoint at: /rest/health

## Generate API keys:

```
sudo podman exec -it shlink sh
cd bin
./cli api-key:generate
```

## Administrative app

Can you the online webapp at:

https://app.shlink.io

and add the server