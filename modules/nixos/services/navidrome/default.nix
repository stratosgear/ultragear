{ options, config, pkgs, lib, ... }:

with lib;
with lib.ultragear;
let cfg = config.ultragear.services.navidrome;
in
{
  options.ultragear.services.navidrome = with types; {
    enable = mkBoolOpt false "Whether or not to enable the Navidrome music server";
    enableDownloads = mkBoolOpt true "Whether or not to enable the ability to download music";
    user = mkOpt str "navidrome" "User to run the service as";
    # group = mkOpt str "navidrome" "Group to run the service as";
    jukebox = mkBoolOpt false "Whether to enable jukebox mode";
    musicDir = mkOpt str "" "The location where music is stored";
    dataDir = mkOpt str "" "The location to store data";
    port = mkOpt int CONSTS.ports.services.navidrome "The port number to run as";
    domainName = mkOpt str "navidrome.${CONSTS.urls.privRootDomain}" "The domain name to expose service as";
    logLevel = mkOpt str "INFO" "The debug log level";
  };

  config = mkIf cfg.enable {

    assertions = [
      {
        assertion = cfg.musicDir != "";
        message = "musicDir option must be set before enabling Navidrome";
      }
      {
        assertion = cfg.dataDir != "";
        message = "dataDir option must be set before enabling Navidrome";
      }
    ];

    # Make sure the datadir exists
    systemd.tmpfiles.rules = [
      "d ${cfg.dataDir} 0750 ${cfg.user} - -"
    ];

    # Activate service
    services.navidrome = {
      enable = true;

      settings = {
        Address = "127.0.0.1";
        Port = cfg.port;
        MusicFolder = cfg.musicDir;
        EnableGravatar = "true";
        DataFolder = cfg.dataDir;
        BaseUrl = "https://${cfg.domainName}";
        EnableDownloads = cfg.enableDownloads;
        LogLevel = cfg.logLevel;
      };
    };

    # Create user/group
    # users.groups."${cfg.group}" = {
    #   name = "${cfg.group}";
    # };
    users.users."${cfg.user}" = {
      name = "${cfg.user}";
      # group = "musiclib";
      # group = "${cfg.group}";
      extraGroups = [ "musiclib" ];
      isSystemUser = true;
    };

    # manually overide the DynamicUser that the Nixos packagers are using, with our
    # own custom user:group, so we can read the dataDir folder.
    systemd.services.navidrome = {
      serviceConfig = {
        DynamicUser = mkForce false;
        User = cfg.user;
        Group = mkForce "musiclib";
      };
    };

    # If in jukebox mode, install a media player too
    environment = mkIf cfg.jukebox {
      systemPackages = [ pkgs.mpv ];
    };

    # Proxy through Traefik
    services.traefik.dynamicConfigOptions.http = {
      routers.navidrome = {
        entryPoints = [ "https" ];
        rule = "Host(`${cfg.domainName}`)";
        service = "navidrome";
        middlewares = "local@file";
      };
      services.navidrome.loadBalancer = {
        passHostHeader = true;
        servers = [{ url = "http://127.0.0.1:${toString cfg.port}"; }];
      };
    };

    # Stanza for system restic backups
    ultragear.services.backup.instances.system = {
      sourceDirectories = [ cfg.dataDir ];
    };

  };
}
