{ options, config, pkgs, lib, ... }:

with lib;
with lib.ultragear;
let cfg = config.ultragear.services.redis;
in
{
  options.ultragear.services.redis = with types; {
    enable = mkBoolOpt false "Whether or not to enable redis service";
  };

  config = mkIf cfg.enable {

    services.redis = {
      enable = true;
    };
   };
}
