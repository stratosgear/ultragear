{
  options,
  config,
  pkgs,
  lib,
  ...
}:

# Based on:
# https://github.com/rcambrj/nix-pia-vpn

with lib;
with lib.ultragear;
let
  cfg = config.ultragear.services.pia-vpn;
  pia-region = "/tmp/pia-region";
  pia-servers = "https://serverlist.piaservers.net/vpninfo/servers/v4";
  pia-select-region = pkgs.writeShellScriptBin "pia-select-region" ''
    #!/usr/bin/env bash

    # Download the JSON data
    json_data=$(curl -s ${pia-servers} | head -1)

    # Parse the JSON data and extract the "id" and "name" from each region entry
    regions=$(echo "$json_data" | jq -r '.regions[] | "\(.name) [\(.id)]"')

    # Sort the regions
    sorted_regions=$(echo "$regions" | sort)

    # Create a temporary variable to store the output of printf
    menu=$(printf "Autoselect []\n%s" "$sorted_regions")

    # Display the menu and get the selected region
    selected_region=$(echo "$menu" | rofi -dmenu -i -theme ~/.config/rofi/appsMenu.rasi -p "Select a region:")

    # Get the selected region's ID
    selected_id=$(echo "$selected_region" | sed 's/.*\[\(.*\)\]/\1/')

    # Write the selected ID to /tmp/pia-region
    echo "$selected_id" > ${pia-region}

    # Exit with the selected ID and region
    echo "You selected region: $selected_region"
    echo "ID: $selected_id"
    exit 0'';
in
{
  options.ultragear.services.pia-vpn = with types; {
    enable = mkBoolOpt false "Private Internet Access VPN service.";

    certificateFile = mkOpt path "/run/secrets/pia/cert" ''
      Path to the CA certificate for Private Internet Access servers.

      This is provided as <filename>ca.rsa.4096.crt</filename>.
    '';

    environmentFile = mkOpt path "/run/secrets/pia/env" ''
      Path to an environment file with the following contents:

      <programlisting>
      PIA_USER=''${username}
      PIA_PASS=''${password}
      </programlisting>
    '';

    interface = mkOpt str "wg0" "WireGuard interface to create for the VPN connection.";

    region = mkOpt str "gr" ''
      Name of the region to connect to.
      See https://serverlist.piaservers.net/vpninfo/servers/v4
    '';

    maxLatency = mkOpt float 0.1 ''
      Maximum latency to allow for auto-selection of VPN server,
      in seconds. Does nothing if region is specified.'';

    netdevConfig = mkOpt str ''
      [NetDev]
      Description = WireGuard PIA network device
      Name = ''${interface}
      Kind = wireguard

      [WireGuard]
      PrivateKey = $privateKey

      [WireGuardPeer]
      PublicKey = $(echo "$json" | jq -r '.server_key')
      AllowedIPs = 0.0.0.0/0, ::/0
      Endpoint = ''${wg_ip}:$(echo "$json" | jq -r '.server_port')
      PersistentKeepalive = 25
    '' "Configuration of 60-[interface].netdev";

    networkConfig = mkOpt str ''
      [Match]
      Name = ''${interface}

      [Network]
      Description = WireGuard PIA network interface
      Address = ''${peerip}/32

      [RoutingPolicyRule]
      To = ''${wg_ip}/32
      Priority = 1000

      # if port forwarding is required, make an exception for that service
      # as it's not accessible from inside the VPN
      [RoutingPolicyRule]
      To = ''${meta_ip}/32
      Priority = 1000

      [RoutingPolicyRule]
      To = 0.0.0.0/0
      Priority = 2000
      Table = 42

      [Route]
      Destination = 0.0.0.0/0
      Table = 42    '' "Configuration of 60-[interface].network";
    # networkConfig = mkOpt str ''
    #   [Match]
    #   Name = ''${interface}

    #   [Network]
    #   Description = WireGuard PIA network interface
    #   Address = ''${peerip}/32

    #   [RoutingPolicyRule]
    #   From = ''${peerip}
    #   Table = 42

    #   [Route]
    #   Table = 42
    #   Destination = 0.0.0.0/0
    # '' "Configuration of 60-[interface].network";

    preUp = mkOpt lines "" "Commands called at the start of the interface setup.";

    postUp = mkOpt lines "" "Commands called at the end of the interface setup.";

    preDown = mkOpt lines "" "Commands called before the interface is taken down.";

    postDown = mkOpt lines "" "Commands called after the interface is taken down.";

    portForward = {
      enable = mkBoolOpt true "port forwarding through the PIA VPN connection.";

      script =
        mkOpt lines ""
          "Script to execute, with <varname>$port</varname> set to the forwarded port.";
    };
  };

  config = mkIf cfg.enable {

    boot.kernelModules = [ "wireguard" ];

    systemd.network.enable = true;
    environment.systemPackages = with pkgs; [ pia-select-region ];

    sops.secrets = {
      "pia/env" = {
        sopsFile = ./. + "../../../../../secrets/vault.yaml";
        restartUnits = [ "pia-vpn" ];
      };
      "pia/cert" = {
        sopsFile = ./. + "../../../../../secrets/vault.yaml";
        restartUnits = [ "pia-vpn" ];
      };
    };

    systemd.services.pia-vpn = {
      description = "Connect to Private Internet Access on ${cfg.interface}";
      path = with pkgs; [
        bash
        curl
        gawk
        jq
        wireguard-tools
      ];
      requires = [ "network-online.target" ];
      after = [
        "network.target"
        "network-online.target"
      ];
      # wantedBy = [ "multi-user.target" ];
      # Removing the wantedBy entry makes the service to NOT autostart
      wantedBy = [];
      before= [ "systemd-networkd-wait-online.service" ];

      unitConfig = {
        ConditionFileNotEmpty = [
          cfg.certificateFile
          cfg.environmentFile
        ];
      };

      serviceConfig = {
        Type = "oneshot";
        RemainAfterExit = true;
        Restart = "on-failure";
        EnvironmentFile = cfg.environmentFile;

        CacheDirectory = "pia-vpn";
        StateDirectory = "pia-vpn";
      };

      script = ''
        printServerLatency() {
          serverIP="$1"
          regionID="$2"
          regionName="$(echo ''${@:3} |
            sed 's/ false//' | sed 's/true/(geo)/')"
          time=$(LC_NUMERIC=en_US.utf8 curl -o /dev/null -s \
            --connect-timeout ${toString cfg.maxLatency} \
            --write-out "%{time_connect}" \
            http://$serverIP:443)
          if [ $? -eq 0 ]; then
            >&2 echo Got latency ''${time}s for region: $regionName
            echo $time $regionID $serverIP
          fi
        }
        export -f printServerLatency

        echo Fetching regions...
        serverlist='${pia-servers}'
        allregions=$((curl --no-progress-meter -m 5 "$serverlist" || true) | head -1)

        # create a bash onliner where a variable piaregion is loaded from /tmp/pia-region. If file does
        # not exist, set the region to ""
        if [ -f /tmp/pia-region ]; then
            piaregion="$(cat ${pia-region})"
        else
            piaregion=""
        fi

        region="$(echo $allregions |
                    jq --arg REGION_ID "$piaregion" -r '.regions[] | select(.id==$REGION_ID)')"
        if [ -z "''${region}" ]; then
          echo Determining region...
          filtered="$(echo $allregions | jq -r '.regions[]
                    ${optionalString cfg.portForward.enable "| select(.port_forward==true)"}
                    | .servers.meta[0].ip+" "+.id+" "+.name+" "+(.geo|tostring)')"
          best="$(echo "$filtered" | xargs -I{} bash -c 'printServerLatency {}' |
                  sort | head -1 | awk '{ print $2 }')"
          if [ -z "$best" ]; then
            >&2 echo "No region found with latency under ${toString cfg.maxLatency} s. Stopping."
            exit 1
          fi
          region="$(echo $allregions |
                    jq --arg REGION_ID "$best" -r '.regions[] | select(.id==$REGION_ID)')"
        fi
        echo Using region $(echo $region | jq -r '.id')

        meta_ip="$(echo $region | jq -r '.servers.meta[0].ip')"
        meta_hostname="$(echo $region | jq -r '.servers.meta[0].cn')"
        wg_ip="$(echo $region | jq -r '.servers.wg[0].ip')"
        wg_hostname="$(echo $region | jq -r '.servers.wg[0].cn')"
        echo "$region" > $STATE_DIRECTORY/region.json

        echo Fetching token from $meta_ip...
        tokenResponse="$(curl --no-progress-meter -m 5 \
          -u "$PIA_USER:$PIA_PASS" \
          --connect-to "$meta_hostname::$meta_ip" \
          --cacert ${cfg.certificateFile} \
          "https://$meta_hostname/authv3/generateToken" || true)"
        if [ "$(echo "$tokenResponse" | jq -r '.status' || true)" != "OK" ]; then
          >&2 echo "Failed to generate token. Stopping."
          exit 1
        fi
        token="$(echo "$tokenResponse" | jq -r '.token')"

        echo Connecting to the PIA WireGuard API on $wg_ip...
        privateKey="$(wg genkey)"
        publicKey="$(echo "$privateKey" | wg pubkey)"
        json="$(curl --no-progress-meter -m 5 -G \
          --connect-to "$wg_hostname::$wg_ip:" \
          --cacert "${cfg.certificateFile}" \
          --data-urlencode "pt=''${token}" \
          --data-urlencode "pubkey=$publicKey" \
          "https://''${wg_hostname}:1337/addKey" || true)"
        status="$(echo "$json" | jq -r '.status' || true)"
        if [ "$status" != "OK" ]; then
          >&2 echo "Server did not return OK. Stopping."
          >&2 echo "$json"
          exit 1
        fi

        echo Creating network interface ${cfg.interface}.
        echo "$json" > $STATE_DIRECTORY/wireguard.json

        gateway="$(echo "$json" | jq -r '.server_ip')"
        servervip="$(echo "$json" | jq -r '.server_vip')"
        peerip=$(echo "$json" | jq -r '.peer_ip')

        mkdir -p /run/systemd/network/
        touch /run/systemd/network/60-${cfg.interface}.{netdev,network}
        chown root:systemd-network /run/systemd/network/60-${cfg.interface}.{netdev,network}
        chmod 640 /run/systemd/network/60-${cfg.interface}.{netdev,network}

        interface="${cfg.interface}"

        cat > /run/systemd/network/60-${cfg.interface}.netdev <<EOF
        ${cfg.netdevConfig}
        EOF

        cat > /run/systemd/network/60-${cfg.interface}.network <<EOF
        ${cfg.networkConfig}
        EOF

        echo Bringing up network interface ${cfg.interface}.

        ${cfg.preUp}

        networkctl reload
        networkctl up ${cfg.interface}

        ${pkgs.systemd}/lib/systemd/systemd-networkd-wait-online -i ${cfg.interface}

        ${cfg.postUp}
      '';

      preStop = ''
        echo Removing network interface ${cfg.interface}.

        interface="${cfg.interface}"

        ${cfg.preDown}

        rm /run/systemd/network/60-${cfg.interface}.{netdev,network} || true

        echo Bringing down network interface ${cfg.interface}.
        networkctl down ${cfg.interface}
        networkctl delete ${cfg.interface}
        networkctl reload

        ${cfg.postDown}
      '';
    };

    systemd.services.pia-vpn-portforward = mkIf cfg.portForward.enable {
      description = "Configure port-forwarding for PIA connection ${cfg.interface}";
      path = with pkgs; [
        curl
        jq
      ];
      after = [ "pia-vpn.service" ];
      bindsTo = [ "pia-vpn.service" ];
      wantedBy = [ "pia-vpn.service" ];

      unitConfig = {
        ConditionFileNotEmpty = [
          cfg.certificateFile
          cfg.environmentFile
        ];
      };

      serviceConfig = {
        Type = "notify";
        Restart = "always";
        CacheDirectory = "pia-vpn";
        StateDirectory = "pia-vpn";
        RestartSec = "10s";
        RestartSteps = "10";
        RestartMaxDelaySec = "15min";
        EnvironmentFile = cfg.environmentFile;
      };

      script = ''
        if [ ! -f $STATE_DIRECTORY/region.json ]; then
          echo "Region information not found; is pia-vpn.service running?" >&2
          exit 1
        fi
        region="$(cat $STATE_DIRECTORY/region.json)"

        if [ ! -f $STATE_DIRECTORY/wireguard.json ]; then
          echo "Connection information not found; is pia-vpn.service running?" >&2
          exit 1
        fi
        wg="$(cat $STATE_DIRECTORY/wireguard.json)"

        meta_ip="$(echo $region | jq -r '.servers.meta[0].ip')"
        meta_hostname="$(echo $region | jq -r '.servers.meta[0].cn')"
        wg_ip="$(echo $region | jq -r '.servers.wg[0].ip')"
        wg_hostname="$(echo $region | jq -r '.servers.wg[0].cn')"
        gateway="$(echo $wg | jq -r '.server_vip')"

        echo Fetching token from $meta_ip...
        tokenResponse="$(curl --no-progress-meter -m 5 \
          -u "$PIA_USER:$PIA_PASS" \
          --connect-to "$meta_hostname::$meta_ip" \
          --cacert "${cfg.certificateFile}" \
          "https://$meta_hostname/authv3/generateToken" || true)"
        if [ "$(echo "$tokenResponse" | jq -r '.status' || true)" != "OK" ]; then
          >&2 echo "Failed to generate token. Stopping."
          exit 1
        fi
        token="$(echo "$tokenResponse" | jq -r '.token')"

        echo "Fetching port forwarding configuration from $gateway..."
        pfconfig="$(curl --no-progress-meter -m 5 \
          --interface ${cfg.interface} \
          --connect-to "$wg_hostname::$gateway:" \
          --cacert "${cfg.certificateFile}" \
          -G --data-urlencode "token=''${token}" \
          "https://''${wg_hostname}:19999/getSignature" || true)"
        if [ "$(echo "$pfconfig" | jq -r '.status' || true)" != "OK" ]; then
          echo "Port forwarding configuration does not contain an OK status. Stopping." >&2
          exit 1
        fi

        if [ -z "$pfconfig" ]; then
          echo "Did not obtain port forwarding configuration. Stopping." >&2
          exit 1
        fi

        signature="$(echo "$pfconfig" | jq -r '.signature')"
        payload="$(echo "$pfconfig" | jq -r '.payload')"
        port="$(echo "$payload" | base64 -d | jq -r '.port')"
        expires="$(echo "$payload" | base64 -d | jq -r '.expires_at')"

        echo "Port forwarding configuration acquired: port $port expires at $(date --date "$expires")."

        systemd-notify --ready

        echo "Enabling port forwarding..."

        while true; do
          response="$(curl --no-progress-meter -m 5 -G \
            --interface ${cfg.interface} \
            --connect-to "$wg_hostname::$gateway:" \
            --cacert "${cfg.certificateFile}" \
            --data-urlencode "payload=''${payload}" \
            --data-urlencode "signature=''${signature}" \
            "https://''${wg_hostname}:19999/bindPort" || true)"
          if [ "$(echo "$response" | jq -r '.status' || true)" != "OK" ]; then
            echo "Failed to bind port. Stopping." >&2
            exit 1
          fi
          echo "Bound port $port. Forwarding will expire at $(date --date="$expires")."
          ${cfg.portForward.script}
          sleep 900
          echo "Checking port forwarding..."
        done
      '';
    };

  };
}
