{ options, config, pkgs, lib, ... }:

with lib;
with lib.ultragear;
let cfg = config.ultragear.services.gitlab;
in
{
  options.ultragear.services.gitlab = with types; {
    enable = mkBoolOpt false "Whether or not to enable gitlab service";
    domainName = mkOpt str CONSTS.urls.gitlab "The domain name to expose service as";
    serverPort = mkOpt int CONSTS.ports.services.gitlab.server "The server port number to run as";
    gitPort = mkOpt int CONSTS.ports.services.gitlab.git "The git/ssh port number to run as";
    dataDir = mkOpt str "" "The location to store data";
  };

  config = mkIf cfg.enable {

    assertions = [
      {
        assertion = cfg.dataDir != "";
        message = "Gitlab dataDir value must be set before enabling Gitlab";
      }
    ];

    systemd.tmpfiles.rules = [
      "d ${cfg.dataDir}/config 0750 root root -"
      "d ${cfg.dataDir}/data 0750 root root -"
      "d ${cfg.dataDir}/logs 0750 root root -"
    ];

    networking.firewall.allowedTCPPorts = [ cfg.gitPort ];

    virtualisation.oci-containers.containers.gitlab = {
      image = "gitlab/gitlab-ee:latest";
      hostname = cfg.domainName;
      ports = [ "${builtins.toString cfg.serverPort}:80" "${builtins.toString cfg.gitPort}:22" ];
      volumes = [
        "${cfg.dataDir}/config:/etc/gitlab"
        "${cfg.dataDir}/data:/var/opt/gitlab"
        "${cfg.dataDir}/logs:/var/log/gitlab"
      ];
    };

    # Proxy through Traefik
    services.traefik.dynamicConfigOptions.http = {
      routers.gitlab = {
        entryPoints = [ "https" ];
        rule = "Host(`${cfg.domainName}`)";
        service = "gitlab";
        middlewares = "local@file";
      };

      services.gitlab.loadBalancer = {
        passHostHeader = true;
        servers = [{ url = "http://127.0.0.1:${toString cfg.serverPort}"; }];
      };
    };

  };
}
