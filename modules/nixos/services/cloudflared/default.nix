{ options, config, pkgs, lib, ... }:

with lib;
with lib.ultragear;
let
  cfg = config.ultragear.services.cloudflared;

  # A very complex (complete?) cloudflared setup:
  # https://github.com/johnae/world/blob/ec2fe02326da14391dabe12fc627ab1f5e846f30/modules/my-cloudflared.nix#L8

  # A guy that is using multiple tunnels for various services:
  # https://github.com/albinvass/infra/blob/cc7a5de91e5cf119aa5fb846a0283bf8e32192ef/nixos/hosts/devbox/cloudflared/default.nix#L22

in
{
  options.ultragear.services.cloudflared = with types; {
    enable = mkBoolOpt false "Whether or not to enable a cloudflare tunnel deamon.";
  };

  config = mkIf cfg.enable {

    environment.systemPackages = [
      pkgs.cloudflared
    ];

    sops.secrets = {
      "cloudflared/creds.json" = {
        owner = "cloudflared";
        group = "cloudflared";
        restartUnits = [ "cloudflared-tunnel-${config.networking.hostName}.service" ];
      };
      "cloudflared/cert.pem" = {
        owner = "cloudflared";
        group = "cloudflared";
        path = "/etc/cloudflared/cert.pem";
        restartUnits = [ "cloudflared-tunnel-${config.networking.hostName}.service" ];
      };
    };

    services.cloudflared = {
      enable = true;
      package = pkgs.cloudflared;

      tunnels = {
        "dread" = {
          credentialsFile = config.sops.secrets."cloudflared/creds.json".path;
          default = "http_status:404";
          ingress = {
            "paperless.hypervasis.com" = {
              service = "http://localhost:8000";
              # originRequest = {
              #   noTLSVerify = true;
              #   originServerName = "paperless.hypervasis.com";
              # };
            };
          };
        };
      };
    };


    # users.users.cloudflared = {
    #   group = "cloudflared";
    #   isSystemUser = true;
    # };
    # users.groups.cloudflared = { };

    # systemd.services.cloudflared = {
    #   wantedBy = [ "multi-user.target" ];
    #   after = [ "network.target" ];
    #   serviceConfig = {
    #     ExecStart = "${pkgs.callPackage ./default.nix {}}/bin/cloudflared tunnel --no-autoupdate run --token=${config.cloudflared.token}";
    #     Restart = "always";
    #     User = "cloudflared";
    #     Group = "cloudflared";
    #   };
    # };
  };

}
