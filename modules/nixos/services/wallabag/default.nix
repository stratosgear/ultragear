{ options, config, pkgs, lib, ... }:

with lib;
with lib.ultragear;
let cfg = config.ultragear.services.wallabag;
in
{
  options.ultragear.services.wallabag = with types; {
    enable = mkBoolOpt false "Whether or not to enable the walabag service";
    port = mkOpt int CONSTS.ports.services.wallabag "The port number to run as";
    domainName = mkOpt str "" "The domain name to expose service as";
    dataDir = mkOpt str "" "The location to store data";

  };

  config = mkIf cfg.enable {

    assertions = [
      {
        assertion = cfg.domainName != "";
        message = "If you choose to activate Wallabag, then you also need to define a domainName to expose it through";
      }
      {
        assertion = cfg.dataDir != "";
        message = "Wallabag dataDir value must be set before enabling Wallabag";
      }
    ];

    services.postgresql = {
      ensureUsers = [
        {
          name = "wallabag";
          ensureDBOwnership = true;
        }
      ];
      ensureDatabases = [ "wallabag" ];
    };

    sops.secrets."wallabag/env" = { };

    # Make sure that the given dataDir, is owned by postgres:postgres
    systemd.tmpfiles.rules = [
      "d ${cfg.dataDir}/images 0750 root root -"
      "d ${cfg.dataDir}/data 0750 root root -"
    ];
    virtualisation.oci-containers.containers."wallabag" = {
      image = "wallabag/wallabag:2.6.8";
      environment = {
        "SYMFONY__ENV__DOMAIN_NAME" = "https://${cfg.domainName}";
        "SYMFONY__ENV__DATABASE_DRIVER" = "pdo_pgsql";
        "SYMFONY__ENV__DATABASE_HOST" = "10.88.0.1";
        "SYMFONY__ENV__DATABASE_PORT" = "5432";
        "SYMFONY__ENV__DATABASE_NAME" = "wallabag";
        "SYMFONY__ENV__DATABASE_USER" = "wallabag";
        "SYMFONY__ENV__FOSUSER_REGISTRATION" = "False";
        "SYMFONY__ENV__REDIS_HOST" = "10.88.0.1";
        "SYMFONY__ENV__REDIS_PORT" = "${builtins.toString CONSTS.ports.redis.wallabag}";
        "POSTGRES_USER" = "wallabag";
        "POPULATE_DATABASE" = "False";
      };
      environmentFiles = [
        config.sops.secrets."wallabag/env".path
      ];
      ports = [ "${builtins.toString cfg.port}:80" ];
      volumes = [
        "${cfg.dataDir}/data:/var/www/wallabag/data"
        "${cfg.dataDir}/images:/var/www/wallabag/web/assets/images"
      ];
    };

    services.redis.servers.wallabag = {
      enable = true;
      port = CONSTS.ports.redis.wallabag;
      databases = 1;
      openFirewall = true;
      bind = null;
    };

    # Proxy through Traefik
    services.traefik.dynamicConfigOptions.http = {
      routers.wallabag = {
        entryPoints = [ "https" ];
        rule = "Host(`${cfg.domainName}`)";
        service = "wallabag";
        middlewares = "local@file";
      };

      services.wallabag.loadBalancer = {
        passHostHeader = true;
        servers = [{ url = "http://127.0.0.1:${toString cfg.port}"; }];
      };
    };

    # Stanza for system restic backups
    ultragear.services.backup.instances.system = {
      sourceDirectories = [ cfg.dataDir ];
      databases = [ "wallabag" ];
    };

  };
}
