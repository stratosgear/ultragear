{ options, config, pkgs, lib, ... }:

with lib;
with lib.ultragear;
let cfg = config.ultragear.services.homarr;
in
{
  options.ultragear.services.homarr = with types; {
    enable = mkBoolOpt false "Whether or not to enable homarr dashboard service";
    port = mkOpt int CONSTS.ports.services.homarr "The port number to run as";
    domainName = mkOpt str CONSTS.urls.homarr "The domain name to expose service as";
    dataDir = mkOpt str "" "The location to store data";
  };

  config = mkIf cfg.enable {

    assertions = [
      {
        assertion = cfg.dataDir != "";
        message = "If Homarr is active you also have to provide a dataDir too";
      }
    ];

    systemd.tmpfiles.rules = [
      "d ${cfg.dataDir}/config 0750 root root -"
      "d ${cfg.dataDir}/icons 0750 root root -"
      "d ${cfg.dataDir}/data 0750 root root -"
    ];

    virtualisation.oci-containers = {
      containers = {
        homarr = {
          image = "ghcr.io/ajnart/homarr:latest";
          autoStart = true;
          volumes = [
            "${cfg.dataDir}/config:/app/data/configs"
            "${cfg.dataDir}/icons:/app/public/icons"
            "${cfg.dataDir}/data:/data"
            "/var/run/podman/podman.sock:/var/run/docker.sock:ro"
          ];
          environment = {
            TZ = config.ultragear.system.time.tz;
          };
          # environmentFiles = [
          #   config.age.secrets.paperless.path
          # ];
          ports = [ "${builtins.toString cfg.port}:7575" ];
        };
      };
    };

    # Proxy through Traefik
    services.traefik.dynamicConfigOptions.http = {
      routers.homarr = {
        entryPoints = [ "https" ];
        rule = "Host(`${cfg.domainName}`)";
        service = "homarr";
        middlewares = "local@file";
      };

      services.homarr.loadBalancer = {
        passHostHeader = true;
        servers = [{ url = "http://127.0.0.1:${toString cfg.port}"; }];
      };
    };

    # Stanza for system restic backups
    ultragear.services.backup.instances.system = {
      sourceDirectories = [ cfg.dataDir ];
    };

  };
}
