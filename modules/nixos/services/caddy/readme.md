# Networking services in home lab

The current configuration requires some cooperation from a local DNS server.

In the current installation we are utilizing an existing PI4 that runs PiHole.

We have setup there at Local Dns / DNS Records an entry for:

dread.home.local pointing to it's IP (192.168.3.81)

and another entry at Local Dns / CNAME Records aother entry for:

paperless.home.local pointing to dread.home.local

Instead of paperless that could be any other [ServiceName]


## SSL for internal services

- https://helgeklein.com/blog/automatic-https-certificates-for-services-on-internal-home-network-without-opening-firewall-port/