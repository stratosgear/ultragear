{ options, config, pkgs, lib, ... }:

with lib;
with lib.ultragear;
let
  cfg = config.ultragear.services.caddy;
in
{
  options.ultragear.services.caddy = with types; {
    enable = mkBoolOpt false "Whether or not to configure caddy proxying.";
    tlsPolicies = mkOpt (listOf attrs) [ ] "Caddy JSON TLS policies";
    routes = mkOpt (listOf attrs) [ ] "Caddy JSON routes for http servers";
    blocks = mkOpt (listOf attrs) [ ] "Caddy JSON error blocks for http servers";
    cidrAllowlist = mkOpt (listOf str) [ ] "CIDR blocks to allow for requests";
  };

  config = mkIf cfg.enable {

    ultragear.services.caddy.cidrAllowlist = [ "127.0.0.1/32" ];
    ultragear.services.caddy.routes = [{
      match = [{ not = [{ remote_ip.ranges = config.ultragear.services.caddy.cidrAllowlist; }]; }];
      handle = [{
        handler = "static_response";
        status_code = "403";
      }];
    }];

    services = {
      # From: https://tailscale.com/kb/1190/caddy-certificates
      # and: https://github.com/nmasur/dotfiles/blob/339691879362ae59eed3257d814783cbe7654e59/modules/nixos/services/caddy.nix
      caddy = {
        enable = true;

        adapter = "''"; # Required to enable JSON
        configFile = pkgs.writeText "Caddyfile" (builtins.toJSON {
          apps.http.servers.main = {
            listen = [ ":443" ];
            routes = config.ultragear.services.caddy.routes;
            errors.routes = config.ultragear.services.caddy.blocks;
            logs = { }; # Uncomment to collect access logs
          };
          apps.http.servers.metrics = { }; # Enables Prometheus metrics
          apps.tls.automation.policies = config.ultragear.services.caddy.tlsPolicies;
          logging.logs.main = {
            encoder = { format = "console"; };
            writer = {
              output = "file";
              filename = "${config.services.caddy.logDir}/caddy.log";
              roll = true;
              roll_size_mb = 1;
            };
            level = "INFO";
          };
        });

        # Add individual virtual hosts as:
        #   virtualHosts = {
        #     "https://crusty.tiger-shark.ts.net" = {
        #       extraConfig = ''
        #         reverse_proxy :${toString config.services.qbittorrent.port}
        #       '';
        #     };
        #   };
      };
    };

    networking.firewall = {
      allowedTCPPorts = [ 80 443 ];
      allowedUDPPorts = [ 443 ];
    };
  };
}
