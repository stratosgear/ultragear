{
  options,
  config,
  pkgs,
  lib,
  ...
}:

with lib;
with lib.ultragear;
let
  cfg = config.ultragear.services.cryptpad;
in
{
  options.ultragear.services.cryptpad = with types; {
    enable = mkBoolOpt false "Whether or not to enable the CryptPad collab suitee";
    port = mkOpt int CONSTS.ports.services.cryptpad "The port number to run as";
    domainName = mkOpt str CONSTS.urls.cryptpad "The domain name to expose service as";
    dataDir = mkOpt str "/data/cryptpad" "The location to store data";
  };

  config = mkIf cfg.enable {

    # sops.secrets = {
    #   "cryptpad.AdminKey" = { };
    # };

    # systemd.tmpfiles.rules = [
    #   "d ${cfg.dataDir} 0750 ${config.services.cryptpad.user} ${config.services.cryptpad.group} -"
    # ];


    # If we are to be using the default nxinx configuration below, it seems that we have
    # to configure and agree on ACME terms
    # security.acme = {
    #   defaults.email = "stratos@gerakakis.net";
    #   acceptTerms = true;
    # };

    services.cryptpad = {
      enable = true;
      configureNginx = false;
      settings = {
        httpUnsafeOrigin = "https://${CONSTS.urls.cryptpad}";
        httpSafeOrigin = "https://${CONSTS.urls.cryptpad}";
        httpAddress = "127.0.0.1";
        httpPort = cfg.port;
        # adminKeys =
        # ["[tom@127.0.0.1:4020/f5bdoXYd9Jlw0pao6HRYE7jMcLl0Ky3+tvI-OG4kBZI=]"];
        # fileStorage = "${dataDir}/datastorage";
        # archivePath = "${dataDir}/data/archive";
        # pinPath = "${dataDir}/data/pins";
        # taskPath = "${dataDir}/data/tasks";
        # blockPath = "${dataDir}/block";
        # blobPath = "${dataDir}/blob";
        # blobStagingPath = "${dataDir}/data/blobstage";
        # decreePath = "${dataDir}/data/decrees";
        # logPath = "${dataDir}/data/logs";
      };
    };

    # Proxy through Traefik
    services.traefik.dynamicConfigOptions.http = {
      routers.cryptpad = {
        entryPoints = [ "https" ];
        rule = "Host(`${cfg.domainName}`)";
        service = "cryptpad";
        middlewares = "local@file";
      };
      services.cryptpad.loadBalancer = {
        passHostHeader = true;
        servers = [ { url = "http://127.0.0.1:${toString cfg.port}"; } ];
      };
    };

    # Stanza for system restic backups
    ultragear.services.backup.instances.system = {
      sourceDirectories = [
        # Backing folders as defined in:
        # https://docs.cryptpad.org/en/admin_guide/maintenance.html#backup-and-migration
        "/var/lib/private/cryptpad/data"
        "/var/lib/private/cryptpad/datastore"
        "/var/lib/private/cryptpad/block"
        "/var/lib/private/cryptpad/blob"
        "/var/lib/private/cryptpad/customize"
        "/var/lib/private/cryptpad/customize.dist"
      ];
    };

  };
}
