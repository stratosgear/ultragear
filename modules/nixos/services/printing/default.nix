{ options, config, pkgs, lib, ... }:

with lib;
with lib.ultragear;
let cfg = config.ultragear.services.printing;
in
{
  options.ultragear.services.printing = with types; {
    enable = mkBoolOpt false "Whether or not to configure printing support.";
  };

  config = mkIf cfg.enable {

    services.printing = {
      enable = true;
      browsing = true;
      browsedConf = ''
        BrowseDNSSDSubTypes _cups,_print
        BrowseLocalProtocols all
        BrowseRemoteProtocols all
        CreateIPPPrinterQueues All

        BrowseProtocols all
      '';
      drivers = with pkgs; [
        epson-escpr2
        brlaser
        gutenprint
        gutenprintBin
      ];

      cups-pdf = {
        enable = true;
        instances.cups-pdf = {
          installPrinter = true;
          settings.Out = "/var/lib/cups-pdf";
        };
      };
    };

    systemd = {

      services = {
        cups.serviceConfig = {
          Restart = "on-failure";
          RestartSec = "5";
        };
        cups-browsed.serviceConfig = {
          Restart = "on-failure";
          RestartSec = "5";
        };
      };

      tmpfiles.rules = [
        "d /var/lib/cups-pdf 755 root root"
      ];
    };
  };
}
