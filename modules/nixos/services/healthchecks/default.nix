{ options, config, pkgs, lib, ... }:

with lib;
with lib.ultragear;
let cfg = config.ultragear.services.healthchecks;
in
{
  options.ultragear.services.healthchecks = with types; {
    enable = mkBoolOpt false "Whether or not to configure Healthchecks.io functionality.";
    user = mkOpt str "healthchecks" "User to run the service as";
    group = mkOpt str "healthchecks" "Group to run the service as";
    dataDir = mkOpt str "" "Location where Healthchecks uses to store data";
    port = mkOpt int CONSTS.ports.services.healthchecks "The port number to run as";
    domainName = mkOpt str CONSTS.urls.healthchecks "The domain name to expose service as";
  };

  config = mkIf cfg.enable {

    systemd.tmpfiles.rules = [
      "d ${cfg.dataDir} 0750 ${cfg.user} ${cfg.group} -"
      "Z ${cfg.dataDir} 0750 ${cfg.user} ${cfg.group} -"
    ];

    sops.secrets = {
      "healthchecks/secret_key" = {
        owner = cfg.user;
        group = cfg.group;
        restartUnits = [ "healthchecks.service" ];
      };
      "healthchecks/env" = {
        owner = cfg.user;
        group = cfg.group;
        restartUnits = [ "healthchecks.service" ];
      };
    };

    services.healthchecks = {
      enable = true;
      # From:
      # https://github.com/EricTheMagician/infrastructure/blob/b580e020527e3aca3c0bdb601d1200d0693204f9/modules/healthchecks.nix#L48
      package = pkgs.healthchecks.overrideAttrs (final: prev:
        let
          localSettings = pkgs.writeText "local_settings.py" ''
            import os
            STATIC_ROOT = os.getenv("STATIC_ROOT")

            with open("${config.sops.secrets."healthchecks/env".path}", "r") as file:
                for line in file.readlines():
                    try:
                        key, value = line.split("=")
                    except:
                        # just in case there are any isssues with parsing the file.
                        # might happen if the file contains an extra line at the
                        # end of the file.
                        continue
                    key = key.strip()
                    value = value.strip()
                    try:
                        value = int(value)
                    except:
                        pass
                    if value == "True":
                        value = True
                    elif value == "False":
                        value = False
                    globals()[key] = value
          '';
        in
        {
          installPhase = ''
            mkdir -p $out/opt/healthchecks
            cp -r . $out/opt/healthchecks
            chmod +x $out/opt/healthchecks/manage.py
            cp ${localSettings} $out/opt/healthchecks/hc/local_settings.py
          '';
        });
      port = cfg.port;
      dataDir = cfg.dataDir;
      settings = {
        SITE_ROOT = "https://${cfg.domainName}";
        REGISTRATION_OPEN = false;
        SECRET_KEY_FILE = config.sops.secrets."healthchecks/secret_key".path;
        DEBUG = false;
      };
    };

    services.postgresql = {
      ensureUsers = [
        {
          name = "healthchecks";
          ensureDBOwnership = true;
        }
      ];
      ensureDatabases = [ "healthchecks" ];
    };

    # Proxy through Traefik
    services.traefik.dynamicConfigOptions.http = {
      routers.healthchecks = {
        entryPoints = [ "https" ];
        rule = "Host(`${cfg.domainName}`)";
        service = "healthchecks";
        middlewares = "local@file";
      };
      services.healthchecks.loadBalancer = {
        passHostHeader = true;
        servers = [{ url = "http://127.0.0.1:${toString cfg.port}"; }];
      };
    };

    # Stanza for system restic backups
    ultragear.services.backup.instances.system = {
      sourceDirectories = [ cfg.dataDir ];
      databases = [ "healthchecks" ];
    };

  };
}
