{ options, config, pkgs, lib, ... }:

with lib;
with lib.ultragear;
let cfg = config.ultragear.services.mympd;
in
{
  options.ultragear.services.mympd = with types; {
    enable = mkBoolOpt false "Whether or not to enable mympd service.";
  };

  # Setup according to the mympd satellite configuration:
  # https://jcorporation.github.io/myMPD/additional-topics/mpd-satellite-setup
  config = mkIf cfg.enable {

    services.mympd = {
      enable = true;
      extraGroups = [ "musiclib" ];
      settings = {
        http_port = CONSTS.ports.services.mympd;
      };
    };


    # Proxy through Traefik
    services.traefik.dynamicConfigOptions.http = {
      routers.music = {
        entryPoints = [ "https" ];
        rule = "Host(`music.${config.networking.hostName}.${CONSTS.urls.privRootDomain}`)";
        service = "music";
        middlewares = "local@file";
      };
      services.music.loadBalancer = {
        passHostHeader = true;
        servers = [{ url = "http://127.0.0.1:${toString CONSTS.ports.services.mympd}"; }];
      };
    };

    # Stanza for system restic backups
    ultragear.services.backup.instances.system = {
      sourceDirectories = [ "/var/lib/private/mympd" ];
      excludePatterns = [ "/var/lib/private/mympd/covercache" ];
    };
  };
}
