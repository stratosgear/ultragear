{ options, config, pkgs, lib, ... }:

with lib;
with lib.ultragear;
let cfg = config.ultragear.services.pgadmin;
in
{
  options.ultragear.services.pgadmin = with types; {
    enable = mkBoolOpt false "Whether or not to activate pgadmin service";
    domainName = mkOpt str "pgadmin.${config.networking.hostName}.${CONSTS.urls.privRootDomain}" "Domain name to expose pgAdmin";
  };

  config = mkIf cfg.enable {

    assertions = [
      {
        assertion = cfg.enable -> cfg.domainName != "";
        message = "If you choose to activate pgAdmin, then you also need to define a domainName to expose it through";
      }
    ];

    sops.secrets = {
      "pgadmin/password" = {
        mode = "0440";
        owner = "pgadmin";
        group = "pgadmin";
        restartUnits = [ "pgadmin.service" ];
      };
    };

    services.pgadmin = {
      enable = true;
      initialEmail = "stratos@gerakakis.net";
      initialPasswordFile = config.sops.secrets."pgadmin/password".path;
      settings = {
        SERVER_MODE = true;
      };
    };

    services.postgresql = {
      ensureDatabases = [ "pgadmin" ];
      ensureUsers = [{
        name = "pgadmin";
        ensureDBOwnership = true;
        # used to manager other databases
        ensureClauses.superuser = true;
      }];
    };


    # Proxy through Traefik
    services.traefik.dynamicConfigOptions.http = {
      routers.pgadmin = {
        entryPoints = [ "https" "http" ];
        rule = "Host(`${ cfg.domainName}`)";
        service = "pgadmin";
        # tls.domains = [{ main = "*.mimas.internal.nobbz.dev"; }];
        tls.certResolver = "letsencrypt";
      };

      services.pgadmin.loadBalancer = {
        passHostHeader = true;
        servers = [{ url = "http://127.0.0.1:${toString config.services.pgadmin.port}"; }];
      };
    };
  };
}
