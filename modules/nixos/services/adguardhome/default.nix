{ options, config, pkgs, lib, ... }:

with lib;
with lib.ultragear;
let cfg = config.ultragear.services.adguardhome;
in
{
  options.ultragear.services.adguardhome = with types; {
    enable = mkBoolOpt false "Whether or not to enable Adguard Home service";
    port = mkOpt int CONSTS.ports.services.adguardHome "Web interface port";
    domainName = mkOpt str CONSTS.urls.adguardHome "The domain name to expose service as";
  };

  config = mkIf cfg.enable {

    services.adguardhome = {
      enable = true;
      mutableSettings = true;
      settings = null;
      # This needs a manual edting of the config file to specify the port that
      # AdGuardHome server is listing on
    };

    # Proxy through Traefik
    services.traefik.dynamicConfigOptions.http = {
      routers.adguardhome = {
        entryPoints = [ "https" ];
        rule = "Host(`${cfg.domainName}`)";
        service = "adguardhome";
        middlewares = "local@file";
      };

      services.adguardhome.loadBalancer = {
        passHostHeader = true;
        servers = [{ url = "http://127.0.0.1:${toString cfg.port}"; }];
      };
    };


    networking.firewall = {
      allowedTCPPorts = [ 53 ];
      allowedUDPPorts = [ 53 ];
    };


    # Stanza for system restic backups
    ultragear.services.backup.instances.system = {
      sourceDirectories = [ "/var/lib/private/AdGuardHome" ];
    };
  };
}
