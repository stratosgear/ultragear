{ options, config, pkgs, lib, ... }:

with lib;
with lib.ultragear;
let cfg = config.ultragear.services.weblate;
in
{
  options.ultragear.services.weblate = with types; {
    enable = mkBoolOpt false "Whether or not to enable weblate service.";
    port = mkOpt int CONSTS.ports.services.weblate "The port number to run as";
    domainName = mkOpt str CONSTS.urls.weblate "The domain name to expose service as";
    dataDir = mkOpt str "" "The location to store data";
  };

  config = mkIf cfg.enable {

    assertions = [
      {
        assertion = cfg.dataDir != "";
        message = "Weblate dataDir value must be set before enabling Weblate";
      }
    ];

    services.postgresql = {
      ensureUsers = [
        {
          name = "weblate";
          ensureDBOwnership = true;
        }
      ];
      ensureDatabases = [ "weblate" ];
    };

    sops.secrets = {
      "weblate/env" = { };
      "weblate/redis-pass" = { };
    };

    # Make sure that the given dataDir, is owned by user 1000:1000
    # https://docs.weblate.org/en/latest/admin/install/docker.html#docker-container-volumes
    systemd.tmpfiles.rules = [
      "d ${cfg.dataDir}/data 0750 stratos root -"
    ];

    virtualisation.oci-containers.containers.weblate = {
      image = "weblate/weblate:5.0.1.1";
      environment = {
        "POSTGRES_USER" = "weblate";
        "POSTGRES_DATABASE" = "weblate";
        "POSTGRES_HOST" = "10.88.0.1";
        "POSTGRES_PORT" = "5432";
        "WEBLATE_DEBUG" = "1";
        "WEBLATE_LOGLEVEL" = "DEBUG";
        "WEBLATE_SITE_TITLE" = "Hypervasis Weblate";
        "WEBLATE_ADMIN_NAME" = "Weblate Admin";
        "WEBLATE_ADMIN_EMAIL" = "stratos@gerakakis.net";
        "WEBLATE_SERVER_EMAIL" = "stratos@gerakakis.net";
        "WEBLATE_DEFAULT_FROM_EMAIL" = "stratos@gerakakis.net";
        "WEBLATE_ALLOWED_HOSTS" = "*";
        "WEBLATE_REGISTRATION_OPEN" = "0";
        "WEBLATE_SITE_DOMAIN" = cfg.domainName;
        "REDIS_HOST" = "10.88.0.1";
        "REDIS_DB" = "0";
        "REDIS_PORT" = "${builtins.toString CONSTS.ports.redis.weblate}";
        "WEBLATE_GITLAB_USERNAME" = "stratosgear";
        "WEBLATE_GITLAB_HOST" = "gitlab.com";
        "WEBLATE_EMAIL_HOST" = "in-v3.mailjet.com";
        "WEBLATE_EMAIL_PORT" = "587";
        "WEBLATE_EMAIL_USE_TLS" = "1";
        "WEBLATE_EMAIL_USE_SSL" = "0";
      };
      environmentFiles = [
        config.sops.secrets."weblate/env".path
      ];
      ports = [ "${builtins.toString cfg.port}:8080" ];
      volumes = [
        "${cfg.dataDir}/data:/app/data"
      ];
    };

    services.redis.servers.weblate = {
      enable = true;
      port = CONSTS.ports.redis.weblate;
      databases = 1;
      openFirewall = true;
      bind = null;
      requirePassFile = config.sops.secrets."weblate/redis-pass".path;
    };

    # Proxy through Traefik
    services.traefik.dynamicConfigOptions.http = {
      routers.weblate = {
        entryPoints = [ "https" ];
        rule = "Host(`${cfg.domainName}`)";
        service = "weblate";
        middlewares = "local@file";
      };

      services.weblate.loadBalancer = {
        passHostHeader = true;
        servers = [{ url = "http://127.0.0.1:${toString cfg.port}"; }];
      };
    };

    # Stanza for system restic backups
    ultragear.services.backup.instances.system = {
      sourceDirectories = [ cfg.dataDir ];
      databases = [ "weblate" ];
    };

  };
}
