{ options, config, pkgs, lib, ... }:

with lib;
with lib.ultragear;
let cfg = config.ultragear.services.postgres;
in
{
  options.ultragear.services.postgres = with types; {
    enable = mkBoolOpt false "Whether or not to enable postgres";
    user = mkOpt str "pgAdmin" "Admin name for postgres";

    # TODO: manually setting the postgres dataDir, requires an administrator to also
    # MANUALLY set access rights to that folder: sudo chown +R postgres:postgres dataDir
    # datadir also needs:
    #    +x to all folders leading up to the datadir
    #    chmod 700 on the folder itself
    # This is a little like chicken and egg issue here, because you cannot assign
    # permissions unless the service (and the associated postgres user) are created. So
    # first start the service, wait until it fails, assign the permissions and restart
    dataDir = mkOpt str "" "Data directory for postgres";
    # TODO: Maybe make the psql backups optional?
    backupDir = mkOpt str "/backups/postgres" "Data directory to store postgresql backup dumps";
    backupTimer = mkOpt str "*-*-* 00/4:00:00" "When to perform backups. This is a systemd.time format string";
    package = mkOpt package pkgs.postgresql "The postgresql package to install";
  };

  config = mkIf cfg.enable {

    assertions = [
      {
        assertion = cfg.dataDir != "";
        message = "Postgres dataDir value must be set before enabling Postgresql";
      }
    ];

    # Make sure that the given dataDir, is owned by postgres:postgres
    systemd.tmpfiles.rules = [
      "d ${cfg.dataDir} 0750 postgres postgres -"
    ];

    services = {
      postgresql = {
        enable = true;
        package = cfg.package;
        dataDir = cfg.dataDir;
        enableTCPIP = true;
        authentication = mkForce ''
          # Generated file; do not edit!
          # TYPE  DATABASE        USER            ADDRESS                 METHOD
          local   all             all                                     trust
          host    all             all             127.0.0.1/32            trust
          host    all             all             ::1/128                 trust
          # podman access for containers
          host    all             all             10.88.0.0/16            scram-sha-256
        '';

        initialScript = pkgs.writeText "backend-initScript" ''
          CREATE ROLE ${cfg.user} WITH LOGIN PASSWORD 'postgres' CREATEDB;
          CREATE DATABASE ${cfg.user};
          GRANT ALL PRIVILEGES ON DATABASE ${cfg.user} TO ${cfg.user};
        '';
      };

    };

    # Allows OCI containers to connect to db
    networking.firewall.interfaces."podman0".allowedTCPPorts = [ 5432 ];

  };
}
