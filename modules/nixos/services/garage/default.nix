{ options, config, pkgs, lib, ... }:

with lib;
with lib.ultragear;
let
  cfg = config.ultragear.services.garage;
  staticGarageToml = ./garage.toml;
  # defaultSopsFile = sops_file_that_contains_garage_secrets
in
{
  options.ultragear.services.garage = with types; {
    enable = mkBoolOpt false "Whether or not to enable garage service.";
    logLevel = mkOpt str "info" "Log level for the garage service";
    dataDir = mkOpt str "/data/garage" "The location to store data";
  };

  config = mkIf cfg.enable
    {
      networking.firewall.allowedTCPPorts = [ 3900 3901 3903 ];

      sops.secrets = {
        "garage/rpc_secret" = { };
        "garage/admin_token" = { };
        "garage/admin_metrics_token" = { };
      };

      # As in: https://github.com/JJ-Atkinson/homelab-config/blob/ecf1ca61ce1b547acdae5ce3951a41de7e76d96c/systems/garage.nix#L17
      sops.templates.garage_toml = {
        content = ''
          # Begin Secrets
          rpc_secret = "${config.sops.placeholder."garage/rpc_secret"}"
          admin.admin_token = "${config.sops.placeholder."garage/admin_token"}"
          admin.metrics_token = "${config.sops.placeholder."garage/admin_metrics_token"}"
          # End Secrets

          # Begin dynamic config
          data_dir = "${cfg.dataDir}/data"
          metadata_dir = "${cfg.dataDir}/meta"
          # End dynamic config

          ${builtins.readFile staticGarageToml}
        '';
        owner = "garage";
        group = "garage";
      };

      environment = {
        systemPackages = [
          pkgs.garage
          pkgs.minio-client
        ];
        etc."garage.toml" = {
          source = lib.mkForce config.sops.templates.garage_toml.path;
        };
      };

      systemd =
        let
          sc = config.systemd.services.garage.serviceConfig;
        in
        {
          tmpfiles.rules = [
            # "d ${cfg.dataDir} garage garage"
            "d '${cfg.dataDir}/data' 0700 garage garage -"
            "d '${cfg.dataDir}/meta' 0700 garage garage -"
            # "d ${cfg.dataDir} ${sc.User} ${sc.Group}"
            # "d '${cfg.dataDir}/data' 0700 ${sc.User} ${sc.Group} - -"
            # "d '${cfg.dataDir}/meta' 0700 ${sc.User} ${sc.Group} - -"
          ];

          services.garage = {
            description = "Garage Object Storage (S3 compatible)";
            after = [ "network.target" "network-online.target" ];
            wants = [ "network.target" "network-online.target" ];
            wantedBy = [ "multi-user.target" ];
            restartTriggers = [
              # (builtins.hashFile "sha256" defaultSopsFile)
              (builtins.hashFile "sha256" staticGarageToml)
            ];

            # this lets custom data directory work by having a real user own the service process
            # the user and its group need to be created in the users section
            serviceConfig = {
              ExecStart = lib.mkForce "${pkgs.garage}/bin/garage server";

              # StateDirectory = "garage";
              User = "garage";
              Group = "garage";
              ReadWritePaths = [ "${cfg.dataDir}/data" "${cfg.dataDir}/meta" ];
              # RequiresMountsFor = [ "${cfg.dataDir}/data" ];
              # DynamicUser = false;
              PrivateTmp = true;
              # ProtectSystem = true;
            };

            environment = {
              RUST_LOG = "garage=${cfg.logLevel}";
            };
          };
        };

      users = {
        groups.garage = { };

        users.garage = {
          isSystemUser = true;
          createHome = false;
          group = "garage";
        };
      };


      # nginx.virtualHosts."s3.notashelf.dev" =
      #   {
      #     locations."/".proxyPass = "http://127.0.0.1:3900";
      #     extraConfig = ''
      #       proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      #       proxy_set_header Host $host;
      #       # Disable buffering to a temporary file.
      #       proxy_max_temp_file_size 0;
      #     '';
      #   }
      #   // lib.sslTemplate;

      # Proxy through Traefik
      services.traefik.dynamicConfigOptions.http = {

        routers.s3Garage = {
          entryPoints = [ "https" ];
          rule = "Host(`${CONSTS.urls.garage.s3}`)";
          service = "s3Garage";
          middlewares = "local@file";
          # tls.certResolver = "letsencrypt";
        };
        services.s3Garage.loadBalancer = {
          passHostHeader = true;
          servers = [{ url = "http://127.0.0.1:3900"; }];
        };

        routers.webGarage = {
          entryPoints = [ "https" ];
          rule = "Host(`${CONSTS.urls.garage.web}`)";
          service = "webGarage";
          middlewares = "local@file";
          # tls.certResolver = "letsencrypt";
        };
        services.webGarage.loadBalancer = {
          passHostHeader = true;
          servers = [{ url = "http://127.0.0.1:3902"; }];
        };

        routers.adminGarage = {
          entryPoints = [ "https" ];
          rule = "Host(`${CONSTS.urls.garage.admin}`)";
          service = "adminGarage";
          middlewares = "local@file";
          # tls.certResolver = "letsencrypt";
        };
        services.adminGarage.loadBalancer = {
          passHostHeader = true;
          servers = [{ url = "http://127.0.0.1:3903"; }];
        };
      };
    };
}
