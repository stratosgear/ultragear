{ options, config, pkgs, lib, ... }:

with lib;
with lib.ultragear;
let
  cfg = config.ultragear.services.nfsServer;

in
{
  options.ultragear.services.nfsServer = with types; {
    enable = mkBoolOpt false "Whether or not to enable nfs sharing";
    exports = mkOpt
      (listOf (submodule {
        options = {
          exportFolder = mkOpt str null "The folder to be nfs shared";
          exportTo = mkOpt str null "The path to use to export the folder as";
          options = mkOpt str null "The options to use for the export";
        };
      })) [ ] "List of shares to export";
  };

  # https://search.nixos.org/options?channel=unstable&query=nfs
  config = mkIf (cfg.enable && cfg.exports != [ ]) {

    services.nfs = {
      server = {
        enable = true;
        exports = concatStringsSep "\n" (map (e: "/export/${e.exportTo} ${e.options}") cfg.exports);
      };
      settings = {
        nfsd = {
          # Remote Direct Memory Access
          rdma = true;
          vers3 = false;
          vers4 = true;
          "vers4.0" = false;
          "vers4.1" = false;
          "vers4.2" = true;
        };
      };
    };

    # Convert exports to filesystem bind mounts for nfs exporting
    fileSystems = listToAttrs (map
      (e: nameValuePair "/export/${e.exportTo}" {
        device = "${e.exportFolder}";
        options = [ "bind" ];
      })
      cfg.exports);

    networking.firewall.allowedTCPPorts = [ 2049 ];
  };
}
