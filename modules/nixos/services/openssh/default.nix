{ options
, config
, pkgs
, lib
, host ? ""
, format ? ""
, inputs ? { }
, ...
}:
with lib;
with lib.ultragear; let
  cfg = config.ultragear.services.openssh;

  user = config.users.users.${config.ultragear.user.name};
  user-id = builtins.toString user.uid;

  # TODO: This is a hold-over from an earlier Snowfall Lib version which used
  # the specialArg `name` to provide the host name.
  name = host;

  other-hosts =
    lib.filterAttrs
      (key: host:
        key != name && (host.config.ultragear.user.name or null) != null)
      ((inputs.self.nixosConfigurations or { }) // (inputs.self.darwinConfigurations or { }));

  other-hosts-config =
    lib.concatMapStringsSep
      "\n"
      (
        name:
        let
          remote = other-hosts.${name};
          remote-user-name = remote.config.ultragear.user.name;
          remote-user-id = builtins.toString remote.config.users.users.${remote-user-name}.uid;

          forward-gpg =
            optionalString (config.programs.gnupg.agent.enable && remote.config.programs.gnupg.agent.enable)
              ''
                RemoteForward /run/user/${remote-user-id}/gnupg/S.gpg-agent /run/user/${user-id}/gnupg/S.gpg-agent.extra
                RemoteForward /run/user/${remote-user-id}/gnupg/S.gpg-agent.ssh /run/user/${user-id}/gnupg/S.gpg-agent.ssh
              '';
        in
        ''
          Host ${name}
            User ${remote-user-name}
            HostName ${name}.${CONSTS.urls.tailscaleDomain}
            ForwardAgent yes
            #Port ${builtins.toString cfg.port}
            ${forward-gpg}
        ''
      )
      (builtins.attrNames other-hosts);
in
{
  options.ultragear.services.openssh = with types; {
    enable = mkBoolOpt false "Whether or not to configure OpenSSH support.";
    authorizedKeys =
      mkOpt (listOf str) (attrsets.attrValues CONSTS.sshKeys.stratos) "The public keys to apply.";
    port = mkOpt port 2222 "The port to listen on (in addition to 22).";
    manage-other-hosts = mkOpt bool true "Whether or not to add other host configurations to SSH config.";
  };

  config = mkIf cfg.enable {
    services.openssh = {
      enable = true;

      settings = {
        PermitRootLogin =
          if format == "install-iso"
          then "yes" ea
          else "no";
        PasswordAuthentication = false;
      };

      extraConfig = ''
        StreamLocalBindUnlink yes
      '';

      ports = [
        22
        # TODO: Not sure why we are opening an additional ssh port here
        # cfg.port
      ];
    };

    programs.ssh.extraConfig = ''
      Host *
        HostKeyAlgorithms +ssh-rsa

      ${optionalString cfg.manage-other-hosts other-hosts-config}
    '';

    ultragear.user.extraOptions.openssh.authorizedKeys.keys =
      cfg.authorizedKeys;

    ultragear.home.extraOptions = {
      programs.zsh.shellAliases =
        foldl
          (aliases: system:
            aliases
            // {
              "ssh-${system}" = "ssh ${system} -t tmux a";
            })
          { }
          (builtins.attrNames other-hosts);
    };
  };
}
