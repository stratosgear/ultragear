{ options, config, pkgs, lib, ... }:

with lib;
with lib.ultragear;
let cfg = config.ultragear.services.docker;
in
{
  options.ultragear.services.docker = with types; {
    enable = mkBoolOpt false "Whether or not to enable docker service";
    dataRoot = mkOpt str "/data/docker" "Location where docker stores data";
    dockerCompose = mkBoolOpt true "Whether to also install docker compose plugin too";
  };

  config = mkIf cfg.enable {

    # From: https://nixos.wiki/wiki/Docker

    systemd.tmpfiles.rules = [
      "d ${cfg.dataRoot} 0750 root root -"
    ];

    virtualisation.docker = {
      enable = true;
      enableOnBoot = true; # default value, anyways
      storageDriver = "btrfs";
      daemon.settings = {
        data-root = "/data/docker";
        # log-driver = "json-file";
      };
    };

    users.extraGroups.docker.members = [ config.ultragear.user.name ];

    environment.systemPackages = [
      pkgs.lazydocker
    ] ++ optionals cfg.dockerCompose [ pkgs.docker-compose ];

  };
}
