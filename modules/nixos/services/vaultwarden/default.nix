{ options, config, pkgs, lib, ... }:

with lib;
with lib.ultragear;
let cfg = config.ultragear.services.vaultwarden;
in
{
  options.ultragear.services.vaultwarden = with types; {
    enable = mkBoolOpt false "Whether or not to enable the vaultwarden service";
    port = mkOpt int CONSTS.ports.services.vaultwarden "The port number to run as";
    domainName = mkOpt str CONSTS.urls.vaultwarden "The domain name to expose service as";
    dataDir = mkOpt str "/data/vaultwarden" "The location to store data";
  };

  config = mkIf cfg.enable {

    # Based on:
    # https://github.com/NobbZ/nixos-config/blob/5c0828b8a5dd35ed83e29dece50dd4d3c9313c4b/nixos/configurations/mimas/vaultwarden.nix

    systemd.tmpfiles.rules = [
      "Z ${cfg.dataDir} 0750 vaultwarden vaultwarden -"
    ];

    sops.secrets = {
      "vaultwarden/env" = { };
    };

    services.vaultwarden = {
      enable = true;
      environmentFile = config.sops.secrets."vaultwarden/env".path;
      # Environment variables as defined in:
      # https://github.com/dani-garcia/vaultwarden/blob/1.32.7/.env.template
      config = {
        DOMAIN = "https://${cfg.domainName}";
        DATABASE_MAX_CONNS = "5";
        DATA_FOLDER = "${cfg.dataDir}";

        # LOG_LEVEL = "debug";

        ROCKET_ADDRESS = "127.0.0.1";
        ROCKET_PORT = "${toString cfg.port}";
        ROCKET_WORKERS = "5";

        SIGNUPS_ALLOWED = false;
      };
    };

    # Force the systemd service to have the dataDir folders as writable
    systemd.services.vaultwarden.serviceConfig = {
      ReadWriteDirectories = "${cfg.dataDir}";
    };

    # Proxy through Traefik
    services.traefik.dynamicConfigOptions.http = {
      routers.vaultwarden = {
        entryPoints = [ "https" ];
        rule = "Host(`${cfg.domainName}`)";
        service = "vaultwarden";
        middlewares = "local@file";
      };
      services.vaultwarden.loadBalancer = {
        passHostHeader = true;
        servers = [{ url = "http://127.0.0.1:${toString cfg.port}"; }];
      };
    };

    # Stanza for system restic backups
    ultragear.services.backup.instances.system = {
      sourceDirectories = [ cfg.dataDir ];
    };

  };
}
