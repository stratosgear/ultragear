{ options, config, pkgs, lib, ... }:

with lib;
with lib.ultragear;
let cfg = config.ultragear.services.samba;
in
{
  options.ultragear.services.samba = with types; {
    enable = mkBoolOpt false "Whether or not to configure samba support.";
    shares = mkOpt (attrsOf attrs) { } "Shares to expose through samba";
  };

  config = mkIf cfg.enable {

    # mainly from: https://nixos.wiki/wiki/Samba

    services = {
      samba-wsdd = {
        enable = true;
        discovery = true;
        workgroup = "WORKGROUP";
      };

      samba = {
        enable = true;
        openFirewall = true;
        securityType = "user";
        extraConfig = ''
          workgroup = WORKGROUP
          server string = ${config.networking.hostName}nas
          netbios name = ${config.networking.hostName}nas
          security = user
          #use sendfile = yes
          min protocol = smb3_00
          #protocol smb3
          # note: localhost is the ipv6 localhost ::1
          hosts allow = ${concatStringsSep " " CONSTS.ips.privNetRanges}
          hosts deny = 0.0.0.0/0
          guest account = nobody
          map to guest = bad user
          load printers = no
          browseable = yes
        '';

        shares = cfg.shares;
        # shares = {
        #   public = {
        #     path = "/data/unsafepool/Public";
        #     browseable = "yes";
        #     "read only" = "no";
        #     "guest ok" = "yes";
        #     "create mask" = "0644";
        #     "directory mask" = "0755";
        #     "force user" = "username";
        #     "force group" = "groupname";
        #   };
        # };

      };
    };

    # fix error in service log
    security.pam.services.samba-smbd.limits = [
      { domain = "*"; type = "soft"; item = "nofile"; value = 16384; }
      { domain = "*"; type = "hard"; item = "nofile"; value = 32768; }
    ];

    networking.firewall = {
      enable = true;
      allowPing = true;
      allowedTCPPorts = [
        5357 # wsdd
        445 # smb
        139 # smb
      ];
      allowedUDPPorts = [
        3702 # wsdd
        137
        138
      ];
      extraCommands = ''iptables -t raw -A OUTPUT -p udp -m udp --dport 137 -j CT --helper netbios-ns'';
    };
  };
}
