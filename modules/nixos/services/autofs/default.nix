{ options, config, pkgs, lib, ... }:

with lib;
with lib.ultragear;
let
  cfg = config.ultragear.services.autofs;

  sharesInstance = with types; {
    paths = mkOpt (arrayOf str) null "The shared paths to use";
  };

  # https://github.com/jakubgs/nixos-config/blob/4c3b76e527ffb2cb2d43e912b12307b1c60f21de/roles/autofs.nix#L28
  defaultNfsOptions = "async,noac,soft,rsize=262144,wsize=262144";

  genNfsShare = host: paths:
    lib.concatStringsSep "\n" (
      map (p: "${p} -fstype=nfs,${defaultNfsOptions} ${host}:/${p}") paths
    );

  genShareConfigFile = host: shares: genFunc:
    pkgs.writeText "autofs-${host}" (genFunc host shares);
in
{
  options.ultragear.services.autofs = with types; {
    enable = mkBoolOpt false "Whether or not to lazy mount network shared folders";
    # shares = mkOpt (attrsOf (submodule { options = sharesInstance; })) { } ''Shares to setup in autofs. example:
    shares = mkOpt attrs { } ''Shares to setup in autofs. example:
    {
      "host1" = [ "path1" "path2" ];
      "host2" = [ "path1" "path3" ];
    };
    '';
  };

  config = mkIf cfg.enable {

    environment.systemPackages = [
      pkgs.nfs-utils
    ];

    services.autofs = {
      enable = true;
      timeout = 60;
      # debug = true;
      autoMaster = ''
        /net -hosts --timeout=5
      '';
      #   autoMaster = lib.concatStrings
      #     (lib.mapAttrsToList
      #       (host: shares: ''
      #         /net/${host}  ${genShareConfigFile host shares genNfsShare}  --timeout 3
      #       '')
      #       cfg.shares);
    };

  };
}
