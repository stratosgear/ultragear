{ options, config, pkgs, lib, ... }:

with lib;
with lib.ultragear;
let cfg = config.ultragear.services.resiliosync;
in
{
  options.ultragear.services.resiliosync = with types; {
    enable = mkBoolOpt false "Whether or not to enable resiliosync service";
    domainName = mkOpt str "sync.${config.networking.hostName}.${CONSTS.urls.privRootDomain}" "The domain name to expose service as";
  };

  config = mkIf cfg.enable {

    services.resilio = {
      enable = true;
      enableWebUI = true;
      httpListenAddr = "0.0.0.0";
      httpListenPort = CONSTS.ports.services.resiliosync;
    };

    # Proxy through Traefik
    services.traefik.dynamicConfigOptions.http = {
      routers.resiliosync = {
        entryPoints = [ "https" ];
        rule = "Host(`${cfg.domainName}`)";
        service = "resiliosync";
      };

      services.resiliosync.loadBalancer = {
        passHostHeader = true;
        servers = [{ url = "http://127.0.0.1:${toString config.services.resilio.httpListenPort}"; }];
      };
    };

  };
}
