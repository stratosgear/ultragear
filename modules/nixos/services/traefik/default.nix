{ options, config, pkgs, lib, ... }:

with lib;
with lib.ultragear;
let cfg = config.ultragear.services.traefik;
in
{
  options.ultragear.services.traefik = with types; {
    enable = mkBoolOpt false "Whether or not to enable traefik proxy server.";
    domainName = mkOpt str "traefik.${config.networking.hostName}.${CONSTS.urls.privRootDomain}" "The domain name to expose service as";
  };

  config = mkIf cfg.enable {

    assertions = [
      {
        assertion = cfg.enable -> cfg.domainName != "";
        message = "If you choose to activate Traefik, then you also need to define a domainName to expose it through";
      }
    ];

    sops.secrets."traefik/env" = {
      sopsFile = ./. + "../../../../../secrets/vault.yaml";
    };

    systemd.services.traefik.serviceConfig.EnvironmentFile = [ config.sops.secrets."traefik/env".path ];

    services.traefik = {
      enable = true;
      staticConfigOptions = {
        # log.level = "DEBUG";

        api.dashboard = true;
        # api.insecure = true;
        pilot.dashboard = false;
        # experimental.http3 = true;

        global = {
          checkNewVersion = false;
          sendAnonymousUsage = false;
        };

        certificatesResolvers.letsencrypt.acme = {
          # storage = "${config.services.traefik.dataDir}/acme.json";
          email = "stratos@gerakakis.net";
          # caServer = "https://acme-staging-v02.api.letsencrypt.org/directory";
          storage = "/var/lib/traefik/acme.json";

          # Debug lets encrypt resolution errors with:
          # https://letsdebug.net/
          dnsChallenge = {
            provider = "cloudflare";
            disablePropagationCheck = true;
            delayBeforeCheck = "20s";
            # resolvers = [ "1.1.1.1:53" "8.8.8.8:53" ];
          };
        };

        # Traefik also handles IMAP/SMTP?
        # https://github.com/natsukagami/nix-home/blob/23d22a317135bd2578468db9f924ef61cc1c95b5/modules/cloud/traefik/default.nix#L60
        entryPoints = {
          http = {
            address = ":80";
            forwardedHeaders.insecure = true;
            http.redirections.entryPoint = {
              to = "https";
              scheme = "https";
            };
          };

          https = {
            address = ":443";
            # enableHTTP3 = true;
            forwardedHeaders.insecure = true;

            # Makes these available to ALL the traefik services
            http.tls = {
              certResolver = "letsencrypt";
            };
          };

          # experimental = {
          #   address = ":1111";
          #   forwardedHeaders.insecure = true;
          # };
        };
      };
    };

    services.traefik.dynamicConfigOptions.http = {
      middlewares = {
        local.ipWhiteList.sourceRange = CONSTS.ips.privNetRanges ++ CONSTS.ips.cloudflareIP4;

        # traefik-stripprefix = {
        #   stripPrefix.prefixes = [
        #     "/traefik"
        #   ];
        # };

        # allows websocket connections?
        sslheader.headers.customrequestheaders.X-Forwarded-Proto = "https";

        add-permanent-slash = {
          redirectregex = {
            permanent = true;
            regex = "^(https?://[^/]+/[a-z0-9_]+)$";
            replacement = ''''${1}/'';
          };
        };
        strip-service-slug = {
          stripprefixregex.regex = "/[a-z0-9_]+";
        };
        our-slash = {
          chain.middlewares = [
            "add-permanent-slash"
            "strip-service-slug"
          ];
        };
      };

      routers = {
        traefik = {
          rule = "Host(`${cfg.domainName}`)";
          entryPoints = [ "https" ];
          service = "api@internal";

        };
      };

    };

    networking.firewall = {
      allowedTCPPorts = [ 80 443 ];
      allowedUDPPorts = [ 443 ];
    };
  };
}
