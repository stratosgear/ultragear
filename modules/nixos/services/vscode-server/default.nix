{ options, config, lib, pkgs, ... }:

with lib;
with lib.ultragear;
let cfg = config.ultragear.services.vscode-server;
in
{
  options.ultragear.services.vscode-server = with types; {
    enable = mkBoolOpt false "Whether or not to enable vscode server.";
  };

  config =
    mkIf cfg.enable {
      services.vscode-server.enable = true;
      services.vscode-server.enableFHS = true;
      };
}

