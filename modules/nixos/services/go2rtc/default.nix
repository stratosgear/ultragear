{ options, config, pkgs, lib, ... }:

with lib;
with lib.ultragear;
let cfg = config.ultragear.services.go2rtc;
in
{
  options.ultragear.services.go2rtc = with types; {
    enable = mkBoolOpt false "Whether or not to enable go2rtc service (IP cameras viewer)";
  };

  config = mkIf cfg.enable {

    services.go2rtc = {
      enable = true;
      settings.ffmpeg.bin = "${pkgs.ffmpeg-full}/bin/ffmpeg";
      settings.streams = {
        Mad1 = "rtsp://admin:541873@192.168.3.108:554/live/profile.0/video";
        Mad2 = "rtsp://admin:936086@192.168.3.109:554/live/profile.0/video";
      };
    };

    # Proxy through Traefik
    services.traefik.dynamicConfigOptions.http = {
      routers.go2rtc = {
        entryPoints = [ "https" ];
        rule = "Host(`${CONSTS.urls.go2rtc}`)";
        service = "go2rtc";
        middlewares = "local@file";
      };
      services.go2rtc.loadBalancer = {
        passHostHeader = true;
        servers = [{ url = "http://127.0.0.1:1984"; }];
      };
    };

  };
}
