{ options, config, pkgs, lib, ... }:

with lib;
with lib.ultragear;
let cfg = config.ultragear.services.uptimeping;
in
{
  options.ultragear.services.uptimeping = with types; {
    enable = mkBoolOpt false "Whether or not to perform regular pings to know if server is up. Requires a [healtchChecks/hcPingUid] secret.";
  };

  config = mkIf cfg.enable {

    sops.secrets."healthchecks/ping" = { };

    systemd.timers."healthchecks-ping" = {
      wantedBy = [ "timers.target" ];
      timerConfig = {
        OnBootSec = "1m";
        OnUnitActiveSec = "1m";
        Unit = "healthchecks-ping.service";
      };
    };

    systemd.services."healthchecks-ping" = {
      script = ''
        set -eu
        ${pkgs.runitor}/bin/runitor -api-url "https://${CONSTS.urls.healthchecks}/ping" -uuid $(cat ${config.sops.secrets."healthchecks/ping".path}) -- echo ping.
      '';
      serviceConfig = {
        Type = "oneshot";
        User = "root";
      };
    };
  };
}
