{
  options,
  config,
  pkgs,
  lib,
  ...
}:

with lib;
with lib.ultragear;
let
  cfg = config.ultragear.pods.bots.croesus;
in
{
  options.ultragear.pods.bots.croesus = with types; {
    enable = mkBoolOpt false "Whether or not to enable the Croesus bot";
    dataDir = mkOpt str "/data/bots/croesus" "The data directory of the bot";
  };

  config = mkIf cfg.enable {

    sops = {
      secrets."gitlab-registry/username" = { };
      secrets."gitlab-registry/token" = { };
      secrets."bots/croesus/env" = { };
    };

    systemd.tmpfiles.rules = [
      "d ${cfg.dataDir} 0750 root root -"
      "Z ${cfg.dataDir} 0750 root root -"
    ];

    # virtualisation.oci-containers.backend = "docker";
    virtualisation.oci-containers.containers."croesus-bot" = {
      image = "registry.gitlab.com/hypervasis/telegram-bots/croesus-bot:1.3";
      login = {
        registry = "registry.gitlab.com";
        # username = readFile config.sops.secrets."gitlab-registry/username".path;
        username = "stratosgear";
        passwordFile = config.sops.secrets."gitlab-registry/token".path;
      };
      environmentFiles = [ config.sops.secrets."bots/croesus/env".path ];
      environment = {
        "DATA_DIR" = "/data";
      };
      volumes = [
        "${cfg.dataDir}:/data"
      ];
    };

    # Stanza for system restic backups
    ultragear.services.backup.instances.system = {
      sourceDirectories = [ cfg.dataDir ];
    };

  };
}
