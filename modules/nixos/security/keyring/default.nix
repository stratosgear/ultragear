{ options, config, lib, pkgs, ... }:

with lib;
with lib.ultragear;
let
  cfg = config.ultragear.security.keyring;
in
{
  options.ultragear.security.keyring = with types; {
    enable = mkBoolOpt false "Whether to enable gnome keyring.";
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      gnome.gnome-keyring
      gnome.libgnome-keyring
    ];
  };
}
