{ options, config, pkgs, lib, inputs, ... }:

with lib;
with lib.ultragear;

# Project info:
# https://github.com/Mic92/sops-nix

let
  cfg = config.ultragear.security.sops;

  isEd25519 = k: k.type == "ed25519";
  getKeyPath = k: k.path;
  keys = builtins.filter isEd25519 config.services.openssh.hostKeys;

  # Conflict of sops-nix with impermanence
  # Workaround from:
  # https://github.com/Mic92/sops-nix/issues/149#issuecomment-1596029524
  regularSecrets = filterAttrs (n: v: !v.neededForUsers) config.sops.secrets;
in
{
  options.ultragear.security.sops = with types; {
    enable = mkBoolOpt false "Whether or not to configure secrets handling through sops.";
  };

  config = mkIf cfg.enable {

    environment.systemPackages = with pkgs;
      [
        sops
      ];

    sops = {
      age.sshKeyPaths = map getKeyPath keys;
      defaultSopsFile = ./. + "../../../../../secrets/${config.networking.hostName}.yaml";
      secrets = {
        age_keys = {
          mode = "0400";
          sopsFile = ./. + "../../../../../secrets/vault.yaml";
          owner = "${config.ultragear.user.name}";
          group = "users";
          path = "/home/${config.ultragear.user.name}/.config/sops/age/keys.txt";
        };
      };
    };
    # sops.age.sshKeyPaths = [ "/etc/ssh/ssh_host_ed25519_key" ];
    # sops.age.sshKeyPaths = [];
    # sops.gnupg.sshKeyPaths = [];

    # Conflict of sops-nix with impermanence. See above.
    system.activationScripts.setupSecrets = mkIf (regularSecrets != { } && config.environment.persistence != { }) {
      deps = [ "persist-files" ];
    };
  };
}
