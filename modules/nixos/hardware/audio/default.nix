{ options
, config
, pkgs
, lib
, ...
}:

with lib;
with lib.ultragear;
let
  cfg = config.ultragear.hardware.audio;

in
{
  options.ultragear.hardware.audio = with types; {
    enable = mkBoolOpt false "Whether or not to enable audio support.";
    alsa-monitor = mkOpt attrs { } "Alsa configuration.";
    nodes =
      mkOpt (listOf attrs) [ ]
        "Audio nodes to pass to Pipewire as `context.objects`.";
    modules =
      mkOpt (listOf attrs) [ ]
        "Audio modules to pass to Pipewire as `context.modules`.";
    extra-packages = mkOpt (listOf package) [
      pkgs.qjackctl
      pkgs.easyeffects
    ] "Additional packages to install.";
  };

  config = mkIf cfg.enable {
    security.rtkit.enable = true;

    services.pipewire = {
      enable = true;
      alsa.enable = true;
      pulse.enable = true;
      jack.enable = true;

      # As per nixos wiki:
      # https://nixos.wiki/wiki/PipeWire#Bluetooth_Configuration
      wireplumber = {
        enable = true;
        # Fossi BT20A amp does not seem to like these
        # extraConfig.bluetoothEnhancements = {
        #   "monitor.bluez.properties" = {
        #     "bluez5.enable-sbc-xq" = true;
        #     "bluez5.enable-msbc" = true;
        #     "bluez5.enable-hw-volume" = true;
        #     "bluez5.roles" = [ "hsp_hs" "hsp_ag" "hfp_hf" "hfp_ag" ];
        #   };
        # };
      };
    };

    environment.etc = {
      # "pipewire/pipewire.conf.d/10-pipewire.conf".source =
      #   pkgs.writeText "pipewire.conf" (builtins.toJSON pipewire-config);
      # "pipewire/pipewire.conf.d/21-alsa.conf".source =
      #   pkgs.writeText "pipewire.conf" (builtins.toJSON alsa-config);

      #       "wireplumber/wireplumber.conf".source =
      #         pkgs.writeText "pipewire.conf" (builtins.toJSON pipewire-config);

      # "wireplumber/scripts/config.lua.d/alsa.lua".text = ''
      #   local input = ${lua-format.generate "sample.lua" cfg.alsa-monitor}

      #   if input.rules == nil then
      #    input.rules = {}
      #   end

      #   local rules = input.rules

      #   for _, rule in ipairs(input.rules) do
      #     table.insert(alsa_monitor.rules, rule)
      #   end
      # '';
    };

    # https://github.com/NixOS/nixpkgs/pull/369391#issuecomment-2565619106
    services.pulseaudio.enable = mkForce false;

    environment.systemPackages = with pkgs;
      [
        pulsemixer
        pavucontrol
        paprefs
      ]
      ++ cfg.extra-packages;

    ultragear.user.extraGroups = [ "audio" ];

    ultragear.home.extraOptions = {
      systemd.user.services.mpris-proxy = {
        Unit.Description = "Mpris proxy";
        Unit.After = [ "network.target" "sound.target" ];
        Service.ExecStart = "${pkgs.bluez}/bin/mpris-proxy";
        Install.WantedBy = [ "default.target" ];
      };
    };
  };
}
