{ options, config, pkgs, lib, ... }:

with lib;
with lib.ultragear;
let cfg = config.ultragear.hardware.bluetooth;
in
{
  options.ultragear.hardware.bluetooth = with types; {
    enable = mkBoolOpt false "Whether or not to enable bluetooth support";
  };

  config = mkIf cfg.enable {

    hardware.bluetooth = {
      enable = true;
      powerOnBoot = true;
      settings = {
        General = {
          Enable = "Source,Sink,Media,Socket";
        };
      };
    };

    environment.systemPackages = with pkgs; [
      blueman
      bluez
      bluez-tools
    ];
    
    services.blueman.enable = true;
  };
}
