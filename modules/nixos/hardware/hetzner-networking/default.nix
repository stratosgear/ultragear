{ options, config, pkgs, lib, ... }:

with lib;
with lib.ultragear;
let cfg = config.ultragear.hardware.hetzner-networking;
in
{
  options.ultragear.hardware.hetzner-networking = with types; {
    enable = mkBoolOpt false "Whether or not to enable Hetzner dedicated server networking support";
  };

  config = mkIf cfg.enable {

    networking.useNetworkd = true;
    networking.useDHCP = false;
    networking.usePredictableInterfaceNames = false;

    systemd.network.networks."10-uplink" = {
      matchConfig.Name = lib.mkDefault "en* eth0";
      networkConfig.DHCP = "ipv4";
      networkConfig.Gateway = "fe80::1";
      networkConfig.IPv6AcceptRA = "no";
    };
  };
}
