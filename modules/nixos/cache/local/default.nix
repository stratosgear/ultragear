{ config, lib, ... }:

with lib;
with lib.ultragear;
let
  cfg = config.ultragear.cache.local;
in
{
  options.ultragear.cache.local = {
    enable = mkEnableOption "Ultragear local cache.nixos.org (as in Madrid homelab) cache";
  };

  config = mkIf cfg.enable {
    ultragear.nix.extra-substituters = {
      "https://${config.ultragear.services.nixosLocalCacher.domainName}".key = "";
    };
  };
}
