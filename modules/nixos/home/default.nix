{ options, config, pkgs, lib, inputs, ... }:

with lib;
with lib.ultragear;
let
  cfg = config.ultragear.home;
  # inherit (inputs) catppuccin;
in
{
  # imports = with inputs; [
  #   home-manager.nixosModules.home-manager
  # ];

  options.ultragear.home = with types; {
    file = mkOpt attrs { }
      (mdDoc "A set of files to be managed by home-manager's `home.file`.");
    configFile = mkOpt attrs { }
      (mdDoc "A set of files to be managed by home-manager's `xdg.configFile`.");
    extraOptions = mkOpt attrs { } "Options to pass directly to home-manager.";
  };

  config = {
    ultragear.home.extraOptions = {
      home.stateVersion = config.system.stateVersion;
      home.file = mkAliasDefinitions options.ultragear.home.file;
      xdg.enable = true;
      xdg.configFile = mkAliasDefinitions options.ultragear.home.configFile;
    };

    home-manager = {
      useUserPackages = true;
      useGlobalPkgs = true;

      users.${config.ultragear.user.name} =
        mkAliasDefinitions options.ultragear.home.extraOptions;
    };
  };
}
