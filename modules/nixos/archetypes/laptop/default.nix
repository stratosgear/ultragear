{ options
, config
, lib
, pkgs
, ...
}:
with lib;
with lib.ultragear; let
  cfg = config.ultragear.archetypes.laptop;
in
{
  options.ultragear.archetypes.laptop = with types; {
    enable =
      mkBoolOpt false "Whether or not this system is a laptop.";
  };

  config = mkIf cfg.enable {
    ultragear = {

      desktop.addons = {
        # xbacklight = enabled
      };


    };

    environment.systemPackages = with pkgs; [
      brightnessctl
    ];


    # From: https://wiki.archlinux.org/title/Backlight#ACPI
    # To allow users from the video group to alter screen brightness (polybar maybe?) we need these:
    services.udev = {
      extraRules = ''
        ACTION=="add", SUBSYSTEM=="backlight", RUN+="${pkgs.coreutils}/bin/chgrp video %S%p/brightness", RUN+="${pkgs.coreutils}/bin/chmod g+w %S%p/brightness"
      '';
      path = [
        pkgs.coreutils # for chgrp
      ];
    };

    ultragear.user.extraGroups = [ "video" ];

  };
}
