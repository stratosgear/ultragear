{ options, config, lib, pkgs, ... }:
with lib;
with lib.ultragear;
let cfg = config.ultragear.archetypes.workstation;
in
{
  options.ultragear.archetypes.workstation = with types; {
    enable =
      mkBoolOpt false "Whether or not to enable the workstation archetype.";
  };

  config = mkIf cfg.enable {
    ultragear = {
      suites = {
        desktop = enabled;
        # development = enabled;
        # art = enabled;
        # video = enabled;
        # social = enabled;
        # media = enabled;
      };

      hardware = {
        audio = enabled;
      };

      # tools = {
      #   appimage-run = enabled;
      # };
    };
  };
}
