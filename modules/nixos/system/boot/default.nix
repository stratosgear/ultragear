{ options
, config
, pkgs
, lib
, ...
}:
with lib;
with lib.ultragear; let
  cfg = config.ultragear.system.boot;
in
{
  options.ultragear.system.boot = with types; {
    enable = mkBoolOpt true "Whether or not to enable booting.";
    type = mkOpt (types.enum [ "efi" "bios" ]) "efi" "What type of booting option to use. Valid options: efi|bios";
    bootDevice = mkOpt str "" "The boot device (/dev/sdX), if we're in bios mode";
    memtest = mkBoolOpt false "Wheteher to enable a memtest during boot";
    generationsLimit = mkOpt int 16 "Number of latest generations to allow booting from";
  };

  config = mkIf cfg.enable {

    assertions = [
      {
        assertion = cfg.type == "bios" -> cfg.bootDevice != "";
        message = "If ultragear.system.boot.type is set to BIOS, the bootDevice is also needed!";
      }
    ];


    boot.loader =
      if cfg.type == "efi" then {
        # no need to set devices, disko will add all devices that have a EF02 partition to the list already
        # devices = [ ];
        systemd-boot = {
          enable = true;
          memtest86.enable = cfg.memtest;
          configurationLimit = cfg.generationsLimit;
          # https://github.com/NixOS/nixpkgs/blob/c32c39d6f3b1fe6514598fa40ad2cf9ce22c3fb7/nixos/modules/system/boot/loader/systemd-boot/systemd-boot.nix#L66
          #editor = false;
        };
        efi.canTouchEfiVariables = true;

        # efiSupport = true;
        # efiInstallAsRemovable = true;
      } else {
        # Use the GRUB 2 boot loader.
        grub = {
          enable = true;
          device = cfg.bootDevice; # or "nodev" for efi only
          memtest86.enable = cfg.memtest;
          configurationLimit = cfg.generationsLimit;
        };
      };
  };
}
