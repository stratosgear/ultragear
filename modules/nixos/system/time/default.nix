{ options, config, pkgs, lib, ... }:

with lib;
with lib.ultragear;
let cfg = config.ultragear.system.time;
in
{
  options.ultragear.system.time = with types; {
    enable = mkBoolOpt false "Whether or not to configure timezone information.";
    tz = mkOpt str "Europe/Madrid" "The timezone to set to";
  };

  config = mkIf cfg.enable {
    time.timeZone = cfg.tz;
  };
}
