{ options, config, pkgs, lib, ... }:

with lib;
with lib.ultragear;
let cfg = config.ultragear.system.locale;
in
{
  options.ultragear.system.locale = with types; {
    enable = mkBoolOpt false "Whether or not to manage locale settings.";
  };

  config = mkIf cfg.enable {
    i18n.defaultLocale = "en_US.UTF-8";

    # Automatically derive location from IP
    location.provider = "geoclue2";
    location.latitude = 40.416775;
    location.longitude = -3.703790;

    console = { keyMap = mkForce "us"; };
  };
}
