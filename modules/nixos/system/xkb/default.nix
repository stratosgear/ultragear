{ options, config, lib, ... }:

with lib;
with lib.ultragear;
let cfg = config.ultragear.system.xkb;
in
{
  options.ultragear.system.xkb = with types; {
    enable = mkBoolOpt false "Whether or not to configure xkb.";
  };

  config = mkIf cfg.enable {
    console.useXkbConfig = true;
    services.xserver = {
      xkb = {
        layout = "us";
        options = "caps:super";
      };
    };
  };
}
