{ options
, config
, pkgs
, lib
, ...
}:
with lib;
with lib.ultragear; let
  cfg = config.ultragear.system.fonts;
in
{
  options.ultragear.system.fonts = with types; {
    enable = mkBoolOpt false "Whether or not to manage fonts.";
    fonts = mkOpt (listOf package) [ ] "Custom font packages to install.";
  };

  config = mkIf cfg.enable {
    environment.variables = {
      # Enable icons in tooling since we have nerdfonts.
      LOG_ICONS = "true";
    };

    environment.systemPackages = with pkgs; [ font-manager ];

    fonts.packages = with pkgs;
      [
        roboto
        noto-fonts
        noto-fonts-cjk-sans
        noto-fonts-cjk-serif
        noto-fonts-emoji

        # nerd-fonts update as of today:
        # https://www.reddit.com/r/NixOS/comments/1h1nc2a/nerdfonts_has_been_separated_into_individual_font/
        nerd-fonts.iosevka
        nerd-fonts.iosevka-term
      ]
      ++ cfg.fonts;
  };
}
