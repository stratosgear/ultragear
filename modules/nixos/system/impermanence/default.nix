{ options, config, pkgs, lib, ... }:

# From:
# https://github.com/nix-community/impermanence

with lib;
with lib.ultragear;
let
  cfg = config.ultragear.system.impermanence;
  fs-diff = pkgs.writeShellScriptBin "fs-diff"
    (builtins.readFile ./fs-diff);
in
{
  options.ultragear.system.impermanence = with types; {
    enable = mkBoolOpt false "Whether or not to enable impermanence";
    rootPartitionDevice = mkOpt str "sda1" "The disk partition where root is mounted (contains /persist)";
    directories = mkOpt (listOf str) [ ] "List of directories to persist";
    files = mkOpt (listOf str) [ ] "List of files to persist";
  };

  config = mkIf cfg.enable {


    # enable impermanence
    environment = {

      # install utility script that shows difference of files between blank root and
      # populated root
      # Usage:
      # sudo mkdir /mnt/check
      # sudo mount -o subvol=/ /dev/[PART] /mnt/check
      # fs-diff.sh
      systemPackages = with pkgs;
        [
          fs-diff
        ];

      # List directories (and files) to persist
      persistence."/persist" = {
        directories = cfg.directories ++ [
          "/var/lib/systemd"
          "/etc/nixos"
          "/etc/ssh"
        ];
        files = cfg.files ++ [
          "/etc/machine-id"
        ];
      };
    };


    boot.initrd = {
      enable = true;
      supportedFilesystems = [ "btrfs" ];

      systemd = {
        enable = true;
        services.restore-root = {
          description = "Rollback btrfs rootfs";
          wantedBy = [ "initrd.target" ];
          requires = [
            "dev-${cfg.rootPartitionDevice}.device"
          ];
          after = [
            "dev-${cfg.rootPartitionDevice}.device"
            # for luks
            # "systemd-cryptsetup@${config.networking.hostName}.service"
          ];
          before = [ "sysroot.mount" ];
          unitConfig.DefaultDependencies = "no";
          serviceConfig.Type = "oneshot";
          script = ''
            mkdir -p /mnt

            # We first mount the btrfs root to /mnt
            # so we can manipulate btrfs subvolumes.
            mount -o subvol=/ /dev/${cfg.rootPartitionDevice} /mnt

            # While we're tempted to just delete /root and create
            # a new snapshot from /root-blank, /root is already
            # populated at this point with a number of subvolumes,
            # which makes `btrfs subvolume delete` fail.
            # So, we remove them first.
            #
            # /root contains subvolumes:
            # - /root/var/lib/portables
            # - /root/var/lib/machines
            #
            # I suspect these are related to systemd-nspawn, but
            # since I don't use it I'm not 100% sure.
            # Anyhow, deleting these subvolumes hasn't resulted
            # in any issues so far, except for fairly
            # benign-looking errors from systemd-tmpfiles.
            btrfs subvolume list -o /mnt/root |
            cut -f9 -d' ' |
            while read subvolume; do
              echo "deleting /$subvolume subvolume..."
              btrfs subvolume delete "/mnt/$subvolume"
            done &&
            echo "deleting /root subvolume..." &&
            btrfs subvolume delete /mnt/root

            echo "restoring blank /root subvolume..."
            btrfs subvolume snapshot /mnt/root-blank /mnt/root

            # Once we're done rolling back to a blank snapshot,
            # we can unmount /mnt and continue on the boot process.
            umount /mnt
          '';
        };

        # Stolen from:
        # https://github.com/kjhoerr/dotfiles/blob/81ba34d55ee87472577411dafc1139974c57dbf2/.config/nixos/os/persist.nix#L108
        # Required because machine-id needs to be mounted very early on booting.
        # Better explanation at:
        # https://discourse.nixos.org/t/what-does-impermanence-add-over-built-in-functionality/27939/19
        services.persisted-files = {
          description = "Hard-link persisted files from /persist";
          wantedBy = [
            "initrd.target"
          ];
          after = [
            "sysroot.mount"
          ];
          unitConfig.DefaultDependencies = "no";
          serviceConfig.Type = "oneshot";
          script = ''
            mkdir -p /sysroot/etc/
            ln -snfT /persist/etc/machine-id /sysroot/etc/machine-id
          '';
        };
      };
    };
  };
}
