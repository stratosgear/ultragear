{ options, config, pkgs, lib, ... }:

with lib;
with lib.ultragear;
let
  cfg = config.ultragear.system.nfsClient;
in
{
  options.ultragear.system.nfsClient = with types; {
    enable = mkBoolOpt false "Whether or not to enable nfs client";
    mounts = mkOpt
      (listOf (submodule {
        options = {
          mountPoint = mkOpt str null "The location to mount the nfs folder";
          exportPoint = mkOpt str null "The NFS export point to mount";
          options = mkOpt (listOf str) [ "x-systemd.automount" "noauto" "x-systemd.idle-timeout=600" "nfsvers=4.2" ] "The options to use for the mount";
        };
      })) [ ] "List of shares to export";
  };

  # https://search.nixos.org/options?channel=unstable&query=nfs
  config = mkIf (cfg.enable && cfg.mounts != [ ]) {

    environment.systemPackages = [
      pkgs.nfs-utils
    ];

    # Mount nfs endpoints
    fileSystems = listToAttrs (map
      (oneMount: nameValuePair "${oneMount.mountPoint}" {
        device = "${oneMount.exportPoint}";
        fsType = "nfs";
        options = oneMount.options;
      })
      cfg.mounts);

  };
}
