{ options, config, lib, pkgs, ... }:

with lib;
with lib.ultragear;
let cfg = config.ultragear.system.info;
in
{
  options.ultragear.system.info = with types; {

    # Get it from: ls -1 /sys/class/backlight/
    backlightClass = mkOpt str "" "The screen backlighting class name.";

    # Get it from: ls -1 /sys/class/power_supply/
    # Usually BAT0 or BAT1
    batteryClass = mkOpt str "" "The power supply battery class name.";

    # Get it from: xrandr
    display1 = mkOpt str "" "The name of the first display";
    display2 = mkOpt str "" "The name of the second display, if any";
    display3 = mkOpt str "" "The name of the third display, if any";
  };

}
