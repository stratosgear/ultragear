{ options, config, pkgs, lib, ... }:

# NOTE:
# OBSOLETE:
# No idea why I still keep it around!
# The proper backups configuration is in: services/backups

with lib;
with lib.ultragear;
let
  cfg = config.ultragear.system.backups;
  excludes = "--exclude-file=" + (pkgs.writeText "restic-excludes.txt"
    (builtins.readFile ./excludes.txt + concatStringsSep "\n" cfg.exclude));
    in
    {
    options.ultragear.system.backups = with types;
  {

    # Options strongly favor a backup implementation based on restic
    enable = mkBoolOpt false "Whether or not to enable file backups support.";
    lanBackups = {
      enable = mkBoolOpt false "Whether or not to enable file backups to a local lan Restic backup server";
      server = mkOpt str "" "Hostname of the local lan backup server";
      port = mkOpt str "" "Server Port of the local lan Restic backup server";
    };
    remoteBackups = {
      enable = mkBoolOpt false "Whether or not to enable file backups to a remote backup server";
      server = mkOpt str "" "Hostname of the remote backup server";
    };
    user = mkOpt str "root" "The user to perform the backups as";
    actAsServer = mkBoolOpt false "Whether to act as a backup server";
    serverPort = mkOpt str "8009" "Restic server port number";
    serverDataDir = mkOpt str "/data/backups/asServer" "Location of backups for server backups";
    paths = mkOpt (listOf str) [ ] "List of paths to include in the normal daily system backups";
    repo = mkOpt str "/backups/restic" "The location of the local backups";
    exclude = mkOpt (listOf str) [ ] "List of paths to exclude from the normal daily system backups";
    systemdTimerConfig = mkOpt attrs
      {
        # Calendar Based
        # OnCalendar = "23h";
        # RandomizedDelaySec = "3h";
        # Will keep track of latest execution time, and upon reboot it will retrigger
        # missed executions
        # Use `systemctl clean --what=state <time-name>` to reset timer
        # https://www.freedesktop.org/software/systemd/man/latest/systemd.timer.html#Persistent=
        Persistent = true;
        AccuracySec = "5m";
        # Monotonic
        OnBootSec = "10min";
        OnUnitActiveSec = "17h";
      } "Backup frequency/timer";
    pruneOpts = mkOpt (listOf str) [
      "--keep-daily 10"
      "--keep-weekly 5"
      "--keep-monthly 13"
      "--keep-yearly 2"
    ] "Prune options for the backups";
  };

  config = mkIf cfg.enable {

    assertions = [
      {
        assertion = cfg.lanBackups.enable -> (cfg.lanBackups.server != "" && cfg.lanBackups.port != "");
        message = "If lanBackups.enabled then lanBackups.server and lanBackups.port need to be filled in.";
      }
      {
        assertion = cfg.remoteBackups.enable -> cfg.remoteBackups.server != "";
        message = "If remoteBackups are enabled the remoteBackupsServer option needs to be populated.";
      }

    ];

    sops.secrets = {
      "restic/localBackups/passphrase" = { };
      "restic/localBackups/hcPingUid" = { };
    } // (lib.optionalAttrs cfg.lanBackups.enable {
      "restic/lanBackups/passphrase" = { };
      "restic/lanBackups/hcPingUid" = { };
    }) // (lib.optionalAttrs cfg.remoteBackups.enable {
      "restic/remoteBackups/passphrase" = { };
      "restic/remoteBackups/hcPingUid" = { };
      "backblaze/restic/backups.env" = { };
    });

    # Default and hardcoded paths for Backups
    ultragear.system.backups.paths = [
      "/home/${config.ultragear.user.name}/Documents"
    ];
    ultragear.system.backups.exclude = [
      "/home/${config.ultragear.user.name}/Documents/syncthing"
    ];

    services.restic = {

      server = mkIf cfg.actAsServer {
        enable = true;
        dataDir = cfg.serverDataDir;
        privateRepos = true;
        listenAddress = ":${toString cfg.serverPort}";
        extraFlags = [
          "--no-auth"
        ];
      };

      backups = {
        localBackups = {
          user = cfg.user;
          # environmentFile = config.sops.secrets."hosts/${config.networking.hostName}/restic_env".path;
          passwordFile = config.sops.secrets."restic/localBackups/passphrase".path;
          paths = cfg.paths;
          repository = cfg.repo;
          timerConfig = cfg.systemdTimerConfig;
          initialize = true;
          # backupPrepareCommand = mkIf config.services.postgresql.enable ''
          #   mkdir -p /root/restic-backup
          #   ${pkgs.sudo}/bin/sudo -u postgres ${pkgs.postgresql}/bin/pg_dumpall | ${pkgs.zstd}/bin/zstd --rsyncable > /root/restic-backup/restic-postgres.sql.zst
          # '';
          # backupCleanupCommand = mkIf config.services.postgresql.enable ''
          #   rm -rf /root/restic-backup
          # '';
          pruneOpts = cfg.pruneOpts;
          # extraBackupArgs = lib.mkIf (cfg.excludes != [ ]) (builtins.map (x: "--exclude \"" + x + "\"") cfg.excludes);
          extraBackupArgs = [
            "--verbose=2"
            # Since backups are mostly background services we do not care about
            # progress estimates
            "--no-scan"
            "--compression max"
            excludes
          ];
          backupCleanupCommand = ''
            ${pkgs.runitor}/bin/runitor -api-url "https://${CONSTS.urls.healthchecks}/ping" -uuid $(cat ${config.sops.secrets."restic/localBackups/hcPingUid".path}) -- echo Lan backup success.
          '';
        };

        lanBackups = mkIf cfg.lanBackups.enable {
          user = cfg.user;
          # environmentFile = config.sops.secrets."hosts/${config.networking.hostName}/restic_env".path;
          passwordFile = config.sops.secrets."restic/lanBackups/passphrase".path;
          paths = cfg.paths;
          repository = "rest:http://${cfg.lanBackups.server}.local:${cfg.lanBackups.port}/${config.networking.hostName}/lanBackups/";
          timerConfig = cfg.systemdTimerConfig;
          initialize = true;
          # backupPrepareCommand = mkIf config.services.postgresql.enable ''
          #   mkdir -p /root/restic-backup
          #   ${pkgs.sudo}/bin/sudo -u postgres ${pkgs.postgresql}/bin/pg_dumpall | ${pkgs.zstd}/bin/zstd --rsyncable > /root/restic-backup/restic-postgres.sql.zst
          # '';
          # backupCleanupCommand = mkIf config.services.postgresql.enable ''
          #   rm -rf /root/restic-backup
          # '';
          pruneOpts = cfg.pruneOpts;
          # extraBackupArgs = lib.mkIf (cfg.excludes != [ ]) (builtins.map (x: "--exclude \"" + x + "\"") cfg.excludes);
          extraBackupArgs = [
            "--verbose=2"
            # Since backups are mostly background services we do not care about
            # progress estimates
            "--no-scan"
            "--compression max"
            excludes
          ];
          backupCleanupCommand = ''
            ${pkgs.runitor}/bin/runitor -api-url "https://${CONSTS.urls.healthchecks}/ping" -uuid $(cat ${config.sops.secrets."restic/lanBackups/hcPingUid".path}) -- echo Lan backup success.
          '';
        };

        remoteBackups = mkIf cfg.remoteBackups.enable {
          user = cfg.user;
          environmentFile = config.sops.secrets."backblaze/restic/backups.env".path;
          passwordFile = config.sops.secrets."restic/remoteBackups/passphrase".path;
          paths = cfg.paths;
          repository = "${cfg.remoteBackups.server}/${config.networking.hostName}";
          timerConfig = cfg.systemdTimerConfig;
          initialize = true;
          # backupPrepareCommand = mkIf config.services.postgresql.enable ''
          #   mkdir -p /root/restic-backup
          #   ${pkgs.sudo}/bin/sudo -u postgres ${pkgs.postgresql}/bin/pg_dumpall | ${pkgs.zstd}/bin/zstd --rsyncable > /root/restic-backup/restic-postgres.sql.zst
          # '';
          # backupCleanupCommand = mkIf config.services.postgresql.enable ''
          #   rm -rf /root/restic-backup
          # '';
          pruneOpts = cfg.pruneOpts;
          # extraBackupArgs = lib.mkIf (cfg.excludes != [ ]) (builtins.map (x: "--exclude \"" + x + "\"") cfg.excludes);
          extraBackupArgs = [
            "--verbose=2"
            # Since backups are mostly background services we do not care about
            # progress estimates
            "--no-scan"
            "--compression max"
            excludes
          ];
          backupCleanupCommand = ''
            ${pkgs.runitor}/bin/runitor -api-url "https://${CONSTS.urls.healthchecks}/ping" -uuid $(cat ${config.sops.secrets."restic/remoteBackups/hcPingUid".path}) -- echo Remote backup success.
          '';
        };
      };
    };

    networking.firewall.allowedTCPPorts = [
      (builtins.fromJSON cfg.serverPort)
    ];

  };
  }
