{ options, config, lib, pkgs, inputs, ... }:

with lib;
with lib.ultragear; let
  cfg = config.ultragear.suites.server;
in
{
  options.ultragear.suites.server = with types; {
    enable = mkBoolOpt true "Whether or not to isntall basic server functionality/traits";
  };

  config = mkIf cfg.enable {

    environment.systemPackages = with pkgs; [
      # inputs.superfile.packages.x86_64-linux.default
    ];

    # autoupgrade and autoreboot server on kernel upgrades
    # Not sure I like this!!!
    # system.autoUpgrade = {
    #   enable = true;
    #   allowReboot = true;
    #   rebootWindow = {
    #     lower = "01:00";
    #     upper = "07:00";
    #   };


    #   #  Not sure how this works
    #   # flake = "someflake";
    # };
  };
}
