{
  options,
  config,
  lib,
  pkgs,
  ...
}:

with lib;
with lib.ultragear;
let
  cfg = config.ultragear.suites.bootstrap;
in
{
  options.ultragear.suites.bootstrap = with types; {
    enable = mkBoolOpt false "Whether or not to enable basic minimum bootstrapping";
  };

  config = mkIf cfg.enable {

    # some really basic tools that ought to exist everywhere
    environment.systemPackages = [

    ];

    ultragear = {
      nix = {
        enable = true;
        extra-substituters = {
          "https://ultragear.cachix.org".key =
            "ultragear.cachix.org-1:fIBH3wFNWTVWBcn4E2RdrXfGrydSODB/t6boV52KavU=";
          "https://nix-community.cachix.org".key =
            "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs=";
          "https://cache.flox.dev".key = "flox-cache-public-1:7F4OyH7ZCnFhcze3fJdfyXYLQw/aV7GEed86nQ7IsOs=";
        };
      };

      tools = {
        git = enabled;
      };

      services = {
        openssh = enabled;
        # vscode-server = enabled;
      };

      system = {
        locale = enabled;
        time = enabled;
      };

      hardware = {
        networking = enabled;
      };
    };
  };
}
