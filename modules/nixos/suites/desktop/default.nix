{
  options,
  config,
  lib,
  pkgs,
  ...
}:
with lib;
with lib.ultragear;
let
  cfg = config.ultragear.suites.desktop;
in
{
  options.ultragear.suites.desktop = with types; {
    enable = mkBoolOpt false "Whether or not to enable desktop configuration.";
  };

  config = mkIf cfg.enable {

    ultragear = {

      desktop = {

        i3 = enabled;

      };

      apps = {
        firefox = enabled;
        chromium = enabled;
      };

      services = {
        printing = enabled;
      };

      system = {
        fonts = enabled;
        xkb = enabled;
      };

    };

    services.udisks2.enable = true;

    # Runs a color correction daemon, but don't know how it works
    # services.colord.enable = true;

    environment.systemPackages = [
      pkgs.xfce.mousepad # text editor
      pkgs.evince # pdf viewer
      pkgs.file-roller # archive manager

      pkgs.arandr # display manager

      pkgs.mpv # video player
      pkgs.qimgv # image viewer

      pkgs.udiskie # manage usb external disks

      # Maybe reinstall when wayland issues are fixed
      #pkgs.deskflow # share mouse keyboard across computers
    ];

  };
}
