{
  options,
  config,
  lib,
  pkgs,
  inputs,
  ...
}:
with lib;
with lib.ultragear;
let
  cfg = config.ultragear.suites.development;
in
{
  options.ultragear.suites.development = with types; {
    enable = mkBoolOpt false "Whether or not to enable common development configuration.";
  };

  config = mkIf cfg.enable {

    networking.firewall.allowedTCPPorts = [
    ];

    services = {
      # Fixes various issues of missing /bin/bash (for example running python invoke
      # scripts)
      # https://github.com/Mic92/envfs
      envfs.enable = true;

    };

    environment.systemPackages = with pkgs; [
      openfortivpn
      teams-for-linux
      sshfs
      dbeaver-bin
      difftastic
      devenv
      cachix
      age
      distrobox
      devpod-desktop
      devpod
      vscodium-fhs
      # disabling since it does not build
      # https://github.com/NixOS/nixpkgs/issues/355157
      # https://github.com/NixOS/nixpkgs/pull/355366
      # gitbutler
      plantuml
      graphviz
      meld
      ansible

      #python
      python3
      uv

      # ripgrep
      ripgrep

      # qemu emulation
      quickemu

      # flox environments
      inputs.flox.packages.${pkgs.system}.default
    ];

    environment.etc = {
      "openfortivpn/config".text = ''
        ### configuration file for openfortivpn, see man openfortivpn(1) ###
        host = vpn.planetek.it
        port = 443
        username = gerakakis
        pppd-use-peerdns = 1
        pppd-accept-remote = 1
        # persistent = 60
      '';
    };

    ultragear = {

      apps = {
        vscode = enabled;
      };

      cli-apps = { };

      tools = {
        # at = enabled;
        direnv = enabled;
        # go = enabled;
        # http = enabled;
        # qmk = enabled;
        podman = enabled;
      };

    };
  };
}
