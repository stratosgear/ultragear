{
  options,
  config,
  lib,
  pkgs,
  ...
}:
with lib;
with lib.ultragear;
let
  cfg = config.ultragear.suites.common;
in
{
  options.ultragear.suites.common = with types; {
    enable = mkBoolOpt false "Whether or not to enable common configuration.";
  };

  config = mkIf cfg.enable {

    # some really basic tools that ought to exist everywhere
    environment = {
      systemPackages = with pkgs; [
        imagemagick
        btop
        lolcat
        neofetch
        tree
        lsof
        micro
        ncdu
        inetutils
        pv
        snowfallorg.flake
        lazygit
        rclone
        # Shows installed package version differences
        # https://blog.tjll.net/previewing-nixos-system-updates/
        nvd

        # custom scripts
        ultragear.list-iommu
        ultragear.ug-albumify
        ultragear.ug-coverMosaic
        ultragear.ug-ultragear
        ultragear.nixos-cli

        # TODO: These are manually installed as dependencies of other packages
        # I need to find a way to install packages as deps of python scripts.
        # See: yt-albumify
        ffmpeg
        id3v2
        yt-dlp
        xdotool
      ];
      variables = {
        EDITOR = "micro";
      };
    };

    services = {
      avahi = {
        enable = true;
        nssmdns4 = true;
        publish.enable = true;
        publish.addresses = true;
        publish.workstation = true;
        openFirewall = true;
      };
    };

    ultragear = {

      cli-apps = {
        tmux = enabled;
        zellij = enabled;
        yazi = enabled;
      };

      tools = {
        misc = enabled;
        comma = enabled;

        # Magically fixes issues of type:
        # Could not start dynamically linked executable: /projects/hypervasis/FlowersOfEvil/repos/flowers-webapp/backend/.venv/bin/python3
        # NixOS cannot run dynamically linked executables intended for generic
        # linux environments out of the box. For more information, see:
        # https://nix.dev/permalink/stub-ld
        nix-ld = enabled;

      };

      hardware = {
        storage = enabled;
      };

      security = {
        sops = enabled;
        gpg = enabled;
      };

      system = {
        boot = enabled;
        locale = enabled;
        time = enabled;
      };

      services = {
        tailscale = enabled;
        scrutiny.collector = true;

        # zabbix = {
        #   enable = true;
        #   agent = true;
        #   agentConnectTo = CONSTS.urls.zabbix;
        #   agentOpenFirewall = true;
        # };
      };
    };
  };
}
