{ options, config, pkgs, lib, ... }:

with lib;
with lib.ultragear;
let cfg = config.ultragear.custom.musicLib;
in
{
  options.ultragear.custom.musicLib = with types; {
    enable = mkBoolOpt false "Whether or not to enable hosting of the music library on this host";
    dataDir = mkOpt str "" "Location where the music library is stored";
  };

  config = mkIf cfg.enable {

    users = {

      users.syncthing = mkIf config.ultragear.services.syncthing.enable {
        extraGroups = [ "musiclib" ];
      };
      users."${config.ultragear.services.navidrome.user}" = mkIf config.ultragear.services.navidrome.enable {
        extraGroups = [ "musiclib" ];
      };
    };

    systemd.tmpfiles.rules = [
      "d ${cfg.dataDir} 0770 ${config.ultragear.user.name} musiclib -"
      "Z ${cfg.dataDir} 0770 ${config.ultragear.user.name} musiclib -"
    ];
  };
}
