{ options
, config
, pkgs
, lib
, ...
}:
with lib;
with lib.ultragear; let
  cfg = config.ultragear.user;
  defaultIconFileName = "profile.jpg";
  defaultIcon = pkgs.stdenvNoCC.mkDerivation {
    name = "default-icon";
    src = ./. + "/${defaultIconFileName}";

    dontUnpack = true;

    installPhase = ''
      cp $src $out
    '';

    passthru = { fileName = defaultIconFileName; };
  };
  propagatedIcon =
    pkgs.runCommandNoCC "propagated-icon"
      { passthru = { fileName = cfg.icon.fileName; }; }
      ''
        local target="$out/share/ultragear-icons/user/${cfg.name}"
        mkdir -p "$target"

        cp ${cfg.icon} "$target/${cfg.icon.fileName}"
      '';
in
{
  options.ultragear.user = with types; {
    name = mkOpt str "stratos" "The name to use for the user account.";
    fullName = mkOpt str "Stratos Gerakakis" "The full name of the user.";
    email = mkOpt str "stratos@gerakakis.net" "The email of the user.";
    initialPassword =
      mkOpt str "password"
        "The initial password to use when the user is first created.";
    icon =
      mkOpt (nullOr package) defaultIcon
        "The profile picture to use for the user.";
    prompt-init = mkBoolOpt true "Whether or not to show an initial message when opening a new shell.";
    extraGroups = mkOpt (listOf str) [ ] "Groups for the user to be assigned.";
    extraOptions =
      mkOpt attrs { }
        (mdDoc "Extra options passed to `users.users.<name>`.");
  };

  config = {
    environment.systemPackages = with pkgs; [
      # cowsay
      # fortune
      # lolcat
      # ultragear.cowsay-plus
      propagatedIcon
    ];

    programs.zsh = {
      enable = true;
      autosuggestions.enable = true;
      histFile = "$XDG_CACHE_HOME/zsh.history";
    };

    ultragear.home = {
      file = {
        "Desktop/.keep".text = "";
        "Documents/.keep".text = "";
        "Downloads/.keep".text = "";
        "Music/.keep".text = "";
        "Pictures/.keep".text = "";
        "Videos/.keep".text = "";
        "work/.keep".text = "";
        ".face".source = cfg.icon;
        "Pictures/${
          cfg.icon.fileName or (builtins.baseNameOf cfg.icon)
        }".source =
          cfg.icon;
      };

      extraOptions = {
        home.shellAliases = {
          lc = "${pkgs.colorls}/bin/colorls --sd";
          lcg = "lc --gs";
          lcl = "lc -1";
          lclg = "lc -1 --gs";
          lcu = "${pkgs.colorls}/bin/colorls -U";
          lclu = "${pkgs.colorls}/bin/colorls -U -1";
        };

      };
    };

    users = {

      # Create some custom groups
      groups.musiclib = {
        gid = 27001;
      };

      # create main user
      users.${cfg.name} =
        {
          isNormalUser = true;

          # inherit (cfg) name initialPassword;
          inherit (cfg) name;

          home = "/home/${cfg.name}";
          group = "users";
          hashedPassword = "$6$fth8aNS.CWpcGJkD$eKQI2wc3n7zib6Rpw50sQpILRkkJNtaDLJoW/zHwfMMappd3rYZNdagO5lRFuld0XgHL7wgJcgpFaP.abO9Wf/";

          shell = pkgs.zsh;

          # Arbitrary user ID to use for the user. Since I only
          # have a single user on my machines this won't ever collide.
          # However, if you add multiple users you'll need to change this
          # so each user has their own unique uid (or leave it out for the
          # system to select).
          uid = 1000;

          extraGroups = [
            "wheel"
            "musiclib"
            # Allows access to serial ports
            "dialout"
          ] ++ cfg.extraGroups;
        }
        // cfg.extraOptions;
    };
  };
}
