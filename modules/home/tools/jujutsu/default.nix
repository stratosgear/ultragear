{
  lib,
  config,
  pkgs,
  inputs,
  ...
}:

let
  inherit (lib) types mkEnableOption mkIf;
  inherit (lib.ultragear) mkOpt enabled;

  cfg = config.ultragear.home.tools.jujutsu;
  user = config.ultragear.home.user;
in
{
  options.ultragear.home.tools.jujutsu = {
    enable = mkOpt types.bool false "Whether to enable jujutsu";
    userName = mkOpt types.str user.fullName "The name to configure jujutsu with.";
    userEmail = mkOpt types.str user.email "The email to configure jujutsu with.";
    signingKey = mkOpt types.str "/home/stratos/.ssh/id_ed25519.pub" "The key ID to sign commits with.";
    signByDefault = mkOpt types.bool true "Whether to sign commits by default.";
  };

  config = mkIf cfg.enable {
    programs = {
      jujutsu = {
        enable = true;
        settings = {
          user = {
            name = cfg.userName;
            email = cfg.userEmail;
          };
          signing = {
            sign-all = cfg.signByDefault;
            backend = "ssh";
            key = cfg.signingKey;
          };
        };
      };
    };
  };
}
