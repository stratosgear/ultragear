{ config, lib, options, pkgs, inputs, ... }:

# Inspiration from:
# https://github.com/ironman820/home-manager/blob/a6761aefd998262a2db41f675b078bb7b443917b/homes/x86_64-linux/niceastman%40e105-laptop/default.nix

let
  inherit (lib) mkAliasDefinitions mkDefault mkIf mkMerge;
  inherit (lib.ultragear) mkBoolOpt mkOpt;
  inherit (lib.types) attrs path;
  inherit (inputs) sops-nix;

  cfg = config.ultragear.home.tools.sops;
  mode = "0400";
  # sopsFile = ./secrets/work-keys.yaml;

in
{

  # NOTE: Needed to be imported here otherwise wouldn't work
  # https://github.com/KingdomInternal-dev-mipa/apex-hbb/blob/57ee69281c3f071367b9d994514c91e2ad32d3fb/modules/home/security/sops/default.nix#L15
  imports = [
    sops-nix.homeManagerModules.sops
  ];

  options.ultragear.home.tools.sops = {
    enable = mkBoolOpt true "Enable root secrets";
    age = mkOpt attrs { } "Age Attributes";
    defaultSopsFile = mkOpt path ./secrets/vault.yaml "Default SOPS file path for all users/hosts";
    install = mkBoolOpt false "Install sops in home manager";
    secrets = mkOpt attrs { } "SOPS secrets.";
  };

  config = mkIf cfg.enable
    {

      ultragear.home.tools.sops = {
        age = {
          keyFile = "${config.xdg.configHome}/sops/age/keys.txt";
          sshKeyPaths = [ ];
        };
      };

      home.packages = mkIf cfg.install (with pkgs; [ sops ]);

      home.activation.setupEtc = config.lib.dag.entryAfter [ "writeBoundary" ] ''
        /run/current-system/sw/bin/systemctl start --user sops-nix
      '';

      sops = {
        age = mkAliasDefinitions options.ultragear.home.tools.sops.age;
        defaultSopsFile =
          mkAliasDefinitions options.ultragear.home.tools.sops.defaultSopsFile;
        gnupg.sshKeyPaths = [ ];
        secrets = mkAliasDefinitions options.ultragear.home.tools.sops.secrets;
      };
    };
}
