{ options, config, lib, pkgs, ... }:

with lib;
with lib.ultragear;
let cfg = config.ultragear.home.tools.keychain;
in
{
  options.ultragear.home.tools.keychain = with types; {
    enable = mkBoolOpt false "Whether or not to enable keychain agent.";
  };

  config = mkIf cfg.enable {

    programs.keychain = {
      enable = true;
      keys = [ "id_ed25519" ];
      enableZshIntegration = config.ultragear.home.cli-apps.zsh.enable;
    };

  };
}
