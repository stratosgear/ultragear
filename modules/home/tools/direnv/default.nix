{ options, config, lib, pkgs, ... }:

with lib;
with lib.ultragear;
let cfg = config.ultragear.home.tools.direnv;
in
{
  options.ultragear.home.tools.direnv = with types; {
    enable = mkBoolOpt false "Whether or not to enable direnv.";
  };

  config = mkIf cfg.enable {
    programs.direnv = {
      enable = true;
      nix-direnv = enabled;
    };
  };
}
