{
  lib,
  config,
  pkgs,
  inputs,
  ...
}:

let
  inherit (lib) types mkEnableOption mkIf;
  inherit (lib.ultragear) mkOpt enabled;
  inherit (inputs) git-alias;

  cfg = config.ultragear.home.tools.git;
  user = config.ultragear.home.user;
in
{
  options.ultragear.home.tools.git = {
    enable = mkEnableOption "Git";
    userName = mkOpt types.str user.fullName "The name to configure git with.";
    userEmail = mkOpt types.str user.email "The email to configure git with.";
    signingKey = mkOpt types.str "/home/stratos/.ssh/id_ed25519.pub" "The key ID to sign commits with.";
    signByDefault = mkOpt types.bool true "Whether to sign commits by default.";
  };

  config = mkIf cfg.enable {
    programs = {
      gitui = {
        enable = true;
        # Theme is catpuccin machiato
        # https://github.com/catppuccin/gitui
        theme = ''
                (
              selected_tab: Some("Reset"),
              command_fg: Some("#cad3f5"),
              selection_bg: Some("#5b6078"),
              selection_fg: Some("#cad3f5"),
              cmdbar_bg: Some("#1e2030"),
              cmdbar_extra_lines_bg: Some("#1e2030"),
              disabled_fg: Some("#8087a2"),
              diff_line_add: Some("#a6da95"),
              diff_line_delete: Some("#ed8796"),
              diff_file_added: Some("#a6da95"),
              diff_file_removed: Some("#ee99a0"),
              diff_file_moved: Some("#c6a0f6"),
              diff_file_modified: Some("#f5a97f"),
              commit_hash: Some("#b7bdf8"),
              commit_time: Some("#b8c0e0"),
              commit_author: Some("#7dc4e4"),
              danger_fg: Some("#ed8796"),
              push_gauge_bg: Some("#8aadf4"),
              push_gauge_fg: Some("#24273a"),
              tag_fg: Some("#f4dbd6"),
              branch_fg: Some("#8bd5ca")
          )
        '';
      };
      git = {
        enable = true;
        difftastic = {
          enable = true;
          background = "dark";
          color = "auto";
        };
        inherit (cfg) userName userEmail;
        lfs = enabled;
        signing = {
          key = cfg.signingKey;
          inherit (cfg) signByDefault;
        };

        extraConfig = {
          init = {
            defaultBranch = "master";
          };
          pull = {
            rebase = true;
          };
          push = {
            autoSetupRemote = true;
          };
          core = {
            whitespace = "trailing-space,space-before-tab";
          };
          safe = {
            directory = "${user.home}/work/config";
          };
          gpg = {
            format = "ssh";
          };
          # git-alias must be defined as input in flake.nix
          include.path = "${git-alias}/gitalias.txt";

          diff = {
            tool = "meld";
          };
        };
      };
    };
  };
}
