{ lib, config, pkgs, ... }:
with lib;
with lib.ultragear;
let
  inherit (lib) types mkEnableOption mkIf;
  cfg = config.ultragear.home.tools.ssh;
in
{
  options.ultragear.home.tools.ssh = {
    enable = mkEnableOption "SSH";
  };

  config = mkIf cfg.enable {

    programs.ssh = {
      extraConfig = ''
        Host *
          HostKeyAlgorithms +ssh-ed25519
      '';
    };
  };
}
