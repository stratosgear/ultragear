{ lib
, config
, pkgs
, ...
}:
with lib;
with lib.ultragear; let
  cfg = config.ultragear.home.desktop.assets;
in
{
  options.ultragear.home.desktop.assets = {
    enable = mkEnableOption "Enable file assets in ~/assets ";
  };

  config = mkIf cfg.enable {
    home.file."assets" = {
      source = ./../../../../assets;
      recursive = true;
    };
  };
}
