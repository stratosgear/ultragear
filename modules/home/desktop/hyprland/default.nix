{
  options,
  config,
  lib,
  pkgs,
  inputs,
  osConfig ? { },
  ...
}:

with lib;
with lib.ultragear;
let
  waybar_config = pkgs.substituteAll {
    src = ./config/waybar/config;
    modules_right = osConfig.ultragear.desktop.hyprland.waybar_modules_right;
  };

  hyprland-vars = pkgs.writeShellScriptBin "hyprland-vars.sh" (
    builtins.readFile ./scripts/hyprland_vars.sh
  );

  hyprland-keybinds = pkgs.writeShellScriptBin "hyprland-keybinds.sh" (
    builtins.readFile ./scripts/hyprland_keybinds.sh
  );

in
{

  config = mkIf osConfig.ultragear.desktop.hyprland.enable or false {

    ultragear.home.tools.sops.secrets = {
      "apeKey" = {
        mode = "0400";
        path = "${config.home.homeDirectory}/.config/ug-monkeytype-waybar/apekey";
      };
    };

    wayland.windowManager.hyprland = {
      enable = true;

      plugins = [
        # inputs.hyprland-plugins.packages.${pkgs.stdenv.hostPlatform.system}.<plugin>
        # pkgs.hyprlandPlugins.hyprexpo
        # pkgs.hyprlandPlugins.hyprtrails
        # inputs.hyprland-plugins.packages.${pkgs.system}.hyprscroller
        pkgs.hyprlandPlugins.hyprscroller
        # pkgs.hyprlandPlugins.hy3
      ];

      settings = {
        # monitors
        # see https://wiki.hyprland.org/Configuring/Monitors/
        monitor = osConfig.ultragear.desktop.hyprland.monitors;

        # startup (custom)
        "exec-once" = [
          "nm-applet --indicator"
          "blueman-applet"
          "waybar"
          "swaync"
          "udiskie"
          "hypridle"
          "wpaperd -d"
          "copyq --start-server"
          "hyprpm reload -n"
          "${pkgs.wlsunset}/bin/wlsunset -l 40.4 -L -3.7 -t 3800"
          "gsettings set org.gnome.desktop.interface cursor-theme BreezeX-RosePine-Linux"
          "gsettings set org.gnome.desktop.interface cursor-size 24"
          "hyprctl setcursor BreezeX-RosePine-Linux 24"
          "gsettings set org.gnome.desktop.interface gtk-theme \"arc-dark\""
          "gsettings set org.gnome.desktop.interface color-scheme \"prefer-dark\""
        ];

        # my programs
        # see https://wiki.hypland.org/Configuring/Keywords/
        "$terminal" = "ghostty";
        "$fileManager" = "pcmanfm";
        "$menu" = "rofi -show drun -show-icons -config ~/.config/rofi/appsMenu.rasi";
        "$runningApps" = "rofi -show window -show-icons -config ~/.config/rofi/appsMenu.rasi";

        # "$bar" = "/home/jamison/nixos/home/hyprland/reload.sh";

        # environment variables
        # see https://wiki.hypland.org/Configuring/Environment-variables/
        env = [
          "XCURSOR_SIZE,22"
          "HYPRCURSOR_SIZE,24"
          "HYPRCURSOR_THEME,rose-pine-hyprcursor"
          "XCURSOR_THEME,rose-pine-hyprcursor"
          "QT_QPA_PLATFORMTHEME,qt6ct"
        ];

        # look and feel
        # see https://wiki.hypland.org/Configuring/Variables/
        general = {
          gaps_in = 2;
          gaps_out = 0;

          border_size = 2;

          "col.active_border" = "rgba(33ccffee) rgba(00ff99ee) 45deg";
          "col.inactive_border" = "rgba(595959aa)";

          resize_on_border = false;

          allow_tearing = false;

          # layout = "dwindle";
          layout = "scroller";
        };

        cursor = {
          # avoids an annoying issue on Freecad, where tooltips make the cursor center
          # on the window
          no_warps = true;
        };

        # see https://wiki.hypland.org/Configuring/Variables/#decoration
        decoration = {
          rounding = 2;

          # change transparency of focused and unfocused windows
          fullscreen_opacity = 1.0;
          active_opacity = 1.0;
          inactive_opacity = 1.0;

          shadow = {
            enabled = true;
            range = 4;
            render_power = 3;
            color = "rgba(1a1a1aee)";
          };

          blur = {
            enabled = true;
            size = 3;
            passes = 2;
            vibrancy = 0.1696;
          };
        };

        # see https://wiki.hypland.org/Configuring/Variables/#animations
        animations = {
          enabled = true;

          bezier = [
            "easeOutQuint,0.23,1,0.32,1"
            "easeInOutCubic,0.65,0.05,0.36,1"
            "linear,0,0,1,1"
            "almostLinear,0.5,0.5,0.75,1.0"
            "quick,0.15,0,0.1,1"
          ];

          animation = [
            "global, 1, 10, default"
            "border, 1, 5.39, easeOutQuint"
            "windows, 1, 4.79, easeOutQuint"
            "windowsIn, 1, 4.1, easeOutQuint, popin 87%"
            "windowsOut, 1, 1.49, linear, popin 87%"
            "fadeIn, 1, 1.73, almostLinear"
            "fadeOut, 1, 1.46, almostLinear"
            "fade, 1, 3.03, quick"
            "layers, 1, 3.81, easeOutQuint"
            "layersIn, 1, 4, easeOutQuint, fade"
            "layersOut, 1, 1.5, linear, fade"
            "fadeLayersIn, 1, 1.79, almostLinear"
            "fadeLayersOut, 1, 1.39, almostLinear"
            "workspaces, 1, 1.94, almostLinear, fade"
            "workspacesIn, 1, 1.21, almostLinear, fade"
            "workspacesOut, 1, 1.94, almostLinear, fade"
          ];
        };

        # see https://wiki.hypland.org/Configuring/Dwindle-Layout/
        dwindle = {
          pseudotile = true;
          preserve_split = true;
        };

        plugin = {
          scroller = {
            focus_wrap = 0;
            column_widths = "onesixth onethird onehalf twothirds fivesixths one";
            monitor_options = "(HDMI-A-1=(column_default_width=onethird;window_default_height=one),DP-1=(column_default_width=onehalf;window_default_height=one))";
          };
        };

        # see https://wiki.hypland.org/Configuring/Master-Layout/
        master = {
          new_status = "master";
        };

        # see https://wiki.hypland.org/Configuring/Variables/#misc
        misc = {
          force_default_wallpaper = -1;
          disable_hyprland_logo = true;
        };

        # input
        # see https://wiki.hypland.org/Configuring/Variables/#misc
        input = {
          kb_layout = "us,gr";
          kb_variant = "";
          kb_model = "";

          # caps:super ; Make Caps Lock act as Super
          # grp:alt_space_toggle : Switch locales with alt+space
          # fkeys:basic_13-24 : fkeys f13-f24 will map to basic Fxx keycodes and NOT XF86 keys (XF86Tools, etc)
          kb_options = "caps:super, grp:alt_space_toggle fkeys:basic_13-24";

          kb_rules = "";

          follow_mouse = 2;

          sensitivity = 0;

          touchpad = {
            natural_scroll = true;
          };
        };

        # see https://wiki.hypland.org/Configuring/Variables/#gestures
        gestures = {
          workspace_swipe = false;
        };

        # see https://wiki.hypland.org/Configuring/Variables/#gestures
        device = {
          name = "logitech-wireless-mouse-mx-master-1";
          sensitivity = -0.5;
        };

        # keybindings
        # Example binds, see https://wiki.hyprland.org/Configuring/Binds/ for more

        # see https://wiki.hypland.org/Configuring/Keywords/
        "$mainMod" = "SUPER";

        bind = [
          "$mainMod, Return, exec, $terminal                                    # Open Terminal"
          "$mainMod SHIFT, KP_Enter, exec, $fileManager                         # Open File Manager"

          # How weird: On my Chuwi laptop the Enter key emits a: KP_Enter and not a Return keycode
          "$mainMod, KP_Enter, exec, $terminal                                  # Open Terminal"
          "$mainMod SHIFT, Return, exec, $fileManager                           # Open File Manager"

          "$mainMod SHIFT, Q, killactive,                                       # Kill current window"
          "$mainMod, period, exec, $menu                                        # Open Application Menu"
          "$mainMod, comma, exec, $runningApps                                  # Open Application Menu"
          # "$mainMod, P, pseudo, # dwindle"
          # "$mainMod, J, togglesplit, # dwindle"

          # Toggle between last window
          "$mainMod, Z, focuscurrentorlast,                                     # Toggle last window"
          "$mainMod, U, focusurgentorlast,                                      # Toggle urgent window"

          # Floating windows
          "$mainMod, V, togglefloating,                                         # Float window"
          "$mainMod, C, centerwindow,                                           # Center floating window"
          "$mainMod, P, pin,                                                    # Pin window"

          # Fullscreen
          "$mainMod, F, fullscreen,                                             # Toggle Fullscreen"
          "$mainMod SHIFT, F, scroller:fitsize, active                          # Set window width to full screen width"
          "$mainMod CONTROL, F, scroller:fitsize, visible                       # Set visible windows to fit full screen width"

          # fullscreen with NO border
          #"$mainMod SHIFT, F, fullscreen, 1                                     # Toggle Fullscreen (with border)"

          #screenshoting
          ''$mainMod,         M, exec, grim -g "$(slurp)" -t ppm - | satty --filename - --output-filename "/tmp/satty-$(date '+%Y%m%d-%H:%M:%S').png"      # Screenshot area''
          # screenshoting window snippet from: https://github.com/emersion/slurp/issues/16#issuecomment-2374038213
          ''$mainMod SHIFT,   M, exec, grim -g "$(hyprctl clients -j | jq -r --argjson active "$(hyprctl monitors -j | jq -c '[.[].activeWorkspace.id]')" '.[] | select((.hidden | not) and (.workspace.id as $id | $active | contains([$id]))) | "\(.at[0]),\(.at[1]) \(.size[0])x\(.size[1])"' | slurp)" -t ppm - | satty --filename - --output-filename "/tmp/satty-$(date '+%Y%m%d-%H:%M:%S').png"   # Screenshot window''
          ''$mainMod CONTROL, M, exec, grim -g "$(slurp -o)" -t ppm - | satty --filename -  --output-filename "/tmp/satty-$(date '+%Y%m%d-%H:%M:%S').png"   # Screenshot monitor''

          # Groups
          # Not to be used with hyprscroller
          #"$mainMod, G, togglegroup,                                            # Create window group"

          # Move into group
          # "$mainMod CONTROL, left, moveintogroup, l                             # Move window left into group"
          # "$mainMod CONTROL, right, moveintogroup, r                            # Move window right into group"
          # "$mainMod CONTROL, up, moveintogroup, u                               # Move window up into group"
          # "$mainMod CONTROL, down, moveintogroup, d                             # Move window down into group"

          # Move out of group
          # "$mainMod ALT, left, moveoutofgroup, l                                # Move window left out of group"
          # "$mainMod ALT, right, moveoutofgroup, r                               # Move window right out of group"
          # "$mainMod ALT, up, moveoutofgroup, u                                  # Move window up out of group"
          # "$mainMod ALT, down, moveoutofgroup, d                                # Move window down out of group"

          # Cycle between groupped windows
          # "$mainMod, bracketleft, changegroupactive, b                          # Cycle left into grouped windows"
          # "$mainMod, bracketright, changegroupactive, f                         # Cycle right into grouped windows"

          # Move focus with mainMod + arrow keys
          "$mainMod, left, movefocus, l                                         # Focus window left"
          "$mainMod, right, movefocus, r                                        # Focus window right"
          "$mainMod, up, movefocus, u                                           # Focus window up"
          "$mainMod, down, movefocus, d                                         # Focus window down"

          # Move window with mainMod + arrow keys
          "$mainMod SHIFT, left, movewindow, l                                  # Move window left"
          "$mainMod SHIFT, right, movewindow, r                                 # Move window right"
          "$mainMod SHIFT, up, movewindow, u                                    # Move window up"
          "$mainMod SHIFT, down, movewindow, d                                  # Move window down"

          # Switch workspaces with mainMod + [0-9]
          "$mainMod, 1, workspace, 1                                            # Switch to workspace 1"
          "$mainMod, 2, workspace, 2                                            # Switch to workspace 2"
          "$mainMod, 3, workspace, 3                                            # Switch to workspace 3"
          "$mainMod, 4, workspace, 4                                            # Switch to workspace 4"
          "$mainMod, 5, workspace, 5                                            # Switch to workspace 5"
          "$mainMod, 6, workspace, 6                                            # Switch to workspace 6"
          "$mainMod, 7, workspace, 7                                            # Switch to workspace 7"
          "$mainMod, 8, workspace, 8                                            # Switch to workspace 8"
          "$mainMod, 9, workspace, 9                                            # Switch to workspace 9"
          "$mainMod, 0, workspace, 10                                           # Switch to workspace 0"

          # Move active window to a workspace with mainMod + SHIFT + [0-9]
          "$mainMod SHIFT, 1, movetoworkspace, 1                                # Move window to workspace 1"
          "$mainMod SHIFT, 2, movetoworkspace, 2                                # Move window to workspace 2"
          "$mainMod SHIFT, 3, movetoworkspace, 3                                # Move window to workspace 3"
          "$mainMod SHIFT, 4, movetoworkspace, 4                                # Move window to workspace 4"
          "$mainMod SHIFT, 5, movetoworkspace, 5                                # Move window to workspace 5"
          "$mainMod SHIFT, 6, movetoworkspace, 6                                # Move window to workspace 6"
          "$mainMod SHIFT, 7, movetoworkspace, 7                                # Move window to workspace 7"
          "$mainMod SHIFT, 8, movetoworkspace, 8                                # Move window to workspace 8"
          "$mainMod SHIFT, 9, movetoworkspace, 9                                # Move window to workspace 9"
          "$mainMod SHIFT, 0, movetoworkspace, 10                               # Move window to workspace 0"

          # Move active workspace to other monitor
          "$mainMod ALT, left, movecurrentworkspacetomonitor, l                 # Move workspace to monitor left"
          "$mainMod ALT, right, movecurrentworkspacetomonitor, r                # Move workspace to monitor right"
          "$mainMod ALT, up, movecurrentworkspacetomonitor, u                   # Move workspace to monitor up"
          "$mainMod ALT, down, movecurrentworkspacetomonitor, d                 # Move workspace to monitor down"

          # Example special workspace (scratchpad)
          "$mainMod, S, togglespecialworkspace, magic                           # Show scratchpad"
          "$mainMod SHIFT, S, movetoworkspace, special:magic                    # Move window to scratchpad"

          # Hyprscroller keybinds
          # Submap Window resize for hyprscroller
          # full resize keymap defined below in extraConfig
          "$mainMod SHIFT, R, submap, resize                                    # Resize windows submap"

          "$mainMod, bracketright, scroller:cyclesize, 1                        # Resize to next column width size"
          "$mainMod, bracketleft, scroller:cyclesize, -1                        # Resize to previous column width size"

          "$mainMod ALT, comma, scroller:setmode, col                           # HIDEME"
          "$mainMod ALT, comma, scroller:admitwindow,                           # Admit window under the left column "
          "$mainMod ALT, comma, scroller:fitsize, all                           # HIDEME"
          "$mainMod ALT, comma, scroller:setmode, row                           # HIDEME"
          "$mainMod ALT, period,  scroller:setmode, col                         # HIDEME"
          "$mainMod ALT, period,  scroller:expelwindow,                         # Expel window to new column on the right"
          "$mainMod ALT, period,  scroller:movefocus, l                         # HIDEME"
          "$mainMod ALT, period,  scroller:fitsize, all                         # HIDEME"
          "$mainMod ALT, period,  scroller:movefocus, r                         # HIDEME"
          "$mainMod ALT, period,  scroller:setmode, row                         # HIDEME"

          "$mainMod SHIFT, comma, scroller:alignwindow, left                    # Align column to left side of monitor"
          "$mainMod SHIFT, period, scroller:alignwindow, right                  # Align column to right side of monitor"
          "$mainMod SHIFT, C, scroller:alignwindow, center                      # Align column to center of monitor"
          "$mainMod, TAB, scroller:toggleoverview,        r                     # Show window overview"
          # END Hyprscroller keybinds

          # core hyprland functionality
          "ALT CONTROL, H, exec, copyq show                                     # Open clipboard manager"

          # Show keybindings
          "$mainMod SHIFT, slash, exec, ${hyprland-keybinds}/bin/hyprland-keybinds.sh   # Show Keybindings"

          # OBS Global keybinds
          # https://wiki.hyprland.org/hyprland-wiki/pages/Configuring/Binds/#global-keybinds
          "SHIFT,F12,pass,^(com\.obsproject\.Studio)$                           # Switch selfie to top left"
          "CONTROL,F12,pass,^(com\.obsproject\.Studio)$                         # Switch selfie to top right"
          "ALT,F12,pass,^(com\.obsproject\.Studio)$                             # Switch selfie to bottom right"
          "SHIFT CONTROL,F12,pass,^(com\.obsproject\.Studio)$                   # Switch selfie to bottom left"
          "SHIFT SUPER,F12,pass,^(com\.obsproject\.Studio)$                     # Switch selfie to bottom left"
          "SUPER,F12,pass,^(com\.obsproject\.Studio)$                           # Switch selfie to bottom left"
        ];

        binde = [
          # Live Decoration settings
          "$mainMod SHIFT,   bracketright , exec , sh ${hyprland-vars}/bin/hyprland-vars.sh --inc_gaps_in               # Increase inner gaps"
          "$mainMod SHIFT,   bracketleft  , exec , sh ${hyprland-vars}/bin/hyprland-vars.sh --dec_gaps_in               # Decrease inner gaps"
          "$mainMod CONTROL, bracketright , exec , sh ${hyprland-vars}/bin/hyprland-vars.sh --inc_gaps_out              # Increase outer gaps"
          "$mainMod CONTROL, bracketleft  , exec , sh ${hyprland-vars}/bin/hyprland-vars.sh --dec_gaps_out              # Decrease outer gaps"
          "$mainMod SHIFT,   semicolon    , exec , sh ${hyprland-vars}/bin/hyprland-vars.sh --inc_border                # Increase window border"
          "$mainMod SHIFT,   apostrophe   , exec , sh ${hyprland-vars}/bin/hyprland-vars.sh --dec_border                # Decrease window border"
          "$mainMod CONTROL, semicolon    , exec , sh ${hyprland-vars}/bin/hyprland-vars.sh --inc_rounding              # Increase window corner rounding"
          "$mainMod CONTROL, apostrophe   , exec , sh ${hyprland-vars}/bin/hyprland-vars.sh --dec_rounding              # Decrease window corner rounding"
          "$mainMod SHIFT,   equal        , exec , sh ${hyprland-vars}/bin/hyprland-vars.sh --reset                     # Reset decorations"
        ];

        bindm = [
          # Move/resize windows with mainMod + LMB/RMB and dragging
          "$mainMod, mouse:272, movewindow                                      # Move window with mouse"
          "$mainMod, mouse:273, resizewindow                                    # Resize window with mouse"
        ];

        bindel = [
          # Laptop multimedia keys for volume and LCD brightness
          ",XF86AudioRaiseVolume, exec, wpctl set-volume @DEFAULT_AUDIO_SINK@ 5%+ # Increase Volume"
          ",XF86AudioLowerVolume, exec, wpctl set-volume @DEFAULT_AUDIO_SINK@ 5%-       # Decrease Volume"
          ",XF86AudioMute, exec, wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle     # Toggle volume mute"
          ",XF86AudioMicMute, exec, wpctl set-mute @DEFAULT_AUDIO_SOURCE@ toggle        # Toggle microphone mute"
          ",XF86MonBrightnessUp, exec, brightnessctl s 10%+                     # Increase Brightness"
          ",XF86MonBrightnessDown, exec, brightnessctl s 10%-                   # Decrease brightness"
        ];

        bindl = [
          # Requires playerctl
          ",XF86AudioNext, exec, playerctl next                                 # Play next song"
          ",XF86AudioPause, exec, playerctl play-pause                          # Pause current song"
          ",XF86AudioPrev, exec, playerctl previous                             # Play previous song"
        ];

        # windows and workspaces
        # see https://wiki.hyprland.org/Configuring/Window-Rules/
        # see https://wiki.hyprland.org/Configuring/Workspace-Rules/
        windowrulev2 = [
          "suppressevent maximize, class:.*"

          # Fix some dragging issues with XWayland
          "nofocus,class:^$,title:^$,xwayland:1,floating:1,fullscreen:0,pinned:0"

          # Deal with Thunderbird popups
          "float, class:(thunderbird), title: ^$"
          "move 75% 5%, class:(thunderbird), title: ^$"
          "tile, class:(thunderbird), title: ^(.*Write.*)$"
          "float,class:^(thunderbird)$,title:^(.*)(Reminder)(.*)$"

          # clipse clipboard manager
          # ensure you have a floating window class set if you want this behavior
          "float,class:(clipse)"
          # set the size of the window as necessary
          "size 622 652,class:(clipse)"

          # copyq clipboard manager
          "float, class:^(com.github.hluk.copyq)$"
          "size 50% 66%, class:^(com.github.hluk.copyq)$"
          "center, class:^(com.github.hluk.copyq)$"

          # Float "Save as" windows
          "float, title:^(Save As)"

          # Float, popupwebcamfeed vlc window
          "float, title:(popupwebcamfeed)"

        ];

      };
      extraConfig = ''
        # will start a submap called "resize"
        submap = resize
        # sets repeatable binds for resizing the active window
        binde = , right, resizeactive, 80 0
        binde = , left, resizeactive, -80 0
        binde = , up, resizeactive, 0 -80
        binde = , down, resizeactive, 0 80
        binde = SHIFT, right, moveactive, 10 0
        binde = SHIFT, left, moveactive, -10 0
        binde = SHIFT, up, moveactive, 0 -10
        binde = SHIFT, down, moveactive, 0 10
        # use reset to go back to the global submap
        bind = , escape, submap, reset
        # will reset the submap, meaning end the current one and return to the global one
        submap = reset
      '';
    };

    # copy auxiliary files
    xdg.configFile = {
      "hypr/hyprlock.conf".source = mkForce ./config/hypr/hyprlock.conf;
      "hypr/hypridle.conf".source = mkForce ./config/hypr/hypridle.conf;
      "waybar/style.css".source = mkForce ./config/waybar/style.css;
      "waybar/macchiato.css".source = mkForce ./config/waybar/macchiato.css;
      "waybar/power_menu.xml".source = mkForce ./config/waybar/power_menu.xml;
      "wpaperd/config.toml".source = mkForce ./config/wpaperd/config.toml;
      "xdg-desktop-portal/hyprland-portals.conf".source =
        mkForce ./config/xdg-desktop-portal/hyprland-portals.conf;
      "waybar/config".text = fileWithText waybar_config '''';
    };

  };
}
