#!/usr/bin/env bash
# From: https://github.com/jason9075/rofi-hyprland-keybinds-cheatsheet

HYPR_CONF="$HOME/.config/hypr/hyprland.conf"

# extract the keybinding from hyprland.conf
mapfile -t BINDINGS < <(grep -E '^bind(e?m?l?(el)?)\s*=\s*' "$HYPR_CONF" | grep -v 'HIDEME' | \
    sed -e 's/  */ /g' -e 's/bind.*\s*=\s*//g' -e 's/, /,/g' -e 's/ # /,/' | \
    awk -F, -v q="'" '{cmd=""; for(i=3;i<NF;i++) cmd=cmd $(i) " ";print "<b>"$1 " + " $2 "</b>  <i>" $NF ",</i><span color=" q "gray" q " size=" q "small" q ">" cmd "</span>"}')

CHOICE=$(printf '%s\n' "${BINDINGS[@]}" | rofi -dmenu -i -markup-rows -p "Hyprland Keybinds:" -theme ~/.config/rofi/appsMenu.rasi)

# Extract cmd from the selected choice
CMD=$(echo "$CHOICE" | sed -n "s/.*<span color='gray'>\(.*\)<\/span>.*/\1/p")

# Check if CMD is not empty
if [ -z "$CMD" ]; then
    exit 1
fi

# Execute the command if it starts with exec, otherwise use hyprctl dispatch
if [[ $CMD == exec* ]]; then
    eval "$CMD"
else
    hyprctl dispatch "$CMD"
fi
