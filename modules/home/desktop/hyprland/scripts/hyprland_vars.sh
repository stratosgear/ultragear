#!/usr/bin/env bash

function inc_gaps_in () {
  gaps_in=$(hyprctl -j getoption general:gaps_in | jq '.custom' | awk '{print $1}' | cut -c 2-)
  hyprctl keyword general:gaps_in $((gaps_in+2))
}

function dec_gaps_in () {
  gaps_in=$(hyprctl -j getoption general:gaps_in | jq '.custom' | awk '{print $1}' | cut -c 2-)
  hyprctl keyword general:gaps_in $((gaps_in-2 < 0 ? 0 : gaps_in-2))
}

function inc_gaps_out () {
  gaps_out=$(hyprctl -j getoption general:gaps_out | jq '.custom' | awk '{print $1}' | cut -c 2-)
  hyprctl keyword general:gaps_out $((gaps_out+2))
}

function dec_gaps_out () {
  gaps_out=$(hyprctl -j getoption general:gaps_out | jq '.custom' | awk '{print $1}' | cut -c 2-)
  hyprctl keyword general:gaps_out $((gaps_out-2 < 0 ? 0 : gaps_out-2))
}

function inc_border () {
  border=$(hyprctl -j getoption general:border_size | jq '.int')
  hyprctl keyword general:border_size $((border+2))
}

function dec_border () {
  border=$(hyprctl -j getoption general:border_size | jq '.int')
  hyprctl keyword general:border_size $((border-2<0 ? 0 : border-2))
}

function inc_rounding () {
  rounding=$(hyprctl -j getoption decoration:rounding | jq '.int')
  hyprctl keyword decoration:rounding $((rounding+2))
}

function dec_rounding () {
  rounding=$(hyprctl -j getoption decoration:rounding | jq '.int')
  hyprctl keyword decoration:rounding $((rounding-2 < 0 ? 0 : rounding-2))
}

function reset () {
  hyprctl keyword general:gaps_in 2
  hyprctl keyword general:gaps_out 2
  hyprctl keyword general:border_size 2
  hyprctl keyword decoration:rounding 2
}

while [[ $# -gt 0 ]]; do
  case $1 in
    --inc_gaps_in)   inc_gaps_in;   shift ;;
    --dec_gaps_in)   dec_gaps_in;   shift ;;
    --inc_gaps_out)  inc_gaps_out;  shift ;;
    --dec_gaps_out)  dec_gaps_out;  shift ;;
    --inc_border)  inc_border;  shift ;;
    --dec_border)  dec_border;  shift ;;
    --inc_rounding)  inc_rounding;  shift ;;
    --dec_rounding)  dec_rounding;  shift ;;
    --reset)  reset;  shift ;;
    *)               printf "Error: Unknown option %s" "$1"; exit 1 ;;
  esac
done