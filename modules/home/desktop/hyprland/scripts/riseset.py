import math
import json
import warnings
import subprocess
from datetime import datetime, timedelta, time, timezone
import zoneinfo as zi
from time import sleep

# README
# Cannot make this to work.  The ramp up and ramp down works but when calling multiple time hyprsunset
# it flickers on temp changes


# CONSTANT
TO_RAD = math.pi/180.0


class SunTimeException(Exception):

    def __init__(self, message):
        super(SunTimeException, self).__init__(message)


class Sun:
    """
    Approximated calculation of sunrise and sunset datetimes. Adapted from:
    https://stackoverflow.com/questions/19615350/calculate-sunrise-and-sunset-times-for-a-given-gps-coordinate-within-postgresql
    """
    def __init__(self, lat, lon):
        self._lat = lat
        self._lon = lon

        self.lngHour = self._lon / 15

    def get_sunrise_time(self, at_date=datetime.now(), time_zone=timezone.utc):
        """
        :param at_date: Reference date. datetime.now() if not provided.
        :param time_zone: pytz object with .tzinfo() or None
        :return: sunrise datetime.
        :raises: SunTimeException when there is no sunrise and sunset on given location and date.
        """
        time_delta = self.get_sun_timedelta(at_date, time_zone=time_zone, is_rise_time=True)
        if time_delta is None:
            raise SunTimeException('The sun never rises on this location (on the specified date)')
        else:
            return datetime.combine(at_date, time(tzinfo=time_zone)) + time_delta

    def get_sunset_time(self, at_date=datetime.now(), time_zone=timezone.utc):
        """
        Calculate the sunset time for given date.
        :param at_date: Reference date. datetime.now() if not provided.
        :param time_zone: pytz object with .tzinfo() or None
        :return: sunset datetime.
        :raises: SunTimeException when there is no sunrise and sunset on given location and date.
        """
        time_delta = self.get_sun_timedelta(at_date, time_zone=time_zone, is_rise_time=False)
        if time_delta is None:
            raise SunTimeException('The sun never rises on this location (on the specified date)')
        else:
            return datetime.combine(at_date, time(tzinfo=time_zone)) + time_delta

    def get_sun_timedelta(self, at_date, time_zone, is_rise_time=True, zenith=90.8):
        """
        Calculate sunrise or sunset date.
        :param at_date: Reference date
        :param time_zone: pytz object with .tzinfo() or None
        :param is_rise_time: True if you want to calculate sunrise time.
        :param zenith: Sun reference zenith
        :return: timedelta showing hour, minute, and second of sunrise or sunset
        """

        # If not set get local timezone from datetime
        if time_zone is None:
            time_zone = datetime.now().tzinfo

        # 1. first get the day of the year
        N = at_date.timetuple().tm_yday

        # 2. convert the longitude to hour value and calculate an approximate time
        if is_rise_time:
            t = N + ((6 - self.lngHour) / 24)
        else:   # sunset
            t = N + ((18 - self.lngHour) / 24)

        # 3a. calculate the Sun's mean anomaly
        M = (0.9856 * t) - 3.289

        # 3b. calculate the Sun's true longitude
        L = M + (1.916 * math.sin(TO_RAD*M)) + (0.020 * math.sin(TO_RAD * 2 * M)) + 282.634
        L = self._force_range(L, 360)   # NOTE: L adjusted into the range [0,360)

        # 4a. calculate the Sun's declination
        sinDec = 0.39782 * math.sin(TO_RAD*L)
        cosDec = math.cos(math.asin(sinDec))

        # 4b. calculate the Sun's local hour angle
        cosH = (math.cos(TO_RAD*zenith) - (sinDec * math.sin(TO_RAD*self._lat))) / (cosDec * math.cos(TO_RAD*self._lat))

        if cosH > 1:
            return None     # The sun never rises on this location (on the specified date)
        if cosH < -1:
            return None     # The sun never sets on this location (on the specified date)

        # 4c. finish calculating H and convert into hours
        if is_rise_time:
            H = 360 - (1/TO_RAD) * math.acos(cosH)
        else:   # setting
            H = (1/TO_RAD) * math.acos(cosH)
        H = H / 15

        # 5a. calculate the Sun's right ascension
        RA = (1/TO_RAD) * math.atan(0.91764 * math.tan(TO_RAD*L))
        RA = self._force_range(RA, 360)     # NOTE: RA adjusted into the range [0,360)

        # 5b. right ascension value needs to be in the same quadrant as L
        Lquadrant = (math.floor(L/90)) * 90
        RAquadrant = (math.floor(RA/90)) * 90
        RA = RA + (Lquadrant - RAquadrant)

        # 5c. right ascension value needs to be converted into hours
        RA = RA / 15

        # 6. calculate local mean time of rising/setting
        T = H + RA - (0.06571 * t) - 6.622

        # 7a. adjust back to UTC
        UT = T - self.lngHour

        if time_zone:
            # 7b. adjust back to local time
            UT += time_zone.utcoffset(at_date).total_seconds() / 3600

        # 7c. rounding and impose range bounds
        UT = round(UT, 2)
        if is_rise_time:
            UT = self._force_range(UT, 24)

        # 8. return timedelta
        return timedelta(hours=UT)

    @staticmethod
    def _force_range(v, max):
        # force v to be >= 0 and < max
        if v < 0:
            return v + max
        elif v >= max:
            return v - max
        return v

# Your latitude and longitude
LATITUDE=40.43
LONGITUDE=-4
TIMEZONE='Europe/Madrid'

# The required temperature in Kelvin to apply
REQUIRED_TEMP=2000

# The ramp up/down period on sunrise and sunset (minutes)
DURATION=6

# NOTHING TO CHANGE BELOW THIS LINE

# The Base temperature
_BASE_TEMP=6650
_TEMP_DIFF=_BASE_TEMP - REQUIRED_TEMP
_CURR_TEMP=-1
_RAMP_STEP_HEIGHT=80
_PREV_ID=None

def set_temp(temp: float):
    global _CURR_TEMP, _PREV_ID
    t = round(temp/_RAMP_STEP_HEIGHT) * _RAMP_STEP_HEIGHT
    if t != _CURR_TEMP:
        previd = _PREV_ID
        if temp == 0:
            # _PREV_ID = subprocess.Popen(["nohup", "hyprsunset", "-i", "0</dev/null", "1>/dev/null", "2>/dev/null"]).pid 
            _PREV_ID = subprocess.Popen(["hyprsunset", "-i"]).pid 
        else:
            # _PREV_ID = subprocess.Popen(["nohup", "hyprsunset", "-t", str(temp), "0</dev/null", "1>/dev/null", "2>/dev/null"]) .pid
            _PREV_ID = subprocess.Popen(["timeout", "2m", "hyprsunset", "-t", str(t)]) .pid
        _CURR_TEMP = t
        # if previd:
        #     subprocess.run(["kill", "-9", str(previd)])
        return t
    else:
        return _CURR_TEMP

def notify(msg):
        print(json.dumps({
        "text": msg,
        }), flush=True)

sun = Sun(LATITUDE, LONGITUDE)


while True:

    _TEMP_DIFF=_BASE_TEMP - REQUIRED_TEMP
    # _RAMP_TEMP_STEPS = int((_BASE_TEMP - REQUIRED_TEMP) / _RAMP_STEP_HEIGHT)
    # _RAMP_SLEEP_TIME = math.floor(DURATION*60 / _RAMP_TEMP_STEPS)
    _RAMP_SLEEP_TIME = 2

    now = datetime.now()
    # print(f"Current time: {now}")

    # sunrise = datetime.combine(datetime.now(), sun.get_sunrise_time().astimezone(zi.ZoneInfo(TIMEZONE)).time())
    sunrise = datetime(2024,12,21,16,24,0)
    # sunset = datetime.combine(datetime.now(), sun.get_sunset_time().astimezone(zi.ZoneInfo(TIMEZONE)).time())
    sunset = datetime(2024,12,21,14,40,0)

    # ramp times
    sunrise_ramp_start = sunrise - timedelta(minutes=DURATION/2)
    sunset_ramp_start = sunset - timedelta(minutes=DURATION/2)
    sunrise_ramp_end = sunrise + timedelta(minutes=DURATION/2)
    sunset_ramp_end = sunset + timedelta(minutes=DURATION/2)


    if now < sunrise_ramp_start:
        # print("In night period")
        set_temp(REQUIRED_TEMP)
        notify(f"Night: {REQUIRED_TEMP}K")
        sleep_duration = sunrise_ramp_start - now
        sleep(sleep_duration.total_seconds())
    elif now >= sunrise_ramp_start and now < sunrise_ramp_end:
        # print("In sunrise ramp period")
        secs = (now - sunrise_ramp_start).total_seconds()
        ramp_perc =  secs / (DURATION * 60)
        temp = REQUIRED_TEMP + _TEMP_DIFF * ramp_perc
        temp_used = set_temp(temp)
        notify(f"Sunrise: {temp_used}K/{_BASE_TEMP}K")
        sleep(_RAMP_SLEEP_TIME)
    elif now >= sunrise_ramp_end and now < sunset_ramp_start:
        # print("In daylight period")
        set_temp(0)
        notify("Day: OFF")
        sleep_duration = int((sunset_ramp_start - now).total_seconds()+1)
        sleep(sleep_duration)
    elif now >= sunset_ramp_start and now < sunset_ramp_end:
        # print("In sunset ramp period")
        secs = (now - sunset_ramp_start).total_seconds()
        ramp_perc =  secs / (DURATION * 60) 
        temp = _BASE_TEMP - _TEMP_DIFF * ramp_perc 
        temp_used = set_temp(temp)
        notify(f"Sunset: {temp_used}K/{REQUIRED_TEMP}K")
        sleep(_RAMP_SLEEP_TIME)
    elif now >= sunset_ramp_end:
        # print("In night period", file=sys.stderr)
        set_temp(REQUIRED_TEMP)
        notify(f"Night: {REQUIRED_TEMP}K")
        sleep_duration = (now.replace(hour=23, minute=59, second=59, microsecond=999999) - now).total_seconds() + 1
        sleep(sleep_duration+1)
