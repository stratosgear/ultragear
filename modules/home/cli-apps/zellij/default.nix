{ lib
, config
, pkgs
, ...
}:
with lib;
with lib.ultragear; let
  cfg = config.ultragear.home.cli-apps.zellij;
in
{
  options.ultragear.home.cli-apps.zellij = {
    enable = mkEnableOption "Enable zellij multiplexer";
  };

  config = mkIf cfg.enable {
    programs = {
      zellij = {
        enable = true;
        # This creates a new zellij session each time, with the result of having a ton
        # of empty sessions over time
        # enableZshIntegration = config.ultragear.home.cli-apps.zsh.enable;
        enableZshIntegration = false;
      };

    };
  };
}
