{ lib
, config
, pkgs
, ...
}:
with lib;
with lib.ultragear; let
  cfg = config.ultragear.home.cli-apps.atuin;
in
{
  options.ultragear.home.cli-apps.atuin = {
    enable = mkEnableOption "Enable terminal history capturing with atuin";
  };

  config = mkIf cfg.enable {

    ultragear.home.tools.sops.secrets = {
      "atuin/key" = {
        mode = "0400";
        path = "${config.home.homeDirectory}/.local/share/atuin/key";
      };
    };

    programs = {
      atuin = {
        enable = true;
        enableZshIntegration = config.ultragear.home.cli-apps.zsh.enable;
        flags = [
          "--disable-up-arrow"
          # "--disable-ctrl-r"
        ];
        # https://docs.atuin.sh/configuration/config/
        settings = {
          # default is true anyways
          auto_sync = true;
          # we do not need check for updates as we are declarative
          update_check = false;

          key_path = config.ultragear.home.tools.sops.secrets."atuin/key".path;
          style = "compact";
          enter_accept = false;
          inline_height = 24;
          invert = true;
          show_preview = true;
          workspaces = true;
          filter_mode_shell_up_key_binding = "session";
          prefers_reduced_motion = true;
        };
      };

    };
  };
}
