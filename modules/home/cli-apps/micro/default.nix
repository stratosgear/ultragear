{ lib
, config
, pkgs
, ...
}:
with lib;
with lib.ultragear; let
  cfg = config.ultragear.home.cli-apps.micro;
in
{
  options.ultragear.home.cli-apps.micro = {
    enable = mkEnableOption "Enable micro terminal editor";
  };

  config = mkIf cfg.enable {
    programs = {
      micro = {
        enable = true;
        settings = {
          mkparents = true;
        };
      };

      zsh.shellAliases = mkIf config.ultragear.home.cli-apps.zsh.enable {
        m = "${pkgs.micro}/bin/micro";
      };
    };

  };
}
