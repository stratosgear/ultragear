{
  lib,
  config,
  pkgs,
  ...
}:
let
  inherit (lib) mkEnableOption mkIf;

  cfg = config.ultragear.home.cli-apps.zsh;
in
{
  options.ultragear.home.cli-apps.zsh = {
    enable = mkEnableOption "Enable zsh shell";
  };

  config = mkIf cfg.enable {
    programs = {
      zsh = {
        enable = true;
        prezto.enable = true;
        autosuggestion.enable = true;
        enableCompletion = true;
        syntaxHighlighting.enable = true;

        initExtra = ''
          # Fix an issue with tmux.
          export KEYTIMEOUT=1

          # Use emacs bindings.
          set -o emacs

          ${pkgs.figlet}/bin/figlet -f smslant $(cat /etc/static/hostname) | ${pkgs.lolcat}/bin/lolcat

          # Improved vim bindings.
          # source ${pkgs.zsh-vi-mode}/share/zsh-vi-mode/zsh-vi-mode.plugin.zsh

          # autorun zellij on new shells
          #if [[ -z "$ZELLIJ" ]]; then
          # if [ -n "$SSH_CONNECTION" ] && [ -z "$ZELLIJ" ]; then

          #   zellij a main || zellij -s main || zellij

          #   # if [[ "$ZELLIJ_AUTO_EXIT" == "true" ]]; then
          #   #     exit
          #   # fi

          # fi
        '';

        shellAliases = {
          say = "${pkgs.figlet}/bin/figlet -f small";
          z = "zellij a main || zellij -s main || zellij";
          ug = "ultragear";
        };

        plugins = [
          {
            name = "zsh-nix-shell";
            file = "nix-shell.plugin.zsh";
            src = pkgs.fetchFromGitHub {
              owner = "chisui";
              repo = "zsh-nix-shell";
              rev = "v0.4.0";
              sha256 = "037wz9fqmx0ngcwl9az55fgkipb745rymznxnssr3rx9irb6apzg";
            };
          }
        ];
      };

      starship = {
        enable = true;
        settings = {
          cmd_duration = {
            show_milliseconds = true;
          };
          directory = {
            home_symbol = " ";
          };
          character = {
            success_symbol = "[➜](bold green) ";
            error_symbol = "[✗](bold red) ";
            vicmd_symbol = "[](bold blue) ";
          };
        };
      };
    };
  };
}
