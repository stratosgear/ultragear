{ lib, config, ... }:

let
  inherit (lib) mkEnableOption mkIf;
  inherit (lib.ultragear) enabled;

  cfg = config.ultragear.home.cli-apps.home-manager;
in
{
  options.ultragear.home.cli-apps.home-manager = {
    enable = mkEnableOption "home-manager";
  };

  config = mkIf cfg.enable {
    programs.home-manager = enabled;
  };
}
