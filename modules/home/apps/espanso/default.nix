{
  options,
  config,
  lib,
  pkgs,
  ...
}:

with lib;
with lib.ultragear;
let
  cfg = config.ultragear.home.apps.espanso;
in
{
  options.ultragear.home.apps.espanso = with types; {
    enable = mkBoolOpt false "Whether or not to enable the espanso service.";
  };

  config = mkIf cfg.enable {

    ultragear.home.tools.sops.secrets = {
      "pkip" = {
        mode = "0400";
        path = "${config.home.homeDirectory}/pkip";
      };
    };

    # services.espanso = {
    #   enable = true;
    #   package = pkgs.espanso-wayland;
    # };

    xdg.configFile = {
      "espanso/config/default.yml".source = mkForce ./config/default.yml;
      "espanso/match/base.yml".source = mkForce ./match/base.yml;
      "espanso/user/kitty.yml".source = mkForce ./user/kitty.yml;
    };

  };
}
