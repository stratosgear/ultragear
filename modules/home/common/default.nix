{
  options,
  config,
  osConfig ? { },
  lib,
  pkgs,
  ...
}:

with lib;
with lib.ultragear;
let
  cfg = config.ultragear.home.common;
in
{
  options.ultragear.home.common = with types; {
    enable = mkBoolOpt false "Enable common home-manager options between multiple user profiles";
  };

  config = mkIf cfg.enable {

    ultragear.home = {

      user = {
        enable = true;
        name = config.snowfallorg.user.name;
      };

      apps = {
        # espanso = mkIf osConfig.ultragear.suites.desktop.enable {
        #   enable = true;
        # };
      };

      cli-apps = {
        zsh = enabled;
        home-manager = enabled;
        micro = enabled;
        zellij = enabled;
        atuin = enabled;
      };

      tools = {
        git = enabled;
        jujutsu = enabled;
        direnv = enabled;
        ssh = enabled;
        keychain = enabled;
        sops = { };
      };
    };

  };
}
