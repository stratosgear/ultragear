{ lib, ... }:

rec {

  CONSTS = {

    # Ranges of 62000-63000 seem to not confict with any popular services
    # https://en.wikipedia.org/wiki/List_of_TCP_and_UDP_port_numbers
    ports = {
      redis = {
        wallabag = 62101;
        weblate = 62102;
        forgejo = 62103;
        fief = 62104;
      };
      services = {
        healthchecks = 62201;
        shlink = 62202;
        wallabag = 62203;
        weblate = 62204;
        resticServer = 62205;
        forgejo = {
          server = 62206;
          git = 2222;
        };
        homepage = 62207;
        scrutiny = 62208;
        gitlab = {
          server = 62209;
          git = 2222;
        };
        homarr = 62210;
        dashdot = 62211;
        zabbixWeb = 62212;
        navidrome = 62213;
        cockpit = 62214;
        vaultwarden = 62215;
        mpd = {
          server = 62216;
          stream = 62217;
        };
        nixosLocalCache = 62218;
        pihole = {
          internal = 62219;
          external = 62220;
        };
        adguardHome = 62221;
        resiliosync = 62222;
        homeassistant = 62223;
        mympd = 62224;
        kanboard = 62225;
        fief = 62226;
        cryptpad = 62227;
      };

    };

    urls = {
      pubRootDomain = "hypervasis.com";
      privRootDomain = "gerakakis.net";

      garage = {
        admin = "s3admin.${CONSTS.urls.privRootDomain}";
        s3 = "s3.${CONSTS.urls.privRootDomain}";
        web = "s3web.${CONSTS.urls.privRootDomain}";
      };
      adguardHome = "adguard.${CONSTS.urls.privRootDomain}";
      backups = {
        dread = "backups.dread.${CONSTS.urls.privRootDomain}";
        vern = "backups.vern.${CONSTS.urls.privRootDomain}";
      };
      cryptpad = "collab.${CONSTS.urls.pubRootDomain}";
      fief = "fief.${CONSTS.urls.pubRootDomain}";
      forgejo = "code.${CONSTS.urls.pubRootDomain}";
      gitlab = "gitlab.${CONSTS.urls.pubRootDomain}";
      healthchecks = "healthchecks.${CONSTS.urls.pubRootDomain}";
      homarr = "dash.${CONSTS.urls.privRootDomain}";
      homepage = "dash.${CONSTS.urls.privRootDomain}";
      homeassistant = "ha.${CONSTS.urls.privRootDomain}";
      go2rtc = "ipcams.${CONSTS.urls.privRootDomain}";
      paperlessngx = "paperless.${CONSTS.urls.privRootDomain}";
      pihole = "pihole.${CONSTS.urls.privRootDomain}";
      scrutiny = "smartctl.${CONSTS.urls.privRootDomain}";
      shlink = "yourl.es";
      tailscaleDomain = "bearded-python.ts.net";
      vaultwarden = "vw.${CONSTS.urls.privRootDomain}";
      wallabag = "wallabag.${CONSTS.urls.pubRootDomain}";
      weblate = "weblate.${CONSTS.urls.pubRootDomain}";
      zabbix = "zabbix.${CONSTS.urls.privRootDomain}";
    };

    sshKeys = {
      stratos = {
        iocaine = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGnvOdbpfS51yVj1XiFuglaWlUxt5brl1/BfufaYHahm stratos@iocaine";
        vezzini = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJ1X3YKSEkZ3/dookENMzm4o93HOmXJI+K9qXVZi+qMw stratos@vezzini";
        vern = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIKcLFQ3yfggWB08zLK3fDTQZUdjoKo/qVNTmcWfRUicN stratos@vern";
        dread = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIN0xnK9m2AqVbjpUcOFIjYw8lqmBkaO2CLhxtJHxVNK3 stratos@dread";
        pi3 = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIMxrIdkCQ9+8jJkReMGCmo1jVdfiCS92rYGcPd/Jg8CX stratos@pi3";
        ion = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFnJKPcbfLcojkihWzjXLVVpyP9HTHn27EubUIl4qs2+ stratos@ion";
        fezzik = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFL1VcKPrMLPaf932H7xHXO4y/f9XiMQOgG6W4RRlwWV stratos@fezzik";
        cliffs-of-insanity = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAOXBdS37gLEUHam/sDPkSPnjxPmG1MdaYnKI0v6FMkP stratos@cliffs-of-insanity";
      };
    };

    ips = {
      privNetRanges = [
        "127.0.0.1/32" # local host
        "192.168.0.0/16" # local network
        "10.0.0.0/8" # local network
        "172.16.0.0/12" # docker network
        "100.64.0.0/10" # vpn network
      ];
      # Cloudflare IPv4: https://www.cloudflare.com/ips-v4
      cloudflareIP4 = [
        "173.245.48.0/20"
        "103.21.244.0/22"
        "103.22.200.0/22"
        "103.31.4.0/22"
        "141.101.64.0/18"
        "108.162.192.0/18"
        "190.93.240.0/20"
        "188.114.96.0/20"
        "197.234.240.0/22"
        "198.41.128.0/17"
        "162.158.0.0/15"
        "104.16.0.0/13"
        "104.24.0.0/14"
        "172.64.0.0/13"
        "131.0.72.0/22"
      ];
    };

  };
}
