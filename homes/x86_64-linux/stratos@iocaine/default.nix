{ lib, pkgs, config, osConfig ? { }, format ? "unknown", ... }:

with lib.ultragear;
{
  ultragear.home = {
    common = enabled;
    desktop.assets = enabled;
  };
}
