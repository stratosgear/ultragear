{
  lib,
  pkgs,
  config,
  format ? "unknown",
  ...
}:

with lib.ultragear;
{
  ultragear.home = {
    common = enabled;
    desktop.assets = enabled;
  };

  # Since this home-manager configuration is a standalone configuration
  # not backed by a system setup (not on a nixos system), we need to
  # manually set the version of the state version.
  home.stateVersion = "24.11";

  # To apply changes run:
  #  home-manager switch --flake ".#username@hostname"

}
