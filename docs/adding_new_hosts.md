# Adding new hosts to ultragear

Follow official nixos installation wiki:
https://nixos.wiki/wiki/NixOS_Installation_Guide


## Getting internet while installing

Prefferably with an ethernet cable, in which case you should already have internet
access.

Otherwise for wifi do:

wpa_passphrase ESSID | sudo tee /etc/wpa_supplicant.conf

The manual mentions then to:

systemctl restart wpa_supplicant

but that fails.  Instead try:

sudo wpa_supplicant -c /etc/wpa_supplicant.conf -B -i wlp4s0

Get your proper `wlpXsX` interface name from: ifconfig -a

## Do the installation remotely

I find it inconvenient to use the remote console, I would prefer to use my local
terminal.

Do a: passwd to change tha password of the nixos user

Do a :sudo su and then passwd to chaneg the root password too.

Then from your local machine: ssh nixos@192.168.X.X to connect to your new host


## After create nixos configuration files

### For configuration.nix

Set proper timezone.


Add section:

```
  nix = {
      settings = {
        experimental-features = "nix-command flakes";
        http-connections = 50;
        trusted-users = [ "root" "stratos" ];
        allowed-users = [ "root" "stratos" ];
      };
  };
```

and user:

```
  users.users.stratos = {
    isNormalUser = true;
    extraGroups = [ "wheel" ]; # Enable ‘sudo’ for the user.
    hashedPassword = "$6$fth8aNS.CWpcGJkD$eKQI2wc3n7zib6Rpw50sQpILRkkJNtaDLJoW/zHwfMMappd3rYZNdagO5lRFuld0XgHL7wgJcgpFaP.abO9Wf/";
    packages = with pkgs; [
      git
      micro
    ];
  };
```

Also add:

```
  # Enable the OpenSSH daemon.
  services.openssh.enable = true;

  security.sudo.wheelNeedsPassword = false;
```

### For hardware-configuration.nix

Usually on swapDevices enter:

  swapDevices = [ { device = "/.swapfile"; }];

if you have created a swapfile.

Or steal other usefull info from other hosts from this repo:
systems/x86_64-linux/XXX/hardware-configuration.nix


```
cd /mnt
sudo nixos-install
```

Reboot into your new nixos


## Install Ultragear


A few things before you do so.

### Fix sops secrets

From the new host, get the system sops key:

```
nix-shell -p ssh-to-age --run 'cat /etc/ssh/ssh_host_ed25519_key.pub | ssh-to-age'
```

Paste the reply (that starts with age...) into `./sops.yaml`

Make additional changes in that file, should be easy to see the structure

Create a new secrets/[hostname].yml secrets file.

Bare minimum for Ultragear is the default backup secret keys (then add more secrets
as needed):

```
backups:
    passphrases:
        system: long-random-password
    environmentFiles:
        system: |
            AWS_ACCESS_KEY_ID=000XXXXXXX
            AWS_SECRET_ACCESS_KEY=K000LAYYYYYYYYYYY
            RESTIC_REST_USERNAME=hostname?
            RESTIC_REST_PASSWORD=some-long-password
```

Create backup secret keys from Backblaze B2:
https://secure.backblaze.com/app_keys.htm


Re-key home-manager keys:

```
sops updatekeys secrets/vault.yaml
sops updatekeys modules/home/tools/sops/secrets/vault.yaml
```
### Create new healthcheck endpoints


### Create a new system

More likely copy and paste and edit an existing one from:

```
systems/x86_64-linux/[hostname]
```


For hardware-configuration.nix, more likely you will have to copy the original config
from when you installed nixos, and then start making changes to it if required.

### Create new user

Create new entries in

./homes/x86_64-linx/stratos@[hostname]

(More likely copy and paste of an existing one)


## Deploy the new system

You can remotely install your new host, by running from your most convenient existing
nixos machine:

```
ug deploy [hostname]
```

Or you can git clone the Ultragear git repo from your new machine and start working from
there:

```
git clone git@gitlab.com:stratosgear/ultragear.git
```

First time (since you will not have the `ug` command available you will have to proved
the full command)

```
sudo nixos-rebuild switch --flake /home/stratos/ultragear/#[hostname]
```