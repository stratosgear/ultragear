# What is this?

This an attempt the maintain and at the same time document my dotfiles.

In a fit of mental derangement I decided to both go **all**
[Nixos](https://nixos.org) on this and at the **same time** bring all the
personal systems that I maintain along.


The information contained in these published pages is mainly for my own use as a
documentation for the installation and maintenance of the systems that I am
administering.


Since I have used a lot of similar repos online as a reference, it is only fair if I
publicly publish my own repo too.  Hopefully, one day, it might be of good enough
quality to be used as a reference too. 😄

!!! note annotate "Watch for the landmines"

    As I am a complete newbie to both Nixos, please beware of any accidental landmines
    that might lie around.