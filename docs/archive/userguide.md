# How to use

This repo is used for two main tasks:

- Bootstraping new hosts
- Customizing existing hosts


## Bootstraping new hosts

A host means a new laptop, desktop, server (vm?)

Bootstraping it means to put a very basic bootable Nixos installation on it.

Detailed instructions on bootstrapping appears in the relevant page. In summary it is
triggered with a:

`nix run github:nix-community/nixos-anywhere -- --flake .#[hostname] [hostname]`

## Customizing existing hosts

Once a basic OS installation is performed you can proceed with further customizations,
like installing and configuring programs, services etc

Again, further instructions for customization is available at the Hosts Costumization
pages.  In summary they are triggered with:

`sudo nixos-rebuild switch --flake .#[hostname]`

for systemwide updates, or with:

`home-manager switch --flake .#[username]@[hostname]`

for user settings

