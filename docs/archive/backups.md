# Backups Strategy

Backups are performed with [restic](https://restic.readthedocs.io/en/stable/)


- [Backups Strategy](#backups-strategy)
  - [Backups](#backups)
    - [Backups schedule](#backups-schedule)
    - [Creating passwords for Restic Server](#creating-passwords-for-restic-server)
  - [Restores](#restores)
    - [List snapshots](#list-snapshots)
    - [See contents of a snapshot](#see-contents-of-a-snapshot)
    - [See stats of the repo](#see-stats-of-the-repo)
    - [Mount a snapshot to recover files](#mount-a-snapshot-to-recover-files)
  - [Disastery Recovery](#disastery-recovery)



## Backups

Backups are part of `ultragear.services` defined in
`modules/nixos/services/backup/default.nix`

In order to activate them on a host, configure with:

```
ultragear.services =
      backup = {

        # set the current host as a Restic Server (or not)
        hostBackups = true;
        backupsDataDir = "/storage-box/resticBackups";
        serverDomain = CONSTS.urls.backups.vern;

        # Define an instance of a backup policy, arbitrarily called `system`
        # Can define multiple instances if so required.

        instances.system = {
          excludePatterns = [ ".git" ];
          sourceDirectories = [
            "/projects"
            "/home/stratos/.config/ultragear"
          ];

          # location where to store the backups.
          # Usually three different locations
          #  - onLocal: On the same machine 
          #  - onBackblaze: Remotely, offsite, on a Backblaze B2 bucket 
          #  - onVern: Remotely, on a local lan machine, as a Restic Server repo 
          repositories = [
            {
              path = "/backups/system";
              name = "onLocal";
              hcPingUid = "c2d16366-d977-4b7d-a90b-a904607da9e6";
            }
            {
              path = "s3:s3.us-west-000.backblazeb2.com/ultragear-backups/${config.networking.hostName}/system";
              name = "onBackblaze";
              hcPingUid = "c0337d05-66b4-4a85-85f6-95a49dc3d788";
            }
            {
              path = "rest:https://${CONSTS.urls.backups.vern}/${config.networking.hostName}/system";
              name = "onVern";
              hcPingUid = "b9c41399-411d-42c8-b336-b50460d982e3";
            }
          ];
        };
      };
```

They also have to be configured with the required secrets:

```
backups:
    passphrases:
        system: SomeSecret
    environmentFiles:
        system: |
            AWS_ACCESS_KEY_ID=SomeId
            AWS_SECRET_ACCESS_KEY=SomeKey
            RESTIC_REST_USERNAME=SomeUsername
            RESTIC_REST_PASSWORD=SomePassword
```

These are meant to set the :
- `passphrase.system`: password for the encrypted restic backups
- `environmentFiles.system.AWS_*`: Login credentials to access Backblaze B2 bucket
- `environmentFiles.system.RESTIC_REST_*`: Login credentials to access Restic backup
  server


### Backups schedule

They default to daily backups, but configuration can be overriden through `timerConfig`
(see the backup script mentioned above)

They can also be manually triggered with:

`restic-system_onLocal backup`

See the Restore section about how to construct the `restic-system_onLocal` script name. 

### Creating passwords for Restic Server

Create Restic Server accounts:

nix-shell -p pkgs.apacheHttpd
htpasswd -B -n dread


## Restores

Restore intructions on recovering some files when accidentally lost but access on a
given host is still possible.

In cases of Disaster Recovery, when access to a given host is completely lost, see the
"Disaster Recovery" section below.

All restore functionality is accessed through some convenience wrapper scripts that
allow execution of restic commands.

The script names follow the pattern of:

`restic-[instanceName]_[repoName] [cmd] [opts]`

where:

- `instanceName`: The backup instance name as defined in the backup configuration.
  Defaults to: system
- `repoName`: The backup repository name as defined in the backup configuration. Usually
  one of `onLocal`, `onBackblaze` or `onVern`
- `cmd`: one of Restic commands.  They are explained below
- `opts`: Restic command options.  They are explained below

Example: `restic-system_onLocal list snapshot`

**All** commands have to be run as `sudo`

### List snapshots

`sudo restic-system_onLocal snapshots`

### See contents of a snapshot

Grab the snapshot id from the previous command and `ls` it:

`sudo restic-system_onLocal ls 0034e6b1`

This just shows a huge list of all backed up files


### See stats of the repo

`sudo restic-system_onLocal stats`


### Mount a snapshot to recover files

`sudo restic-system_onLocal mount /tmp/restore`

- The directory must already exist
- The command will block the terminal
  - Use another terminal to navigate the mounted directory and restore any files that
    you want.

## Disastery Recovery

TBD