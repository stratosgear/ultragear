# Secrets handling

Extract host ssh key:
- `nix-shell -p ssh-to-age --run 'cat /etc/ssh/ssh_host_ed25519_key.pub | ssh-to-age'`

Add it to .sops.yaml.

Create new secrets file for host, at secrets/[hostname].yaml:

Fill in some required initial secrets:
```
restic:
    localBackups:
        passphrase: rocking-object-lucid-patronize
        hcPingUid: 1234-1234-1234
    lanBackups:
        passphrase: some-secret
        hcPingUid: 1234-1234-1234
    remoteBackups:
        passphrase: skid-excretory-emcee-golf
        hcPingUid: 1234-1234-1234
backblaze:
    restic:
        backups.env: |
            AWS_ACCESS_KEY_ID=xxxxxx
            AWS_SECRET_ACCESS_KEY=K0000xxxxxxxyyyyyyy
```
NOTE:
- lanBackups and remoteBackups are optional, if you choose to activate them
- Same for backblaze backups if you activate remoteBackups


# Personal ssh key

- ssh-keygen -t ed25519
- Copy the .pub version into /lib/consts/default.nix
- And maybe add it to gitlab/github
