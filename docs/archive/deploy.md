# Remote deploy

deploy --magic-rollback false .#vezzini


The deactivation of magic-rollback is due to issue:

https://github.com/serokell/deploy-rs/issues/78