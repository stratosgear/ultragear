wiki page on nix.dev:

https://nix.dev/tutorials/nixos/installing-nixos-on-a-raspberry-pi.html


# Building

Build an iso image like:

nix build .#sd-aarch64Configurations.pi4


# Build logs:

✗  nix build .#sd-aarch64Configurations.pi4
trace: warning: `overrideScope'` (from `lib.makeScope`) has been renamed to `overrideScope`.
warning: Ignoring setting 'auto-allocate-uids' because experimental feature 'auto-allocate-uids' is not enabled
warning: Ignoring setting 'impure-env' because experimental feature 'configurable-impure-env' is not enabled
error: builder for '/nix/store/bmi8p9w7m1d9yinrbpk6h1a5cdl0bkai-linux-6.1.63-stable_20231123-modules-shrunk.drv' failed with exit code 1;
       last 50 log lines:
       >   copying dependency: /nix/store/gaxzrdl8840rsd18a7vgrq1bqxvfsjjs-linux-6.1.63-stable_20231123-modules/lib/modules/6.1.63/kernel/drivers/firewire/firewire-core.ko.xz
       >   copying dependency: /nix/store/gaxzrdl8840rsd18a7vgrq1bqxvfsjjs-linux-6.1.63-stable_20231123-modules/lib/modules/6.1.63/kernel/drivers/firewire/firewire-ohci.ko.xz
       > root module: sbp2
       >   dependency already copied: /nix/store/gaxzrdl8840rsd18a7vgrq1bqxvfsjjs-linux-6.1.63-stable_20231123-modules/lib/modules/6.1.63/kernel/drivers/firewire/firewire-core.ko.xz
       >   copying dependency: /nix/store/gaxzrdl8840rsd18a7vgrq1bqxvfsjjs-linux-6.1.63-stable_20231123-modules/lib/modules/6.1.63/kernel/drivers/firewire/firewire-sbp2.ko.xz
       > root module: virtio_net
       >   copying dependency: /nix/store/gaxzrdl8840rsd18a7vgrq1bqxvfsjjs-linux-6.1.63-stable_20231123-modules/lib/modules/6.1.63/kernel/drivers/virtio/virtio.ko.xz
       >   copying dependency: /nix/store/gaxzrdl8840rsd18a7vgrq1bqxvfsjjs-linux-6.1.63-stable_20231123-modules/lib/modules/6.1.63/kernel/drivers/virtio/virtio_ring.ko.xz
       >   copying dependency: /nix/store/gaxzrdl8840rsd18a7vgrq1bqxvfsjjs-linux-6.1.63-stable_20231123-modules/lib/modules/6.1.63/kernel/net/core/failover.ko.xz
       >   copying dependency: /nix/store/gaxzrdl8840rsd18a7vgrq1bqxvfsjjs-linux-6.1.63-stable_20231123-modules/lib/modules/6.1.63/kernel/drivers/net/net_failover.ko.xz
       >   copying dependency: /nix/store/gaxzrdl8840rsd18a7vgrq1bqxvfsjjs-linux-6.1.63-stable_20231123-modules/lib/modules/6.1.63/kernel/drivers/net/virtio_net.ko.xz
       > root module: virtio_pci
       >   dependency already copied: /nix/store/gaxzrdl8840rsd18a7vgrq1bqxvfsjjs-linux-6.1.63-stable_20231123-modules/lib/modules/6.1.63/kernel/drivers/virtio/virtio.ko.xz
       >   dependency already copied: /nix/store/gaxzrdl8840rsd18a7vgrq1bqxvfsjjs-linux-6.1.63-stable_20231123-modules/lib/modules/6.1.63/kernel/drivers/virtio/virtio_ring.ko.xz
       >   copying dependency: /nix/store/gaxzrdl8840rsd18a7vgrq1bqxvfsjjs-linux-6.1.63-stable_20231123-modules/lib/modules/6.1.63/kernel/drivers/virtio/virtio_pci_modern_dev.ko.xz
       >   copying dependency: /nix/store/gaxzrdl8840rsd18a7vgrq1bqxvfsjjs-linux-6.1.63-stable_20231123-modules/lib/modules/6.1.63/kernel/drivers/virtio/virtio_pci_legacy_dev.ko.xz
       >   copying dependency: /nix/store/gaxzrdl8840rsd18a7vgrq1bqxvfsjjs-linux-6.1.63-stable_20231123-modules/lib/modules/6.1.63/kernel/drivers/virtio/virtio_pci.ko.xz
       > root module: virtio_mmio
       >   dependency already copied: /nix/store/gaxzrdl8840rsd18a7vgrq1bqxvfsjjs-linux-6.1.63-stable_20231123-modules/lib/modules/6.1.63/kernel/drivers/virtio/virtio.ko.xz
       >   dependency already copied: /nix/store/gaxzrdl8840rsd18a7vgrq1bqxvfsjjs-linux-6.1.63-stable_20231123-modules/lib/modules/6.1.63/kernel/drivers/virtio/virtio_ring.ko.xz
       >   copying dependency: /nix/store/gaxzrdl8840rsd18a7vgrq1bqxvfsjjs-linux-6.1.63-stable_20231123-modules/lib/modules/6.1.63/kernel/drivers/virtio/virtio_mmio.ko.xz
       > root module: virtio_blk
       >   dependency already copied: /nix/store/gaxzrdl8840rsd18a7vgrq1bqxvfsjjs-linux-6.1.63-stable_20231123-modules/lib/modules/6.1.63/kernel/drivers/virtio/virtio.ko.xz
       >   dependency already copied: /nix/store/gaxzrdl8840rsd18a7vgrq1bqxvfsjjs-linux-6.1.63-stable_20231123-modules/lib/modules/6.1.63/kernel/drivers/virtio/virtio_ring.ko.xz
       >   copying dependency: /nix/store/gaxzrdl8840rsd18a7vgrq1bqxvfsjjs-linux-6.1.63-stable_20231123-modules/lib/modules/6.1.63/kernel/drivers/block/virtio_blk.ko.xz
       > root module: virtio_scsi
       >   dependency already copied: /nix/store/gaxzrdl8840rsd18a7vgrq1bqxvfsjjs-linux-6.1.63-stable_20231123-modules/lib/modules/6.1.63/kernel/drivers/virtio/virtio.ko.xz
       >   dependency already copied: /nix/store/gaxzrdl8840rsd18a7vgrq1bqxvfsjjs-linux-6.1.63-stable_20231123-modules/lib/modules/6.1.63/kernel/drivers/virtio/virtio_ring.ko.xz
       >   copying dependency: /nix/store/gaxzrdl8840rsd18a7vgrq1bqxvfsjjs-linux-6.1.63-stable_20231123-modules/lib/modules/6.1.63/kernel/drivers/scsi/virtio_scsi.ko.xz
       > root module: virtio_balloon
       >   dependency already copied: /nix/store/gaxzrdl8840rsd18a7vgrq1bqxvfsjjs-linux-6.1.63-stable_20231123-modules/lib/modules/6.1.63/kernel/drivers/virtio/virtio.ko.xz
       >   dependency already copied: /nix/store/gaxzrdl8840rsd18a7vgrq1bqxvfsjjs-linux-6.1.63-stable_20231123-modules/lib/modules/6.1.63/kernel/drivers/virtio/virtio_ring.ko.xz
       >   copying dependency: /nix/store/gaxzrdl8840rsd18a7vgrq1bqxvfsjjs-linux-6.1.63-stable_20231123-modules/lib/modules/6.1.63/kernel/drivers/virtio/virtio_balloon.ko.xz
       > root module: virtio_console
       >   dependency already copied: /nix/store/gaxzrdl8840rsd18a7vgrq1bqxvfsjjs-linux-6.1.63-stable_20231123-modules/lib/modules/6.1.63/kernel/drivers/virtio/virtio.ko.xz
       >   dependency already copied: /nix/store/gaxzrdl8840rsd18a7vgrq1bqxvfsjjs-linux-6.1.63-stable_20231123-modules/lib/modules/6.1.63/kernel/drivers/virtio/virtio_ring.ko.xz
       >   copying dependency: /nix/store/gaxzrdl8840rsd18a7vgrq1bqxvfsjjs-linux-6.1.63-stable_20231123-modules/lib/modules/6.1.63/kernel/drivers/char/virtio_console.ko.xz
       > root module: mptspi
       >   dependency already copied: /nix/store/gaxzrdl8840rsd18a7vgrq1bqxvfsjjs-linux-6.1.63-stable_20231123-modules/lib/modules/6.1.63/kernel/drivers/scsi/scsi_transport_spi.ko.xz
       >   copying dependency: /nix/store/gaxzrdl8840rsd18a7vgrq1bqxvfsjjs-linux-6.1.63-stable_20231123-modules/lib/modules/6.1.63/kernel/drivers/message/fusion/mptbase.ko.xz
       >   copying dependency: /nix/store/gaxzrdl8840rsd18a7vgrq1bqxvfsjjs-linux-6.1.63-stable_20231123-modules/lib/modules/6.1.63/kernel/drivers/message/fusion/mptscsih.ko.xz
       >   copying dependency: /nix/store/gaxzrdl8840rsd18a7vgrq1bqxvfsjjs-linux-6.1.63-stable_20231123-modules/lib/modules/6.1.63/kernel/drivers/message/fusion/mptspi.ko.xz
       > root module: vmxnet3
       >   copying dependency: /nix/store/gaxzrdl8840rsd18a7vgrq1bqxvfsjjs-linux-6.1.63-stable_20231123-modules/lib/modules/6.1.63/kernel/drivers/net/vmxnet3/vmxnet3.ko.xz
       > root module: vsock
       >   copying dependency: /nix/store/gaxzrdl8840rsd18a7vgrq1bqxvfsjjs-linux-6.1.63-stable_20231123-modules/lib/modules/6.1.63/kernel/net/vmw_vsock/vsock.ko.xz
       > root module: simplefb
       >   builtin dependency: simplefb
       > root module: sun4i-drm
       > modprobe: FATAL: Module sun4i-drm not found in directory /nix/store/gaxzrdl8840rsd18a7vgrq1bqxvfsjjs-linux-6.1.63-stable_20231123-modules/lib/modules/6.1.63
       For full logs, run 'nix log /nix/store/bmi8p9w7m1d9yinrbpk6h1a5cdl0bkai-linux-6.1.63-stable_20231123-modules-shrunk.drv'.
error: 1 dependencies of derivation '/nix/store/ala27ak5hnn6idshxg09a7idwgnwd8av-initrd-linux-6.1.63-stable_20231123.drv' failed to build
error: 1 dependencies of derivation '/nix/store/1izd0dd7a8sgp2j298vm58hwzjfxc9p2-nixos-system-pi4-24.05.20240131.b8b232a.drv' failed to build
error: 1 dependencies of derivation '/nix/store/q1nlmcy4gsz93xyhjs9aqyqyibbaczai-ext4-fs.img.zst.drv' failed to build
error: 1 dependencies of derivation '/nix/store/0xjk6s95y5hyih52v8wg3qpzksxmxx0r-nixos-sd-image-24.05.20240131.b8b232a-aarch64-linux.img.drv' failed to build

stratos in 🌐 vern in ultragear on  master [$+] is 📦 v via 🐍 took 45m3s
