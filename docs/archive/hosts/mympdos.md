# myMPDOS

myMPDos is a Raspberry Pi image (aarch64) based on Alpine Linux, and it is running
entirely in RAM. It contains a copy of mps and myMPD audio service, used as an audio
player running on a PI3 with an Audio DAC.

Although this host is not running nixos, here are some setup instructions


## SD card preparation

Download latest image from:
https://github.com/jcorporation/myMPDos/releases

Burn it on sdcard with:

```
sudo dd bs=4M if=/home/stratos/Downloads/isos/myMPDos-aarch64-1.4.1-20240128.img of=/dev/sd[X] status=progress oflag=sync
```

Create the following bootstrap.txt in the root of the sdcard:

```
# Advanced bootstrap configuration file for myMPDos

ROOT_PASSWORD="mympdos"

TIMEZONE="Europe/Madrid"

# Keybord layout
KEYBOARD_LAYOUT="us"
KEYBOARD_VARIANT="us"

# Automounting of USB devices
ENABLE_AUTOMOUNT="true"

# Automatic MPD configuration (outputs)
ENABLE_CONFIGMPD="true"

# Software
#MPD version
#1 = MPD stable
#2 = MPD master branch - unstable
MPD_VERSION="1"

# Add packages: usbutils raspberrypi busybox-extras net-tools
ADVANCED_SOFTWARE="true"

# Add user defined packages (space separated list)
EXTRA_SOFTWARE="tailscale nfs-utils micro"

# Services to start
ENABLE_CRON="false"
ENABLE_RNGD="true"

# MPD settings
# Mixer
ENABLE_MIXER="true"
SOFTWARE_MIXER_FALLBACK="false"

# Upsampling
UPSAMPLING="false"
AUDIO_OUTPUT_FORMAT="192000:24:2"
SAMPLERATE_CONVERTER="soxr very high"

# Or use the Resampler
RESAMPLER="libsamplerate"
LIBSAMPLERATE_TYPE="0"

#RESAMPLER="soxr"
#SOXR_QUALITY="high"
#SOXR_THREADS="1"

# Network settings
IP_HOSTNAME="pi3"
#If WLAN_ENABLED is not set to true eth0 is configured
WLAN_ENABLE="false"
#WLAN_SSID="ssid of your wlan"
#WLAN_KEYMGMT="WPA-PSK"
#WLAN_PSK="psk of the ssid"
IP_TYPE="dhcp"
#IP_COPY_DHCP_TO_STATIC="true"
#Comment IP_TYPE line and uncomment other IP_* settings to configure a static ip
#IP_TYPE="static"
#IP_ADDRESS=""
#IP_NETMASK=""
#IP_GATEWAY=""
#Set to true to configure dns settings
#IP_DNS_CONFIGURE="false"
#IP_DNS_SERVER1=""
#IP_DNS_SERVER2=""
#IP_DNS_DOMAIN=""
#IP_DNS_SEARCH=""

# Pi settings
# Install bluetooth packages
BT_ENABLE="false"

# Enable internal bluetooth chip
BT_INTERNAL="false"

# myMPDos repository
MYMPDOS_REPOSITORY="https://raw.githubusercontent.com/jcorporation/myMPDos/master/repository"
#MYMPDOS_REPOSITORY="https://raw.githubusercontent.com/jcorporation/myMPDos/devel/repository"

# List of audio hats: https://jcorporation.github.io/myMPDos/references/audio-hats
AUDIOHAT="iqaudio-dacplus"
DISABLE_HDMI="false"

# myMPD configuration
# https://jcorporation.github.io/myMPD/configuration/configuration-files
#MYMPD_ACL=""
#MYMPD_ALBUM_GROUP_TAG="Date"
#MYMPD_ALBUM_MODE="adv"
#MYMPD_COVERCACHE_KEEP_DAYS=32
#MYMPD_HTTP=true
#MYMPD_HTTP_HOST="[::]"
#MYMPD_HTTP_PORT=80
#MYMPD_LOGLEVEL=5
#MYMPD_LUALIBS=all
#MYMPD_URI=""
#MYMPD_SAVE_CACHES=true
#MYMPD_SCRIPTACL=""
#MYMPD_STICKERS=true
#MYMPD_SSL=true
#MYMPD_SSL_PORT=443
#MYMPD_SSL_SAN=""
#MYMPD_CUSTOM_CERT=false
#MYMPD_SSL_CERT=""
#MYMPD_SSL_KEY=""
```

## Post Initial boot setup

Boot the sdcard on the PI3.

Log in with root and do some initial setup:

### Tailscale

Start and login with tailscale:

```
rc-update add tailscale
rc-service tailscale start
tailscale login
```

and persist in the alpine overlay:

```
lbu include /var/lib/tailscale
lbu commit
```

### Mount NFS

Mount nfs music folder folder:

```
rc-update add netmount default
rwdata.sh
mkdir /srv/mpd
rwdata.sh
echo "vern:/export/music /srv/mpd nfs soft,_netdev 0 0" >> /etc/fstab
mount -a
save.sh
```

### Edit mpd.conf

Make the following edits to `/etc/mpd.conf`:

```
music_directory "/srv/mpd/music"
playlist_directory "/srv/mpd/playlists"
database {
    plugin    "proxy"
    host      "mpd.vern.gerakakis.net"
    port      "6600"
    keepalive "yes"
}
bind_to_address "/run/mpd/socket"
```

Restart mpd with:  `service mpd restart`

Persist changes (from
https://github.com/jcorporation/myMPDos/issues/38#issuecomment-2067703134) with:

```
cp /etc/mpd.conf /etc/mympdos/custom/mpd.conf
lbu_commit
```


## Debugging

Check connection to proxied mpd:

openssl s_client -connect mpd.vern.gerakakis.net:443