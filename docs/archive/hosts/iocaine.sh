#!/usr/bin/env bash

set -eu

# We do NOT touch sda, as it already has a working windows installation
# Disk /dev/sda: 223.57 GiB, 240057409536 bytes, 468862128 sectors
# Disk model: PNY CS900 240GB

# partition sdb
# Disk /dev/sdb: 3.64 TiB, 4000787030016 bytes, 7814037168 sectors
# Disk model: ST4000DM004-2CV1
wipefs -afi /dev/sdb
parted --script /dev/sdb --\
  mklabel gpt \
  mkpart primary btrfs 1MB 100%

# Partition /dev/nvme0n1
# Disk /dev/nvme0n1: 931.51 GiB, 1000204886016 bytes, 1953525168 sectors
# Disk model: KINGSTON SFYRS1000G
wipefs -afi /dev/nvme0n1
parted --script /dev/nvme0n1 --\
  mklabel gpt \
  mkpart primary btrfs 1MB -64GB \
  mkpart primary linux-swap -64GB 100%


# format swap
mkswap -L swap /dev/nvme0n1p2
swapon /dev/nvme0n1p2


# make btrfs for both disks
mkfs.btrfs -L kingssd -f /dev/nvme0n1p1
mkfs.btrfs -L rusthdd -f /dev/sdb1

# Create/mount Partitions

# create ssd subvolumes
mount /dev/nvme0n1p1 /mnt
# mount partitions
btrfs subvolume create /mnt/root
btrfs subvolume create /mnt/nix
btrfs subvolume create /mnt/log
btrfs subvolume create /mnt/home
# unmount
umount /mnt

# create hdd subvolumes
mount /dev/sdb1 /mnt
# mount partitions
btrfs subvolume create /mnt/data
btrfs subvolume create /mnt/projects
btrfs subvolume create /mnt/backups
# unmount
umount /mnt


# Mount in proper positions
mount /dev/nvme0n1p1 -o compress=zstd,noatime,ssd,discard=async,space_cache=v2,subvol=root /mnt

mkdir -p /mnt/boot
# Boot already exists on sda1
mount /dev/sda1 /mnt/boot

mkdir /mnt/nix
mount /dev/nvme0n1p1 -o compress=zstd,noatime,ssd,discard=async,space_cache=v2,subvol=nix /mnt/nix

mkdir -p /mnt/var/log
mount /dev/nvme0n1p1 -o compress=zstd,noatime,ssd,discard=async,space_cache=v2,subvol=log /mnt/var/log

mkdir /mnt/home
mount /dev/nvme0n1p1 -o compress=zstd,noatime,ssd,discard=async,space_cache=v2,subvol=home /mnt/home

mkdir -p /mnt/data
mount /dev/sdb1 -o compress=zstd,noatime,subvol=data /mnt/data

mkdir -p /mnt/projects
mount /dev/sdb1 -o compress=zstd,noatime,subvol=projects /mnt/projects

mkdir -p /mnt/backups
mount /dev/sdb1 -o compress=zstd,noatime,subvol=backups /mnt/backups

# Finally generate config files
# nixos-generate-config --root /mnt
