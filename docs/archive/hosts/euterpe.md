Download latest aarch64-linux build from:
`https://hydra.nixos.org/job/nixos/trunk-combined/nixos.sd_image.aarch64-linux`


Check where the sd card is mounted:
`sudo fdisk -l`


Burn to sd card:
```
sudo umount /dev/sdc
zstd -d  nixos-sd-image-24.05pre587158.cbc4211f0aff-aarch64-linux.img.zst
sudo dd if=nixos-sd-image-24.05pre587158.cbc4211f0aff-aarch64-linux.img of=/dev/sdc bs=4M
sync
sudo umount /dev/sdc
```

Move card to pi3
Boot it

It would help if you have keyboard and monitor.
```
passwd
sudo systemctl start sshd
```

## Option 1

Install git in order to checkout ultragear:
```
nix-shell -p git
git clone git@gitlab.com:/stratosgear/ultragear
cd ultragear
sudo nixos-rebuild switch --flake .#pi3
```

Note: Build fails more likely due to heavy derivation memory issues.. :(

## Option 2
(From: https://www.eisfunke.com/posts/2023/nixos-on-raspberry-pi.html)

Build the required derivations on a beafier machine:
`nix build .#nixosConfigurations.pi3.config.system.build.toplevel  --print-out-paths`
Outs: /nix/store/blk11hnkh5xlnrasgl7lsggm54rif6aq-nixos-system-pi3-24.05.20240223.cbc4211

The above will spit the path of where the packages are stored. Use the path in the next command

And then copy them over to pi3:
Note: make sure you have passwordless login, because nix copy does NOT ask for password
`nix copy /nix/store/blk11hnkh5xlnrasgl7lsggm54rif6aq-nixos-system-pi3-24.05.20240223.cbc4211 --to ssh://root@192.168.3.90`

Finally, on the pi3 itself, apply them:
`nixos-install --root / --system /nix/store/blk11hnkh5xlnrasgl7lsggm54rif6aq-nixos-system-pi3-24.05.20240223.cbc4211 --no-root-password`

and reboot

## Option 3

Drop Nixos!!!  Cannot make it work :(
Actually I got nixosto boot on pi3, but cannot use overlays in order to load the IQAudio
DigiDac+ dac, thus I cannot use it as an audio server/jukebox...!!!


Install arch instead:
https://archlinuxarm.org/platforms/armv8/broadcom/raspberry-pi-3

Backup SD card (so we do not have to do the above again, if we need to restart):
dd if=/dev/sdc | gzip > virgin-arch-on-pi3.img.gz

Restore SD card:
gunzip -c virgin-arch-on-pi3.img.gz | sudo dd of=/dev/sdc bs=4M status=progress


Complete installation:
```
su
pacman-key --init
pacman-key --populate archlinuxarm
pacman -Syu
```


Basic administration:
```
su
useradd -m -G wheel -s /bin/bash stratos
passwd stratos

# to activate wheel, uncomment the line %wheel ALL=(ALL) ALL, with
visudo

# or do it with one command:
sudo sed -i 's/^# %wheel ALL=(ALL:ALL) ALL/%wheel ALL=(ALL:ALL) ALL/' sudoers

su stratos
pacman -S sudo micro btop

# install paru
sudo pacman -S --needed base-devel git
git clone https://aur.archlinux.org/paru.git
cd paru
# the following will take like 30mins to compile
makepkg -si
cd ..
rm -rf paru
paru

reboot
```

```
ssh-keygen

```
# https://raspberrypi.stackexchange.com/questions/11735/using-pi-to-stream-all-audio-output-from-my-pc-to-my-stereo
# For arch specific: https://wiki.archlinux.org/title/PulseAudio#Networked_audio

Install pulseaudio:
paru -S pulseaudio pulseaudio-module-zeroconf avahi-daemon


Install required apps:

packages:
mympd libnewt tailscale nfs-utils

Install groups:
groupadd musiclib -g 27001
usermod -aG musiclib mpd
usermod -aG musiclib stratos
