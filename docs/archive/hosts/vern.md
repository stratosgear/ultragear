# vern

## Backup original server

Docker volumes:

Compress folder:
tar --zstd -cf /save/somewhere.zst folderName

Uncompress folder (will create folder in current dir)
tar --zstd -xf somewhere.zst




`vern` is a dedicated server on Hetzner

Containers:
https://dollarrandom.com/posts/home-lab/nix-as-docker-host/


Following along:
- https://mhu.dev/posts/2024-01-06-nixos-on-hetzner/

As dedicated hetzner server:
- https://github.com/LGUG2Z/nixos-hetzner-robot-starter


disko-config samples:
- https://github.com/lutzgo/nixos-config/blob/e5d46ec7e4c7eef785d85dd702b6ed8c919ebe5d/templates/disko/efi-luks-btrfs-swap-raid1.nix#L7


# Installing through nix-anywhere

install log messages:

```
### Installing NixOS ###
Pseudo-terminal will not be allocated because stdin is not a terminal.
Warning: Permanently added '95.216.117.22' (ED25519) to the list of known hosts.
installing the boot loader...
setting up /etc...
updating GRUB 2 menu...
installing the GRUB 2 boot loader on /dev/sda...
Installing for i386-pc platform.
Installation finished. No error reported.
installing the GRUB 2 boot loader on /dev/sdb...
Installing for i386-pc platform.
/nix/store/w8wc02l6nl9sqb6fml23s70dsrqqvhy7-grub-2.12/sbin/grub-install: warning: Attempting to install GRUB to a disk with multiple partition labels.  This is not supported yet..
/nix/store/w8wc02l6nl9sqb6fml23s70dsrqqvhy7-grub-2.12/sbin/grub-install: error: embedding is not possible, but this is required for cross-disk install.
/nix/store/wf1kx3m8bx4v7bzdwsnq8prhxsqw4yc6-install-grub.pl: installation of GRUB on /dev/sdb failed: No such file or directory
installation finished!
umount: /mnt/boot unmounted
umount: /mnt/home unmounted
umount: /mnt/nix unmounted
umount: /mnt/var/local unmounted
umount: /mnt/var/log unmounted
umount: /mnt unmounted
### Waiting for the machine to become reachable again ###
ssh: connect to host 95.216.117.22 port 22: Connection refused
### Done! ###
```

```
### Installing NixOS ###
Pseudo-terminal will not be allocated because stdin is not a terminal.
Warning: Permanently added '95.216.117.22' (ED25519) to the list of known hosts.
installing the boot loader...
setting up /etc...
updating GRUB 2 menu...
installing the GRUB 2 boot loader on /dev/sda...
Installing for i386-pc platform.
Installation finished. No error reported.
installing the GRUB 2 boot loader into /boot...
Installing for x86_64-efi platform.
Installation finished. No error reported.
installation finished!
umount: /mnt/boot unmounted
umount: /mnt/home unmounted
umount: /mnt/nix unmounted
umount: /mnt/var/local unmounted
umount: /mnt/var/log unmounted
umount: /mnt unmounted
### Waiting for the machine to become reachable again ###
ssh: connect to host 95.216.117.22 port 22: Connection refused
### Done! ###
```

installer logs for disko

```
+ udevadm trigger --subsystem-match=block
+ udevadm settle
+ device=/dev/disk/by-partlabel/EFI
+ extraArgs=()
+ declare -a extraArgs
+ format=vfat
+ mountOptions=('defaults')
+ declare -a mountOptions
+ mountpoint=/boot
+ type=filesystem
+ mkfs.vfat /dev/disk/by-partlabel/EFI
mkfs.fat 4.2 (2021-01-31)
+ sgdisk --align-end --new=3:0:+4G --change-name=3:swap --typecode=3:8300 /dev/sda
The operation has completed successfully.
+ partprobe /dev/sda
+ udevadm trigger --subsystem-match=block
+ udevadm settle
+ device=/dev/disk/by-partlabel/swap
+ extraArgs=()
+ declare -a extraArgs
+ randomEncryption=
+ resumeDevice=1
+ type=swap
+ mkswap /dev/disk/by-partlabel/swap
Setting up swapspace version 1, size = 4 GiB (4294963200 bytes)
no label, UUID=4b94244b-c5bc-4bd4-a418-1c88ded1cc50
+ sgdisk --align-end --new=4:0:-0 --change-name=4:rootfs --typecode=4:8300 /dev/sda
The operation has completed successfully.
+ partprobe /dev/sda
+ udevadm trigger --subsystem-match=block
+ udevadm settle
+ device=/dev/disk/by-partlabel/rootfs
+ extraArgs=('-f' '-m raid1 -d raid1' '/dev/sdb')
+ declare -a extraArgs
+ mountOptions=('defaults')
+ declare -a mountOptions
+ mountpoint=
+ type=btrfs
+ mkfs.btrfs /dev/disk/by-partlabel/rootfs -f -m raid1 -d raid1 /dev/sdb
btrfs-progs v6.6.3
See https://btrfs.readthedocs.io for more information.

NOTE: several default settings have changed in version 5.15, please make sure
      this does not affect your deployments:
      - DUP for metadata (-m dup)
      - enabled no-holes (-O no-holes)
      - enabled free-space-tree (-R free-space-tree)

Label:              (null)
UUID:               52808366-32d8-49e8-b59f-817f219c392e
Node size:          16384
Sector size:        4096
Filesystem size:    7.27TiB
Block group profiles:
  Data:             RAID1             1.00GiB
  Metadata:         RAID1             1.00GiB
  System:           RAID1             8.00MiB
SSD detected:       no
Zoned device:       no
Incompat features:  extref, skinny-metadata, no-holes, free-space-tree
Runtime features:   free-space-tree
Checksum:           crc32c
Number of devices:  2
Devices:
   ID        SIZE  PATH
    1     3.63TiB  /dev/disk/by-partlabel/rootfs
    2     3.64TiB  /dev/sdb

++ mktemp -d
+ MNTPOINT=/tmp/tmp.zC3yX7XZUO
+ mount /dev/disk/by-partlabel/rootfs /tmp/tmp.zC3yX7XZUO -o subvol=/
+ trap 'umount $MNTPOINT; rm -rf $MNTPOINT' EXIT
+ SUBVOL_ABS_PATH=/tmp/tmp.zC3yX7XZUO//home
++ dirname /tmp/tmp.zC3yX7XZUO//home
+ mkdir -p /tmp/tmp.zC3yX7XZUO
+ btrfs subvolume create /tmp/tmp.zC3yX7XZUO//home
Create subvolume '/tmp/tmp.zC3yX7XZUO/home'
++ umount /tmp/tmp.zC3yX7XZUO
++ rm -rf /tmp/tmp.zC3yX7XZUO
++ mktemp -d
+ MNTPOINT=/tmp/tmp.gILuSO4Ipi
+ mount /dev/disk/by-partlabel/rootfs /tmp/tmp.gILuSO4Ipi -o subvol=/
+ trap 'umount $MNTPOINT; rm -rf $MNTPOINT' EXIT
+ SUBVOL_ABS_PATH=/tmp/tmp.gILuSO4Ipi//nix
++ dirname /tmp/tmp.gILuSO4Ipi//nix
+ mkdir -p /tmp/tmp.gILuSO4Ipi
+ btrfs subvolume create /tmp/tmp.gILuSO4Ipi//nix
Create subvolume '/tmp/tmp.gILuSO4Ipi/nix'
++ umount /tmp/tmp.gILuSO4Ipi
++ rm -rf /tmp/tmp.gILuSO4Ipi
++ mktemp -d
+ MNTPOINT=/tmp/tmp.K4RXHTKHWt
+ mount /dev/disk/by-partlabel/rootfs /tmp/tmp.K4RXHTKHWt -o subvol=/
+ trap 'umount $MNTPOINT; rm -rf $MNTPOINT' EXIT
+ SUBVOL_ABS_PATH=/tmp/tmp.K4RXHTKHWt//root
++ dirname /tmp/tmp.K4RXHTKHWt//root
+ mkdir -p /tmp/tmp.K4RXHTKHWt
+ btrfs subvolume create /tmp/tmp.K4RXHTKHWt//root
Create subvolume '/tmp/tmp.K4RXHTKHWt/root'
++ umount /tmp/tmp.K4RXHTKHWt
++ rm -rf /tmp/tmp.K4RXHTKHWt
++ mktemp -d
+ MNTPOINT=/tmp/tmp.ITKEL03hTB
+ mount /dev/disk/by-partlabel/rootfs /tmp/tmp.ITKEL03hTB -o subvol=/
+ trap 'umount $MNTPOINT; rm -rf $MNTPOINT' EXIT
+ SUBVOL_ABS_PATH=/tmp/tmp.ITKEL03hTB//var_local
++ dirname /tmp/tmp.ITKEL03hTB//var_local
+ mkdir -p /tmp/tmp.ITKEL03hTB
+ btrfs subvolume create /tmp/tmp.ITKEL03hTB//var_local
Create subvolume '/tmp/tmp.ITKEL03hTB/var_local'
++ umount /tmp/tmp.ITKEL03hTB
++ rm -rf /tmp/tmp.ITKEL03hTB
++ mktemp -d
+ MNTPOINT=/tmp/tmp.OmPfEq44vk
+ mount /dev/disk/by-partlabel/rootfs /tmp/tmp.OmPfEq44vk -o subvol=/
+ trap 'umount $MNTPOINT; rm -rf $MNTPOINT' EXIT
+ SUBVOL_ABS_PATH=/tmp/tmp.OmPfEq44vk//var_log
++ dirname /tmp/tmp.OmPfEq44vk//var_log
+ mkdir -p /tmp/tmp.OmPfEq44vk
+ btrfs subvolume create /tmp/tmp.OmPfEq44vk//var_log
Create subvolume '/tmp/tmp.OmPfEq44vk/var_log'
++ umount /tmp/tmp.OmPfEq44vk
++ rm -rf /tmp/tmp.OmPfEq44vk
+ device=/dev/sdb
+ imageSize=2G
+ name=_dev_sdb
+ type=disk
+ device=/dev/sdb
+ type=gpt
+ sgdisk --align-end --new=1:0:-0 --change-name=1:disk-_dev_sdb-btrfs --typecode=1:8300 /dev/sdb
Creating new GPT entries in memory.
The operation has completed successfully.
+ partprobe /dev/sdb
+ udevadm trigger --subsystem-match=block
+ udevadm settle
+ device=/dev/disk/by-partlabel/disk-_dev_sdb-btrfs
+ extraArgs=()
+ declare -a extraArgs
+ mountOptions=('defaults')
+ declare -a mountOptions
+ mountpoint=
+ type=btrfs
+ mkfs.btrfs /dev/disk/by-partlabel/disk-_dev_sdb-btrfs
btrfs-progs v6.6.3
See https://btrfs.readthedocs.io for more information.

NOTE: several default settings have changed in version 5.15, please make sure
      this does not affect your deployments:
      - DUP for metadata (-m dup)
      - enabled no-holes (-O no-holes)
      - enabled free-space-tree (-R free-space-tree)

Label:              (null)
UUID:               14bf1f7f-e973-47a0-af86-eef3381947c1
Node size:          16384
Sector size:        4096
Filesystem size:    3.64TiB
Block group profiles:
  Data:             single            8.00MiB
  Metadata:         DUP               1.00GiB
  System:           DUP               8.00MiB
SSD detected:       no
Zoned device:       no
Incompat features:  extref, skinny-metadata, no-holes, free-space-tree
Runtime features:   free-space-tree
Checksum:           crc32c
Number of devices:  1
Devices:
   ID        SIZE  PATH
    1     3.64TiB  /dev/disk/by-partlabel/disk-_dev_sdb-btrfs

+ set -efux
+ device=/dev/sda
+ imageSize=2G
+ name=_dev_sda
+ type=disk
+ device=/dev/sda
+ type=gpt
+ device=/dev/sdb
+ imageSize=2G
+ name=_dev_sdb
+ type=disk
+ device=/dev/sdb
+ type=gpt
+ device=/dev/sda
+ imageSize=2G
+ name=_dev_sda
+ type=disk
+ device=/dev/sda
+ type=gpt
+ device=/dev/disk/by-partlabel/rootfs
+ extraArgs=('-f' '-m raid1 -d raid1' '/dev/sdb')
+ declare -a extraArgs
+ mountOptions=('defaults')
+ declare -a mountOptions
+ mountpoint=
+ type=btrfs
+ findmnt /dev/disk/by-partlabel/rootfs /mnt/
+ mount /dev/disk/by-partlabel/rootfs /mnt/ -o compress=zstd -o noatime -o subvol=/root -o X-mount.mkdir
+ device=/dev/sda
+ imageSize=2G
+ name=_dev_sda
+ type=disk
+ device=/dev/sda
+ type=gpt
+ device=/dev/disk/by-partlabel/EFI
+ extraArgs=()
+ declare -a extraArgs
+ format=vfat
+ mountOptions=('defaults')
+ declare -a mountOptions
+ mountpoint=/boot
+ type=filesystem
+ findmnt /dev/disk/by-partlabel/EFI /mnt/boot
+ mount /dev/disk/by-partlabel/EFI /mnt/boot -t vfat -o defaults -o X-mount.mkdir
+ device=/dev/sda
+ imageSize=2G
+ name=_dev_sda
+ type=disk
+ device=/dev/sda
+ type=gpt
+ device=/dev/disk/by-partlabel/swap
+ extraArgs=()
+ declare -a extraArgs
+ randomEncryption=
+ resumeDevice=1
+ type=swap
+ swapon --show
++ readlink -f /dev/disk/by-partlabel/swap
+ grep -q '^/dev/sda3 '
+ swapon /dev/disk/by-partlabel/swap
+ device=/dev/sda
+ imageSize=2G
+ name=_dev_sda
+ type=disk
+ device=/dev/sda
+ type=gpt
+ device=/dev/disk/by-partlabel/rootfs
+ extraArgs=('-f' '-m raid1 -d raid1' '/dev/sdb')
+ declare -a extraArgs
+ mountOptions=('defaults')
+ declare -a mountOptions
+ mountpoint=
+ type=btrfs
+ findmnt /dev/disk/by-partlabel/rootfs /mnt/home
+ mount /dev/disk/by-partlabel/rootfs /mnt/home -o compress=zstd -o noatime -o subvol=/home -o X-mount.mkdir
+ device=/dev/sda
+ imageSize=2G
+ name=_dev_sda
+ type=disk
+ device=/dev/sda
+ type=gpt
+ device=/dev/disk/by-partlabel/rootfs
+ extraArgs=('-f' '-m raid1 -d raid1' '/dev/sdb')
+ declare -a extraArgs
+ mountOptions=('defaults')
+ declare -a mountOptions
+ mountpoint=
+ type=btrfs
+ findmnt /dev/disk/by-partlabel/rootfs /mnt/nix
+ mount /dev/disk/by-partlabel/rootfs /mnt/nix -o compress=zstd -o noatime -o subvol=/nix -o X-mount.mkdir
+ device=/dev/sda
+ imageSize=2G
+ name=_dev_sda
+ type=disk
+ device=/dev/sda
+ type=gpt
+ device=/dev/disk/by-partlabel/rootfs
+ extraArgs=('-f' '-m raid1 -d raid1' '/dev/sdb')
+ declare -a extraArgs
+ mountOptions=('defaults')
+ declare -a mountOptions
+ mountpoint=
+ type=btrfs
+ findmnt /dev/disk/by-partlabel/rootfs /mnt/var/local
+ mount /dev/disk/by-partlabel/rootfs /mnt/var/local -o compress=zstd -o noatime -o subvol=/var_local -o X-mount.mkdir
+ device=/dev/sda
+ imageSize=2G
+ name=_dev_sda
+ type=disk
+ device=/dev/sda
+ type=gpt
+ device=/dev/disk/by-partlabel/rootfs
+ extraArgs=('-f' '-m raid1 -d raid1' '/dev/sdb')
+ declare -a extraArgs
+ mountOptions=('defaults')
+ declare -a mountOptions
+ mountpoint=
+ type=btrfs
+ findmnt /dev/disk/by-partlabel/rootfs /mnt/var/log
+ mount /dev/disk/by-partlabel/rootfs /mnt/var/log -o compress=zstd -o noatime -o subvol=/var_log -o X-mount.mkdir
+ rm -rf /tmp/tmp.eoSAxKqunP
Connection to 95.216.117.22 closed.
```

Installing EFI partition with systemd-boot:

```
### Installing NixOS ###
Pseudo-terminal will not be allocated because stdin is not a terminal.
Warning: Permanently added '95.216.117.22' (ED25519) to the list of known hosts.
installing the boot loader...
setting up /etc...
Initializing machine ID from random generator.
Created "/boot/EFI".
Created "/boot/EFI/systemd".
Created "/boot/EFI/BOOT".
Created "/boot/loader".
Created "/boot/loader/entries".
Created "/boot/EFI/Linux".
Copied "/nix/store/vfmf8qh892jfl107hih0yfnic00byjgj-systemd-254.6/lib/systemd/boot/efi/systemd-bootx64.efi" to "/boot/EFI/systemd/systemd-bootx64.efi".
Copied "/nix/store/vfmf8qh892jfl107hih0yfnic00byjgj-systemd-254.6/lib/systemd/boot/efi/systemd-bootx64.efi" to "/boot/EFI/BOOT/BOOTX64.EFI".
! Mount point '/boot' which backs the random seed file is world accessible, which is a security hole! !
! Random seed file '/boot/loader/.#bootctlrandom-seedd7231ef1fe60ce81' is world accessible, which is a security hole! !
Random seed file /boot/loader/random-seed successfully written (32 bytes).
installation finished!
umount: /mnt/boot unmounted
umount: /mnt/home unmounted
umount: /mnt/nix unmounted
umount: /mnt/var/local unmounted
umount: /mnt/var/log unmounted
umount: /mnt unmounted
### Waiting for the machine to become reachable again ###
ssh: connect to host 95.216.117.22 port 22: Connection refused
### Done! ###
```