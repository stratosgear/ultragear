# manual backups

``` ~/.pgpass
localhost:5454:*:postgres:password
```

## As .sql
pg_dump --clean -h localhost -p 5454 -U postgres -d wallabag | gzip > wallabag.sql.gz

Restore with:
gunzip -c wallabag.sql.gz | psql -h localhost -p 5454 -U postgres -d wallabag


## As psql custom-format dump

It also drops the db and restores from backup!!!

pg_dump -p 5454 -U postgres -h localhost -Fc  --compress=zstd:9 wallabag >
wallabag.zstd9.dump

restore with:
pg_restore --clean --if-exists -h localhost -p 5454 -U postgres -d wallabag wallabag.zstd9.dump

