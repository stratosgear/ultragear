## Create buckets

With:
garage bucket create [nextcloud-bucket]

Check with:
garage bucket list
garage bucket info nextcloud-bucket


##  Creating keys

Create with:
garage key create [nextcloud-app-key]

Output looks like:
Key name: nextcloud-app-key
Key ID: GK3515373e4c851ebaad366558
Secret key: 7d37d093435a41f2aab8f13c19ba067d9776c90215f56614adad6ece597dbb34
Authorized buckets:



Check key info witK
garage key list
garage key info [someKey]


## Access rights

garage bucket allow \
  --read \
  --write \
  --owner \
  nextcloud-bucket \
  --key nextcloud-app-key



## Minio client

Set alias:
mc alias set \
  [garage] \
  https://s3.gerakakis.net \
  <access key> \
  <secret key> \
  --api S3v4