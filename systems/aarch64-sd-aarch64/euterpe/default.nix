{ lib, modulesPath, pkgs, ... }:
with lib;
with lib.ultragear; {

  imports = [
    ./hardware-configuration.nix
  ];

  # Preserve space by disabling documentation and enaudo ling
  # automatic garbage collection
  documentation.nixos.enable = false;


  ultragear = {

    # settings that define this system
    system = {

      info = {
        display1 = "DP-1";
        display2 = "HDMI-1";
      };

      boot = {
        # Raspberry Pi requires a specific bootloader.
        enable = mkForce false;
      };
    };

    suites = {
      bootstrap = enabled;
      # common = enabled;
    };

  };

  system.stateVersion = "23.11";
}
