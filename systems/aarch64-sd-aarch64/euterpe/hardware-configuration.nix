{ config, lib, pkgs, modulesPath, ... }:

{

  # Boot settings a s given in this article:
  # https://eipi.xyz/blog/installing-nixos-on-a-rasberry-pi-3/
  # But trying to build an image on iocaine results in multiple of:

  # trace: warning: The option set for `boot.loader.raspberrypi` has been recommended against
  # for years, and is now formally deprecated.
  # It is possible it already did not work like you expected.
  # It never worked on the Raspberry Pi 4 family.
  # These options will be removed by NixOS 24.11.
  # boot.loader.grub.enable = false;

  # boot.kernelPackages = pkgs.linuxPackages_latest;
  # #boot.kernelParams = ["cma=256M"];

  # boot.loader.generic-extlinux-compatible.enable = true;
  # boot.loader.raspberryPi.uboot.enable = true;
  # boot.loader.raspberryPi.enable = true;
  # boot.loader.raspberryPi.version = 3;
  # boot.tmp.cleanOnBoot = true;


  #
  # Following setting from:
  # https://github.com/loafofpiecrust/dotnix/blob/ff396befacbcc3c47c5f6835f678dfe264b77d9d/headless-rpi3.nix#L6

  # Options for the initial installation image
  sdImage = {
    imageBaseName = "nixos-rpi-server";
    compressImage = false;
  };

  # Allow non-free firmware
  hardware.enableRedistributableFirmware = true;

  # Selected pieces of minimal profile
  environment.noXlibs = true;
  services.udisks2.enable = false;
  xdg.autostart.enable = false;
  xdg.mime.enable = false;
  xdg.sounds.enable = false;
  xdg.icons.enable = false;

  # Limit journal size
  services.journald.extraConfig = ''
    SystemMaxUse=256M
  '';

  boot.loader.raspberryPi.version = 3;
  boot.loader.timeout = 5;

  # Enable basic ALSA audio
  sound.enable = false;
  # Enables audio and allows booting without HDMI
  boot.loader.raspberryPi.firmwareConfig = ''
    disable_splash=1
    boot_delay=0
    dtparam=audio=off
    hdmi_force_hotplug=1
    hdmi_blanking=2
    dtoverlay=pi3-disable-bt
  '';
  boot.kernelParams = [ "console=ttyS1,115200n8" "psi=1" "quiet" ];
  boot.blacklistedKernelModules = [ "btusb" ];

  nix.settings = {
    # Trust sudo users to send nix packages over
    trusted-users = [ "@wheel" ];
    # Limit nix to 2 cores in case of long rebuilds
    max-jobs = 2;
    auto-optimise-store = true;
  };
  nix.daemonCPUSchedPolicy = "batch";
  nix.extraOptions = ''
    extra-experimental-features = nix-command flakes
  '';

  fileSystems = {
    "/" = {
      device = "/dev/disk/by-label/NIXOS_SD";
      fsType = "ext4";
    };
  };

  swapDevices = [{ device = "/swapfile"; size = 1024; }];
}
