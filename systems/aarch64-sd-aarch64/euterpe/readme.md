Guides:
- https://nixos.wiki/wiki/NixOS_on_ARM/Raspberry_Pi_3
- https://eipi.xyz/blog/installing-nixos-on-a-rasberry-pi-3/
  - https://discourse.nixos.org/t/raspberry-pi-3-documentation/11426/2
- https://myme.no/posts/2022-12-01-nixos-on-raspberrypi.html


Interesting issue:
- https://discourse.nixos.org/t/how-to-move-from-building-sd-card-images-to-be-able-to-remotely-deploy-new-versions/25057
