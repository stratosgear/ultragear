{
  lib,
  modulesPath,
  pkgs,
  config,
  ...
}:
with lib;
with lib.ultragear;
{

  imports = [
    ./hardware-configuration.nix
    (modulesPath + "/installer/scan/not-detected.nix")
    (modulesPath + "/profiles/qemu-guest.nix")
    ./disk-config.nix
  ];

  # maybe more:
  # https://github.com/shazow/nixfiles/blob/69e0e385013e7c0479385b3e9223c439cff784bd/hardware/framework-13-amd.nix#L22

  ultragear = {

    cache.local = enabled;

    # settings that define this system
    system = {
      boot = {
        enable = true;
        type = "efi";
      };
      info = {
        batteryClass = "BAT1";
        backlightClass = "intel_backlight";
        display1 = "eDP-1";
        display2 = "DP-2";
      };
      time.tz = "Europe/Madrid";

      nfsClient = {
        enable = true;
        mounts = [
          {
            mountPoint = "/nfs/music";
            # exportPoint = "vern.priv.gerakakis.net:/export/music";
            exportPoint = "vern:/export/music";
          }
          {
            mountPoint = "/nfs/stuff";
            # exportPoint = "vern.priv.gerakakis.net:/export/stuff";
            exportPoint = "vern:/export/stuff";
          }
        ];
      };
    };

    suites = {
      bootstrap = enabled;
      common = enabled;
    };

    desktop = {
      i3.enable = mkForce false;
      hyprland = {
        enable = true;
        monitors = [ ",highres,auto,1" ];
        #  We are not ready to add the monkeytype module yet, until fixed
        # "custom/monkeytype",
        waybar_modules_right = ''
          "custom/media",
          "pulseaudio",
          "network",
          "cpu",
          "memory",
          "temperature",
          "backlight",
          "hyprland/language",
          "battery",
          "idle_inhibitor",
          "power-profiles-daemon",
          "tray",
          "custom/power",
          "clock",
          "custom/notification",
        '';
      };
      addons = {
        # following is only needed with i3 usage
        # polybar = {
        #   barPrimary = {
        #     modulesRight = "filesystem pulseaudio xkeyboard memory cpu wlan eth backlight battery date";
        #   };
        # };
        dropbox = enabled;
      };
    };

    apps = {
      # firefox.pixelsPerPx = "1.4";
    };

    archetypes = {
      workstation = enabled;
      laptop = enabled;
    };

    services = {
      # syncthing = enabled;

      # docker = {
      #   enable = true;
      #   dataRoot = "/data/docker";
      # };

      backup = {
        instances.system = {
          excludePatterns = [
            ".git"
            ".Trash-1000"
          ];
          sourceDirectories = [
            # "/projects"
            "/home/stratos/Documents"
          ];
          repositories = [
            {
              path = "/backups/system";
              name = "onLocal";
              hcPingUid = "3c9b53c8-e230-4ffc-9882-1b6952224336";
            }
            {
              path = "s3:s3.us-west-000.backblazeb2.com/ultragear-backups/${config.networking.hostName}/system";
              name = "onBackblaze";
              hcPingUid = "408617df-2801-472a-ae70-b7e66b691ecd";
            }
            {
              path = "rest:https://${CONSTS.urls.backups.vern}/${config.networking.hostName}/system";
              name = "onVern";
              hcPingUid = "0fd542ba-febf-41eb-a4da-cdb1ebb98b23";
            }
          ];
        };
      };
    };

    hardware = {
      bluetooth = enabled;
    };
  };

  environment.systemPackages = [
    pkgs.gnubg
    pkgs.libreoffice-fresh
    pkgs.hunspell
    pkgs.hunspellDicts.en-us

    pkgs.obsidian
	  pkgs.vscodium
  ];

  system.stateVersion = "24.11";
}
