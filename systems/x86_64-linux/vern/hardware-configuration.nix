{ config, lib, modulesPath, ... }: {
  # Do not modify this file!  It was generated by ‘nixos-generate-config’
  # and may be overwritten by future invocations.  Please make changes
  # to /etc/nixos/configuration.nix instead.

  imports =
    [
      (modulesPath + "/installer/scan/not-detected.nix")
    ];

  boot.initrd.availableKernelModules = [ "xhci_pci" "ahci" "usbhid" "usb_storage" "sd_mod" "sr_mod" ];
  boot.initrd.kernelModules = [ ];
  boot.kernelModules = [ "kvm-intel" ];
  boot.extraModulePackages = [ ];

  fileSystems."/" = {
    device = "/dev/disk/by-uuid/508b785e-3c6c-4138-8f47-ba570d3ab94d";
    fsType = "btrfs";
    options = [ "noatime" "compress=zstd" "subvol=root" ];
  };

  fileSystems."/boot" = {
    device = "/dev/disk/by-uuid/AB23-740D";
    fsType = "vfat";
    options = [ "fmask=0137" "dmask=0027" ];
  };

  fileSystems."/nix" = {
    device = "/dev/disk/by-uuid/508b785e-3c6c-4138-8f47-ba570d3ab94d";
    fsType = "btrfs";
    options = [ "noatime" "compress=zstd" "subvol=nix" ];
  };

  fileSystems."/var/log" = {
    device = "/dev/disk/by-uuid/508b785e-3c6c-4138-8f47-ba570d3ab94d";
    fsType = "btrfs";
    options = [ "noatime" "compress=zstd" "subvol=log" ];
  };

  fileSystems."/home" = {
    device = "/dev/disk/by-uuid/508b785e-3c6c-4138-8f47-ba570d3ab94d";
    fsType = "btrfs";
    options = [ "noatime" "compress=zstd" "subvol=home" ];
  };

  fileSystems."/data" = {
    device = "/dev/disk/by-uuid/508b785e-3c6c-4138-8f47-ba570d3ab94d";
    fsType = "btrfs";
    options = [ "noatime" "compress=zstd" "subvol=data" ];
  };

  fileSystems."/backups" = {
    device = "/dev/disk/by-uuid/508b785e-3c6c-4138-8f47-ba570d3ab94d";
    fsType = "btrfs";
    options = [ "noatime" "compress=zstd" "subvol=backups" ];
  };

  swapDevices =
    [{ device = "/dev/disk/by-uuid/6b8ec27b-de74-4c30-a025-8028690cb560"; }];


  # Enables DHCP on each ethernet and wireless interface. In case of scripted networking
  # (the default) this is the recommended approach. When using systemd-networkd it's
  # still possible to use this option, but it's recommended to use it in conjunction
  # with explicit per-interface declarations with `networking.interfaces.<interface>.useDHCP`.
  networking.useDHCP = lib.mkDefault true;
  # networking.interfaces.enp0s31f6.useDHCP = lib.mkDefault true;

  nixpkgs.hostPlatform = lib.mkDefault "x86_64-linux";
  hardware.cpu.intel.updateMicrocode = lib.mkDefault config.hardware.enableRedistributableFirmware;
}
