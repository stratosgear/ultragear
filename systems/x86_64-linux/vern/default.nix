{
  lib,
  modulesPath,
  pkgs,
  config,
  ...
}:
with lib;
with lib.ultragear;
{

  imports = [
    ./hardware-configuration.nix
    (modulesPath + "/installer/scan/not-detected.nix")
    ./disk-config.nix
  ];

  boot.binfmt.emulatedSystems = [ "aarch64-linux" ];

  ultragear = {

    system = {
      boot = {
        type = "efi";
      };
    };

    tools = {
      podman = enabled;
    };

    suites = {
      bootstrap = enabled;
      common = enabled;
    };

    services.backup = {
      hostBackups = true;
      backupsDataDir = "/storage-box/resticBackups";
      serverDomain = CONSTS.urls.backups.vern;

      instances.system = {
        sourceDirectories = [
          "/data/stuff"
          "/data/safe/syncthing/boox/notes"
          "/data/safe/syncthing/boox/books"
          "/data/safe/syncthing/boox/obsidian/stratosgear"
        ];
        repositories = [
          {
            path = "/storage-box/vernSystemBackups";
            name = "onLocal";
            hcPingUid = "d1811fd0-5cdf-4ecf-9de9-e9930173046a";
          }
          {
            path = "s3:s3.us-west-000.backblazeb2.com/ultragear-backups/vern/system";
            name = "onBackblaze";
            hcPingUid = "0659279b-ac94-464e-9101-75e204a8e9a7";
          }
          {
            path = "rest:https://${CONSTS.urls.backups.dread}/${config.networking.hostName}/system";
            name = "onDread";
            hcPingUid = "817686fe-d60e-4146-b6f0-16ba720d0b4c";
          }
        ];
      };
    };

    # No idea why the Telegram bot fails with:
    # telegram.error.TimedOut: Timed out
    # Seems a connectin to api.telegram.com fails, but cannot figure out why!

    # pods.bots.croesus = enabled;

    services = {

      forgejo = {
        enable = true;
        dataDir = "/data/forgejo";
      };

      # cryptpad = enabled;

      # Disabling as they produce: Bad Gateway errors
      # garage = {
      #   enable = true;
      #   dataDir = "/data/garage";
      #   # logLevel = "debug";
      # };

      vaultwarden = {
        enable = true;
      };

      scrutiny = {
        enable = true;
      };

      # gitlab = {
      #   enable = true;
      #   dataDir = "/data/gitlab";
      # };

      traefik = enabled;

      postgres = {
        enable = true;
        dataDir = "/data/postgres";
        package = pkgs.postgresql_15;
      };

      # TODO: 2024/02/22 12:55:05, fails with build errors on:
      # FAILED pony/orm/tests/test_json.py::TestJson::test_query_true - AssertionError:
      # None is not True
      # PLUS many more
      #pgadmin = enabled;

      healthchecks = {
        enable = true;
        dataDir = "/data/healthchecks";
      };

      uptimeping = enabled;

      shlink = enabled;
      fief = enabled;

      wallabag = {
        enable = true;
        dataDir = "/data/wallabag";
        domainName = CONSTS.urls.wallabag;
      };

      # homepage = {
      #   enable = true;
      #   dataDir = "/data/homepage";
      #   domainName = CONSTS.urls.homepage;
      # };

      # zabbix = {
      #   enable = true;
      #   server = true;
      #   web = true;
      # };

      homarr = {
        enable = true;
        dataDir = "/data/homarr";
      };

      # weblate = {
      #   enable = true;
      #   dataDir = "/data/weblate";
      # };

      syncthing = {
        enable = true;
        configDir = "/data/syncthing_config";
      };

      nfsServer = {
        enable = true;
        exports = [
          {
            exportFolder = "/data/stuff";
            exportTo = "stuff";
            options = "100.64.0.0/10(rw,sync,no_subtree_check,no_root_squash)";
          }
          {
            exportFolder = "/data/music";
            exportTo = "music";
            options = "100.64.0.0/10(rw,sync,no_subtree_check,no_root_squash)";
          }
        ];

      };

      navidrome = {
        enable = true;
        dataDir = "/data/navidrome";
        musicDir = "/data/music/data";
      };

      mpd = {
        enable = true;
        dataDir = "/data/mpd";
        musicDir = "/data/music/data";
        playlistDir = "/data/music/playlists";
      };
      mympd = enabled;

      # Disbaling as it does not seem to work
      # cockpit-tls: failed to create server listening fd: Address family not supported by protocol
      # cockpit = enabled;
    };

    custom = {
      musicLib = {
        enable = true;
        dataDir = "/data/music";
      };
    };

  };

  # mount storage-box
  # as per: https://nixos.wiki/wiki/Samba#cifs_mount
  sops.secrets."storage-box" = { };
  environment.systemPackages = [ pkgs.cifs-utils ];
  fileSystems."/storage-box" = {
    device = "//u358488.your-storagebox.de/backup";
    fsType = "cifs";
    options =
      let
        # this line prevents hanging on network split
        automount_opts = "x-systemd.automount,noauto,x-systemd.idle-timeout=60,x-systemd.device-timeout=5s,x-systemd.mount-timeout=5s";

      in
      [ "${automount_opts},credentials=${config.sops.secrets."storage-box".path}" ];
  };

  system.stateVersion = "23.05";
}
