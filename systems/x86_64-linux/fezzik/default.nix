{ lib, modulesPath, pkgs, config, ... }:
with lib;
with lib.ultragear; {

  imports = [
    ./hardware-configuration.nix
    ./disk-config.nix
  ];


  ultragear = {

    cache.local = enabled;

    # settings that define this system
    system = {
      boot = {
        enable = true;
        type = "bios";
        bootDevice = "/dev/sda";
      };
      info = {
        display1 = "DP-1";
        display2 = "HDMI-1";
      };
    };

    suites = {
      bootstrap = enabled;
      common = enabled;
      server = enabled;
    };

    pods.bots.croesus = enabled;

    services = {

      ha = {
        enable = true;
        dataDir = "/data/ha";
      };

      postgres = {
        enable = true;
        dataDir = "/data/psql";
        package = pkgs.postgresql_15;
      };

      syncthing = enabled;

      # FIXME: Fails on initial bootstrap of the os. turning off
      # scrutiny.collector = mkForce false;

      traefik = enabled;

      go2rtc = enabled;

      nixosLocalCacher = {
        enable = true;
        cacheDir = "/data/nixoscache";
      };

      adguardhome = enabled;

      backup = {
        instances.system = {
          repositories = [

            {
              path = "s3:s3.us-west-000.backblazeb2.com/ultragear-backups/${config.networking.hostName}/system";
              name = "onBackblaze";
              hcPingUid = "c0337d05-66b4-4a85-85f6-95a49dc3d788";
            }
            {
              path = "rest:https://${CONSTS.urls.backups.vern}/${config.networking.hostName}/system";
              name = "onVern";
              hcPingUid = "b9c41399-411d-42c8-b336-b50460d982e3";
            }
          ];
        };
      };
    };
  };


  system.stateVersion = "23.11";
}
