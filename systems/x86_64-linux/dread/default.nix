{ lib, modulesPath, pkgs, config, ... }:
with lib;
with lib.ultragear; {

  imports = [
    ./hardware-configuration.nix
    (modulesPath + "/installer/scan/not-detected.nix")
    (modulesPath + "/profiles/qemu-guest.nix")
    ./disk-config.nix
  ];

  # # Use the GRUB 2 boot loader.
  # boot.loader.grub.enable = true;
  # boot.loader.grub.device = "/dev/sda"; # or "nodev" for efi only

  ultragear = {

    cache.local = enabled;

    apps = {
      abcde = enabled;
    };

    # settings that define this system
    system = {
      boot = {
        enable = true;
        type = "bios";
        bootDevice = "/dev/sda";
      };

      # impermanence = {
      #   enable = true;
      #   rootPartitionDevice = "sdf1";
      # };

      info = {
        batteryClass = "BAT1";
        backlightClass = "amdgpu_bl0";
      };

      nfsClient = {
        enable = true;
        mounts = [
          {
            mountPoint = "/nfs/music";
            exportPoint = "vern:/export/music";
          }
        ];
      };

    };

    suites = {
      bootstrap = enabled;
      common = enabled;
    };

    services = {

      traefik = enabled;

      # TODO: 2024/03/24 19:13:05  Munin does not compile but a recent MR has been
      # merged.  Try this later on!
      # munin = {
      #   enable = true;
      #   server=true;
      # };

      backup = {
        hostBackups = true;
        # HACK: Create a proper btrfs subvolume for: /data/safe/backups and then change
        # the below
        backupsDataDir = "/data/raid5/apps/backups/resticServer";
        serverDomain = CONSTS.urls.backups.dread;
        instances.system = {
          repositories = [
            {
              path = "/backups/system";
              name = "onLocal";
              hcPingUid = "77794ec4-6019-4bd3-ad9b-b7f46cfe6f67";
            }
            {
              path = "s3:s3.us-west-000.backblazeb2.com/ultragear-backups/${config.networking.hostName}/system";
              name = "onBackblaze";
              hcPingUid = "a9c689c0-f033-4b25-a7c7-6448c555b973";
            }
            {
              path = "rest:https://${CONSTS.urls.backups.vern}/${config.networking.hostName}/system";
              name = "onVern";
              hcPingUid = "f23ce83a-bb34-40c3-b698-60579f353b69";
            }
          ];
        };
      };

      postgres = {
        enable = true;
        dataDir = "/data/raid5/apps/psql";
        package = pkgs.postgresql_15;
      };

      # Disabling paperless...
      # paperless = enabled;

      syncthing = {
        enable = true;
        configDir = "/data/raid5/syncthing_config";
        dataDir = "/data/raid5/syncthing";
      };
    };

  };

  system.stateVersion = "23.05";
}
