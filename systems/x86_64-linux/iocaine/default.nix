{
  lib,
  modulesPath,
  pkgs,
  config,
  ...
}:
with lib;
with lib.ultragear;
{

  imports = [
    ./hardware-configuration.nix
    (modulesPath + "/installer/scan/not-detected.nix")
    (modulesPath + "/profiles/qemu-guest.nix")
    ./disk-config.nix
  ];

  # Allows us to build aarch64-linux systems (Raspberries, for example)
  boot.binfmt.emulatedSystems = [ "aarch64-linux" ];

  ultragear = {

    cache.local = enabled;

    # settings that define this system
    system = {
      boot = {
        enable = true;
        type = "efi";
      };
      info = {
        display1 = "DP-1";
        display2 = "HDMI-1";
      };

      nfsClient = {
        enable = true;
        mounts = [
          {
            mountPoint = "/nfs/music";
            # exportPoint = "vern.priv.gerakakis.net:/export/music";
            exportPoint = "vern:/export/music";
          }
          {
            mountPoint = "/nfs/stuff";
            # exportPoint = "vern.priv.gerakakis.net:/export/stuff";
            exportPoint = "vern:/export/stuff";
          }
        ];
      };
    };

    games = {
      steam = enabled;
    };

    suites = {
      bootstrap = enabled;
      common = enabled;
      development = enabled;
    };

    apps = {
      obsidian = {
        enable = true;
        dataDir = "/data/syncthing/obsidian/Stratosgear";
      };

      # other
      # vlc = enabled;
      # yt-music = enabled;
      # gparted = enabled;

    };

    desktop = {

      i3.enable = mkForce false;

      hyprland = {
        enable = true;
        monitors = [
          "HDMI-A-1, 2560x1080@74.991, 0x0, 1"
          "DP-1, 1920x1080@60, 2560x0, 1"
        ];
      };

      addons = {
        obs = enabled;
        dropbox = enabled;
      };
    };

    archetypes = {
      workstation = enabled;
    };

    # pods.bots.croesus = enabled;

    services = {

      pia-vpn = enabled;

      syncthing = enabled;
      # munin = enabled;
      traefik = enabled;

      # kanboard = {
      #   enable = true;
      #   dataDir = "/data/kanboard";
      # };

      docker = {
        enable = true;
        dataRoot = "/data/docker";
      };

      backup = {
        instances.system = {
          excludePatterns = [
            ".git"
            ".Trash-1000"
          ];
          sourceDirectories = [
            "/projects"
            "/home/stratos/.config/ultragear"
          ];
          repositories = [
            {
              path = "/backups/system";
              name = "onLocal";
              hcPingUid = "c2d16366-d977-4b7d-a90b-a904607da9e6";
            }
            {
              path = "s3:s3.us-west-000.backblazeb2.com/ultragear-backups/${config.networking.hostName}/system";
              name = "onBackblaze";
              hcPingUid = "c0337d05-66b4-4a85-85f6-95a49dc3d788";
            }
            {
              path = "rest:https://${CONSTS.urls.backups.vern}/${config.networking.hostName}/system";
              name = "onVern";
              hcPingUid = "b9c41399-411d-42c8-b336-b50460d982e3";
            }
          ];
        };
      };
    };

    hardware = {
      bluetooth = enabled;
    };
  };

  environment.systemPackages = [
    pkgs.gnubg
    pkgs.libreoffice-fresh
    pkgs.hunspell
    pkgs.hunspellDicts.en-us

    # Not using keybr for typing lessons anymore
    # pkgs.ultragear.ug-sqlitize-keybr-stats
    # pkgs.ultragear.ug-keybr-stats
    pkgs.qmk
    pkgs.qmk-udev-rules

    # local development with kubernetes
    pkgs.minikube
    pkgs.kubectl
    pkgs.kubernetes-helm

    pkgs.thunderbird # email client
    pkgs.telegram-desktop # Telegram client
    pkgs.scrcpy # screen share of mobile/tablet
    pkgs.android-tools # adb

    pkgs.simplescreenrecorder # screen recorder

    pkgs.blender # 3D editor
    pkgs.freecad # Freecad
    pkgs.openscad-unstable # Openscad
    pkgs.bambu-studio # Bambu Studio slicer and printer
    # temporarily disable to upgrade flake
    # pkgs.orca-slicer # Test driving orca-slicer t
  ];

  system.stateVersion = "23.11";
}
