{
  lib,
  modulesPath,
  pkgs,
  config,
  ...
}:
with lib;
with lib.ultragear;
{

  imports = [
    ./hardware-configuration.nix
    (modulesPath + "/installer/scan/not-detected.nix")
    (modulesPath + "/profiles/qemu-guest.nix")
    ./disk-config.nix
  ];

  # maybe more:
  # https://github.com/shazow/nixfiles/blob/69e0e385013e7c0479385b3e9223c439cff784bd/hardware/framework-13-amd.nix#L22

  ultragear = {

    cache.local = enabled;

    # settings that define this system
    system = {
      boot = {
        enable = true;
        type = "efi";
      };
      info = {
        batteryClass = "BAT1";
        backlightClass = "amdgpu_bl0";
        display1 = "eDP-1";
        display2 = "DP-2";
      };
      time.tz = "Europe/Madrid";

      nfsClient = {
        enable = true;
        mounts = [
          {
            mountPoint = "/nfs/music";
            # exportPoint = "vern.priv.gerakakis.net:/export/music";
            exportPoint = "vern:/export/music";
          }
          {
            mountPoint = "/nfs/stuff";
            # exportPoint = "vern.priv.gerakakis.net:/export/stuff";
            exportPoint = "vern:/export/stuff";
          }
        ];
      };
    };

    suites = {
      bootstrap = enabled;
      common = enabled;
      development = enabled;
    };

    desktop = {
      hyprland = {
        enable = true;
        monitors = [ ",highres,auto,1.6" ];
        #  We are not ready to add the monkeytype module yet, until fixed
        # "custom/monkeytype",
        waybar_modules_right = ''
          "custom/media",
          "pulseaudio",
          "network",
          "cpu",
          "memory",
          "temperature",
          "backlight",
          "hyprland/language",
          "battery",
          "idle_inhibitor",
          "power-profiles-daemon",
          "tray",
          "custom/power",
          "clock",
          "custom/notification",
        '';
      };
      addons = {
        # following is only needed with i3 usage
        # polybar = {
        #   barPrimary = {
        #     modulesRight = "filesystem pulseaudio xkeyboard memory cpu wlan eth backlight battery date";
        #   };
        # };
        dropbox = enabled;
      };
    };

    apps = {
      firefox.pixelsPerPx = "1.4";
    };

    archetypes = {
      workstation = enabled;
      laptop = enabled;
    };

    services = {

      pia-vpn = enabled;
      
      syncthing = enabled;

      docker = {
        enable = true;
        dataRoot = "/data/docker";
      };

      backup = {
        instances.system = {
          excludePatterns = [
            ".git"
            ".Trash-1000"
          ];
          sourceDirectories = [
            "/projects"
            "/home/stratos/Documents"
          ];
          repositories = [
            {
              path = "/backups/system";
              name = "onLocal";
              hcPingUid = "54428dfd-7f5c-43cf-a58c-df98b10a7b49";
            }
            {
              path = "s3:s3.us-west-000.backblazeb2.com/ultragear-backups/${config.networking.hostName}/system";
              name = "onBackblaze";
              hcPingUid = "962b076b-281e-4e40-bc33-d55d61286eb9";
            }
            {
              path = "rest:https://${CONSTS.urls.backups.vern}/${config.networking.hostName}/system";
              name = "onVern";
              hcPingUid = "d5b5eb5c-2d32-48b3-afb2-b22788d0588c";
            }
          ];
        };
      };
    };

    hardware = {
      bluetooth = enabled;
    };
  };

  environment.systemPackages = [
    pkgs.gnubg
    pkgs.libreoffice-fresh
    pkgs.hunspell
    pkgs.hunspellDicts.en-us
    pkgs.framework-tool

    pkgs.obsidian
    pkgs.thunderbird # email client

  ];

  system.stateVersion = "23.05";
}
