{ config, lib, pkgs, modulesPath, ... }:

{
  imports =
    [
      (modulesPath + "/installer/scan/not-detected.nix")
    ];

  # Most setting copied from:
  # https://git.lyte.dev/lytedev/nix/src/commit/1d192f702c079e9d4cd2bd7c6e7d02d0e3836feb/nixos/foxtrot.nix#L193


  boot = {
    kernelPackages = pkgs.linuxPackages_latest;

    loader = {
      efi.canTouchEfiVariables = true;
      systemd-boot.enable = true;
    };

    # NOTE(oninstall):
    # sudo filefrag -v /swap/swapfile | awk '$1=="0:" {print substr($4, 1, length($4)-2)}'
    # the above won't work for btrfs, instead you need
    # btrfs inspect-internal map-swapfile -r /swap/swapfile
    # https://wiki.archlinux.org/title/Power_management/Suspend_and_hibernate#Hibernation_into_swap_file
    # many of these come from https://wiki.archlinux.org/title/Framework_Laptop_13#Suspend
    kernelParams = [
      "rtc_cmos.use_acpi_alarm=1"
      "amdgpu.sg_display=0"
      "acpi_osi=\"!Windows 2020\""

      # "nvme.noacpi=1" # maybe causing crashes upon waking?

      # NOTE(oninstall):
      # sudo btrfs inspect-internal map-swapfile -r /.swapvol/swap_file
      "resume_offset=9974867"
    ];
    initrd.availableKernelModules = [ "xhci_pci" "nvme" "thunderbolt" "usb_storage" "uas" "sd_mod" ];
    kernelModules = [ "kvm-amd" ];
    # extraModprobeConfig = ''
    #   options cfg80211 ieee80211_regdom="ES"
    # '';
  };

  security.pam.services.i3lock = {
    # i3lock doesn't support fprintd.
    fprintAuth = false;
  };

  services = {

    fwupd = {
      enable = true;
      extraRemotes = [ "lvfs-testing" ];

    };

    fprintd = {
      enable = true;
      # tod.enable = true;
      # tod.driver = pkgs.libfprint-2-tod1-goodix;
    };

    # this conflicts with tlp, if enabled
    # upower.enable = true;

    # as suggested by:
    # https://wiki.nixos.org/wiki/Hardware/Framework/Laptop_13
    power-profiles-daemon.enable = true;
  };


  # powerManagement = {
  #   enable = true;
  #   powertop.enable = true;
  #   cpuFreqGovernor = lib.mkDefault "ondemand";
  # };

  hardware = {

    cpu.amd.updateMicrocode = lib.mkDefault config.hardware.enableRedistributableFirmware;

    graphics.extraPackages = [
      # pkgs.rocmPackages.clr.icd

      # temporarily disable to upgrade flake
      # https://github.com/NixOS/nixpkgs/issues/348903
      pkgs.amdvlk

      # encoding/decoding acceleration
      pkgs.libvdpau-va-gl
      pkgs.vaapiVdpau
    ];

    # wirelessRegulatoryDatabase = true;

    # https://community.frame.work/t/framework-nixos-linux-users-self-help/31426/166I
    # framework.amd-7040.preventWakeOnAC = true;

    bluetooth = {
      enable = true;
      # NOTE: when resuming from hibernation, it would be nice if this would
      # also resume the power state at the time of hibernation, as it seems to only do
      # so at boot events only (not hibernation wakes)
      powerOnBoot = true;
    };

  };

  networking = {
    useDHCP = lib.mkDefault true;
    networkmanager.wifi.powersave = false;
  };

  nixpkgs.hostPlatform = lib.mkDefault "x86_64-linux";

  # use updated ppd for framework 13:
  # source: https://community.frame.work/t/tracking-ppd-v-tlp-for-amd-ryzen-7040/39423/137?u=lytedev

  # swapDevices = [
  # #   # TODO: move this to disko?
  # #   # NOTE(oninstall):
  # #   # sudo btrfs subvolume create /swap
  # #   # sudo btrfs filesystem mkswapfile --size 32g --uuid clear /swap/swapfile
  # #   # sudo swapon /swap/swapfile
  #   {device = "/.swapvol/swap_file";}
  # ];

  # findmnt -no UUID -T /swap/swapfile
  boot.resumeDevice = "/dev/disk/by-uuid/db2dcdea-30d5-4808-9ed2-24804af1ee7f";

  services.logind = {
    lidSwitch = "suspend-then-hibernate";
    powerKey = "suspend-then-hibernate";
    powerKeyLongPress = "poweroff";
    # HandleLidSwitchDocked=ignore
    # extraConfig = ''
    #   HandlePowerKey=suspend-then-hibernate
    #   IdleActionSec=1m
    #   IdleAction=suspend-then-hibernate
    # '';
  };
  systemd.sleep.extraConfig = "HibernateDelaySec=2m";

}
