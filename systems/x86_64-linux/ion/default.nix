{
  lib,
  modulesPath,
  pkgs,
  ...
}:
with lib;
with lib.ultragear;
{

  imports = [
    ./hardware-configuration.nix
    ./disk-config.nix
  ];

  ultragear = {

    cache.local = enabled;

    # settings that define this system
    system = {
      boot = {
        enable = true;
        type = "bios";
        bootDevice = "/dev/sda";
      };
      info = {
        display1 = "DP-1";
        display2 = "HDMI-1";
      };
    };

    suites = {
      bootstrap = enabled;
      common = enabled;
      server = enabled;
      desktop = enabled;
    };

    desktop = {

      # i3.enable = true;

    };

    apps = {

    };

    services = {

      # FIXME: Fails on initial bootstrap of the os. turning off
      scrutiny.collector = mkForce false;

    };

  };
  environment.systemPackages = [
    pkgs.gnubg
  ];

  system.stateVersion = "24.11";
}
