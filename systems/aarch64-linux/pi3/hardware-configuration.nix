{ config, lib, pkgs, modulesPath, ... }:

{

  boot = {
    kernelPackages = pkgs.linuxKernel.packages.linux_rpi3;
    initrd.availableKernelModules =  [
      "mmc_block"
    ];
    loader = {
      grub.enable = false;
      generic-extlinux-compatible.enable = true;
      # raspberryPi = {
      #   enable =  true;
      #   version = 3;
      #   firmwareConfig = ''
      #     # # overclock some (https://www.tomshardware.com/how-to/overclock-any-raspberry-pi#overclocking-values-for-all-models-of-pi-xa0)
      #     # arm_freq=1500
      #     # over_voltage=4
      #     dtoverlay=iqaudio-dacplus,auto_mute_amp,unmute_amp
      #   '';
      # };
    };

    # prevent `modprobe: FATAL: Module ahci not found`
  };

  # # Activate the IQAUDIO PI-DAC+
  # # From: https://github.com/cyber-murmel/dome-sound-system/blob/3caa76066823e63589337b8075476f6852de0152/nixos/audio/iqaudio-dac.nix
  # boot.loader.raspberryPi.firmwareConfig = ''
  #   dtoverlay=iqaudio-dacplus,auto_mute_amp,unmute_amp
  # '';

  # turn off bluetooth, since we do not use it
  boot.blacklistedKernelModules = [ "btusb" "btbcm" "hci_uart" ];
  hardware.bluetooth.enable = false;

  # Allow non-free firmware
  hardware.enableRedistributableFirmware = true;

  # A bunch of boot parameters needed for optimal runtime on RPi 3b+
  # boot.kernelParams = [ "cma=256M" ];

  # Limit journal size
  services.journald.extraConfig = ''
    Storage = volatile
    RuntimeMaxFileSize = 10M;
    SystemMaxUse=256M
  '';

  fileSystems = {
    "/" = {
      device = "/dev/disk/by-label/NIXOS_SD";
      fsType = "ext4";
    };
    "/net/vern/music" = {
      device = "vern:/data/music";
      fsType = "nfs";
      options = [
        "nofail"
        "noauto"
        "noatime"
        "x-systemd.automount"
        "x-systemd.idle-timeout=5min"
        "nodev"
        "nosuid"
        "_netdev"
        "noexec"
        # "uid=navidrome"
        # "gid=navidrome"
        # "allow_other"
        "ro"
      ];
    };
  };

  swapDevices = [{ device = "/swapfile"; size = 1024; }];
  # nixpkgs.hostPlatform = lib.mkDefault "aarch64-linux";
}
