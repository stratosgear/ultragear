{ pkgs, config, lib, modulesPath, inputs, ... }:

with lib;
with lib.ultragear;
{
  imports = with inputs.nixos-hardware.nixosModules; [
    ./hardware-configuration.nix
    (modulesPath + "/installer/scan/not-detected.nix")
    (modulesPath + "/installer/sd-card/sd-image-aarch64.nix")
  ];

  # Preserve space by disabling nixos documentation
  documentation = {
    info.enable = mkDefault false;
    man.enable = mkDefault false;
    nixos.enable = mkDefault false;
  };

  # don't include a 'command not found' helper
  programs.command-not-found.enable = mkDefault false;

  # # disable firewall (needs iptables)
  # networking.firewall.enable = mkDefault false;

  # disable polkit
  security.polkit.enable = mkDefault false;

  # disable audit
  security.audit.enable = mkDefault false;

  # disable udisks
  services.udisks2.enable = mkDefault false;

  # disable containers
  boot.enableContainers = mkDefault false;

  # build less locales
  # This isn't perfect, but let's expect the user specifies an UTF-8 defaultLocale
  i18n.supportedLocales = [ (config.i18n.defaultLocale + "/UTF-8") ];

  # Out Of Memory daemon not recommended for rpi
  systemd.oomd.enable = mkDefault false;

  ultragear = {

    hardware = {
      pi-audio = enabled;
    };

    # settings that define this system
    system = {

      info = {
        display1 = "DP-1";
        display2 = "HDMI-1";
      };
    };

    suites = {
      bootstrap = enabled;
      common = enabled;
      server = enabled;
    };

    system = {
      boot = {
        # Raspberry Pi requires a specific bootloader.
        enable = mkForce false;
      };
    };

    services = {


      # No local disks, so no need monitoring smartctl
      scrutiny.collector = mkForce false;

      traefik = enabled;

      navidrome = {
        enable = true;
        enableDownloads = false;
        musicDir = "/net/vern/music";
        dataDir = "/data/safe/navidrome";
        jukebox = true;
        logLevel = "DEBUG";
      };

    };

  };

  system.stateVersion = "23.05";
}
